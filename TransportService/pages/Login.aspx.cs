﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class Login : System.Web.UI.Page
    {
        private void ClearContent()
        {
            txtPassword.Text = string.Empty;
            txtUserName.Text = string.Empty;
            lblError.Text = string.Empty;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.Abandon();
                Session.Clear();
            }
        }

        protected void LoginButton_Click1(object sender, EventArgs e)
        {
            var login = UserFacade.CheckLogin(txtUserName.Text, txtPassword.Text);
            if (login)
            {
                
                Session["User"] = txtUserName.Text;
                Session["EmployeeID"] = UserFacade.GetEmployeeIDByUser(txtUserName.Text);
                Response.Redirect("Index.aspx");
                ClearContent();
            }
            else
                ClearContent();
            Session.Abandon();
            Session.Clear();

            lblError.Text = "Username and Password Invalid";
            //Response.Redirect("Login.aspx");
        }

    }
}