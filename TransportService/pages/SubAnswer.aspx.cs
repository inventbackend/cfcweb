﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class SubAnswers : System.Web.UI.Page
    {
        public string gUserId, gRoleId, gQuestionId, gQuestionText;
        public Core.Model.User vUser;
        public string lParam { get; set; }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            BindData();

            txtNo.Text = "";
            txtAnswerText.Text = "";
            txtAnswerID.Text = "";
            txtAnswerID.ReadOnly = false;
            
    
            PanelFormAnswer.Visible = false;

        }

        private void BindData()
        {
            var mdlAnswerList = AnswerFacade.LoadAnswer(gQuestionId,true, Boolean.Parse(ddlIsactive.SelectedValue), gUserId);

            RptAnswerlist.DataSource = mdlAnswerList;
            RptAnswerlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gUserId = Session["User"].ToString();
            gQuestionId = Request.QueryString["subQuestionID"];

            if (!IsPostBack)
            {
                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                ClearContent();

                if (Request.QueryString["subQuestionID"] != null)
                {
                    gQuestionId = Request.QueryString["subQuestionID"];
                    lblQuestionID.Text = gQuestionId;
                }

                if (Request.QueryString["subQuestionText"] != null)
                {
                    gQuestionText = Request.QueryString["subQuestionText"];
                    lblQuestionText.Text = gQuestionText;
                }

                
            }
        }

        protected void RptAnswerlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtSequence = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtSequence")).Text);
            var dtNo = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtNo")).Text);
            var dtAnswerID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtAnswerID")).Text);
            var dtAnswerText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtAnswerText")).Text);
            var dtsubQuestion = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtsubQuestion")).Text);
            var dtisActive = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtisActive")).Text);


            if (e.CommandName == "Update")
            {
                txtAnswerID.Text = dtAnswerID;
                txtAnswerText.Text = dtAnswerText;
                txtNo.Text = dtNo;
                txtSeq.Text = dtSequence;
                

                btnInsert.Visible = false;
                btnUpdate.Visible = true;
                txtAnswerID.ReadOnly = true;

               PanelFormAnswer.Visible = true;
            }

            if (e.CommandName == "Delete")
            {
                String lResult = AnswerFacade.NonActiveAnswer(dtAnswerID, gUserId);
                ClearContent();
                BindData();
            }


        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var lmdlAnswer = new mdlAnswer();
            lmdlAnswer.AnswerID = txtAnswerID.Text;
            lmdlAnswer.AnswerText = txtAnswerText.Text;
            lmdlAnswer.IsActive = true;
            lmdlAnswer.IsSubQuestion = true;
            lmdlAnswer.No = txtNo.Text;
            lmdlAnswer.QuestionID = lblQuestionID.Text;
            lmdlAnswer.Sequence = Int32.Parse(txtSeq.Text);
            lmdlAnswer.SubQuestion = false;

            String lResult = AnswerFacade.UpdateAnswer(lmdlAnswer, gUserId);
            ClearContent();
            BindData();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var lmdlAnswer = new mdlAnswer();
            lmdlAnswer.AnswerID = txtAnswerID.Text;
            lmdlAnswer.AnswerText = txtAnswerText.Text;
            lmdlAnswer.IsActive = true;
            lmdlAnswer.IsSubQuestion = true;
            lmdlAnswer.No = txtNo.Text;
            lmdlAnswer.QuestionID = lblQuestionID.Text;
            lmdlAnswer.Sequence = Int32.Parse(txtSeq.Text);
            lmdlAnswer.SubQuestion = false;

            String lResult = AnswerFacade.InsertAnswer(lmdlAnswer, gUserId);
            ClearContent();
            BindData();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormAnswer.Visible = true;
            txtAnswerID.Text = AnswerFacade.GenerateAnswerID(true,gUserId);

            btnInsert.Visible = true;
            btnUpdate.Visible = false;
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }

    }
}