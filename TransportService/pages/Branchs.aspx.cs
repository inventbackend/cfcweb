﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class Branchs : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;
        public Core.Model.User vUser;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Sub Area";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        //001
        private void CompanyDDL()
        {
            ddlCompanyID.DataSource = CompanyFacade.LoadCompany2();
            ddlCompanyID.DataTextField = "CompanyName";
            ddlCompanyID.DataValueField = "CompanyID";
            ddlCompanyID.DataBind();
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public string lParam { get; set; }

        private void ClearContent()
        {
            txtBranchName.Text = "";
            txtBranchDescription.Text = "";
            txtBranchID.Text = "";
            txtBranchID.ReadOnly = false;

            BindData("",getBranch());
            PanelFormBranch.Visible = false;
        }

        private void BindData(string lkeyword, string branchid)
        {
            var listMobileConfig = new List<Core.Model.mdlBranch>();

            if (getBranch() == "")
            {
                listMobileConfig = BranchFacade.GetSearch(lkeyword, branchid);
            }
            else
            {
                listMobileConfig = BranchFacade.LoadSomeBranch2(lkeyword, branchid);
            }

            var role = getRole();
            foreach (var branch in listMobileConfig)
            {
                branch.Role = role;
                btnNew.Visible = role;
            }
            
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listMobileConfig;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            //lblPage.Text = pgitems.PageCount.ToString();
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }


            RptBranchlist.DataSource = pgitems;
            RptBranchlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClearContent();
                CompanyDDL();
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            BindData(txtSearchBranchID.Text, getBranch());
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(txtSearchBranchID.Text, getBranch());
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData(txtSearchBranchID.Text, getBranch());
        }

        protected void rptBranch_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkBranchId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    var lBranchs = BranchFacade.GetBranchbyID(id);
                    txtBranchID.Text = lBranchs.BranchID;
                    txtBranchName.Text = lBranchs.BranchName;
                    txtBranchDescription.Text = lBranchs.BranchDescription;
                    ddlCompanyID.SelectedValue = lBranchs.CompanyID;

                    btnInsert.Visible = false;
                    btnUpdate.Visible = true;
                    txtBranchID.ReadOnly = true;

                    PanelFormBranch.Visible = true;

                }
            }

            if (e.CommandName == "Delete")
            {
                Control control;
                control = e.Item.FindControl("lnkBranchId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    string lResult = BranchFacade.DeleteBranch(id);

                    if (lResult == "SQLSuccess")
                    {
                        string lResult2 = MobileConfigFacade.DeleteMobileConfig(id);
                        //Response.Write("<script>alert('Delete Branch Success');</script>");
                    }
                    //else
                    //{
                    //    Response.Write("<script>alert('"+ lResult +"');</script>");
                    //}
                    ClearContent();
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string lResult = BranchFacade.UpdateBranch(txtBranchID.Text, txtBranchName.Text, txtBranchDescription.Text, ddlCompanyID.SelectedValue);

            //if (lResult == "SQLSuccess")
            //{
            //    Response.Write("<script>alert('Update Branch Success');</script>");
            //}
            //else
            //{
            //    Response.Write("<script>alert('" + lResult + "');</script>");
            //}
            ClearContent();

        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            getRole();
            getBranch();
            string lResult = BranchFacade.InsertBranch(txtBranchID.Text, txtBranchName.Text, txtBranchDescription.Text, ddlCompanyID.SelectedValue);

            if (lResult == "SQLSuccess")
            {
                MobileConfigFacade.SetDefaultMobileConfig(txtBranchID.Text);
                string newBranch = vUser.BranchID + "|" + txtBranchID.Text;
                UserFacade.UpdateUserBranch(newBranch, vUser.UserID);
                //Response.Write("<script>alert('Insert Branch Success');</script>");
            }
            //else
            //{
            //    Response.Write("<script>alert('Insert Branch Fail');</script>");
            //}
            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            PanelFormBranch.Visible = true;
            btnInsert.Visible = true;
            btnUpdate.Visible = false;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
        }

        protected void btnSearchBranch_Click(object sender, EventArgs e)
        {
            PageNumber = 0;
            BindData(txtSearchBranchID.Text,getBranch());
            PanelFormBranch.Visible = false;
        }

        //--001
    }
}