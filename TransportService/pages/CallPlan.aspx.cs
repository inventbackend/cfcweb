﻿/* documentation
 *001 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class CallPlan : System.Web.UI.Page
    {
        public string gBranchId, gUserId;

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;
            
            return gBranchId;
        }

        public string lParam { get; set; }

        private void BindData(string keywordDriver, string keywordDate, string keywordBranchId)
        {

            var listCallPlan = new List<Core.Model.mdlCallPlanWeb>();

            if (getBranch() == "")
            {
                listCallPlan = Core.Manager.CallPlanFacade.GetSearch2(keywordDriver, keywordDate, keywordBranchId);
            }
            else
            {

                listCallPlan = Core.Manager.CallPlanFacade.GetSearchCPByBranch2(keywordDriver, keywordDate, keywordBranchId);
            }

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listCallPlan;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            //lblPage.Text = pgitems.PageCount.ToString();
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptCallPlanlist.DataSource = pgitems;
            RptCallPlanlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                BindData("", txtDate.Text, getBranch());

                string keyword = "";
                BindDataEmployee(keyword,getBranch());
                PanelEmployee.Visible = false;

            }
        }

        protected void rptCallPlan_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkCallPlanId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    Response.Redirect("CallPlanDetail.aspx?cid=" + id);

                }
            }
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;
            DateTime outputDateTimeValue = DateTime.ParseExact(txtDate.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

            BindData(txtEmployeeID.Text, outputDateTimeValue.ToString("MM/dd/yyyy"), getBranch());
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;
            DateTime outputDateTimeValue = DateTime.ParseExact(txtDate.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

            BindData(txtEmployeeID.Text, outputDateTimeValue.ToString("MM/dd/yyyy"), getBranch());
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;
            DateTime outputDateTimeValue = DateTime.ParseExact(txtDate.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

            BindData(txtEmployeeID.Text, outputDateTimeValue.ToString("MM/dd/yyyy"), getBranch());
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            lParam = "search";
            PageNumber = 0;
            try
            {
                DateTime outputDateTimeValue = DateTime.ParseExact(txtDate.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                BindData(txtEmployeeID.Text, outputDateTimeValue.ToString("MM/dd/yyyy"), getBranch());
            }
            catch
            {
                DateTime outputDateTimeValue = DateTime.ParseExact(txtDate.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                BindData(txtEmployeeID.Text, outputDateTimeValue.ToString("MM/dd/yyyy"), getBranch());
            }
            
            PanelEmployee.Visible = false;
            //BindDataSearch(txtEmployeeID.Text, txtDate.Text);
        }

        private void BindDataEmployee(string keyword, string branch)
        {
            if (getBranch() == "")
            {
                RptEmployeelist.DataSource = EmployeeFacade.GetSearchAllEmployee(keyword, branch);
                RptEmployeelist.DataBind();
            }
            else
            {
                RptEmployeelist.DataSource = EmployeeFacade.GetSearchEmployeeByBranch(keyword, branch);
                RptEmployeelist.DataBind();
            }
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {
            string keyword = "";

            BindDataEmployee(keyword,getBranch());
            PanelEmployee.Visible = true;
            txtSearchEmployee.Text = string.Empty;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindDataEmployee(txtSearchEmployee.Text,getBranch());
            PanelEmployee.Visible = true;
        }

        protected void btnCancelEmployee_Click(object sender, EventArgs e)
        {
            PanelEmployee.Visible = false;
            txtSearchEmployee.Text = string.Empty;
            txtEmployeeID.Text = string.Empty;
        }

        protected void rptEmployee_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkEmployeeId");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    txtEmployeeID.Text = id;
                    PanelEmployee.Visible = false;
                }
            }
        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchEmployeeID(string prefixText, int count)
        {
            List<string> lEmployees = new List<string>();
            lEmployees = EmployeeFacade.AutoComplEmployee(prefixText, count, "EmployeeID");
            return lEmployees;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchEmployeeName(string prefixText, int count)
        {
            List<string> lEmployees = new List<string>();
            lEmployees = EmployeeFacade.AutoComplEmployee(prefixText, count, "EmployeeName");
            return lEmployees;
        }

    }
}