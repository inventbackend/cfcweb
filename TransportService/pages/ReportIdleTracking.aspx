﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportIdleTracking.aspx.cs" Inherits="TransportService.pages.ReportIdleTracking" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24" ></script>
    <script type="text/javascript" src="js/journey/markerwithlabel.js"></script>
    <script type="text/javascript" src="js/journey/jquery-1.11.3.js"></script>
    <style>
    .tracking {
        color: black;
        background-color: #ffffff;
        font-family:"Lucida Grande", "Arial", sans-serif;
        font-size: 10px;
        text-align: center;
        width:15px;
        white-space:pre-wrap;

    }
    
    .start {
        color: black;
        background-color: #9DDD00;
        font-family:"Lucida Grande", "Arial", sans-serif;
        font-size: 10px;
        text-align: center;
        width:15px;
        white-space:pre-wrap;

    }
</style>

<script type="text/javascript">
    var trackingMrk = <%=json%>;


</script>
<script type="text/javascript">
    var radiusVal = <%=rad%>;


</script>
<%--<script type="text/javascript">
var trackingMrk = [
<asp:Repeater ID="rptTrackingMrk" runat="server">
<ItemTemplate>
            {
            "vehicleID":'<%# Eval("vehicleID") %>',
            "latitude": '<%# Eval("latitude") %>',
            "longitude": '<%# Eval("longitude") %>',
            "time": '<%# Eval("time") %>'
        }
</ItemTemplate>
<SeparatorTemplate>
    ,
</SeparatorTemplate>
</asp:Repeater>
];
</script>--%>
<script type="text/javascript" src="js/idleTracking/googleIdleMaps.js"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class ="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Form Idle Tracking</h1>
        </div>
    </div>
    <div class= "row">
        <div class= "col-lg-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Filter By</h2>
                    <div class="clearfix">
                    </div>
                </div>
                
                <div class="x_content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <fieldset>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                    <p>
                                    <asp:Label ID="lblBranchD" runat="server" Text="Branch ID" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; text-align:left;"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <%--<asp:TextBox ID="txtBranchID" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                        <asp:AutoCompleteExtender ServiceMethod="SearchBranchID" MinimumPrefixLength="1"  
                                            CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtBranchID"  
                                            ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">  
                                        </asp:AutoCompleteExtender> 

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtBranchID" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>--%>


                                       <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                                        </asp:DropDownList>
                                        </p>
                                        <p>
                                             <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID" Width="100px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px; text-align:left;"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="true" CssClass="control-label col-md-3 col-sm-3 col-xs-12" style="margin-top:5px"></asp:Label>
                                        <asp:TextBox ID="txtEmployeeID" runat="server" 
                                                        Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Search Employee"></asp:TextBox>

                                            <asp:Button ID="btnShowEmployee" runat="server" CssClass="btn btn-success " Text="Show Employee" onclick="btnShowEmployee_Click" style="margin-left:10px"/>
                                            <asp:Label ID="lblValblEmployee" runat="server" style="color:Red;" visible="false"></asp:Label> <%-- 002 fernandes --%>
                                            <asp:Panel ID="Panel1" CssClass="form-control" Height="200px" Width="400px" runat="server" ScrollBars="Vertical">
                                            <%--<asp:CheckBoxList ID="blEmployee" runat="server">
                                            </asp:CheckBoxList>--%>
                                                <asp:RadioButtonList ID="rbEmployee" runat="server">
                                                </asp:RadioButtonList>
                                            </asp:Panel>
                                            
                                        
                                        </p>
                                        <br />
                                        <p>
                                            <asp:Label ID="lblDate" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Font-Bold="true" runat="server" Text="Date" style="text-align: left; margin-top:-20px;" Width="100px"></asp:Label>
                                        <asp:Label ID="Label3" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Font-Bold="true" runat="server" Text=":" Width="10px" style="margin-top: -20px"></asp:Label>
                                        <asp:TextBox ID="txtDate" runat="server" Style="margin-left: 0px; margin-top: -25px"
                                            Width="150px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                        <script type="text/javascript">
                                            var picker1 = new Pikaday(
                                                        {
                                                            field: document.getElementById('<%=txtDate.ClientID%>'),
                                                            firstday: 1,
                                                            minDate: new Date('2000-01-01'),
                                                            maxDate: new Date('2020-12-31'),
                                                            yearRange: [2000, 2020],
                                                            format: 'MM-DD-YYYY'
                                                            //                                    setDefaultDate: true,
                                                            //                                    defaultDate : new Date()
                                                        }
                                                        );
                                        </script>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtDate" ValidationGroup="valShowReport" ForeColor="#FF3300"
                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </p>
                                        <br />
                                        <p>
                                            <asp:Button ID="btnShow" CssClass="btn btn-primary" runat="server" Text="SHOW" onclick="btnShow_Click" />
                                        </p>
                                        
                                    





                                </fieldset>
                                
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div id="divMap" style="width:100%; height:500px;"></div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
