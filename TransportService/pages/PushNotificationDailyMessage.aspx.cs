﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using Newtonsoft.Json;

namespace TransportService.pages
{
    public partial class PushNotificationDailyMessage : System.Web.UI.Page
    {
       
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

  
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (getBranch() == "")
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = string.Empty;
                    //txtBranchID.ReadOnly = false;
                }
                else
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch("'" + getBranch() + "'");
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = getBranch();
                    //txtBranchID.ReadOnly = true;
                }

                Panel1.Visible = false;
                btnSelectAll.Visible = false;
                btnUnSelectAll.Visible = false;

                var listDdl = new List<string>();
                listDdl.Add("DAILY MESSAGE");
                listDdl.Add("DOWNLOAD ALL");
                listDdl.Add("KONFIGURASI MOBILE");
                listDdl.Add("KONFIGURASI USER");
                listDdl.Add("CLEAR DATA");
                listDdl.Add("NONACTIVATE KNOX");

                ddlType.DataSource = listDdl;
                ddlType.DataBind();
               



            }
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {
            //--002,004

            //if(txtBranchID.Text == "" && txtEmployeeID.Text == "")
            //{
            //    var lEmployeelist = EmployeeFacade.LoadEmployee();
            //    blEmployee.DataSource = lEmployeelist;
            //    blEmployee.DataTextField = "EmployeeName";
            //    blEmployee.DataValueField = "EmployeeID";
            //    blEmployee.DataBind();
            //}
            if (ddlSearchBranchID.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(),"err_msg","alert('Please Fill Branch ID First');", true);
            }
            else if (txtEmployeeID.Text == "" && ddlSearchBranchID.SelectedValue != "")
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
                blEmployee.DataSource = lEmployeelist;
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();

                Panel1.Visible = true;
                btnSelectAll.Visible = true;
                btnUnSelectAll.Visible = true;
            }
            else
            {
                var lEmployeelist = EmployeeFacade.LoadEmployeelistReport2(txtEmployeeID.Text, ddlSearchBranchID.SelectedValue);
                blEmployee.DataSource = lEmployeelist;
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();

                Panel1.Visible = true;
                btnSelectAll.Visible = true;
                btnUnSelectAll.Visible = true;
            }
            //002,004--

            //Panel1.Visible = true;
            //btnSelectAll.Visible = true;
            //btnUnSelectAll.Visible = true;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }


        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = false;
            }
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();
            string employee = string.Empty;

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    employee += item.Value;
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            //--003
            if (blEmployeeStrList.Count == 0)
            {
                lblValblEmployee.Text = "*Required";
                lblValblEmployee.Visible = true;
                return;
            }
            else
            {
                lblValblEmployee.Visible = false;
            }


            //listDdl.Add("DAILY MESSAGE");
            //    listDdl.Add("DOWNLOAD ALL");
            //    listDdl.Add("CALL PLAN");
            //    listDdl.Add("KONFIGURASI MOBILE");
            //    listDdl.Add("USER KONFIGURASI");
            //    listDdl.Add("CLEAR DATA");
            string notificationID = employee + ddlSearchBranchID.SelectedValue + DateTime.Now.ToString("yyyyMMddHmmss");
            string res = "";
            string title = "";
            //string androidKey = "etNo1C4WrRM:APA91bHcIBkXvIT_okRXXaD9N1x5462T0ELUiAt5399Pfo090r2RZtyj6o0BRw4gOqZdIbOW1M2frwNMneSGeLT-hgOwewj-Y8G3oDFn2FksRt7Y5Kb-JwrhUAMtrEFNnb-ci-oyfvcQ";
            if (ddlType.SelectedValue == "DAILY MESSAGE")
            {
                title = "DAILYMESSAGE";
                res = PushNotificationFacade.PushNotif(notificationID,title, "You Got Daily Messsage", ddlSearchBranchID.SelectedValue, blEmployeeStrList);
            }
            else if (ddlType.SelectedValue == "DOWNLOAD ALL")
            {
                title = "DOWNLOADALL";
                res = PushNotificationFacade.PushNotif(notificationID, title, "You Got New Mobile Configuration", ddlSearchBranchID.SelectedValue, blEmployeeStrList);
            }
            else if (ddlType.SelectedValue == "KONFIGURASI MOBILE")
            {
                title = "KONFIGURASIMOBILE";
                res = PushNotificationFacade.PushNotif(notificationID,"KONFIGURASIMOBILE", "You Got New Mobile Configuration", ddlSearchBranchID.SelectedValue, blEmployeeStrList);
            }
            else if (ddlType.SelectedValue == "KONFIGURASI USER")
            {
                title = "KONFIGURASIUSER";
                res = PushNotificationFacade.PushNotif(notificationID,"KONFIGURASIUSER", "You Got New User Configuration", ddlSearchBranchID.SelectedValue, blEmployeeStrList);
            }
            else if (ddlType.SelectedValue == "CLEAR DATA")
            {
                title = "CLEARDATA";
                res = PushNotificationFacade.PushNotif(notificationID,"CLEARDATA", "You Got New Clear Data", ddlSearchBranchID.SelectedValue, blEmployeeStrList);
            }
            else if (ddlType.SelectedValue == "NONACTIVATE KNOX")
            {
                title = "NONACTIVATEKNOX";
                res = PushNotificationFacade.PushNotif(notificationID, "NONACTIVATEKNOX", "You Got Knox Non Activation", ddlSearchBranchID.SelectedValue, blEmployeeStrList);
            }
            mdlFCMReturn googleResult = JsonConvert.DeserializeObject<mdlFCMReturn>(res);




            if (googleResult.success == 1)
            {
                
                   
                LogNotificationFacade.InsertAndroidKeyLog(notificationID,employee,ddlSearchBranchID.SelectedValue,title);
          
                
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "msg", "alert('Success');", true);
            }
            else if(googleResult.success == 0)
            {
                string error = "";

                foreach(var temp in googleResult.results)
                {
                    error += temp.error;
                }


                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "err_msg", "alert('" + error + "');", true);
            }



        


        }



    }
}