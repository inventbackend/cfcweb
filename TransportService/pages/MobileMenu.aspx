﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="MobileMenu.aspx.cs" Inherits="TransportService.pages.MobileMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                User Role</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form User Role
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                        <asp:Button ID="btnNew" runat="server" Text="Add New" class="btn btn-primary" OnClick="btnNew_Click" />

                        <asp:Panel ID="PanelAddUserRole" runat="server" Width="400px" Height="100%" ScrollBars="Vertical" CssClass="datagrid" Visible="false">
                        <p>
                           <asp:Label ID="lblRole" runat="server" Text="Role ID " Font-Bold="True" style="margin-left:10px"></asp:Label></br>
                           <asp:TextBox ID="txtRoleID" runat="server" Width="100px" CssClass="form-control col-md-3 col-xs-12" style="margin-left:10px"></asp:TextBox>
                        </p>
                        <br /><br />
                        <p>
                           <asp:Label ID="lblRoleNm" runat="server" Text="Role Name " Font-Bold="True" style="margin-left:10px"></asp:Label></br>
                           <asp:TextBox ID="txtRoleNm" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12" style="margin-left:10px"></asp:TextBox>
                           <br /><br />
                           <asp:Button ID="btnSaveUR" runat="server" Text="Save"  
                                    style="margin-left: 10px; margin-top: 0px" onclick="btnSaveUR_Click" CssClass="btn btn-success"></asp:Button>
                           <asp:Button ID="btnCancelUR" runat="server" Text="Cancel"  
                                    style="margin-left: 0px; margin-top: 0px" onclick="btnCancelUR_Click" CssClass="btn btn-success"></asp:Button>
                        </p>
                        </asp:Panel>
                        <br />

                            <p>
                                <asp:Label ID="lblRoleID" runat="server" Text="Role ID"></asp:Label><br />
                                <asp:DropDownList ID="ddlRoleID" runat="server" Width="400px" Height="30px" 
                                    AutoPostBack="True" onselectedindexchanged="ddlRoleID_SelectedIndexChanged"></asp:DropDownList>
                            </p>
                            </div>
                            <div class="col-lg-12">
                                    Menu Access
                            </div>
                                <div class="col-lg-3">
                                
                                <asp:ListBox ID="LbMenu1" runat="server" Height="300px" Width="200px">
                                </asp:ListBox>
                                </div>
                                <div class="col-lg-3" style="margin-left:-50px;">
                                    &nbsp&nbsp&nbsp&nbsp <asp:Button ID="btnAdd" runat="server" Text="Add  >" 
                                        Width="150px" onclick="btnAdd_Click" /><br />
                                    &nbsp&nbsp&nbsp&nbsp <asp:Button ID="btnAddAll" runat="server" 
                                        Text="Add All  >>" Width="150px" onclick="btnAddAll_Click" /><br />
                                    &nbsp&nbsp&nbsp&nbsp <asp:Button ID="btnRemove" runat="server" Text="<  Remove" 
                                        Width="150px" onclick="btnRemove_Click" /><br />
                                    &nbsp&nbsp&nbsp&nbsp <asp:Button ID="btnRemoveAll" runat="server" 
                                        Text="<<  Remove All" Width="150px" onclick="btnRemoveAll_Click" /><br />
                                    </div>
                                <div class="col-lg-3" style="margin-left:-60px;">
                                 <asp:ListBox ID="LbMenu2" runat="server" Height="300px" Width="200px">
                                </asp:ListBox>
                                 </div>
                                 <%--<div class="col-lg-3">
                                      <asp:Button ID="ButtonShowModifyRole" runat="server" Text="Edit Menu Leverage" CssClass="myButton" onclick="btnShowModifyRole_Click" />
                                      <br /><br />
                                      <asp:Panel ID="PanelModifyRole" runat="server" ScrollBars="Vertical" CssClass="datagrid">
                                      <asp:Label ID="Label1" runat="server" Text="Set Editing Status :"></asp:Label>
                                      <asp:CheckBoxList ID="blModifyRole" runat="server">
                                      <asp:ListItem Text="Call Plan" Value="M0002"></asp:ListItem>
                                      <asp:ListItem Text="Cost" Value="SM0014"></asp:ListItem>
                                      <asp:ListItem Text="Branch" Value="SM0019"></asp:ListItem>
                                      <asp:ListItem Text="Reason" Value="SM0025"></asp:ListItem>
                                      <asp:ListItem Text="Daily Message" Value="SM0013"></asp:ListItem>
                                      <asp:ListItem Text="Mobile Config" Value="SM0015"></asp:ListItem>
                                      <asp:ListItem Text="User Config" Value="SM0016"></asp:ListItem>
                                      <asp:ListItem Text="User Management" Value="SM0018"></asp:ListItem>
                                      </asp:CheckBoxList>
                                      </asp:Panel>
                                 </div>   --%>                   
                                <div class="col-lg-12">
                                 <br />
                                 <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="myButton"  
                                         ValidationGroup="valUser" onclick="btnSave_Click" />
                                 <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="myButton" 
                                         onclick="btnCancel_Click"  />
                                 </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">

</asp:Content>
