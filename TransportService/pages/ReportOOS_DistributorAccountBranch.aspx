﻿<%@ Page Title="Report OOS Distributor Acc Area" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportOOS_DistributorAccountBranch.aspx.cs"
    Inherits="TransportService.pages.ReportOOS_DistributorAccountBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../vendors/morris.js/morris.css" rel="stylesheet">
    <script src="js/journey/jquery-1.11.3.js"></script>
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <!--bar fix-->
    <script>
        var arrChart = [];
    </script>
    <!--bar fix-->
    <style>
        .tablefixed
        {
            overflow-x: scroll;
            overflow-y: visible;
            padding-bottom: 1px;
        }
        .td
        {
            padding: 0 40px;
        }
    </style>
    <script type="text/javascript">
        function Navigate() {

            window.history.go(-1);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Report OOS</h3>
            <div class="title_right">
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                <div class="form-group col-md-12 col-xs-12 col-lg-12">
                    <div class="col-md-12 col-xs-12 col-lg-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <input action="action" type="button" value="Back" class="btn btn-sm btn-embossed btn-primary"
                                    onclick="window.history.go(-1);" />
                            </div>
                        </div>
                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                        <li role="presentation" class=""><a href="#tab_content11" id="home-tabb" role="tab"
                            data-toggle="tab" aria-controls="home" aria-expanded="true">Charts</a> </li>
                        <li role="presentation" class="active"><a href="#tab_content22" role="tab" id="profile-tabb"
                            data-toggle="tab" aria-controls="profile" aria-expanded="false">Tables</a> </li>
                    </ul>
                            <div id="myTabContent2" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade" id="tab_content11" aria-labelledby="home-tab">

                                    <div class="row">
                                        <asp:Repeater ID="rptBranchChart" runat="server">
                                            <ItemTemplate>
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="x_panel">
                                                        <div class="x_title">
                                                            <h2>
                                                                <%# Eval("Area") %></h2>
                                                            <ul class="nav navbar-right panel_toolbox">
                                                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                                            </ul>
                                                            <div class="clearfix">
                                                            </div>
                                                        </div>
                                                        <div class="x_content">
                                                            <div id='<%# Eval("ID") %>' style="width: 100%; height: 280px;">
                                                            </div>
                                                            <script>
//                                             $(document).ready(function () {
//                                                 Morris.Bar({
//                                                     element: <%# Eval("ID") %> ,
//                                                     data: <%# Eval("json") %>,
//                                                     xkey: 'VisitWeek',
//                                                     ykeys: ['OOS'],
//                                                     labels: ['OOS'],
//                                                     barRatio: 0.4,
//                                                     barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
//                                                     xLabelAngle: 35,
//                                                     hideHover: 'auto',
//                                                     resize: true
//                                                 });
//                                             });
                                                    arrChart.push(new Morris.Bar({
                                                     element: <%# Eval("ID") %> ,
                                                     data: <%# Eval("json") %>,
                                                     xkey: 'VisitWeek',
                                                     ykeys: ['OOS'],
                                                     labels: ['OOS'],
                                                     barRatio: 0.4,
                                                     barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                                     xLabelAngle: 35,
                                                     hideHover: 'auto',
                                                     resize: true,
                                                     redraw:true }));
                                                            </script>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content22" aria-labelledby="profile-tab">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-xs-12 col-md-12">
                                            <div class="row">
                                                <div class="col-md-12 datagrid tablefixed" runat="server" id="divTableAccount">
                                                    <table style="width: 100%; margin-bottom: 0px;">
                                                        <asp:Repeater ID="rptHeaderReportAccount" runat="server">
                                                            <HeaderTemplate>
                                                                <thead>
                                                                    <th>
                                                                        <asp:Label ID="lblHeader" runat="server" Text='ACCOUNT'></asp:Label>
                                                                    </th>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <th>
                                                                    <asp:Label ID="lWeek" runat="server" Text='<%# Container.DataItem %>'></asp:Label>
                                                                </th>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tr> </thead>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <asp:Repeater ID="rptParentReportAccount" runat="server" OnItemDataBound="ReportAccountItemBound">
                                                            <ItemTemplate>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="left: 0;">
                                                                            <a href='<%# Eval("Link") %>'>
                                                                                <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("AccountID") %>'></asp:Label></a>
                                                                        </td>
                                                                        <asp:Repeater ID="rptChildAccountStock" runat="server">
                                                                            <ItemTemplate>
                                                                                <td>
                                                                                    <asp:Label ID="lblData" runat="server" Text='<%# Eval("OOS") %>'></asp:Label>%
                                                                                </td>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </tr>
                                                                </tbody>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-weight: bolder;">
                                                                    <asp:Label ID="lbltot" runat="server" Text='GRAND TOTAL'></asp:Label>
                                                                </td>
                                                                <asp:Repeater ID="rptGrandTotal" runat="server">
                                                                    <ItemTemplate>
                                                                        <td style="font-weight: bolder;">
                                                                            <asp:Label ID="lblGrandTotal" runat="server" Text='<%# Container.DataItem %>'></asp:Label>%
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--bar fix-->
    <script type="text/javascript">
        $(document).ready(function () {

            $('.nav-tabs a').on('shown.bs.tab', function (event) {
                for (var i = 0; i < arrChart.length; i++) {
                    arrChart[i].redraw();
                }
            });
        });
    </script>
<!--bar fix-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
