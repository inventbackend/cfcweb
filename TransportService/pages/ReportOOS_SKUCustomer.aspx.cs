﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class ReportOOS_SKUCustomer : System.Web.UI.Page
    {
        public string user;

        protected void Page_Load(object sender, EventArgs e)
        {
            user = Session["User"].ToString();

            if (!IsPostBack)
            {               
                int visitWeek = Convert.ToInt32(Request.QueryString["week"]);
                string account = Request.QueryString["account"];
                string productID = Request.QueryString["productid"];
                int year = Convert.ToInt32(Request.QueryString["year"]);
                string role = Request.QueryString["role"];


                if (Request.QueryString["branch"] != null)
                {
                    string branch = Request.QueryString["branch"].ToString();
                    rptCustomers.DataSource = OOSFacade.GetOOSCustomersAccountBranchBySKU(visitWeek, year, productID, account, branch,role, user);
                    rptCustomers.DataBind();
                }
                else
                {
                    rptCustomers.DataSource = OOSFacade.GetOOSCustomers(visitWeek, year, productID, account,role, user);
                    rptCustomers.DataBind();
                }
            }
        }
    }
}