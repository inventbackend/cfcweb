﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class SettingKoordinatWarehouse : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StartDate.Text = DateTime.Now.Date.ToString("dd MMMM yyyy");
                EndDate.Text = DateTime.Now.Date.ToString("dd MMMM yyyy");
                BindData();
            }
        }

        private void BindData()
        {

            StartDate.Text = DateTime.Today.Date.ToString("dd MMMM yyyy");

            //ddlCabang.DataSource = Core.Manager.BranchFacade.LoadBranch2(getBranch());
            if (getBranch() == "")
            {
                ddlCabang.DataSource = Core.Manager.BranchFacade.LoadBranch2(getBranch());
            }
            else
            {
                ddlCabang.DataSource = Core.Manager.BranchFacade.LoadSomeBranch(getBranch());
            }
           

            ddlCabang.DataValueField = "BranchID";
            ddlCabang.DataTextField = "BranchName";
            ddlCabang.DataBind();

            string keyword = txtSearch.Text;
            
            gvCustomer.DataSource = CustomerFacade.GetSearch2(ddlCabang.SelectedValue, keyword);
            gvCustomer.DataBind();
        }

        protected void ddlCabang_SelectedIndexChanged(object sender, EventArgs e)
        {

            string keyword = txtSearch.Text;
            gvCustomer.DataSource = CustomerFacade.GetSearch2(ddlCabang.SelectedValue, keyword);
            gvCustomer.DataBind();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            Session["txtWarehouseId"] = txtWarehouseID.Text;
            Session["CldFrom"] = StartDate.Text;
            Session["CldTo"] = EndDate.Text;
            string queryString = "SettingKoordinatWarehousePop.aspx";
            string newWin = "window.open('" + queryString + "');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void gvCustomer_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {

        }

        protected void gvCustomer_ItemCommand(object source, DataGridCommandEventArgs e)
        {

            if (e.CommandName == "id")
            {
                txtCustomerID.Text = e.Item.Cells[1].Text;

                string CustomerID = e.Item.Cells[1].Text;
                string keyword = txtSearchWarehouse.Text;
                gvWarehouse.DataSource = WarehouseFacade.GetSearch(CustomerID,keyword);
                gvWarehouse.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {

            string keyword = txtSearch.Text;
            gvCustomer.DataSource = CustomerFacade.GetSearch2(ddlCabang.SelectedValue, keyword);
            gvCustomer.DataBind();
        }

        protected void btnSearchWarehouse_Click(object sender, EventArgs e)
        {
            string customerid = txtCustomerID.Text;

            string keyword = txtSearchWarehouse.Text;
            gvWarehouse.DataSource = WarehouseFacade.GetSearch(customerid,keyword);
            gvWarehouse.DataBind();
        }

        protected void gvWarehouse_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {

        }

        protected void gvWarehouse_ItemCommand(object source, DataGridCommandEventArgs e)
        {

            if (e.CommandName == "wid")
            {
                txtWarehouseID.Text = e.Item.Cells[1].Text;
            }
        }
        
    }
}