﻿<%@ Page Title="Call Plan Detail" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="True" CodeBehind="AddCallPlanDetail.aspx.cs" Inherits="AddCallPlanDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link type="text/css" href="css/bootstrap-timepicker.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Itenary Detail</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Form Itenary Detail
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group">
                                <div>
                                    <fieldset>
                                    <p >
                                            <asp:Label ID="lblCPDetailID" runat="server" Text="Itenary Detail ID "></asp:Label></br>
                                            <asp:TextBox ID="txtCPDetailID" runat="server" Width="400px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                        </p>
                                        <p>
                                            <asp:Label ID="lblCallPlanID" runat="server" Text="Itenary ID " Font-Bold="True"></asp:Label></br>
                                            <asp:TextBox ID="txtCallPlanID" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12" 
                                              ></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtCallPlanID" ValidationGroup="callplandetail" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            <%--<asp:DropDownList ID="ddlCallPlanID" runat="server" Width="400px" CssClass="tb5"
                                                Height="28px">
                                            </asp:DropDownList>--%>
                                        </p>
                                        <br />
                                        <p>
                                            <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Font-Bold="True"></asp:Label>
                                            </br>
                                            <asp:TextBox ID="txtCustomerID" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtCustomerID" ValidationGroup="callplandetail" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>

                                            <asp:Button ID="btnShowCustomer" runat="server" Text="Show Customer"  
                                    style="margin-left: 0px; margin-top: 0px" onclick="btnShowCustomer_Click" CssClass="btn btn-success"></asp:Button>
                                    <asp:TextBox ID="txtSearchCustomer" runat="server" style="margin-left: 0px; margin-top: 0px" Width="200px" CssClass="tb5"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Search Customer" 
                                    style="margin-left: 0px; margin-top: 0px" onclick="btnSearch_Click" CssClass="btn btn-success"></asp:Button>
                                <asp:Button ID="btnCancelCustomer" runat="server" Text="Cancel"  
                                    style="margin-left: 0px; margin-top: 0px" onclick="btnCancelCustomer_Click" CssClass="btn btn-success"></asp:Button>
                                        </p>

                                        <asp:Panel ID="PanelCustomer" runat="server" Width="100%" Height="200px" ScrollBars="Vertical" CssClass="datagrid">                   
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>
                                    Customer ID</th>
                                <th>
                                    Customer Name</th>
                                <th>
                                    Customer Address</th>
                                <th>
                                    Branch ID</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="RptCustomerlist" runat="server" 
                                onitemcommand="rptCustomer_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkCustomerId" runat="server" CommandName="Link" 
                                                Text='<%# Eval("CustomerID") %>'></asp:LinkButton>
                                        </td>
                                        <td>
                                            <%# Eval("CustomerName") %>
                                        </td>
                                        <td>
                                            <%# Eval("CustomerAddress") %>
                                        </td>
                                        <td>
                                            <%# Eval("BranchID") %>
                                        </td>
                                    </tr>
                                    </tbody>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
    </asp:Panel>
                                        <p style="display:none;">
                                            <asp:Label ID="lblWarehouseID" runat="server" Text="Warehouse ID" Font-Bold="True"></asp:Label>
                                            </br>
                                            <asp:TextBox ID="txtWarehouseID" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>

                                            <asp:Button ID="btnShowWarehouse" runat="server" Text="Show Warehouse"  
                                    style="margin-left: 10px; margin-top: 0px" onclick="btnShowWarehouse_Click" CssClass="btn btn-success"></asp:Button>
                                    <asp:TextBox ID="txtSearchWarehouse" runat="server" style="margin-left: 0px; margin-top: 0px" Width="200px" CssClass="tb5"></asp:TextBox>
                                <asp:Button ID="btnSearchWarehouse" runat="server" Text="Search Warehouse" 
                                    style="margin-left: 0px; margin-top: 0px" onclick="btnSearchWarehouse_Click" CssClass="btn btn-success"></asp:Button>
                                <asp:Button ID="btnCancelWarehouse" runat="server" Text="Cancel"  
                                    style="margin-left: 0px; margin-top: 0px" onclick="btnCancelWarehouse_Click" CssClass="btn btn-success"></asp:Button>
                                        </p>
                                        <asp:Panel ID="PanelWarehouse" runat="server" Width="100%" Height="200px" ScrollBars="Vertical" CssClass="datagrid">                   
                    <table style="width: 100%">
                        <thead>
                            <tr>
                                <th>
                                    Warehouse ID</th>
                                <th>
                                    Warehouse Name</th>
                                <th>
                                    Warehouse Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="RptWarehouselist" runat="server" 
                                onitemcommand="rptWarehouse_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkWarehouseId" runat="server" CommandName="LinkWarehouse" 
                                                Text='<%# Eval("WarehouseID") %>'></asp:LinkButton>
                                        </td>
                                        <td>
                                            <%# Eval("WarehouseName") %>
                                        </td>
                                        <td>
                                            <%# Eval("WarehouseAddress") %>
                                        </td>
                                    </tr>
                                    </tbody>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
    </asp:Panel>

                                        <p>
                                            <asp:Label ID="lblSequence" runat="server" Text="Sequence" Font-Bold="True"></asp:Label></br>
                                            <asp:TextBox ID="txtSequence" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12" placeholder="fill with number..."></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtSequence" ValidationGroup="callplandetail" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </p>
                                    <br />

                                    <p>
                                    <asp:Label ID="lblTime" runat="server" Text="Time" Font-Bold="True"></asp:Label></br>
                                   
                                        
                                    <asp:TextBox ID="txtTime" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12" placeholder="fill with number..." data-inputmask="'mask': '**:**:00'" MaxLength="7" onkeypress="return isNumber(event)"></asp:TextBox>
       
                                  
                                            
                                            
                                        </p>
                                    <br />

                                    <p>
                                       <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnUpdate_Click"
                                            ValidationGroup="callplandetail" />
                                        <asp:Button ID="btnInsert" runat="server" Text="Insert" CssClass="btn btn-primary" OnClick="btnInsert_Click"
                                            ValidationGroup="callplandetail" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClick="btnCancel_Click" />
                                    </p>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

    <!-- jquery.inputmask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>

</asp:Content>

