﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class ReportOOSAccountBranch : System.Web.UI.Page
    {

        private List<Core.Model.mdlOOSProduct> listAccountProductOOS;
        private List<int> listWeek;
        private int year;
        private string branchID;
        private string role;
        public string user;

        protected void Page_Load(object sender, EventArgs e)
        {
            user = Session["User"].ToString();

            if (!IsPostBack)
            {
                int fromWeek = Convert.ToInt32(Request.QueryString["fromweek"]);
                int toWeek = Convert.ToInt32(Request.QueryString["toweek"]);
                string account = Request.QueryString["account"];
                role = Request.QueryString["role"];
                year = Convert.ToInt32(Request.QueryString["year"]);

                if (branchID != null)
                {
                    listAccountProductOOS = OOSFacade.GetOOSProductPerAccountBranch(Convert.ToInt16(fromWeek), Convert.ToInt16(toWeek), account, branchID, Convert.ToInt16(year),role, user);
                    listWeek = listAccountProductOOS.Select(fld => fld.VisitWeek).Distinct().ToList();

                    rptHeaderReportAccount.DataSource = listWeek;
                    rptHeaderReportAccount.DataBind();
                    Label lblControl = rptHeaderReportAccount.Controls[0].Controls[0].FindControl("lblHeader") as Label;
                    lblControl.Text = "ACCOUNT";

                    rptParentReportAccount.DataSource = listAccountProductOOS;
                    rptParentReportAccount.DataBind();
                }
                else
                {
                    listAccountProductOOS = OOSFacade.GetOOSProductPerAccount(Convert.ToInt16(fromWeek), Convert.ToInt16(toWeek), account, Convert.ToInt16(year),role, user);
                    listWeek = listAccountProductOOS.Select(fld => fld.VisitWeek).Distinct().ToList();
                    listWeek = listAccountProductOOS.Select(fld => fld.VisitWeek).Distinct().ToList();

                    rptHeaderReportAccount.DataSource = listWeek;
                    rptHeaderReportAccount.DataBind();
                    Label lblControl = rptHeaderReportAccount.Controls[0].Controls[0].FindControl("lblHeader") as Label;
                    lblControl.Text = "ACCOUNT";

                    rptParentReportAccount.DataSource = listAccountProductOOS;
                    rptParentReportAccount.DataBind();
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            //object refUrl = ViewState["RefUrl"];
            //if (refUrl != null)
            //    Response.Redirect((string)refUrl);


        }

        protected void ReportAccountItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblAccount = ri.FindControl("lblAccount") as Label;
                Label lblProductID = ri.FindControl("lblProductID") as Label;
                string account = lblAccount.Text;
                Repeater childRepeater = (Repeater)args.Item.FindControl("rptChildAccountStock");



                var listChild = listAccountProductOOS.Where(fld => fld.Account.Equals(account) & fld.ProductID.Equals(lblProductID.Text)).ToList();
                var listChildFinal = new List<Core.Model.mdlOOSProductFinal>();
                foreach (int i in listWeek)
                {
                    var tempList = listChild.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                    if (tempList != null)
                    {
                        var model = new Core.Model.mdlOOSProductFinal();
                        model.Account = tempList.Account;
                        model.OOSCustomer = tempList.OOSCustomer;
                        model.ProductID = tempList.ProductID;
                        model.ProductName = tempList.ProductName;
                        model.VisitWeek = tempList.VisitWeek;
                        model.Link = "ReportOOSCustomer.aspx?week=" + model.VisitWeek + "&account=" + model.Account + "&productid=" + model.ProductID + "&year=" + year + "&branch=" + branchID + "&role=" + role;
                        listChildFinal.Add(model);

                    }
                    else
                    {
                        var model = new Core.Model.mdlOOSProductFinal();

                        //model.Account = "";
                        model.VisitWeek = i;
                        //model.Link = "";
                        //model.OOSCustomer = 0;
                        //model.ProductID = "";
                        //model.ProductName = "";
                        listChildFinal.Add(model);


                    }

                }

                listChildFinal = listChildFinal.OrderBy(fld => fld.VisitWeek).ToList();


                childRepeater.DataSource = listChildFinal;
                childRepeater.DataBind();
            }
        }
    }
}