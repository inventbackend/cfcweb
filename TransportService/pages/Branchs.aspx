﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="Branchs.aspx.cs" Inherits="TransportService.pages.Branchs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Sub Area</h3>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <asp:TextBox ID="txtSearchBranchID" PlaceHolder="Search By Branch Name..." runat="server"
                    class="form-control"></asp:TextBox>
                <span class="input-group-btn">
                    <button id="btnSearchBranch" runat="server" class="btn btn-default" type="button"
                        onserverclick="btnSearchBranch_Click">
                        Search</button>
                </span>
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <%--<div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <div class="form-group">
                    <br />
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <p>
                        <asp:Button ID="btnNew" runat="server" Text="Add Branch" class="btn btn-primary"
                            OnClick="btnNew_Click"></asp:Button>
                        <div class="datagrid" style="width: 100%">
                            <table style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>
                                            Sub Area ID
                                        </th>
                                        <th>
                                            Sub Area Name
                                        </th>
                                        <th>
                                            Sub Area Description
                                        </th>
                                        <th>
                                            Company ID
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptBranchlist" runat="server" OnItemCommand="rptBranch_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lnkBranchId" runat="server" Enabled='<%# Eval("Role") %>' CommandName="Link" Text='<%# Eval("BranchID")%>'></asp:LinkButton>
                                                </td>
                                                <td>
                                                    <%# Eval("BranchName")%>
                                                </td>
                                                <td>
                                                    <%# Eval("BranchDescription")%>
                                                </td>
                                                <td>
                                                    <%# Eval("CompanyID")%>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkDelete" Text="Delete" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Delete this Branch ?');"
                                                        runat="server" CommandName="Delete" />
                                                </td>
                                            </tr>
                                            </tbody>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5">
                                                <div id="paging">
                                                    <ul>
                                                        <li>
                                                            <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                        <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                            <ItemTemplate>
                                                                <li>
                                                                    <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                        runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <li>
                                                            <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PanelFormBranch" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form Sub Area Management </h2>
                    <%--<ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a> </li>
                                <li><a href="#">Settings 2</a> </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>--%>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <br />
                    <div>
                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblBranchID" runat="server" Text="Sub Area ID" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtBranchID" PlaceHolder="Sub Area ID" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox> 
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtBranchName" ValidationGroup="valBranch" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblBranchName" runat="server" Text="Sub Area Name" Font-Bold="True"></asp:Label><br />
                                <asp:TextBox ID="txtBranchName" PlaceHolder="Sub Area Name" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>                                   
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtBranchName" ValidationGroup="valBranch" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblCompanyID" runat="server" Text="Company ID" Font-Bold="True"></asp:Label><br />
                                
                                <asp:DropDownList ID="ddlCompanyID" runat="server" class="form-control" width="400px">
                                </asp:DropDownList>
                                </div>

                            </div>
                            <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblBranchDescription" runat="server" Text="Sub Area Description" Font-Bold="True"></asp:Label><br />           
                                <asp:TextBox ID="txtBranchDescription" PlaceHolder="Sub Area Description" runat="server"
                                     class="form-control" TextMode="MultiLine" width="400px"></asp:TextBox>
                                    
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtBranchDescription" ValidationGroup="valBranch" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                            </div>
                            
                            </div><div>
                            <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click"
                                    ValidationGroup="DailyMsg" />
                                <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                    ValidationGroup="DailyMsg" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btnCancel_Click" />
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
