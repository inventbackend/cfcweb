﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        public static class Globals
        {
            public static string prevPage = string.Empty;
            public static int NOProduct = 0;
            public static string gAreaId = string.Empty;
            public static string gAccount = string.Empty;
            public static string gWeekFrom = string.Empty;
            public static string gWeekTo = string.Empty;
            public static string gTahun = string.Empty;
            public static string gCustomer = string.Empty;
            public static string gRole = string.Empty;
            public static string gUserId = "";
        }

        private string getParamProduct()
        {
            String lResult = "";
            List<String> listProduct = new List<String>();
            var mdlProductList = OSAFacade.GetProduct(Globals.gAccount, Globals.gAreaId, Globals.gCustomer,Globals.gRole, Globals.gUserId);

            foreach (var item in mdlProductList)
            {
                listProduct.Add(item.ProductID);
            }


            int i = Globals.NOProduct;
            lResult = listProduct[i];
            Globals.NOProduct += 1;

            return lResult;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.NOProduct = 0;
            Globals.gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {

                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                else
                    Globals.gUserId = Session["User"].ToString();

                Globals.gAreaId = Request.QueryString["Area"];
                Globals.gWeekFrom = Request.QueryString["Week"];
                Globals.gWeekTo = Request.QueryString["Week"];
                Globals.gTahun = Request.QueryString["Year"];
                Globals.gAccount = Request.QueryString["Account"];
                Globals.gCustomer = Request.QueryString["Customer"];
                Globals.gRole = Request.QueryString["Role"];
           

                if (Globals.gWeekFrom != "")
                {
                    var mdlWeeks = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
                    PagedDataSource pgitemsweek = new PagedDataSource();
                    pgitemsweek.DataSource = mdlWeeks;
                    RptWeeks.DataSource = pgitemsweek;
                    RptWeeks.DataBind();
                }

                BindData(Globals.gWeekFrom, Globals.gWeekTo);
            }
        }

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            string lParamProduct = getParamProduct();
            var listOSAArea = OSAFacade.LoadOSAStock(lParamProduct, Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gCustomer, Globals.gTahun,Globals.gRole, Globals.gUserId);
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildStocklist");
                childRepeater.DataSource = listOSAArea;
                childRepeater.DataBind();
            }
        }

        private void BindData(string lWeekFrom, string lWeekTo)
        {
            var mdlOSAProduct = OSAFacade.GetProduct(Globals.gAccount, Globals.gAreaId, Globals.gCustomer,Globals.gRole, Globals.gUserId);
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = mdlOSAProduct;

            RptOSAProductlist.DataSource = pgitems;
            RptOSAProductlist.DataBind();

        }

        protected void back_Click(object sender, EventArgs e)
        {
            Response.Redirect(Globals.prevPage);
        }
    }
}