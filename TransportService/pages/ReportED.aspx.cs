﻿/* documentation
 * 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Globalization;
using System.Data;
using System.IO;
using Microsoft.Reporting.WebForms;
using Core.Model;

namespace TransportService.pages
{

    public partial class ReportED : System.Web.UI.Page
    {
        private int gNOCustomer = 0;
        private string gUserId = string.Empty;
        private string gAccount = string.Empty;
        private string gCustomer = string.Empty;
        private List<Core.Model.mdlRptEDDetail> gMdlEDList = new List<Core.Model.mdlRptEDDetail>();
        private List<Core.Model.mdlRptED> gEDCustomer = new List<Core.Model.mdlRptED>();
        private List<Core.Model.mdlWeek> gMdlEDWeekList = new List<Core.Model.mdlWeek>();
        private List<Core.Model.mdlGraphData> gTemp_GraphDataList = new List<Core.Model.mdlGraphData>();  //chartcode
        public string user;

        private void getParamGlobals()
        {
        }

        private void ClearContent()
        {
            gNOCustomer = 0;

            //put year in dropdown
            ddlTahun.Items.Clear();

            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= 5; i++)
            {
                // Now just add an entry that's the current year minus the counter
                ddlTahun.Items.Add((currentYear - i).ToString());
            }

            //put Week in dropdown
            int lWeekNow = DateFacade.GetIso8601WeekOfYear(DateTime.Now);
            for (int i = lWeekNow; i >= 0; i--)
            {
                // Now just add an entry that's the current year minus the counter
                ddlWeekFrom.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 52; i >= 0; i--)
            {

                ddlWeekTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            List<String> lArealist = OSAFacade.GetAreas();
            ddlArea.DataSource = lArealist;
            ddlArea.DataBind();

            pnlAccount.Visible = false;
            btnSelectAll.Visible = false;
            btnUnSelectAll.Visible = false;
        }


        //<<chartcode
        private void GetChartData(List<Core.Model.mdlRptEDDetail> mdlED)
        {
            string lCustomerName = mdlED.FirstOrDefault().CustomerName;
            string lBody = string.Empty;
            string lData = string.Empty;
            
            var mdlGraphData = new Core.Model.mdlGraphData();
            var Temp_ED = mdlED.GroupBy(fld => fld.VisitWeek).Select(grp => grp.First()).OrderBy(fld => fld.VisitWeek);
            foreach (var lParamWeek in Temp_ED)
            {
                var Temp_mdlED = mdlED.Where(fld => fld.VisitWeek == lParamWeek.VisitWeek);
                foreach (var lParam in Temp_mdlED)
                {
                    if (lParam.SisaStockTypeID != "Summary")
                    {
                        mdlGraphData = new Core.Model.mdlGraphData();
                        if (lParam.Value != "")
                        {
                            lBody += ",w" + lParam.SisaStockTypeID + ":" + lParam.Value;
                        }
                    }
                }
                lData += "{ weeks: '" + lParamWeek.VisitWeek + "'" + lBody + "},";
            }
            mdlGraphData.element = lCustomerName;
            mdlGraphData.data = lData;
            gTemp_GraphDataList.Add(mdlGraphData);
        }
        //chartcode>>

        private bool CheckAccount()
        {
            //validasi jika Account checkbox nya kosong
            bool lReturn = false;
            int icount = 0;
            foreach (ListItem Item in blAccount.Items)
            {
                if (Item.Selected == true)
                {
                    icount++;
                }
            }

            if (icount == 0)
            {
                Response.Write("<script>alert('Account is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        }
        
        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            //Dapatkan CustomerName & Account
            int i = gNOCustomer;
            gAccount = gEDCustomer[i].Account;
            gCustomer = gEDCustomer[i].CustomerName;
            gNOCustomer += 1;
            if (gAccount == "" || gCustomer == "")
            {
                return;
            }

            var EDChildList = gMdlEDList.Where(fld => fld.Account.Equals(gAccount) && fld.CustomerName.Equals(gCustomer)).ToList();
            var Temp_EDChildList = EDChildList.GroupBy(fld => fld.VisitWeek).Select(grp => grp.First());
            List<Core.Model.mdlSisaStockType> mdlSisaStockTypeIDList = EDFacade.GetlistSisaStockTypeID();

            for (int ix = 0; ix < gMdlEDWeekList.Count; ix++)
            {
                var lFirst_ED = Temp_EDChildList.Where(fld => fld.VisitWeek == gMdlEDWeekList[ix].Week).FirstOrDefault();
                if (lFirst_ED == null)
                {
                    foreach (var lParamtype in mdlSisaStockTypeIDList)
                    {
                        var mdlRptED = new Core.Model.mdlRptEDDetail();
                        mdlRptED.VisitWeek = gMdlEDWeekList[ix].Week;
                        mdlRptED.LinkValue = "reportED_SKU.aspx?TypeID=" + mdlRptED.SisaStockTypeID + "&Week=" + mdlRptED.VisitWeek + "&Account=" + gAccount + "&Customer=" + gCustomer + "&Year=" + ddlTahun.SelectedValue + "&Area=" + ddlArea.SelectedValue + "&role=" + rblRole.SelectedValue;

                        mdlRptED.SisaStockTypeID = lParamtype.SisaStockTypeID;
                        if (lParamtype.SisaStockTypeID == "summary")
                        {
                            mdlRptED.Value = "<b>0</b>";
                            mdlRptED.LinkValue = "reportED_SKU.aspx?TypeID=summary&Week=" + mdlRptED.SisaStockTypeID + "&Week=" + mdlRptED.VisitWeek + "&Account=" + gAccount + "&Customer=" + gCustomer + "&Year=" + ddlTahun.SelectedValue + "&Area=" + ddlArea.SelectedValue + "&role=" + rblRole.SelectedValue;
                        }
                        else
                        {
                            mdlRptED.Value = "0";
                        }

                        EDChildList.Add(mdlRptED);

                    }
                }
            }
            

            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildEDlist");
                childRepeater.DataSource = EDChildList.OrderBy(n => n.VisitWeek);
                childRepeater.DataBind();
            }

            GetChartData(EDChildList); //chartcode
        }

        private void BindDataWeeks()
        {
            gMdlEDWeekList = OSAFacade.GetWeeks(ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue);
            PagedDataSource pgitemsweek = new PagedDataSource();
            pgitemsweek.DataSource = gMdlEDWeekList;
            RptWeeks.DataSource = pgitemsweek;
            RptWeeks.DataBind();

            RptSisaStockType.DataSource = pgitemsweek;
            RptSisaStockType.DataBind();
        }

        private void BindData()
        {
            //var listOSAGrandTotal = new List<Core.Model.mdlOSAData>();
            //var mdlOSACustomerList = new List<Core.Model.mdlOSACustomer>();

            List<String> listParam = new List<String>();
            listParam = new List<String>();
            foreach (ListItem item in blAccount.Items)
            {
                if (item.Selected)
                {
                    listParam.Add(item.Value);
                }
            };

            
            gMdlEDList = EDFacade.LoadED_Detail(ddlArea.SelectedValue, listParam, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue,ddlTahun.SelectedValue,rblRole.SelectedValue, user);
            gEDCustomer = EDFacade.LoadED(ddlArea.SelectedValue, listParam, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue, rblRole.SelectedValue, user);

            //var gTemp1EDCustomer = gMdlEDList.Select(fld => new { fld.CustomerName, fld.Account, fld.Channel }).Distinct().ToList();
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = gEDCustomer;
            RptEDCustomerlist.DataSource = pgitems;
            RptEDCustomerlist.DataBind();

            //Proses isi GrandTotal
            var EDGrandTotalList = EDFacade.LoadGrandTotalED(ddlArea.SelectedValue, listParam, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue, rblRole.SelectedValue, user);
            
            


            //var EDGrandTotalList = gMdlEDList
            //                                    .GroupBy(grp => new {grp.VisitWeek, grp.SisaStockTypeID })
            //                                    .Select(fld => new mdlRptEDDetail2
            //                                    {
            //                                        VisitWeek = fld.Key.VisitWeek,
            //                                        SisaStockTypeID = fld.Key.SisaStockTypeID,
            //                                        Value = fld.Sum(x => Decimal.Parse(x.Value))
            //                                    });

            var Temp_EDGrandTotalList = EDGrandTotalList.GroupBy(fld => fld.VisitWeek).Select(grp => grp.First());
            List<Core.Model.mdlSisaStockType> mdlSisaStockTypeIDList = EDFacade.GetlistSisaStockTypeID();
            for (int i = 0; i < gMdlEDWeekList.Count; i++)
            {
                var lFirst_ED = Temp_EDGrandTotalList.Where(fld => fld.VisitWeek == gMdlEDWeekList[i].Week).FirstOrDefault();
                if (lFirst_ED == null)
                {
                    foreach (var lParamtype in mdlSisaStockTypeIDList)
                    {
                        var mdlRptED = new Core.Model.mdlRptEDDetail();
                        mdlRptED.VisitWeek = gMdlEDWeekList[i].Week;
                        if (EDGrandTotalList.Count == 0)
                        {
                            mdlRptED.Channel = "";
                            mdlRptED.Account = "";
                            mdlRptED.CustomerName = "";
                        }
                        else
                        {
                            mdlRptED.Channel = Temp_EDGrandTotalList.FirstOrDefault().Channel;
                            mdlRptED.Account = Temp_EDGrandTotalList.FirstOrDefault().Account;
                            mdlRptED.CustomerName = Temp_EDGrandTotalList.FirstOrDefault().CustomerName;
                        }
                        
                        
                        mdlRptED.SisaStockTypeID = lParamtype.SisaStockTypeID;
                        mdlRptED.Value = "0";
                        EDGrandTotalList.Add(mdlRptED);

                    }
                }
            }



            //<<GrandTotal
            PagedDataSource pgitems2 = new PagedDataSource();
            pgitems2.DataSource = EDGrandTotalList.OrderBy(n => n.VisitWeek).ToArray();
            RptChildGrandTotal.DataSource = pgitems2;
            RptChildGrandTotal.DataBind();

            //<<chartcode
            PagedDataSource pgitems3 = new PagedDataSource();
            pgitems3.DataSource = gTemp_GraphDataList;
            RptChartlist.DataSource = pgitems3;
            RptChartlist.DataBind();
            //chartcode>>

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gTemp_GraphDataList = new List<Core.Model.mdlGraphData>();  //chartcode
            gNOCustomer = 0;
            user = Session["User"].ToString();

            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                else
                    user = Session["User"].ToString();

                ClearContent();

                ////get user branchid
                //ggUserId = Session["User"].ToString();
                //var vUser = UserFacade.GetUserbyID(ggUserId);
                //ggAreaId = vUser.BranchID;

                //BindDataWeeks();
                //BindData(ggWeekFrom, ggWeekTo, "0");
            }
        }

        protected void btnShowAccount_Click(object sender, EventArgs e)
        {
            gTemp_GraphDataList = new List<Core.Model.mdlGraphData>();  //chartcode

            blAccount.DataSource = OSAFacade.GetAccounts();
            blAccount.DataBind();

            pnlAccount.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {

            foreach (ListItem item in blAccount.Items)
            {
                item.Selected = false;
            }
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blAccount.Items)
            {
                item.Selected = true;
            };
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //Check Checkbox Account
            if (CheckAccount() == true)
                return;

            BindDataWeeks();
            //getParam();
            gNOCustomer = 0;
            BindData();
        }

        protected void RptEDCustomerlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "ToCustomer")
            //{
            //    Control control;
            //    control = e.Item.FindControl("lnkToCustomer");
            //    if (control != null)
            //    {
            //        string id;

            //        id = ((LinkButton)control).Text;
            //        Response.Redirect("ReportOSA_Product.aspx?AreaId=" + ggAreaId + "&Account=" + ggAccount + "&Customer=" + id + "&WeekFrom=" + ggWeekFrom + "&WeekTo=" + ggWeekTo + "&Tahun=" + ggTahun);
            //    }
            //}
        }

        //Export To Excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            //Check Checkbox Account
            if (CheckAccount() == true)
                return;

            List<String> listAccount = new List<String>();
            foreach (ListItem item in blAccount.Items)
            {
                if (item.Selected)
                {// If the item is selected, add the value to the list.

                    listAccount.Add(item.Value);
                }
            }

            string LastWeek = EDFacade.GetWeeks(ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue,ddlTahun.SelectedValue);
            var mdlEDExcel = EDFacade.LoadEDExcel(ddlArea.SelectedValue, listAccount, LastWeek, ddlTahun.SelectedValue, rblRole.SelectedValue, user);

            ReportDataSource rds = new ReportDataSource("dsED", mdlEDExcel);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportED.rdlc");
            ReportViewer1.LocalReport.Refresh();

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportED" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
        }

        //protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ggAreaId = ddlArea.SelectedValue;
        //    List<String> lAccountlist = OSAFacade.GetAccounts(ggAreaId);
        //    ddlAccount.DataSource = lAccountlist;
        //    ddlAccount.DataBind();
        //}

    } 
}