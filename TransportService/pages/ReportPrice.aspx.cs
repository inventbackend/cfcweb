﻿/* documentation
 * 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Globalization;
using System.Data;
using System.IO;
using Microsoft.Reporting.WebForms;
using Core.Model;

namespace TransportService.pages
{
    public partial class ReportPrice : System.Web.UI.Page
    {
        private int gNO = 0;
        private string gAccount = string.Empty;
        private string gCustomer = string.Empty;
        private string gProductID = string.Empty;
        public string gUserId = string.Empty;
        private List<mdlProduct> gListProductSelected = new List<mdlProduct>();
        private List<mdlCustomer2> gListCustomerSelected = new List<mdlCustomer2>();
        private List<Core.Model.mdlRptPriceDetail> gMdlPriceDetailList = new List<Core.Model.mdlRptPriceDetail>();
        private List<Core.Model.mdlRptPriceDetail> gMdlPriceDetailList2 = new List<Core.Model.mdlRptPriceDetail>();
        private List<Core.Model.mdlWeek> gMdlEDWeekList = new List<Core.Model.mdlWeek>();

        private void ClearContent()
        {
            gNO = 0;

            //put year in dropdown
            ddlTahun.Items.Clear();

            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= 5; i++)
            {
                // Now just add an entry that's the current year minus the counter
                ddlTahun.Items.Add((currentYear - i).ToString());
            }

            //put Week in dropdown
            int lWeekNow = DateFacade.GetIso8601WeekOfYear(DateTime.Now);
            for (int i = lWeekNow; i >= 0; i--)
            {
                // Now just add an entry that's the current year minus the counter
                ddlWeekFrom.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 52; i >= 0; i--)
            {

                ddlWeekTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            List<String> lArealist = OSAFacade.GetAreas();
            ddlArea.DataSource = lArealist;
            ddlArea.DataBind();


            ddlAccount.DataSource = AccountFacade.GetAccount(ddlArea.SelectedValue);
            ddlAccount.DataTextField = "AccountID";
            ddlAccount.DataValueField = "AccountID";
            ddlAccount.DataBind();

            
            pnlProduct.Visible = false;
            btnSelectAllProduct.Visible = false;
            btnUnSelectAllProduct.Visible = false;

            pnlCustomer.Visible = false;
            btnSelectAllCustomer.Visible = false;
            btnUnSelectAllCustomer.Visible = false;
        }

        //<<chartcode
        private List<mdlGraphData> GetChartData(List<Core.Model.mdlRptPriceDetail> mdlPrice)
        {
            string lBody = string.Empty;
            string lData = string.Empty;
            string lxKeys = string.Empty;
            string lLabels = string.Empty;

            var mdlGraphDataList = new List<Core.Model.mdlGraphData>();  //chartcode
            
            //var Temp_ED = mdlPrice.GroupBy(fld => fld.VisitWeek).Select(grp => grp.First()).OrderBy(fld => fld.VisitWeek);

            foreach(var Product in gListProductSelected) //loop Product
            {
                var mdlGraphData = new Core.Model.mdlGraphData();
                foreach (var lWeeks in gMdlEDWeekList) //loop Week
                {
                    foreach (var lCustomers in gListCustomerSelected) //loop Customer
                    {
                        var Temp_price = mdlPrice.FirstOrDefault(fld => fld.ProductID == Product.ProductID && fld.VisitWeek == lWeeks.Week && fld.CustomerID == lCustomers.CustomerID);
                        if(Temp_price != null)
                        lBody += "," + Temp_price.CustomerName.Trim().Replace(" ", "") + ":" + Temp_price.NormalPrice;
                    }
                    lData += "{ week: '" + lWeeks.Week + "'" + lBody + "},";
                    lBody = "";
                }

                foreach (var lCustomers in gListCustomerSelected) //loop Customer ke 2 untuk isi data xkeys labels
                {
                    if (lxKeys == "")
                    {
                        lxKeys = "'" + lCustomers.CustomerName.Trim().Replace(" ", "") + "'";
                        lLabels = "'" + lCustomers.CustomerName + "'";
                    }
                    else
                    {
                        lxKeys += ",'" + lCustomers.CustomerName.Trim().Replace(" ", "") + "'";
                        lLabels += ",'" + lCustomers.CustomerName + "'";
                    }
                }
                    

                mdlGraphData.element = Product.ProductName;
                mdlGraphData.data = lData;
                mdlGraphData.labels = lLabels;
                mdlGraphData.yKey = lxKeys;
                mdlGraphDataList.Add(mdlGraphData);
                lData = "";
                lLabels = "";
                lxKeys = "";             
            }
            return mdlGraphDataList;
        }
        //chartcode>>

        private bool CheckAccount()
        {
            //<<validasi jika Account checkbox nya kosong
            bool lReturn = false;
            int icount = 0;
            foreach (ListItem Item in ddlAccount.Items)
            {
                if (Item.Selected == true)
                    icount++;              
            }

            if (icount == 0)
            {
                Response.Write("<script>alert('Account is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        }
        
        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            //Dapatkan CustomerName & ProductID
            int i = gNO;
            gCustomer = gMdlPriceDetailList2[i].CustomerID;
            gProductID = gMdlPriceDetailList2[i].ProductID;
            gNO += 1;
            if (gCustomer == "" || gProductID == "")
            {
                return;
            }

            var PriceChildList = gMdlPriceDetailList.Where(fld => fld.CustomerID.Equals(gCustomer) && fld.ProductID.Equals(gProductID)).ToList();
            var Temp_EDChildList = PriceChildList.GroupBy(fld => fld.VisitWeek).Select(grp => grp.First());
            //List<Core.Model.mdlSisaStockType> mdlSisaStockTypeIDList = EDFacade.GetlistSisaStockTypeID();

            for (int ix = 0; ix < gMdlEDWeekList.Count; ix++)
            {
                var lFirst_Price = Temp_EDChildList.Where(fld => fld.VisitWeek == gMdlEDWeekList[ix].Week).FirstOrDefault();
                if (lFirst_Price == null)
                {
                    var lmdlRptPrice = new Core.Model.mdlRptPriceDetail();
                    lmdlRptPrice.PromoPrice = "0.00";
                    lmdlRptPrice.NormalPrice = "0.00";
                    lmdlRptPrice.VisitWeek = gMdlEDWeekList[ix].Week;

                    PriceChildList.Add(lmdlRptPrice);
                }
            }

            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildPricelist");
                childRepeater.DataSource = PriceChildList.OrderBy(n => n.VisitWeek);
                childRepeater.DataBind();
            }
        }

        private void BindDataWeeks()
        {
            //<<Untuk Mendapatkan Week yg Sedang Aktif
            gMdlEDWeekList = OSAFacade.GetWeeks(ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue);
            PagedDataSource pgitemsweek = new PagedDataSource();
            pgitemsweek.DataSource = gMdlEDWeekList;
            RptWeeks.DataSource = pgitemsweek;
            RptWeeks.DataBind();

            PagedDataSource pgitemsweek2 = new PagedDataSource();
            pgitemsweek2.DataSource = gMdlEDWeekList;
            RptType.DataSource = pgitemsweek2;
            RptType.DataBind();
        }

        private void BindData()
        {
            
            string listParamCustomer = string.Empty;
            foreach (ListItem item in blCustomer.Items)
            {
                //loop mendapatkan Data dr dropdownlist Customer
                var gmdlCustomer2 = new mdlCustomer2();
                if (item.Selected)
                {

                    gmdlCustomer2.CustomerID = item.Value;
                    gmdlCustomer2.CustomerName = item.Text;
                    gListCustomerSelected.Add(gmdlCustomer2);
                    if (listParamCustomer == "")
                    {
                        listParamCustomer = item.Value;
                    }
                    else
                    {
                        listParamCustomer += "," + item.Value;
                    }
                }
            };

            //loop mendapatkan Data dr dropdownlist Product
            string listParamProduct = string.Empty;
            foreach (ListItem item in blProduct.Items)
            {
                var gmdlProduct = new mdlProduct();
                if (item.Selected)
                {
                    gmdlProduct.ProductID = item.Value;
                    gmdlProduct.ProductName = item.Text;
                    gListProductSelected.Add(gmdlProduct);

                    if (listParamProduct == "")
                    {
                        listParamProduct = item.Value;
                    }
                    else
                    {
                        listParamProduct += "," + item.Value;
                    }
                }
            };

            gMdlPriceDetailList = PriceFacade.LoadPrice(ddlArea.SelectedValue, ddlAccount.SelectedValue, listParamCustomer, listParamProduct, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue, gUserId);
            var gMdlPrice = gMdlPriceDetailList.Select(s => new { s.BranchID, s.ProductID, s.ProductName, s.CustomerID, s.CustomerName }).Distinct();

            foreach (var lparam in gMdlPrice){
                var mdlPriceDetail2 = new Core.Model.mdlRptPriceDetail();
                mdlPriceDetail2.CustomerID = lparam.CustomerID;
                mdlPriceDetail2.ProductID = lparam.ProductID;
                gMdlPriceDetailList2.Add(mdlPriceDetail2);
            }

            //<<DataBind Price Header
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = gMdlPrice.ToArray();
            RptPricelist.DataSource = pgitems;
            RptPricelist.DataBind();
            //>>DataBind Price Header

            
            //<<DataBind chartcode
            var mdlGraphDataList = GetChartData(gMdlPriceDetailList);
            PagedDataSource pgitems3 = new PagedDataSource();
            pgitems3.DataSource = mdlGraphDataList;
            RptChartlist.DataSource = pgitems3;
            RptChartlist.DataBind();
            //>>DataBind chartcode

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gNO = 0;
            gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                else
                    gUserId = Session["User"].ToString();

                ClearContent();

                ////get user branchid
                //ggUserId = Session["User"].ToString();
                //var vUser = UserFacade.GetUserbyID(ggUserId);
                //ggAreaId = vUser.BranchID;

                //BindDataWeeks();
                //BindData(ggWeekFrom, ggWeekTo, "0");
            }
        }
  
        protected void btnShow_Click(object sender, EventArgs e)
        {
            //Check Checkbox Account
            if (CheckAccount() == true)
                return;

            BindDataWeeks();
            //getParam();
            gNO = 0;
            BindData();
        }

        //Export To Excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            //Check Checkbox Account
            if (CheckAccount() == true)
                return;

            string listParamCustomer = string.Empty;
            foreach (ListItem item in blCustomer.Items)
            {
                //loop mendapatkan Data dr dropdownlist Customer
                if (item.Selected)
                {
                    if (listParamCustomer == "")
                    {
                        listParamCustomer = item.Value;
                    }
                    else
                    {
                        listParamCustomer += "," + item.Value;
                    }
                }
            };

            //loop mendapatkan Data dr dropdownlist Product
            string listParamProduct = string.Empty;
            foreach (ListItem item in blProduct.Items)
            {
                if (item.Selected)
                {
                    if (listParamProduct == "")
                    {
                        listParamProduct = item.Value;
                    }
                    else
                    {
                        listParamProduct += "," + item.Value;
                    }
                }
            };


            var mdlPriceExcel = PriceFacade.LoadPrice(ddlArea.SelectedValue, ddlAccount.SelectedValue, listParamCustomer, listParamProduct, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue, gUserId);


            ReportDataSource rds = new ReportDataSource("dsPrice", mdlPriceExcel);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportPrice.rdlc");
            ReportViewer1.LocalReport.Refresh();

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportPrice" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
        }

        protected void btnShowAccount_Click(object sender, EventArgs e)
        {
            ddlAccount.DataSource = AccountFacade.GetAccount("");
            ddlAccount.DataTextField = "ProductName";
            ddlAccount.DataValueField = "ProductID";
            ddlAccount.DataBind();
        }

        protected void btnProduct_Click(object sender, EventArgs e)
        {
            blProduct.DataSource = ProductFacade.GetProduct();
            blProduct.DataTextField = "ProductName";
            blProduct.DataValueField = "ProductID";
            blProduct.DataBind();
            
            pnlProduct.Visible = true;
            btnSelectAllProduct.Visible = true;
            btnUnSelectAllProduct.Visible = true;
        }

        protected void btnCustomer_Click(object sender, EventArgs e)
        {
            blCustomer.DataSource = OSAFacade.GetCustomer("", ddlAccount.SelectedValue, ddlArea.SelectedValue, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue,"");
            blCustomer.DataTextField = "CustomerName";
            blCustomer.DataValueField = "CustomerID";
            blCustomer.DataBind();

            pnlCustomer.Visible = true;
            btnSelectAllCustomer.Visible = true;
            btnUnSelectAllCustomer.Visible = true;
        }

        protected void btnSelectAllProduct_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blProduct.Items)
            {
                item.Selected = true;
            };
        }

        protected void btnUnSelectAllProduct_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blProduct.Items)
            {
                item.Selected = false;
            }
        }

        protected void btnSelectAllCustomer_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blCustomer.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAllCustomer_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blCustomer.Items)
            {
                item.Selected = false;
            }
        }

        protected void ddlArea_TextChanged(object sender, EventArgs e)
        {
            ddlAccount.DataSource = AccountFacade.GetAccount(ddlArea.SelectedValue);
            ddlAccount.DataTextField = "AccountID";
            ddlAccount.DataValueField = "AccountID";
            ddlAccount.DataBind();
        }
    } 
}