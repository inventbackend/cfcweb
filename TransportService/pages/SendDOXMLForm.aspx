﻿<%@ Page Title="Send DO XML" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="SendDOXMLForm.aspx.cs" Inherits="TransportService.SendDOXMLForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
              <div class="title_left">
                <h3>Form Send DO XML</h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="col-md-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Form Basic Elements <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left">
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Ship From</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <asp:TextBox ID="txtshipFrom" runat="server"  class="form-control"></asp:TextBox>
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtshipFrom" ValidationGroup="SendDOXML" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>

                      </div> 
                      <div class="form-group">       
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Shipper No</label>                                   
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <asp:TextBox ID="txtshipperNo" class="form-control" runat="server"  ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtshipperNo" ValidationGroup="SendDOXML" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                      </div>
                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Ship Date</label>  
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <asp:TextBox ID="txtshipDate" class="form-control" runat="server"  ></asp:TextBox>
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtshipDate" ValidationGroup="SendDOXML" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Veh Ref</label>  
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <asp:TextBox ID="txtvehRef" runat="server"  class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtvehRef" ValidationGroup="SendDOXML" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Arrive Time</label>  
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <asp:TextBox ID="txtarriveTime" runat="server"  class="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtarriveTime" ValidationGroup="SendDOXML" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator><br />
                        </div>
                      </div>

                      <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Auto Invoice</label>  
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <asp:DropDownList ID="ddlAutoInv" class="form-control" runat="server" Width="200px">
                                                <asp:ListItem>False</asp:ListItem>
                                                <asp:ListItem>True</asp:ListItem>   
                                            </asp:DropDownList><br />
                        </div>
                      </div>

                      <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Auto Post</label>  
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <asp:DropDownList ID="ddlAutoPost" class="form-control" runat="server" Width="200px">
                                            <asp:ListItem>False</asp:ListItem>
                                            <asp:ListItem>True</asp:ListItem>
                                            </asp:DropDownList><br />
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                      <div class="ln_solid"></div>
                      </div>
                      
                      <div class="form-group">
                      
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        
                          <asp:Button ID="btnSend" runat="server" Text="Send" class="btn btn-success" OnClick="btnSend_Click" ValidationGroup="SendDOXML" />
                          <asp:Button ID="btnClear" runat="server" Text="Clear" class="btn btn-primary" OnClick="btnClear_Click" />
                        </div>
                      </div>

                    </form>
                    
                  </div>
                </div>
              </div>
</asp:Content>
