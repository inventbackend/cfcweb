﻿<%@ Page Title="LOGIN" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TransportService.pages.Login" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CFC</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="css/custom.css" rel="stylesheet">
  </head>

  <body style="background:#F7F7F7;">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class=" form">
                <section class="login_content">
                        <form id="form" runat="server">
                        <h1>Login Form</h1>
                        </section>
                            <fieldset>
                            <div>
                            <asp:Label ID="lblError" runat="server" ForeColor="#FF3300" Font-Size="Medium" Font-Bold="true"></asp:Label>
                            </div>   
                                <div>
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" placeholder="UserName" ></asp:TextBox>  
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*harus diisi" 
                                    ControlToValidate="txtUserName" ValidationGroup="login" ForeColor="#FF3300" 
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>  
                                    <asp:TextBox ID="txtPassword"  runat="server"  CssClass="form-control" 
                                        placeholder="Password" TextMode="Password" ></asp:TextBox>                    
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*harus diisi" 
                                    ControlToValidate="txtPassword" ValidationGroup="login" ForeColor="#FF3300" 
                                    SetFocusOnError="True"></asp:RequiredFieldValidator> 
                                    
                                </div>                               
                                
                                
                            </fieldset>
                   <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" onclick="LoginButton_Click1" ValidationGroup="login" CssClass="btn-lg btn-success btn-block"/>   
                  </form>
                 
                    
            </div>
        </div>   
    </div>
</body>
</html>
