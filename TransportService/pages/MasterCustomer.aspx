﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="MasterCustomer.aspx.cs" Inherits="TransportService.pages.MasterCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
        rel="stylesheet">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <%--Temp Variabel view--%>
        <input type="hidden" value="" id="hidCustomerID" runat="server" />
        
        <%-->>--%>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
            <div class="title_left">
                <h3>
                    Master Customer</h3>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
            <ol class="breadcrumb pull-right">
                <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                <li class="active">Customer</li>
            </ol>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="x_panel">
                      <%--Modal Info Survey--%>
            <div id="ModalInfoReg" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Info Reg. Customer</h4>
                        </div>
                        <div class="modal-body">

                            
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            <asp:Label ID="lblSurveyID" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:Label ID="lblEmployeeID" runat="server" Text="" Visible="false"></asp:Label>
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    ID</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    : <asp:Label ID="lblCustomerID" runat="server" Text="Label" Font-Bold="True" Font-Size="Medium"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Name</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    : <asp:Label ID="lblCustomerName" runat="server" Text="Label" Font-Bold="True" Font-Size="Medium"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                   Address </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    : <asp:Label ID="lblAddress" runat="server" Text="Label" Font-Bold="True" Font-Size="Medium"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                   Area </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    : <asp:Label ID="lblArea" runat="server" Text="Label" Font-Bold="True" Font-Size="Medium"></asp:Label>
                                </div>
                            </div>
                            <div class="row"></div>
                        </div>
                 
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <asp:Button ID="btnExport" runat="server" Text="Export" 
                                CssClass="btn btn-success" onclick="btnExport_Click"
                                     />
                            
                        </div>

                      </div>
                    </div>
                  </div>
            <%-->>--%>
            <%--Modal Survey--%>
            <div id="ModalImportCustomer" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">Import Customer</h4>
                        </div>
                        <div class="modal-body">
                    
                                <table id="tblSurveyCustomer" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>
                                </th>
                                <th>
                                    Survey
                                </th>
                                <th>
                                    Employee Name
                                </th>
                                <th>
                                    Date
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptResultSurvey" runat="server" OnItemCommand="rptResultSurvey_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnShow" runat="server" Text="Show" class="btn btn-warning btn-xs"
                                                CommandName="Show"></asp:Button>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("SurveyID")%>' ID="dtSurveyID"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("EmployeeID") %>' ID="dtEmployeeID" Visible = "true"></asp:Label>
                                            <asp:Label runat="server" Text='<%# Eval("EmployeeName") %>' ID="dtEmployeeName"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Date") %>' ID="dtDate"></asp:Label>
                                        </td>
                                    </tr>
                            
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                   
                        </div>
                      </div>
                    </div>
                  </div>
            <%-->>--%>
  
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <asp:Button ID="btnNew" runat="server" Text="Add New" class="btn btn-primary pull-right"
                    OnClick="btnNew_Click"></asp:Button>
                    <asp:Button ID="btnMigrasiCustomer" runat="server" Text="Import From Survey" class="btn btn-success pull-right"
                    OnClick="btnMigrasiCustomer_Click"></asp:Button>
            </div>
            <div class="col-md-12 col-sm-12 col-sm-12 col-xs-12 ">
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                    <table id="dtCustomer" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>
                                </th>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Address
                                </th>
                                <th>
                                    Type
                                </th>
                                <th>
                                    City
                                </th>
                                <th>
                                    Country Region
                                </th>
                                <th>
                                    MD
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="RptCustomerlist" runat="server" OnItemCommand="rptCustomer_ItemCommand">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-warning btn-xs"
                                                CommandName="Update"></asp:Button>
                                        </td>
                                        <td>
                                            <%-- <a href="<%# String.Format("javascript:UpdateAction('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}');", Eval("CustomerName"),Eval("CustomerAddress"),Eval("CustomerTypeID"),Eval("City"),Eval("CountryRegionCode"),Eval("Account"),Eval("Distributor"),Eval("EmployeeID"),Eval("CustomerID"))%>">
                                            <%# Eval("CustomerID") %></a>--%>
                                            <asp:Label runat="server" Text='<%# Eval("CustomerID") %>' ID="dtCustomerID"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("CustomerName") %>' ID="dtCustomerName"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("CustomerAddress") %>' ID="dtCustomerAddress"></asp:Label>
                                        </td>
                                        <td>
                                             <asp:Label runat="server" Text='<%# Eval("CustomerTypeID") %>' ID="dtCustomerTypeID"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("City") %>' ID="dtCity"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("CountryRegionCode") %>' ID="dtCountryRegionCode"></asp:Label>
                                        </td>
<%--                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Account") %>' ID="dtAccount"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Distributor") %>' ID="dtDistributor"></asp:Label>
                                        </td>--%>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("EmployeeID") %>' ID="dtEmployeeID"></asp:Label>
                                        </td>
                                    </tr>
                                 
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
        </div>
       
    </div>
    <asp:Panel ID="PanelFormCustomer" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form Customer Management
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Customer Name</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:TextBox ID="txtCustomerName"  runat="server" CssClass="form-control"
                                        placeholder="Nama Toko"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Alamat</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:TextBox ID="txtAddress"  runat="server" CssClass="form-control"
                                        Rows="3" TextMode="MultiLine" placeholder="Alamat Toko"></asp:TextBox>
                                </div>
                                <br />
                            </div>
                            
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Customer type</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:DropDownList ID="ddlCustomerType" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12"><br /></div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    City</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:DropDownList ID="ddlCity" CssClass="form-control" runat="server">
                                        <asp:ListItem Text="JABODETABEK" Value="JABODETABEK" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                      
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Country Region</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:DropDownList ID="ddlCountryRegionCode" CssClass="form-control" runat="server">
                                        
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12"><br /></div>

                            <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Account</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:DropDownList ID="ddlAccount" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
             
                            <div class="col-md-6 col-sm-6 col-xs-12" style="display: none;" >
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Distributor</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <asp:DropDownList ID="ddlDistributor" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 col-xs-12"><br /></div>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                    Employee</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
<%--                                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
                                    <asp:RadioButtonList ID="rblEmployee" runat="server">
                                    </asp:RadioButtonList>--%>
                                    <asp:ListBox ID="ddlEmployee" runat="server"  data-placeholder="Employee" CssClass="form-control select2"></asp:ListBox>
                            
                            </div>
                                </div>
                            
                        </div>
                        <div class="row">
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success"
                                    OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn btn-success"
                                    OnClick="btnUpdate_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         <</div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
<%--    <script type="text/javascript" src="js/modal.js"></script>--%>
    <!-- Datatables -->
    <script type="text/javascript" src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script type="text/javascript" src="../vendors/jszip/dist/jszip.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Datatables -->
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2({});
            $('#dtCustomer').DataTable({
            });
            $('#tblSurveyCustomer').DataTable({
            });
        });

        $("#ModalImportCustomer").on('hide.bs.modal', function () {
            window.location.href = "MasterCustomer.aspx";
        });

        $("#ModalInfoReg").on('hide.bs.modal', function () {
            $('#ModalImportCustomer').modal('show');
        });
    </script>
<%--    <script type="text/javascript">

        function AddNewAction() {
            var txtCustomerName = document.getElementById('<%= txtCustomerName.ClientID %>');
            var txtAddress = document.getElementById('<%= txtAddress.ClientID %>');
            var ddlCustomerType = document.getElementById('<%= ddlCustomerType.ClientID %>');
            var ddlCity = document.getElementById('<%= ddlCity.ClientID %>');
            var ddlCountryRegionCode = document.getElementById('<%= ddlCountryRegionCode.ClientID %>');
            var ddlAccount = document.getElementById('<%= ddlAccount.ClientID %>');
            var ddlDistributor = document.getElementById('<%= ddlDistributor.ClientID %>');
            //var rblEmployee = document.getElementById('<%= rblEmployee.ClientID %>');

            var btnUpdate = document.getElementById('<%= btnUpdate.ClientID %>');
            var btnSubmit = document.getElementById('<%= btnSubmit.ClientID %>');

            btnUpdate.style.display = "none";
            btnSubmit.style.display = "block";

            txtCustomerName.value = "";
            txtAddress.value = "";
            ddlCustomerType.value = "";
            ddlCity.value = "";
            ddlCountryRegionCode.value = "";
            ddlAccount.value = "";
            ddlDistributor.value = "";

//            var radio = rblEmployee.getElementsByTagName("input");
//            for (var i = 0; i < radio.length; i++) {

//                radio[i].checked = false;

//            }

            modal.style.display = "block";
        }

        function UpdateAction(customerName, address, customerType, city, countryRegionCode, account, distributor, employee, customerID) {

            var hidCustomerID = document.getElementById('<%= hidCustomerID.ClientID %>');
            var txtCustomerName = document.getElementById('<%= txtCustomerName.ClientID %>');
            var txtAddress = document.getElementById('<%= txtAddress.ClientID %>');
            var ddlCustomerType = document.getElementById('<%= ddlCustomerType.ClientID %>');
            var ddlCity = document.getElementById('<%= ddlCity.ClientID %>');
            var ddlCountryRegionCode = document.getElementById('<%= ddlCountryRegionCode.ClientID %>');
            var ddlAccount = document.getElementById('<%= ddlAccount.ClientID %>');
            var ddlDistributor = document.getElementById('<%= ddlDistributor.ClientID %>');
//            var rblEmployee = document.getElementById('<%= rblEmployee.ClientID %>');

            var btnUpdate = document.getElementById('<%= btnUpdate.ClientID %>');
            var btnSubmit = document.getElementById('<%= btnSubmit.ClientID %>');

            btnUpdate.style.display = "block";
            btnSubmit.style.display = "none";

            hidCustomerID.value = customerID;
            txtCustomerName.value = customerName;
            txtAddress.value = address;
            ddlCustomerType.value = customerType;
            ddlCity.value = city;
            ddlCountryRegionCode.value = countryRegionCode;
            ddlAccount.value = account;
            ddlDistributor.value = distributor;



            var radio = rblEmployee.getElementsByTagName("input");
            for (var i = 0; i < radio.length; i++) {
                if (radio[i].value == employee) {
                    radio[i].checked = true;
                }
            }

            modal.style.display = "block";



        }
</script>--%>
<%--    <script type="text/javascript">
        function myFunction() {
            // Declare variables 
            var input, filter, table, tr, td, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("<%= rblEmployee.ClientID %>");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

</script>--%>
</asp:Content>
