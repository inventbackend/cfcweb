﻿/* documentation
 * 001 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Data;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace TransportService.pages
{
    public partial class ReportOSA_Product : System.Web.UI.Page
    {
        public static class Globals
        {
            public static int NOProduct = 0;
            public static string gAreaId = string.Empty;
            public static string gAccount = string.Empty;
            public static string gWeekFrom = string.Empty;
            public static string gWeekTo = string.Empty;
            public static string gTahun = string.Empty;
            public static string gCustomer = string.Empty;
            public static string gRole = string.Empty;
            public static string gUserId = "";
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        private void ClearContent()
        {
            //PnlCustomer.Visible = false;
            //btnSelectAll.Visible = false;
            //btnUnSelectAll.Visible = false;
        }

        private string getParamProduct()
        {
            String lResult = "";
            List<String> listProduct = new List<String>();
            var mdlProductList = OSAFacade.GetProduct(Globals.gAccount, Globals.gAreaId, Globals.gCustomer,Globals.gRole, Globals.gUserId);

            foreach (var item in mdlProductList)
            {
                listProduct.Add(item.ProductID);
            }


            int i = Globals.NOProduct;
            lResult = listProduct[i];
            Globals.NOProduct += 1; 

            return lResult;
        }

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            string lParamProduct = getParamProduct();
            var listOSAArea = OSAFacade.LoadOSAStock(lParamProduct, Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gCustomer, Globals.gTahun,Globals.gRole, Globals.gUserId);
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildStocklist");
                childRepeater.DataSource = listOSAArea;
                childRepeater.DataBind();
            }
        }

        private void BindData(string lWeekFrom, string lWeekTo)
        {
            var mdlOSAProduct = OSAFacade.GetProduct(Globals.gAccount, Globals.gAreaId, Globals.gCustomer,Globals.gRole, Globals.gUserId);
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = mdlOSAProduct;

            RptOSAProductlist.DataSource = pgitems;
            RptOSAProductlist.DataBind();
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.NOProduct = 0;
            Globals.gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                else
                    Globals.gUserId = Session["User"].ToString();

                Globals.gAreaId = Request.QueryString["AreaId"];
                Globals.gWeekFrom = Request.QueryString["WeekFrom"];
                Globals.gWeekTo = Request.QueryString["WeekTo"];
                Globals.gTahun = Request.QueryString["Tahun"];
                Globals.gAccount = Request.QueryString["Account"];
                Globals.gCustomer = Request.QueryString["Customer"];
                Globals.gRole = Request.QueryString["Role"];
                ClearContent();

                if (Globals.gWeekFrom != "")
                {
                    var mdlWeeks = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
                    PagedDataSource pgitemsweek = new PagedDataSource();
                    pgitemsweek.DataSource = mdlWeeks;
                    RptWeeks.DataSource = pgitemsweek;
                    RptWeeks.DataBind();
                }

                BindData(Globals.gWeekFrom, Globals.gWeekTo);
            }
        }

        //Export To Excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {

            var mdlOSAStockExcel = Core.Manager.OSAFacade.LoadOSAStockExcel(Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gCustomer, Globals.gTahun,Globals.gRole, Globals.gUserId);
            ReportDataSource rds = new ReportDataSource("DataSet1", mdlOSAStockExcel);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOSAStock.rdlc");
            ReportViewer1.LocalReport.Refresh();

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOSA_Stock" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download


            ////Create a dummy GridView
            //GridView GridView1 = new GridView();
            //GridView1.AllowPaging = false;
            //GridView1.DataSource = dt;
            //GridView1.DataBind();

            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition",
            // "attachment;filename=EXC_ReportOSA_Product.xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //for (int i = 0; i < GridView1.Rows.Count; i++)
            //{
            //    //Apply text style to each Row
            //    GridView1.Rows[i].Attributes.Add("class", "textmode");
            //}
            //GridView1.RenderControl(hw);

            ////style to format numbers to string
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //Response.Write(style);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();
        }

        //protected void btnShowCustomer_Click(object sender, EventArgs e)
        //{
        //    if (txtCustomerID.Text == "")
        //    {
        //        rdCustomer.DataSource = OSAFacade.GetCustomer(Globals.gAreaId, Globals.gAccount);
        //        rdCustomer.DataTextField = "CustomerName";
        //        rdCustomer.DataValueField = "CustomerID";
        //        rdCustomer.DataBind();
        //    }
        //    else
        //    {
        //        rdCustomer.DataSource = OSAFacade.GetCustomer(txtCustomerID.Text, Globals.gAreaId, Globals.gAccount);
        //        rdCustomer.DataTextField = "CustomerName";
        //        rdCustomer.DataValueField = "CustomerID";
        //        rdCustomer.DataBind();
        //    }

        //    PnlCustomer.Visible = true;
        //    btnSelectAll.Visible = true;
        //    btnUnSelectAll.Visible = true;

        //}

        //protected void btnUnSelectAll_Click(object sender, EventArgs e)
        //{

        //    foreach (ListItem item in rdCustomer.Items)
        //    {
        //        item.Selected = false;
        //    }
        //}

        //protected void btnSelectAll_Click(object sender, EventArgs e)
        //{
        //    foreach (ListItem item in rdCustomer.Items)
        //    {
        //        item.Selected = true;
        //    }
        //}

        //protected void btnShow_Click(object sender, EventArgs e)
        //{
        //    if (Globals.gWeekFrom != "")
        //    {
        //        var mdlWeeks = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo);
        //        PagedDataSource pgitemsweek = new PagedDataSource();
        //        pgitemsweek.DataSource = mdlWeeks;
        //        RptWeeks.DataSource = pgitemsweek;
        //        RptWeeks.DataBind();
        //    }

        //    Globals.NOProduct = 0;
        //    BindData(Globals.gWeekFrom, Globals.gWeekTo);
        //}

    }
    
}