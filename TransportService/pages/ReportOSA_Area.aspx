﻿<%@ Page Title="Report OSA Area" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportOSA_Area.aspx.cs" Inherits="TransportService.pages.ReportOSA_Area" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<%--<!-- gauge.js -->
    <script>
        var opts = {
            lines: 12,
            angle: 0,
            lineWidth: 0.4,
            pointer: {
                length: 0.75,
                strokeWidth: 0.042,
                color: '#1D212A'
            },
            limitMax: 'false',
            colorStart: '#1ABC9C',
            colorStop: '#1ABC9C',
            strokeColor: '#F0F3F3',
            generateGradient: true
        };
        var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

        gauge.maxValue = 6000;
        gauge.animationSpeed = 32;
        gauge.set(3200);
        gauge.setTextField(document.getElementById("gauge-text"));
    </script>
    <!-- /gauge.js -->--%>

    <!-- morris.js -->
    <link href="../vendors/morris.js/morris.css" rel="stylesheet">
    <script src="js/journey/jquery-1.11.3.js"></script>
    <script src="../vendors/raphael/raphael.min.js"></script>
    <script src="../vendors/morris.js/morris.min.js"></script>
    <!--bar fix-->
    <script>
        var arrChart = [];
    </script>
    <!--bar fix-->
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                OSA Area
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <%--<<box parameter--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Parameter <small>Area</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblWeekFrom" runat="server" Text="Week From" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="lblttdua1" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlWeekFrom" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblWeekTo" runat="server" Text="Week To" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="lblttdua2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlWeekTo" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:TextBox ID="txtBranchID" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"
                            PlaceHolder="Search Branch"></asp:TextBox>
                        <asp:Button ID="btnShowBranch" runat="server" CssClass="btn btn-success" Text="Show Branch"
                            Style="margin-left: 10px" OnClick="btnShowBranch_Click" />
                        <asp:Label ID="lblValblBranch" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                        <asp:Panel ID="PnlBranch" CssClass="form-control" Height="200px" Width="400px" runat="server"
                            ScrollBars="Vertical" Style="margin-left: 10px">
                            <asp:CheckBoxList ID="blBranch" runat="server">
                            </asp:CheckBoxList>
                        </asp:Panel>
                        <br />
                        <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                            OnClick="btnSelectAll_Click" Style="margin-left: 10px" />
                        <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL"
                            OnClick="btnUnSelectAll_Click" Style="margin-left: 10px" />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblTahun" runat="server" Text="Tahun" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="lblttdua3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlTahun" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <asp:RadioButtonList ID="rblRole" RepeatDirection="Horizontal"  runat="server">
                            <asp:ListItem Selected="True" Text="All &nbsp &nbsp" Value="" />
                            <asp:ListItem Text="MD &nbsp &nbsp" Value="R0001" />
                            <asp:ListItem Text="BP" Value="R0006" />
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-embossed btn-primary "
                            Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                        <asp:Button ID="btnExportExcel" runat="server" Text="Export To Excel" class="btn btn-embossed btn-primary "
                            Font-Size="14px" OnClick="btnExportExcel_Click" ValidationGroup="valShowReport" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--box parameter>>--%>
    <%--<<tab chart and table--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <i class="fa fa-bars"></i>Charts & Tables<small>Area</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                        <li role="presentation" class=""><a href="#tab_content11" id="home-tabb" role="tab"
                            data-toggle="tab" aria-controls="home" aria-expanded="true">Charts</a> </li>
                        <li role="presentation" class="active"><a href="#tab_content22" role="tab" id="profile-tabb"
                            data-toggle="tab" aria-controls="profile" aria-expanded="false">Tables</a> </li>
                    </ul>
                    <div id="myTabContent2" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade" id="tab_content11" aria-labelledby="home-tab">
                            <asp:Repeater ID="RptChartlist" runat="server">
                                <ItemTemplate>
                                    <!-- bar chart -->
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>
                                                    <small>Branch : </small>
                                                    <%# Eval("element")%></h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                                </ul>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="x_content">
                                                <div id='<%# Eval("element") %>_chart' style="width: 100%; height: 280px;">
                                                </div>
                                                
                                                <script>
                                                
//                                                     $(document).ready(function () {
//                                                     Morris.Bar({
//                                                     
//                                                     element: '<%# Eval("element") %>_chart' ,
//                                                     data: [<%# Eval("data") %>],
//                                                     xkey: 'weeks',
//                                                     ykeys: ['percents'],
//                                                     labels: ['Percent'],
//                                                     barRatio: 0.4,
//                                                     barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
//                                                     xLabelAngle: 35,
//                                                     hideHover: 'auto',
//                                                     resize: true
//                                                       });
//                                                    
//                                                });
                                                    arrChart.push(new Morris.Bar({
                                                        element: '<%# Eval("element") %>_chart',
                                                        data: [<%# Eval("data") %>],
                                                        xkey: 'weeks',
                                                        ykeys: ['percents'],
                                                        labels: ['Percent'],
                                                        barRatio: 0.4,
                                                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                                        xLabelAngle: 35,
                                                        hideHover: 'auto',
                                                        resize: true,
                                                     redraw:true 
                                                     }));
                                                </script>
                                                <!-- jVectorMap -->
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /bar charts -->
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content22" aria-labelledby="profile-tab">
                            <div class="datagrid" style="width: 100%">
                                <table style="width: 100%; margin-bottom: 0px;">
                                    <thead>
                                        <tr>
                                            <th>
                                                Area
                                            </th>
                                            <asp:Repeater ID="RptWeeks" runat="server">
                                                <ItemTemplate>
                                                    <th>
                                                        <asp:Label ID="lWeek" runat="server" Text='<%# Eval("Week") %>'></asp:Label>
                                                    </th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <asp:Repeater ID="RptOSAArealist" runat="server" OnItemDataBound="ItemBound" OnItemCommand="RptOSAArealist_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lnkToAccount" Text='<%# Eval("ID") %>' runat="server" CommandName="ToAccount" />
                                                        </td>
                                                        <%--Repeater for child data percentage--%>
                                                        <asp:Repeater ID="RptChildPercentlist" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                    <asp:Label ID="Percent" runat="server" Text='<%# Eval("Percent") %>'></asp:Label>
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <b>Grand Total</b>
                                            </td>
                                            <asp:Repeater ID="RptChildGrandTotal" runat="server">
                                                <ItemTemplate>
                                                    <td>
                                                        <b>
                                                            <asp:Label ID="GrandTotal" runat="server" Text='<%# Eval("Percent") %>'></asp:Label></b>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </tbody>
                                    <%--<tfoot>
                                    <tr>
                                        <td colspan="8">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                         </td>
                                    </tr>
                                </tfoot>--%>
                                </table>
                            </div>
                        </div>
                        <asp:Panel ID="PanelReport" runat="server" Height="500px" Width="100%" Visible="False">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt">
                                <LocalReport ReportPath="Reports\ReportOSAArea.rdlc">
                                </LocalReport>
                            </rsweb:ReportViewer>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--bar fix-->
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('.nav-tabs a').on('shown.bs.tab', function (event) {
                for (var i = 0; i < arrChart.length; i++) {
                    arrChart[i].redraw();
                }
            });
        });
    </script>
<!--bar fix-->
    <%--tab chart and table>>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">

</asp:Content>
