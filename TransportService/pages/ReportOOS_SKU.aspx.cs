﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class ReportOOS_SKU : System.Web.UI.Page
    {
        private List<Core.Model.mdlOOSByBranchSKU> listOOSProductPerBranch;
        private List<int> listWeek;
        private List<Core.Model.mdlOOSByAccountSKU> listAccountProductOOS;
        public string user;

        protected void Page_Load(object sender, EventArgs e)
        {
            user = Session["User"].ToString();

            if (!IsPostBack)
            {                
                DateTime dtmNow = DateTime.Now;
                List<int> dsWeek = new List<int>();
                List<int> dsWeekFrom = new List<int>();
                List<int> dsyear = new List<int>();

                int weekNow = DateFacade.GetIso8601WeekOfYear(dtmNow);
                for (int i = weekNow; i >= 1; i--)
                {
                    dsWeekFrom.Add(i);
                }


                for (int i = 54; i >= 1; i--)
                {
                    dsWeek.Add(i);
                }

                for (int i = dtmNow.Year - 3; i <= dtmNow.Year; i++)
                {
                    dsyear.Add(i);
                }


                ddlYear.DataSource = dsyear;
                ddlYear.SelectedIndex = dsyear.Count - 1;
                ddlYear.DataBind();

                ddlWeekFrom.DataSource = dsWeekFrom;
                ddlWeekFrom.DataBind();

                ddlWeekTo.DataSource = dsWeek;
                ddlWeekTo.DataBind();

                var listProduct = ProductFacade.GetProduct();
                rblProductID.DataSource = listProduct;
                rblProductID.DataTextField = "ProductName";
                rblProductID.DataValueField = "ProductID";
                rblProductID.DataBind();

                var listBranch = BranchFacade.LoadBranch();

                cblArea.DataSource = listBranch;
                cblArea.DataTextField = "BranchID";
                cblArea.DataValueField = "BranchName";
                cblArea.DataBind();

                cblArea.Visible = true;
                cblAccount.Visible = false;
                divTable.Visible = false;
                divTableAccount.Visible = false;

                cblAccount.DataSource = AccountFacade.GetAccount(StringFacade.JoinListItemCollection(cblArea));
                cblAccount.DataTextField = "AccountID";
                cblAccount.DataValueField = "AccountID";
                cblAccount.DataBind();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            if (ddlBy.SelectedValue == "AREA")
            {
                divTable.Visible = true;
                divTableAccount.Visible = false;
                string area = "";
                foreach (ListItem item in cblArea.Items)
                {
                    if (item.Selected == true)
                    {
                        if (item == cblArea.Items[cblArea.Items.Count - 1])
                        {
                            area += item.Text;
                        }
                        else
                        {
                            area += item.Text + ",";
                        }
                    }
                }

                listOOSProductPerBranch = OOSFacade.GetOOSByBranchSKU(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), area, Convert.ToInt16(ddlYear.SelectedValue), rblProductID.SelectedValue,rblRole.SelectedValue, user);
                listWeek = listOOSProductPerBranch.Select(fld => fld.VisitWeek).Distinct().ToList();
                var listBranch = listOOSProductPerBranch.Select(fld => fld.BranchID).Distinct().ToList();
                var newlistBranch = new List<Core.Model.mdlOOSArea>();
                foreach (var temp in listBranch)
                {
                    var model = new Core.Model.mdlOOSArea();
                    model.Area = temp;
                    model.Link = "ReportOOS_SKUAccountBranch.aspx?fromweek=" + ddlWeekFrom.SelectedValue + "&toweek=" + ddlWeekTo.SelectedValue + "&branchid=" + temp + "&year=" + ddlYear.SelectedValue + "&productid=" + rblProductID.SelectedValue + "&role=" + rblRole.SelectedValue;
                    newlistBranch.Add(model);
                }

                rptHeader.DataSource = listWeek;
                rptHeader.DataBind();
                Control HeaderTemplate = rptHeader.Controls[0].Controls[0];

                Label lblHeader = HeaderTemplate.FindControl("lblHeader") as Label;
                lblHeader.Text = "AREA";

                rptparent.DataSource = newlistBranch;
                rptparent.DataBind();

                var listChartBranch = new List<Core.Model.mdlOOSChart>();
                int chartID = 1;
                foreach(var branch in newlistBranch)
                {
                    var tempListOOS = listOOSProductPerBranch.Where(fld => fld.BranchID.Equals(branch.Area));
                    //var listChartBranchData = new List<Core.Model.mdlOOSChartData>();
                    var listChartBranchData = tempListOOS.Select(fld => new { fld.VisitWeek, fld.OOS }).OrderBy(fld=>fld.VisitWeek).ToList();

                    string json = Core.Services.RestPublisher.Serialize(listChartBranchData);
                    var chart = new Core.Model.mdlOOSChart();
                    chart.ID = "chart"+ chartID.ToString();
                    chart.Area = branch.Area;
                    chart.json = json;
                    listChartBranch.Add(chart);

                    chartID++;
                    
                }




                rptBranchChart.DataSource = listChartBranch;
                rptBranchChart.DataBind();

                var listOOSGrandTotal = new List<decimal>();
                foreach (int i in listWeek)
                {
                    decimal totalOOS = listOOSProductPerBranch.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.OutOfStock);

                    decimal totalListed = totalOOS + listOOSProductPerBranch.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.Listed);
                    
                    decimal average = 0;

                    if (totalListed == 0)
                    {
                        average = 0;
                    }
                    else
                    {
                        average = (totalOOS / totalListed) * 100;
                    }
                    //decimal average = listOOSProductPerBranch.Where(fld => fld.VisitWeek.Equals(i)).Average(r => r.OOS);
                    listOOSGrandTotal.Add(decimal.Round(average, 2));
                }

                rptGrandTotal.DataSource = listOOSGrandTotal;
                rptGrandTotal.DataBind();
            }
            else
            {
                divTableAccount.Visible = true;
                divTable.Visible = false;
                string account = "";
                foreach (ListItem item in cblAccount.Items)
                {
                    if (item.Selected == true)
                    {
                        if (item == cblArea.Items[cblArea.Items.Count - 1])
                        {
                            account += item.Text;
                        }
                        else
                        {
                            account += item.Text + ",";
                        }
                    }

                }

                listAccountProductOOS = OOSFacade.GetOOSByAccountSKU(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), account, rblProductID.SelectedValue, Convert.ToInt16(ddlYear.SelectedValue),rblRole.SelectedValue, user);
                listWeek = listAccountProductOOS.Select(fld => fld.VisitWeek).Distinct().ToList();
                var listAccount = listAccountProductOOS.Select(fld => new { fld.Account,fld.ProductID,fld.ProductName }).Distinct().ToList();


                rptHeaderReportAccount.DataSource = listWeek;
                rptHeaderReportAccount.DataBind();
                Label lblControl = rptHeaderReportAccount.Controls[0].Controls[0].FindControl("lblHeader") as Label;
                lblControl.Text = "ACCOUNT";


                rptParentReportAccount.DataSource = listAccount;
                rptParentReportAccount.DataBind();

                var listChartAccount = new List<Core.Model.mdlOOSChart>();
                int chartID = 1;
                foreach (var acc in listAccount)
                {
                    var tempListOOS = listAccountProductOOS.Where(fld => fld.Account.Equals(acc.Account));
                    //var listChartBranchData = new List<Core.Model.mdlOOSChartData>();
                    var listChartBranchData = tempListOOS.Select(fld => new { fld.VisitWeek, fld.OOS }).OrderBy(fld => fld.VisitWeek).ToList();

                    string json = Core.Services.RestPublisher.Serialize(listChartBranchData);
                    var chart = new Core.Model.mdlOOSChart();
                    chart.ID = "chart" + chartID.ToString();
                    chart.Area = acc.Account;
                    chart.json = json;
                    listChartAccount.Add(chart);

                    chartID++;

                }

                rptBranchChart.DataSource = listChartAccount;
                rptBranchChart.DataBind();


                var listOOSGrandTotal = new List<decimal>();
                foreach (int i in listWeek)
                {

                    decimal totalOOS = 0;
                    decimal totalListed = 0;
                    decimal average = 0;
                    foreach (var acc in listAccount)
                    {
                        totalOOS = totalOOS + listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i) && fld.Account.Equals(acc.Account)).Sum(r => r.OutOfStock);
                        if (listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i) && fld.Account.Equals(acc.Account)).Sum(r => r.OutOfStock) > 0)
                            totalListed = totalListed + listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i) && fld.Account.Equals(acc.Account)).Sum(r => r.Listed);
                    }
                    if ((totalOOS + totalListed) != 0)
                    {
                        average = totalOOS / (totalListed + totalOOS) * 100;
                    }
                    //decimal average = listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i)).Average(r => r.OOS);
                    listOOSGrandTotal.Add(decimal.Round(average, 2));
                }

                rptAccountGrandTotal.DataSource = listOOSGrandTotal;
                rptAccountGrandTotal.DataBind();

               
            }
        }
        private string branchID;
        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ddlBy.SelectedItem.ToString() == "AREA")
                {
                    Label lblBranch = ri.FindControl("lblArea") as Label;
                    string branch = lblBranch.Text;
                    branchID = branch;
                    Repeater childRepeater = (Repeater)args.Item.FindControl("rptChild");

                    var listBranch = listOOSProductPerBranch.Where(fld => fld.BranchID.Equals(branch)).ToList();
                    foreach (int i in listWeek)
                    {
                        var tempBranch = listBranch.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                        if (tempBranch == null)
                        {
                            var model = new Core.Model.mdlOOSByBranchSKU();
                            model.BranchID = branch;
                            model.Link = "";
                            model.ProductID = rblProductID.SelectedValue;
                            model.Listed = 0;
                            model.OutOfStock = 0;
                            model.VisitWeek = i;
                            listBranch.Add(model);
                        }
                    }
                    listBranch = listBranch.OrderBy(fld => fld.VisitWeek).ToList();
                    childRepeater.DataSource = listBranch;
                    childRepeater.DataBind();
                }
                else
                {

                }
            }
        }

       


        protected void ReportAccountItemBound(object sender, RepeaterItemEventArgs args)
        {
             RepeaterItem ri = args.Item;
             if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
             {
                 Label lblAccount = ri.FindControl("lblAccount") as Label;
                 Label lblProductID = ri.FindControl("lblProductID") as Label;
                 string account = lblAccount.Text;
                 Repeater childRepeater = (Repeater)args.Item.FindControl("rptChildAccountStock");



                 var listChild = listAccountProductOOS.Where(fld => fld.Account.Equals(account) & fld.ProductID.Equals(lblProductID.Text)).ToList();
                 var listChildFinal = new List<Core.Model.mdlOOSByAccountSKU>();
                 foreach (int i in listWeek)
                 {
                     var tempList = listChild.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                     if (tempList != null)
                     {
                         var model = new Core.Model.mdlOOSByAccountSKU();
                         model.Account = tempList.Account;
                         model.OOS = tempList.OOS;
                         model.ProductID = tempList.ProductID;
                         model.ProductName = tempList.ProductName;
                         model.VisitWeek = tempList.VisitWeek;
                         model.Link = "ReportOOS_SKUCustomer.aspx?week=" + model.VisitWeek + "&account=" + model.Account + "&productid=" + model.ProductID + "&year=" + ddlYear.SelectedValue + "&role=" + rblRole.SelectedValue;
                         listChildFinal.Add(model);

                     }
                     else
                     {
                         var model = new Core.Model.mdlOOSByAccountSKU();

                         //model.Account = "";
                         model.VisitWeek = i;
                         //model.Link = "";
                         //model.OOSCustomer = 0;
                         //model.ProductID = "";
                         //model.ProductName = "";
                         listChildFinal.Add(model);


                     }

                 }

                 listChildFinal = listChildFinal.OrderBy(fld => fld.VisitWeek).ToList();


                 childRepeater.DataSource = listChildFinal;
                 childRepeater.DataBind();
             }
        }

        protected void btnShowExcel_Click(object sender, EventArgs e)
        {

        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            if (ddlBy.SelectedValue.ToString() == "AREA")
            {
                SetCblAreaChecked();
                SetCblAccountUnchecked();
            }
            else
            {

                SetCblAccountChecked();
                SetCblAreaUnchecked();
            }
        }



        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            SetCblAreaUnchecked();
            SetCblAccountUnchecked();
        }

        protected void ddlBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBy.SelectedValue.ToString() == "AREA")
            {
                divTable.Visible = false;
                //divTableAccount.Visible = false;
                cblArea.Visible = true;
                cblAccount.Visible = false;
            }
            else
            {
                divTable.Visible = false;
                //divTableAccount.Visible = false;
                cblArea.Visible = false;
                cblAccount.Visible = true;
            }

            SetCblAreaUnchecked();
            SetCblAccountUnchecked();
        }
  
        //////////////////////////////////////////////////////////////////////////////////
    
        private string getAccount()
        {
            string account = "";
            foreach (ListItem item in cblAccount.Items)
            {
                if (item.Selected == true)
                {
                    if (item == cblArea.Items[cblArea.Items.Count - 1])
                    {
                        account += item.Text;
                    }
                    else
                    {
                        account += item.Text + ",";
                    }
                }

            }

            return account;
        }

        private string getArea()
        {
            string area = "";
            foreach (ListItem item in cblArea.Items)
            {
                if (item.Selected == true)
                {
                    if (item == cblArea.Items[cblArea.Items.Count - 1])
                    {
                        area += item.Text;
                    }
                    else
                    {
                        area += item.Text + ",";
                    }
                }

            }

            return area;
        }
        private void SetCblAreaUnchecked()
        {
            foreach (ListItem item in cblArea.Items)
            {
                item.Selected = false;
            }
        }

        private void SetCblAreaChecked()
        {
            foreach (ListItem item in cblArea.Items)
            {
                item.Selected = true;
            }
        }

        private void SetCblAccountChecked()
        {
            foreach (ListItem item in cblAccount.Items)
            {
                item.Selected = true;
            }
        }



        private void SetCblAccountUnchecked()
        {
            foreach (ListItem item in cblAccount.Items)
            {
                item.Selected = false;
            }
        }
    }
}