﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class QuestionCategoryDetails : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId, gQuestionCategoryId, gQuestionCategoryText;
        public Core.Model.User vUser;

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        public string lParam { get; set; }

        private void ClearContent()
        {
            BindData();

            
            PanelFormQuestionCategory.Visible = false;
        }

        private void BindData()
        {
            var mdlQuestion_CategoryDetailList = new List<Core.Model.mdlQuestion_CategoryDetail>();

            mdlQuestion_CategoryDetailList = QuestionCategoryFacade.LoadQuestionCategoryDetail(gQuestionCategoryId,Boolean.Parse(ddlIsactive.SelectedValue), gUserId);

            //var role = getRole();
            //foreach (var mdlQuestionCategory in mdlQuestion_CategoryDetailList)
            //{
            //    mdlQuestionCategory.Role = role;
            //    btnNew.Visible = role;
            //}

            RptQuestionCategoryDetaillist.DataSource = mdlQuestion_CategoryDetailList;
            RptQuestionCategoryDetaillist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gUserId = Session["User"].ToString();
            gQuestionCategoryId = Request.QueryString["QuestionCategoryID"];

            if (!IsPostBack)
            {
                ddlQuestionSet.DataSource = QuestionSetFacade.LoadQuestionSetDDL(true,gUserId);
                ddlQuestionSet.DataTextField = "QuestionSetText";
                ddlQuestionSet.DataValueField = "QuestionSetID";
                ddlQuestionSet.DataBind();

                ddlQuestionCategoryID.DataSource = QuestionCategoryFacade.LoadQuestionCategoryDDL(true,gUserId);
                ddlQuestionCategoryID.DataTextField = "QuestionCategoryText";
                ddlQuestionCategoryID.DataValueField = "QuestionCategoryID";
                ddlQuestionCategoryID.DataBind();

                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                var qsQuestionCategoryID = Request.QueryString["QuestionCategoryID"];
                if (qsQuestionCategoryID != null)
                {
                    gQuestionCategoryId = qsQuestionCategoryID;
                }

                var qsQuestionCategoryText = Request.QueryString["QuestionCategoryText"];
                if (qsQuestionCategoryID != null)
                {
                    gQuestionCategoryText = qsQuestionCategoryText;
                }

                lblQuestionCategoryID.Text = gQuestionCategoryId;
                lblQuestionCategoryText.Text = gQuestionCategoryText;

                ClearContent();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var lmdlQuestion_CategoryDetail = new mdlQuestion_CategoryDetail();
            lmdlQuestion_CategoryDetail.QuestionCategoryID = ddlQuestionCategoryID.SelectedValue;
            lmdlQuestion_CategoryDetail.QuestionSetID = ddlQuestionSet.SelectedValue;
            QuestionCategoryFacade.UpdateQuestionCategoryDetail(lmdlQuestion_CategoryDetail,gUserId);
            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var lmdlQuestion_CategoryDetail = new mdlQuestion_CategoryDetail();
            lmdlQuestion_CategoryDetail.QuestionCategoryID = ddlQuestionCategoryID.SelectedValue;
            lmdlQuestion_CategoryDetail.QuestionSetID = ddlQuestionSet.SelectedValue;
            QuestionCategoryFacade.InsertQuestionCategoryDetail(lmdlQuestion_CategoryDetail,gUserId);

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormQuestionCategory.Visible = true;
            //ddlQuestionCategoryID.SelectedValue = QuestionCategoryFacade.GenerateQuestionCategoryID();

            btnInsert.Visible = true;
            btnUpdate.Visible = false; 
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }

        protected void RptQuestionCategoryDetaillist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtQuestionCategoryID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionCategoryID")).Text);
            //var dtQuestionCategoryText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionCategoryText")).Text);
            var dtQuestionSetID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionSetID")).Text);

            if (e.CommandName == "Link")
            {
                String Url = string.Format("Question_CategoryDetail.aspx?QuestionCategoryID={0}", dtQuestionCategoryID);
                Response.Redirect(Url);
            }

            if (e.CommandName == "Update")
            {
                ddlQuestionCategoryID.SelectedValue = dtQuestionCategoryID;
                ddlQuestionSet.SelectedValue = dtQuestionSetID;

                btnInsert.Visible = false;
                btnUpdate.Visible = true;


                PanelFormQuestionCategory.Visible = true;
            }

            if (e.CommandName == "Delete")
            {
                QuestionCategoryFacade.NonActiveQuestioncategory(dtQuestionCategoryID, gUserId);
                ClearContent();

            }
        }
    }
}