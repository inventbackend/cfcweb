﻿<%@ Page Title="Daily Message Role Management" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="DailyMessageDetail.aspx.cs" Inherits="TransportService.pages.DailyMessageDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Form Daily Message <small>ACCESS ROLE</small></h2>
                <%--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>--%>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <br />
                <div class="form-group">
                    <asp:Label ID="lbl1" runat="server" Text="Employee :" Cssclass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px;" Width="100px" Font-Bold="True"></asp:Label>
                    <asp:TextBox ID="txtSearch" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                    <asp:Button ID="btnSearch2" runat="server" Text="Search" OnClick="btnSearch2_Click"
                        class="btn btn-primary" Style="margin-left: 5px;"></asp:Button>
                    <asp:Button ID="btnBack" runat="server" Text="Home" OnClick="btnBack_Click" class="btn btn-primary"
                        Style="margin-left: -5px;"></asp:Button>

                        <br />
                        <br />
                    <div class="datagrid" style="width: 1000px">
                        <table style="width: 100%; margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>
                                        Message ID
                                    </th>
                                    <th>
                                        Employee ID
                                    </th>
                                    <th>
                                        Employee Name
                                    </th>
                                    <th>
                                        Branch ID
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptDailyMsglist" runat="server" OnItemCommand="rptDailyMsg_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="MsgID" runat="server" Text='<%# Eval("MessageID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="EmployeeID" runat="server" Text='<%# Eval("EmployeeID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="EmployeeName" runat="server" Text='<%# Eval("EmployeeName") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="BranchID" runat="server" Text='<%# Eval("BranchID") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkDelete" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete The Access Role of this Daily Message ?');"
                                                    runat="server" CommandName="Delete" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <tfoot>
                                    <tr>
                                        <td colspan="5">
                                            <div id="paging">
                                                <ul>
                                                    <li>
                                                        <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                    <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                    runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <li>
                                                        <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                </ul>
                                            </div>
                                    </tr>
                                </tfoot>
                        </table>
                    </div>
                    <br />
                    <asp:Button ID="btnAdd" runat="server" Text="Add New" class="btn btn-primary" OnClick="btnAdd_Click" />
                </div>
            </div>
        </div>
        <%-------------------------------------------Form Input------------------------------------------------%>
        <asp:Panel ID="PanelFormDailyMsg" runat="server" ScrollBars="Vertical" Width="1000px">
            <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                    <%--<div class="x_title">
                        <h2>
                            Form Basic Elements <small>different form elements</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a> </li>
                                    <li><a href="#">Settings 2</a> </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        <div class="clearfix">
                        </div>
                    </div>--%>
                    <div class="x_content">
                        <br />
                        <div>
                            <fieldset>
                                <legend>Form Manage Daily Message Access Role</legend>
                                <p>
                                    <asp:Label ID="lblMsgID" runat="server" Text="Message ID" FONT-BOLD="true"></asp:Label><br />
                                    <asp:TextBox ID="txtMsgID" runat="server" Width="400px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                </p>
                                <br /> <br />
                                <p>
                                    <asp:Label ID="lblBranch" runat="server" Text="Branch ID" FONT-BOLD="true"></asp:Label><br />
                                    <%--<asp:TextBox ID="txtBranch" runat="server" Width="150px" CssClass="form-control col-md-3 col-xs-12"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtBranch" ValidationGroup="DailyMsg" ForeColor="#FF3300"
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:Button ID="btnShowBranch" runat="server" Text="Show Branch" Style="margin-left: 9px;
                                        margin-top: 0px" OnClick="btnShowBranch_Click" CssClass="btn btn-success"></asp:Button>
                                    <asp:TextBox ID="txtSearchBranch" runat="server" Width="150px" CssClass="tb5"></asp:TextBox>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search Branch" Style="margin-left: 9px;
                                        margin-top: 0px" OnClick="btnSearch_Click" CssClass="btn btn-success"></asp:Button>
                                    <asp:Button ID="btnCancelBranch" runat="server" Text="Cancel" Style="margin-left: 9px;
                                        margin-top: 0px;" OnClick="btnCancelBranch_Click" CssClass="btn btn-success"></asp:Button>--%>
                                        <asp:DropDownList ID="ddlSearchBranchID" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                                        </asp:DropDownList>
                                </p>
                                <asp:Panel ID="PanelBranch" runat="server" Width="100%" Height="200px" ScrollBars="Vertical"
                                    CssClass="datagrid" style="margin-left:10px">
                                    <table style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Branch ID
                                                </th>
                                                <th>
                                                    Branch Name
                                                </th>
                                                <th>
                                                    Branch Description
                                                </th>
                                                <th>
                                                    Company ID
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="RptBranchlist" runat="server">
                                            <%--OnItemCommand="rptBranch_ItemCommand"--%>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:LinkButton ID="lnkBranchId" runat="server" CommandName="Link" Text='<%# Eval("BranchID") %>'></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <%# Eval("BranchName") %>
                                                        </td>
                                                        <td>
                                                            <%# Eval("BranchDescription") %>
                                                        </td>
                                                        <td>
                                                            <%# Eval("CompanyID") %>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </asp:Panel>
                                <p>
                                    <asp:Label ID="lblMsgDesc" runat="server" Text="Employee" FONT-BOLD="true"></asp:Label>
                                    <br />
                                    <asp:Button ID="btnShowEmployee" runat="server" Text="Show" OnClick="btnShowEmployee_Click"
                                        CssClass="btn btn-success"></asp:Button>
                                    <asp:Label ID="lblValbl" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                                    <asp:Panel ID="Panel1" CssClass="form-control" Height="400px" Width="400px" runat="server"
                                        ScrollBars="Vertical">
                                        <asp:CheckBoxList ID="blEmployee" runat="server">
                                        </asp:CheckBoxList>
                                    </asp:Panel>
                                    <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                                        OnClick="btnSelectAll_Click" />
                                    <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success" Text="UNSELECT ALL"
                                        OnClick="btnUnSelectAll_Click" />
                                    <%--<asp:TextBox ID="txtMsgDesc" runat="server" Width="600px" Height="300px" CssClass="tb5" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" 
                                          ControlToValidate="txtMsgDesc" ValidationGroup="DailyMsg" ForeColor="#FF3300" 
                                          SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                    <p>
                                    </p>
                                    <br />
                                    <asp:Button ID="btnInsert" runat="server" class="btn btn-primary" 
                                        OnClick="btnInsert_Click" Text="Insert" ValidationGroup="DailyMsg" />
                                    <asp:Button ID="btnCancel" runat="server" class="btn btn-primary" 
                                        OnClick="btnCancel_Click" Text="Cancel" />
                                    <p>
                                    </p>
                                </p>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
