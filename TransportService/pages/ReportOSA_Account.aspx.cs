﻿/* documentation
 * 001 nanda 06/11/2017 - Optimasi Looping Data 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Globalization;
using System.Data;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace TransportService.pages
{
    public partial class ReportOSA_Account : System.Web.UI.Page
    {
        public static class Globals
        {
            public static int NOAccount = 0;
            public static string gUserId = string.Empty;
            public static string gAreaId = string.Empty;
            public static string gWeekFrom = string.Empty;
            public static string gWeekTo = string.Empty;
            public static string gTahun = string.Empty;
            public static string gRole = string.Empty;
            //public static List<String> gAreaList = new List<String>();

            public static List<Core.Model.mdlWeek> mdlOSAWeekList = new List<Core.Model.mdlWeek>();
            public static List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            public static DataTable DtAccount = new DataTable();
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public void getParam()
        {
            if (Request.QueryString["AreaId"] == null)
            {
                //get user branchid
                //Globals.gAreaId = ddlArea.SelectedValue;
                Globals.gWeekFrom = ddlWeekFrom.SelectedValue;
                Globals.gWeekTo = ddlWeekTo.SelectedValue;
                Globals.gTahun = ddlTahun.SelectedValue;
            }
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        private void ClearContent()
        {
            Globals.NOAccount = 0;
            //Globals.gUserId = string.Empty;
            Globals.gAreaId = string.Empty;
            Globals.gWeekFrom = string.Empty;
            Globals.gWeekTo = string.Empty;
            Globals.gTahun = string.Empty;

            Globals.mdlOSAWeekList = new List<Core.Model.mdlWeek>();
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            Globals.DtAccount = new DataTable();

            //put year in dropdown
            ddlTahun.Items.Clear();

            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= 5; i++)
            {
                // Now just add an entry that's the current year minus the counter
                ddlTahun.Items.Add((currentYear - i).ToString());
            }

            //put Week in dropdown
            int lWeekNow = GetIso8601WeekOfYear(DateTime.Now);
            for (int i = lWeekNow; i >= 0; i--)
            {
                // Now just add an entry that's the current year minus the counter
                ddlWeekFrom.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 52; i >= 0; i--)
            {

                ddlWeekTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            PnlAccount.Visible = false;
            btnSelectAll.Visible = false;
            btnUnSelectAll.Visible = false;
        }

        private bool CheckAccount()
        {
            //validasi jika account checkbox nya kosong
            bool lReturn = false;
            int icount = 0;
            foreach (ListItem Item in blAccount.Items)
            {
                if (Item.Selected == true)
                {
                    icount++;
                }
            }

            if (icount == 0)
            {
                Response.Write("<script>alert('Account is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        } 

        //<<chartcode
        private void GetChartData(List<Core.Model.mdlOSAData> mdlOSAArealist)
        {
            String lData = string.Empty;
            //List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();
            Core.Model.mdlGraphData details = new Core.Model.mdlGraphData();
            foreach (var lParam in mdlOSAArealist)
            {
                details = new Core.Model.mdlGraphData();
                details.element = lParam.ID;
                if (lParam.Percent != "")
                {
                    lData += "{ weeks: '" + lParam.Week + "', percents: " + lParam.Percent.Replace("%", "") + "},";
                }
                details.data = lData;
            }
            if (details.data != null)
                Globals.Temp_dataList.Add(details);
            //return Temp_dataList;
        }
        //chartcode>>

        private string getParamAccount()
        {
            String lResult = "";
            List<String> listAccount = new List<String>();
            foreach (ListItem item in blAccount.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    listAccount.Add(item.Value);
                }
            }
            int i = Globals.NOAccount;

            lResult = listAccount[i];

            Globals.NOAccount += 1;

            return lResult;
        }

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            string lParamAccount = getParamAccount();
            if (lParamAccount == "")
            {
                return;
            }


            //<<01 Nanda
            /* Desription row
             * row[0] : Field ID
             * row[1] : Field Week
             * row[4] : Field Percent
            */
            var listOSAAccount = new List<Core.Model.mdlOSAData>();
            //listOSAAccount = OSAFacade.LoadOSAAccount(lParamAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
            DataRow[] result = Globals.DtAccount.Select("Account = '" + lParamAccount + "'");
            int nx = 0;
            var mdlOSA = new Core.Model.mdlOSAData();
            foreach (DataRow row in result)
            {
                for (int i = nx; i < Globals.mdlOSAWeekList.Count; i++)
                {
                    if (Globals.mdlOSAWeekList[i].Week == row[1].ToString())
                    {
                        mdlOSA = new Core.Model.mdlOSAData();
                        mdlOSA.ID = row[0].ToString();
                        mdlOSA.Week = row[1].ToString();
                        mdlOSA.Percent = row[2].ToString().Substring(0, 5) + "%";

                        if (Request.QueryString["AreaId"] != null)
                            mdlOSA.Link = "reportOSA_Account_Customer.aspx?week=" + mdlOSA.Week + "&Account=" + mdlOSA.ID + "&year=" + Globals.gTahun + "&Area=" + Globals.gAreaId + "&Role=" + Globals.gRole;
                        else
                            mdlOSA.Link = "reportOSA_Account_Customer.aspx?week=" + mdlOSA.Week + "&Account=" + mdlOSA.ID + "&year=" + Globals.gTahun + "&Role=" + Globals.gRole;
                        
                        listOSAAccount.Add(mdlOSA);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSA = new Core.Model.mdlOSAData();
                        mdlOSA.ID = row[0].ToString();
                        mdlOSA.Week = row[1].ToString();
                        mdlOSA.Percent = "";
                        listOSAAccount.Add(mdlOSA);
                    }
                }
                
            }
            
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildPercentlist");
                childRepeater.DataSource = listOSAAccount;
                childRepeater.DataBind();
            }

            GetChartData(listOSAAccount); //chartcode
        }

        private void BindDataWeeks()
        {
            Globals.mdlOSAWeekList = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
            PagedDataSource pgitemsweek = new PagedDataSource();
            pgitemsweek.DataSource = Globals.mdlOSAWeekList;
            RptWeeks.DataSource = pgitemsweek;
            RptWeeks.DataBind();
        }

        private void BindData(string lWeekFrom, string lWeekTo, string lFlag)
        {
            string role = rblRole.SelectedValue;
            List<String> listParam = new List<String>();
            var mdlOSAAreaList = new List<Core.Model.mdlOSAData>();
            var listOSAGrandTotal = new List<Core.Model.mdlOSAData>();
            if (lFlag == "0")
            {

                var lAccountlist = OSAFacade.GetAccounts(Globals.gAreaId);
                foreach (ListItem item in blAccount.Items)
                {
                    item.Selected = true;
                    var mdlOSAArea = new Core.Model.mdlOSAData();
                    mdlOSAArea.ID = item.Value;
                    listParam.Add(item.Value);
                    mdlOSAAreaList.Add(mdlOSAArea);
                }
            }
            else
            {
                foreach (ListItem item in blAccount.Items)
                {
                    if (item.Selected)
                    {
                        var mdlOSAArea = new Core.Model.mdlOSAData();
                        mdlOSAArea.ID = item.Value;
                        listParam.Add(item.Value);
                        mdlOSAAreaList.Add(mdlOSAArea);
                    }
                }
            }


            Globals.mdlOSAWeekList = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
            Globals.gUserId = Session["User"].ToString();

            if (Request.QueryString["AreaId"] != null)
            {
                //LoadDataAccount
                Globals.DtAccount = OSAFacade.LoadOSAAccountDT(listParam, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);

                //GrandTotal
                listOSAGrandTotal = OSAFacade.LoadGrandTotalAccountByWeek(listParam, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            }
            else
            {
                //Check Checkbox Branch
                if (CheckAccount() == true)
                    return;

                //LoadDataAccount
                Globals.DtAccount = OSAFacade.LoadOSAAccountDT2(listParam, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
                //GrandTotal
                listOSAGrandTotal = OSAFacade.LoadGrandTotalAccountByWeek2(listParam, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            }
            
            PagedDataSource pgitems2 = new PagedDataSource();
            pgitems2.DataSource = listOSAGrandTotal;
            RptChildGrandTotal.DataSource = pgitems2;
            RptChildGrandTotal.DataBind();

            //Account
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = mdlOSAAreaList;
            RptOSAAccountlist.DataSource = pgitems;
            RptOSAAccountlist.DataBind();

            //<<chartcode
            PagedDataSource pgitems3 = new PagedDataSource();
            pgitems3.DataSource = Globals.Temp_dataList;
            RptChartlist.DataSource = pgitems3;
            RptChartlist.DataBind();
            //chartcode>>
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            Globals.NOAccount = 0;
            Globals.gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }

                ClearContent(); 
                if (Request.QueryString["AreaId"] == null)
                {
                    //get user branchid
                    Globals.gUserId = Session["User"].ToString();
                    var vUser = UserFacade.GetUserbyID(Globals.gUserId);
                    Globals.gAreaId = vUser.BranchID;

                    //Globals.gAreaList = Globals.gAreaId.Split('|').ToList();
                    //ddlArea.DataSource = AreaList;
                    //ddlArea.DataBind();

                    dvdirect.Visible = true;

                    //Globals.gAreaId = ddlArea.SelectedValue;
                    Globals.gWeekFrom = ddlWeekFrom.SelectedValue;
                    Globals.gWeekTo = ddlWeekTo.SelectedValue;
                    Globals.gTahun = ddlTahun.SelectedValue;
                    
                }
                else
                {
                    Globals.gAreaId = Request.QueryString["AreaId"];
                    Globals.gWeekFrom = Request.QueryString["WeekFrom"];
                    Globals.gWeekTo = Request.QueryString["WeekTo"];
                    Globals.gTahun = Request.QueryString["Tahun"];
                    Globals.gRole = Request.QueryString["Role"];

                    rblRole.SelectedValue = Globals.gRole;
                    
                    dvdirect.Visible = false;

                    var lAccountlist = OSAFacade.GetAccounts(Globals.gAreaId);
                    blAccount.DataSource = lAccountlist;
                    blAccount.DataBind();

                    BindDataWeeks();
                    BindData(Globals.gWeekFrom, Globals.gWeekTo, "0");
                }
            }
        }

        protected void btnShowAccount_Click(object sender, EventArgs e)
        {
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            if (Request.QueryString["AreaId"] == null)
            {
                var lAccountlist = OSAFacade.GetAccounts();
                blAccount.DataSource = lAccountlist;
                blAccount.DataBind();
            }
            else
            {
                if (txtAccountID.Text == "")
                {
                    var lAccountlist = OSAFacade.GetAccounts(Globals.gAreaId);
                    blAccount.DataSource = lAccountlist;
                    blAccount.DataBind();
                }
                else
                {
                    var lAccountlist = OSAFacade.GetAccounts(txtAccountID.Text, Globals.gAreaId);
                    blAccount.DataSource = lAccountlist;
                    blAccount.DataBind();
                }
            }


            PnlAccount.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blAccount.Items)
            {
                item.Selected = false;
            }
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blAccount.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            getParam();
            Globals.NOAccount = 0;
            BindDataWeeks();
            BindData(Globals.gWeekFrom, Globals.gWeekTo, "1");
        }

        protected void RptOSAAccountlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ToCustomer")
            {
                Control control;
                control = e.Item.FindControl("lnkToCustomer");
                if (control != null)
                {
                    string id;
                    id = ((LinkButton)control).Text;
                    if (Request.QueryString["AreaId"] != null)
                        Response.Redirect("ReportOSA_Customer.aspx?AreaId=" + Globals.gAreaId + "&Account=" + id + "&WeekFrom=" + Globals.gWeekFrom + "&WeekTo=" + Globals.gWeekTo + "&Tahun=" + Globals.gTahun + "&Role=" + Globals.gRole);

                }
            }
        }

        //Export To Excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            //Check Checkbox Branch
            if (CheckAccount() == true)
                return;
            string role = rblRole.SelectedValue;
            getParam();
            List<String> listParam = new List<String>();
            foreach (ListItem item in blAccount.Items)
            {
                if (item.Selected)
                {// If the item is selected, add the value to the list.

                    listParam.Add(item.Value);
                }
            }

            Globals.gUserId = Session["User"].ToString();
            var mdlOSAAccountExcel = new List<Core.Model.mdlOSAExcel>();
            if (Request.QueryString["AreaId"] != null)
               mdlOSAAccountExcel = Core.Manager.OSAFacade.LoadOSAAccountExcel("0",listParam,Globals.gAreaId,Globals.gWeekFrom,Globals.gWeekTo,Globals.gTahun,role,Globals.gUserId);
            else
               mdlOSAAccountExcel = Core.Manager.OSAFacade.LoadOSAAccountExcel("1", listParam, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            
            ReportDataSource rds = new ReportDataSource("DataSet1", mdlOSAAccountExcel);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOSAAccount.rdlc");
            ReportViewer1.LocalReport.Refresh();

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOSA_Account" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
         


            //Create a dummy GridView
            //GridView GridView1 = new GridView();
            //GridView1.AllowPaging = false;
            //GridView1.DataSource = dt;
            //GridView1.DataBind();

            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition",
            // "attachment;filename=EXC_ReportOSA_Account.xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //for (int i = 0; i < GridView1.Rows.Count; i++)
            //{
            //    //Apply text style to each Row
            //    GridView1.Rows[i].Attributes.Add("class", "textmode");
            //}
            //GridView1.RenderControl(hw);

            ////style to format numbers to string
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //Response.Write(style);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();
        }



    } 
}