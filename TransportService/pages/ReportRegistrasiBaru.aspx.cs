﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using Core.Manager;
using System.IO;
using System.Net;
using System.Text;
using Core.Model;

namespace TransportService.pages
{
    public partial class ReportRegistrasiBarus : System.Web.UI.Page
    {
        public string gUserId, gBranchId;

        private bool CheckEmployee()
        {
            //validasi jika branch checkbox nya kosong
            bool lReturn = false;
            if (ddlEmployee.SelectedValue == "" && cbEmployeeAll.Checked == false)
            {
                Response.Write("<script>alert('Employee is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        }

        private bool CheckBranch()
        {
            //validasi jika branch checkbox nya kosong
            bool lReturn = false;
            if (ddlBranch.SelectedValue == "")
            {
                Response.Write("<script>alert('Branch is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        }


        public string getBranch()
        {
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        private void ClearContent()
        {

            ddlBranch.DataSource = BranchFacade.LoadBranch2(getBranch());
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "BranchID";
            ddlBranch.DataBind();


            ddlQuestionCategory.DataSource = QuestionCategoryFacade.LoadQuestionCategoryDDL(true, gUserId);
            ddlQuestionCategory.DataTextField = "QuestionCategoryText";
            ddlQuestionCategory.DataValueField = "QuestionCategoryID";
            ddlQuestionCategory.DataBind();

            //txtBranchID.Text = string.Empty;
            txtStartDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            txtEndDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
            
            PanelReportRegister.Visible = false;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PanelReportRegister.Visible = false;
                ClearContent();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            if (CheckEmployee() == true || CheckBranch() == true)
                return;

            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in ddlEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            try
            {

                var ds = ResultSurveyFacade.LoadResultSurvey(blEmployeeStrList, ddlBranch.SelectedValue, txtStartDate.Text, txtEndDate.Text,ddlQuestionCategory.SelectedValue);
                ReportDataSource rds = new ReportDataSource("ds", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }
            catch
            {
                DateTime txtdateStart = DateTime.ParseExact(txtStartDate.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                DateTime txtdateEnd = DateTime.ParseExact(txtEndDate.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                var ds = ResultSurveyFacade.LoadResultSurvey(blEmployeeStrList, ddlBranch.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlQuestionCategory.SelectedValue);
                ReportDataSource rds = new ReportDataSource("ds", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportResultSurvey.rdlc");

            ReportParameter[] parameter = new ReportParameter[3];
            parameter[0] = new ReportParameter("StartDate", txtStartDate.Text);
            parameter[1] = new ReportParameter("EndDate", txtEndDate.Text);
            parameter[2] = new ReportParameter("ReportTitle", "CHECKLIST");
            ReportViewer1.LocalReport.SetParameters(parameter);

            ReportViewer1.LocalReport.Refresh();

            //PanelReportDO.Visible = true;
            ReportViewer1.Visible = false;

            //preview pdf
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            FileStream fs = new FileStream(Server.MapPath(@"~\report.pdf"), FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            string url = "PDF_Report.aspx";

            StringBuilder sb = new StringBuilder();

            sb.Append("<script type = 'text/javascript'>");

            sb.Append("window.open('");

            sb.Append(url);

            sb.Append("');");

            sb.Append("</script>");

            ClientScript.RegisterStartupScript(this.GetType(),

                    "script", sb.ToString());
        }

        protected void btnShowExcel_Click(object sender, EventArgs e)
        {
            if (CheckEmployee() == true || CheckBranch() == true)
                return;

            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();
            
            // Loop through each item.
            foreach (ListItem item in ddlEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }


            try
            {
                var ds = ResultSurveyFacade.LoadResultSurvey(blEmployeeStrList, ddlBranch.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlQuestionCategory.SelectedValue);
                ReportDataSource rds = new ReportDataSource("ds", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }
            catch
            {
                DateTime txtdateStart = DateTime.ParseExact(txtStartDate.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                DateTime txtdateEnd = DateTime.ParseExact(txtEndDate.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                var ds = ResultSurveyFacade.LoadResultSurvey(blEmployeeStrList, ddlBranch.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlQuestionCategory.SelectedValue);
                ReportDataSource rds = new ReportDataSource("ds", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportResultSurvey.rdlc");

            ReportParameter[] parameter = new ReportParameter[3];
            parameter[0] = new ReportParameter("StartDate", txtStartDate.Text);
            parameter[1] = new ReportParameter("EndDate", txtEndDate.Text);
            parameter[2] = new ReportParameter("ReportTitle", "CHECKLIST");
            ReportViewer1.LocalReport.SetParameters(parameter);

            ReportViewer1.LocalReport.Refresh();
            
            PanelReportRegister.Visible = true;

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportRegistrasiBaru" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
        }

        protected void btnShowPhoto_Click(object sender, EventArgs e)
        {
            if (CheckEmployee() == true || CheckBranch() == true)
                return;

            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in ddlEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            try
            {

                var ds = ResultSurveyFacade.LoadResultSurveyPhoto(blEmployeeStrList, ddlBranch.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlQuestionCategory.SelectedValue);
                ReportDataSource rds = new ReportDataSource("ds", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }
            catch
            {
                DateTime txtdateStart = DateTime.ParseExact(txtStartDate.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                DateTime txtdateEnd = DateTime.ParseExact(txtEndDate.Text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                var ds = ResultSurveyFacade.LoadResultSurveyPhoto(blEmployeeStrList, ddlBranch.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlQuestionCategory.SelectedValue);
                ReportDataSource rds = new ReportDataSource("ds", ds);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);
            }

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportResultSurveyPhoto.rdlc");
            ReportViewer1.LocalReport.EnableExternalImages = true;

            ReportParameter[] parameter = new ReportParameter[3];
            parameter[0] = new ReportParameter("StartDate", txtStartDate.Text);
            parameter[1] = new ReportParameter("EndDate", txtEndDate.Text);
            parameter[2] = new ReportParameter("ReportTitle", "CHECKLIST PHOTO");
            ReportViewer1.LocalReport.SetParameters(parameter);

            ReportViewer1.LocalReport.Refresh();

            //PanelReportDO.Visible = true;
            ReportViewer1.Visible = false;

            //preview pdf
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            FileStream fs = new FileStream(Server.MapPath(@"~\report.pdf"), FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            string url = "PDF_Report.aspx";

            StringBuilder sb = new StringBuilder();

            sb.Append("<script type = 'text/javascript'>");

            sb.Append("window.open('");

            sb.Append(url);

            sb.Append("');");

            sb.Append("</script>");

            ClientScript.RegisterStartupScript(this.GetType(),

                    "script", sb.ToString());
        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlEmployee.DataSource = EmployeeFacade.LoadEmployeelistReport(ddlBranch.SelectedValue);
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataBind();
        }

    }
}