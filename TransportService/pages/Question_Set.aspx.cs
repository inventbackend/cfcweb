﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class QuestionSets : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;
        public Core.Model.User vUser;

        //public string getBranch()
        //{
        //    //get user branchid
        //    gUserId = Session["User"].ToString();
        //    vUser = UserFacade.GetUserbyID(gUserId);
        //    gBranchId = vUser.BranchID;

        //    return gBranchId;
        //}

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }


        public string lParam { get; set; }

        private void ClearContent()
        {
            BindData();

            txtQuestionSetID.Value = "";
            txtQuestionSetName.Text = "";

            PanelFormQuestionSet.Visible = false;
        }

        private void BindData()
        {
            var mdlQuestionSetList = new List<Core.Model.mdlQuestion_Set>();
            var role = getRole();
            mdlQuestionSetList = QuestionSetFacade.LoadQuestionSet(Boolean.Parse(ddlIsactive.SelectedValue),gUserId);

            foreach (var mdlQuestionSet in mdlQuestionSetList)
            {
                mdlQuestionSet.Role = role;
                btnNew.Visible = role;
            }

            RptQuestionSetlist.DataSource = mdlQuestionSetList;
            RptQuestionSetlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {
                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                ClearContent();
            }
        }



        protected void RptQuestionSetlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtQuestionSetID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionSetID")).Text);
            var dtQuestionSetText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionSetText")).Text);

            if (e.CommandName == "Question")
            {
                String Url = string.Format("Question.aspx?QuestionSetID={0}&QuestionSetText={1}", dtQuestionSetID, dtQuestionSetText);
                Response.Redirect(Url);
            }

            if (e.CommandName == "Update")
            {
                txtQuestionSetID.Value = dtQuestionSetID;
                txtQuestionSetName.Text = dtQuestionSetText;

                btnInsert.Visible = false;
                btnUpdate.Visible = true;
         

                PanelFormQuestionSet.Visible = true;
            }

            if (e.CommandName == "Delete")
            {
                QuestionSetFacade.NonActiveQuestionSet(dtQuestionSetID, gUserId);
                //QuestionSetFacade.DeleteQuestionSet(dtQuestionSetID);
                ClearContent();
                
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            mdlQuestion_Set lmdlQuestion_Set = new mdlQuestion_Set();
            lmdlQuestion_Set.QuestionSetID = txtQuestionSetID.Value;
            lmdlQuestion_Set.QuestionSetText = txtQuestionSetName.Text;
            QuestionSetFacade.UpdateQuestionSet(lmdlQuestion_Set,gUserId);
            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            mdlQuestion_Set lmdlQuestion_Set = new mdlQuestion_Set();
            lmdlQuestion_Set.QuestionSetID = txtQuestionSetID.Value;
            lmdlQuestion_Set.QuestionSetText = txtQuestionSetName.Text;
            QuestionSetFacade.InsertQuestionSet(lmdlQuestion_Set,gUserId);

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormQuestionSet.Visible = true;
            txtQuestionSetID.Value = QuestionSetFacade.GenerateQuestionSetID(gUserId);

            btnInsert.Visible = true;
            btnUpdate.Visible = false; 
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}