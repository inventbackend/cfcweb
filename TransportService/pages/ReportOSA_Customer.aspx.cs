﻿/* documentation
 * 001 nanda 06/11/2017 - Optimasi Looping Data 
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Globalization;
using System.Data;
using System.IO;
using Microsoft.Reporting.WebForms;

namespace TransportService.pages
{

    public partial class ReportOSA_Customer : System.Web.UI.Page
    {
        public static class Globals
        {
            public static int NOCustomer = 0;
            public static string gUserId = string.Empty;
            public static string gAreaId = string.Empty;
            public static string gAccount = string.Empty;
            public static string gWeekFrom = string.Empty;
            public static string gWeekTo = string.Empty;
            public static string gTahun = string.Empty;
            public static string gRole = string.Empty;

            public static List<Core.Model.mdlWeek> mdlOSAWeekList = new List<Core.Model.mdlWeek>();
            public static List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            public static DataTable DtCustomer = new DataTable();
        }

        public void getParam()
        {
            if (Request.QueryString["AreaId"] == null)
            {
                //get user branchid
                //Globals.gAreaId = ddlArea.SelectedValue;
                Globals.gWeekFrom = ddlWeekFrom.SelectedValue;
                Globals.gWeekTo = ddlWeekTo.SelectedValue;
                Globals.gTahun = ddlTahun.SelectedValue;
                Globals.gAccount = ddlAccount.SelectedValue;
                BindDataWeeks();
            }
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        private void ClearContent()
        {
            Globals.NOCustomer = 0;
            //Globals.gUserId = string.Empty;
            Globals.gAreaId = string.Empty;
            Globals.gAccount = string.Empty;
            Globals.gWeekFrom = string.Empty;
            Globals.gWeekTo = string.Empty;
            Globals.gTahun = string.Empty;
            Globals.mdlOSAWeekList = new List<Core.Model.mdlWeek>();
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            Globals.DtCustomer = new DataTable();

            //put year in dropdown
            ddlTahun.Items.Clear();

            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= 5; i++)
            {
                // Now just add an entry that's the current year minus the counter
                ddlTahun.Items.Add((currentYear - i).ToString());
            }

            //put Week in dropdown
            int lWeekNow = GetIso8601WeekOfYear(DateTime.Now);
            for (int i = lWeekNow; i >= 0; i--)
            {
                // Now just add an entry that's the current year minus the counter
                ddlWeekFrom.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 52; i >= 0; i--)
            {

                ddlWeekTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            PnlCustomer.Visible = false;
            btnSelectAll.Visible = false;
            btnUnSelectAll.Visible = false;
        }

        private void getParamGlobals()
        {
            Globals.gWeekFrom = ddlWeekFrom.SelectedValue;
            Globals.gWeekTo = ddlWeekTo.SelectedValue;
            Globals.gTahun = ddlTahun.SelectedValue;
            Globals.gAccount = ddlAccount.SelectedValue;
            Globals.mdlOSAWeekList = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
        }

        private string getParamCustomer()
        {
            String lResult = "";
            List<String> listCustomer = new List<String>();

            foreach (ListItem item in blCustomer.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    listCustomer.Add(item.Value);
                }
            }

            int i = Globals.NOCustomer;

            lResult = listCustomer[i];
            Globals.NOCustomer += 1;

            return lResult;
        }

        //<<chartcode
        private void GetChartData(List<Core.Model.mdlOSAData> mdlOSAArealist)
        {
            String lData = string.Empty;
            String lPercent = string.Empty;
            //List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();
            Core.Model.mdlGraphData details = new Core.Model.mdlGraphData();
            foreach (var lParam in mdlOSAArealist)
            {
                details = new Core.Model.mdlGraphData();
                details.element = lParam.CustomerName;

                if (lParam.Percent != "")
                {
                    lData += "{ weeks: '" + lParam.Week + "', percents: " + lParam.Percent.Replace("%", "") + "},";
                }

                details.data = lData;
                lPercent = lParam.Percent;
            }

            if (details.data != null)
                Globals.Temp_dataList.Add(details);
        }
        //chartcode>>

        private bool CheckAccount()
        {
            //validasi jika branch checkbox nya kosong
            bool lReturn = false;
            
            if (ddlAccount.SelectedValue == "")
            {
                Response.Write("<script>alert('Account is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        }

        private bool CheckCustomer()
        {
            //validasi jika branch checkbox nya kosong
            bool lReturn = false;
            int icount = 0;
            foreach (ListItem Item in blCustomer.Items)
            {
                if (Item.Selected == true)
                {
                    icount++;
                }
            }

            if (icount == 0)
            {
                Response.Write("<script>alert('Customer is Empty');</script>");
                lReturn = true;
            }

            return lReturn;
        }
        
        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            string lParamCustomer = getParamCustomer();
            if (lParamCustomer == "")
            {
                return;
            }

            //<<01 Nanda
            /* Desription row
             * row[0] : Field ID
             * row[1] : Field Customer Name
             * row[3] : Field Week
             * row[6] : Field Percent
            */
            var listOSACustomer = new List<Core.Model.mdlOSAData>();
            DataRow[] result = Globals.DtCustomer.Select("CustomerID = '" + lParamCustomer + "'");
            int nx = 0;
            var mdlOSA = new Core.Model.mdlOSAData();
            foreach (DataRow row in result)
            {
                for (int i = nx; i < Globals.mdlOSAWeekList.Count; i++)
                {
                    if (Globals.mdlOSAWeekList[i].Week == row[3].ToString())
                    {
                        mdlOSA = new Core.Model.mdlOSAData();
                        mdlOSA.ID = row[0].ToString();
                        mdlOSA.Week = Globals.mdlOSAWeekList[i].Week;
                        mdlOSA.Percent = row[4].ToString().Substring(0, 5) + "%";
                        mdlOSA.CustomerName = row[1].ToString();
                        listOSACustomer.Add(mdlOSA);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSA = new Core.Model.mdlOSAData();
                        mdlOSA.ID = row[0].ToString();
                        mdlOSA.Week = Globals.mdlOSAWeekList[i].Week;
                        mdlOSA.Percent = "";
                        mdlOSA.CustomerName = row[1].ToString();
                        listOSACustomer.Add(mdlOSA);
                    }
                }

                
            }
            //001 Nanda>>

            
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildPercentlist");
                childRepeater.DataSource = listOSACustomer;
                childRepeater.DataBind();
            }

            GetChartData(listOSACustomer); //chartcode
        }

        private void BindDataWeeks()
        {
            PagedDataSource pgitemsweek = new PagedDataSource();
            pgitemsweek.DataSource = Globals.mdlOSAWeekList;
            RptWeeks.DataSource = pgitemsweek;
            RptWeeks.DataBind();
        }

        private void BindData(string lWeekFrom, string lWeekTo, string lFlag)
        {
            string role = rblRole.SelectedValue;
            var listOSAGrandTotal = new List<Core.Model.mdlOSAData>();
            List<String> listParam = new List<String>();
            var mdlOSACustomerList = new List<Core.Model.mdlOSACustomer>();
            if (lFlag == "0")
            {
                listParam = new List<String>();
                foreach (ListItem item in blCustomer.Items)
                {
                    item.Selected = true;
                    listParam.Add(item.Value);
                };

                mdlOSACustomerList = OSAFacade.GetCustomer("",Globals.gAccount, Globals.gAreaId,Globals.gWeekFrom,Globals.gWeekTo,Globals.gTahun,rblRole.SelectedValue);
            }
            else
            {
                listParam = new List<String>();
                foreach (ListItem item in blCustomer.Items)
                {
                    if (item.Selected)
                    {
                        var mdlOSACustomer = new Core.Model.mdlOSACustomer();
                        mdlOSACustomer.CustomerID = item.Value;
                        mdlOSACustomer.CustomerName = item.Text;
                        mdlOSACustomer.Link = OSAFacade.GetAreabyCustomerID(mdlOSACustomer.CustomerID, Globals.gAccount, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,rblRole.SelectedValue);
                        mdlOSACustomerList.Add(mdlOSACustomer);
                        listParam.Add(item.Value);
                    }
                }  
            }

            Globals.gUserId = Session["User"].ToString();

            if (Request.QueryString["AreaId"] != null)
            {
                //LoadDataAccount
                Globals.DtCustomer = OSAFacade.LoadOSACustomerDT(listParam, Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);             
                //GrandTotal
                listOSAGrandTotal = OSAFacade.LoadGrandTotalCustomerByWeek(listParam, Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            }
            else
            {
                //Check Checkbox Branch
                if (CheckAccount() == true || CheckCustomer() == true)
                    return;

                //LoadDataAccount
                Globals.DtCustomer = OSAFacade.LoadOSACustomerDT2(listParam, Globals.gAccount, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
                //GrandTotal
                listOSAGrandTotal = OSAFacade.LoadGrandTotalCustomerByWeek2(listParam, Globals.gAccount, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            }

            
            PagedDataSource pgitems2 = new PagedDataSource();
            pgitems2.DataSource = listOSAGrandTotal;
            RptChildGrandTotal.DataSource = pgitems2;
            RptChildGrandTotal.DataBind();

            //Customer
            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = mdlOSACustomerList;
            RptOSACustomerlist.DataSource = pgitems;
            RptOSACustomerlist.DataBind();

            //<<chartcode
            PagedDataSource pgitems3 = new PagedDataSource();
            pgitems3.DataSource = Globals.Temp_dataList;
            RptChartlist.DataSource = pgitems3;
            RptChartlist.DataBind();
            //chartcode>>
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            Globals.NOCustomer = 0;
            
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }

                ClearContent();
                if (Request.QueryString["AreaId"] == null)
                {
                    //get user branchid
                    Globals.gUserId = Session["User"].ToString();
                    var vUser = UserFacade.GetUserbyID(Globals.gUserId);
                    Globals.gAreaId = vUser.BranchID;


                    //List<String> AreaList = Globals.gAreaId.Split('|').ToList();
                    //foreach (string item in AreaList)
                    //{
                    //    ddlArea.Items.Add(item);
                    //}
                    //dvdirect.Visible = true;

                    //Globals.gAreaId = ddlArea.SelectedValue;
                    getParamGlobals();

                    List<String> lAccountlist = OSAFacade.GetAccounts();
                    ddlAccount.DataSource = lAccountlist;
                    ddlAccount.DataBind();
                    Globals.gAccount = ddlAccount.SelectedValue;
                }
                else
                {
                    Globals.gAreaId = Request.QueryString["AreaId"];
                    Globals.gAccount = Request.QueryString["Account"];
                    Globals.gWeekFrom = Request.QueryString["WeekFrom"];
                    Globals.gWeekTo = Request.QueryString["WeekTo"];
                    Globals.gTahun = Request.QueryString["Tahun"];
                    Globals.gRole = Request.QueryString["Role"];

                    rblRole.SelectedValue = Globals.gRole;

                    Globals.mdlOSAWeekList = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);

                    blCustomer.DataSource = OSAFacade.GetCustomer("", Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,rblRole.SelectedValue);
                    blCustomer.DataTextField = "CustomerName";
                    blCustomer.DataValueField = "CustomerID";
                    blCustomer.DataBind();

                    BindDataWeeks();
                    BindData(Globals.gWeekFrom, Globals.gWeekTo, "0");

                    dvdirect.Visible = false;
                } 
            }
        }

        protected void btnShowCustomer_Click(object sender, EventArgs e)
        {
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            if (Request.QueryString["AreaId"] != null)
            {
                blCustomer.DataSource = OSAFacade.GetCustomer(txtCustomerID.Text, Globals.gAccount, Globals.gAreaId,Globals.gWeekFrom,Globals.gWeekTo,Globals.gTahun,rblRole.SelectedValue);
                blCustomer.DataTextField = "CustomerName";
                blCustomer.DataValueField = "CustomerID";
                blCustomer.DataBind();
            }
            else
            {
                //Check Checkbox Account
                if (CheckAccount() == true)
                    return;

                Globals.gAccount = ddlAccount.SelectedValue;
                blCustomer.DataSource = OSAFacade.GetCustomer2(txtCustomerID.Text, Globals.gAccount,Globals.gWeekFrom,Globals.gWeekTo,Globals.gTahun,rblRole.SelectedValue);
                blCustomer.DataTextField = "CustomerName";
                blCustomer.DataValueField = "CustomerID";
                blCustomer.DataBind(); 
            }
            
            PnlCustomer.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blCustomer.Items)
            {
                item.Selected = false;
            }
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blCustomer.Items)
            {
                item.Selected = true;
            };
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["AreaId"] == null)
            {
                getParamGlobals();
            }

            //Check Checkbox Customer
            if (CheckCustomer() == true)
                return;

            getParam();
            Globals.NOCustomer = 0;
            BindData(Globals.gWeekFrom, Globals.gWeekTo, "1");
        }

        protected void RptOSACustomerlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "ToCustomer")
            //{
            //    Control control;
            //    control = e.Item.FindControl("lnkToCustomer");
            //    if (control != null)
            //    {
            //        string id;

            //        id = ((LinkButton)control).Text;
            //        Response.Redirect("ReportOSA_Product.aspx?AreaId=" + Globals.gAreaId + "&Account=" + Globals.gAccount + "&Customer=" + id + "&WeekFrom=" + Globals.gWeekFrom + "&WeekTo=" + Globals.gWeekTo + "&Tahun=" + Globals.gTahun);
            //    }
            //}
        }

        //Export To Excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            Globals.gUserId = Session["User"].ToString();
            string role = rblRole.SelectedValue;
            Globals.mdlOSAWeekList = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);

            //Check Checkbox Customer
            if (CheckCustomer() == true)
                return;

            List<String> listParam = new List<String>();
            foreach (ListItem item in blCustomer.Items)
            {
                if (item.Selected)
                {// If the item is selected, add the value to the list.

                    listParam.Add(item.Value);
                }
            }

            var mdlOSACustomerExcel = new List<Core.Model.mdlOSAExcel>();
            if (Request.QueryString["AreaId"] != null)
                mdlOSACustomerExcel = Core.Manager.OSAFacade.LoadOSACustomerExcel("0",listParam, Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            else
                mdlOSACustomerExcel = Core.Manager.OSAFacade.LoadOSACustomerExcel("1",listParam, Globals.gAccount, Globals.gAreaId, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,role, Globals.gUserId);
            

            ReportDataSource rds = new ReportDataSource("DataSet1", mdlOSACustomerExcel);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOSACustomer.rdlc");
            ReportViewer1.LocalReport.Refresh();

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOSA_Customer" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download


            //Create a dummy GridView
            //GridView GridView1 = new GridView();
            //GridView1.AllowPaging = false;
            //GridView1.DataSource = dt;
            //GridView1.DataBind();

            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition",
            // "attachment;filename=EXC_ReportOSA_Customer.xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //for (int i = 0; i < GridView1.Rows.Count; i++)
            //{
            //    //Apply text style to each Row
            //    GridView1.Rows[i].Attributes.Add("class", "textmode");
            //}
            //GridView1.RenderControl(hw);

            ////style to format numbers to string
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //Response.Write(style);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();
        }

        //protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Globals.gAreaId = ddlArea.SelectedValue;
        //    List<String> lAccountlist = OSAFacade.GetAccounts(Globals.gAreaId);
        //    ddlAccount.DataSource = lAccountlist;
        //    ddlAccount.DataBind();
        //}

    } 
}