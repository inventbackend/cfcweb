﻿<%@ Page Title="Report Checklist" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportRegistrasiBaru.aspx.cs" Inherits="TransportService.pages.ReportRegistrasiBarus" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Report Checklist</h3>
        </div>
        <div class="title_right">
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="Label1" runat="server" Text="Question Category" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <asp:DropDownList ID="ddlQuestionCategory" runat="server" class="form-control" Style="margin-left: 0px;">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <div class="col-md-9 col-sm-9 col-xs-12">

                            <asp:ListBox ID="ddlBranch" runat="server"  data-placeholder="Branch" 
                                    CssClass="form-control select2" AutoPostBack="True" 
                                onselectedindexchanged="ddlBranch_SelectedIndexChanged"></asp:ListBox>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblEmployeeID" runat="server" Text="Employee ID"  Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                        <asp:ListBox ID="ddlEmployee" runat="server" SelectionMode="Multiple"  data-placeholder="Employee" CssClass="form-control select2"></asp:ListBox>
                         </div>
                         <div class="col-md-1 col-sm-1 col-xs-12">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:CheckBox ID="cbEmployeeAll" runat="server"  ClientIDMode="Static"
                                     />All
                                     </ContentTemplate>
                                     </asp:UpdatePanel>
                            </div>  
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblDODate" runat="server" Text="Survey Date" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"
                            ></asp:Label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <asp:TextBox ID="txtStartDate" runat="server" PlaceHolder="Start Date" class="form-control" ></asp:TextBox>
                            <script type="text/javascript">
                                var picker1 = new Pikaday(
                                                        {
                                                            field: document.getElementById('<%=txtStartDate.ClientID%>'),
                                                            firstday: 1,
                                                            minDate: new Date('2000-01-01'),
                                                            maxDate: new Date('2020-12-31'),
                                                            yearRange: [2000, 2020]
                                                            //                                    setDefaultDate: true,
                                                            //                                    defaultDate : new Date()
                                                        }
                                                        );
                            </script>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <asp:TextBox ID="txtEndDate" runat="server" PlaceHolder="End Date" class="form-control" ></asp:TextBox>
                            <script type="text/javascript">
                                var picker2 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtEndDate.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                 }
                                                                          );
                            </script>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12">
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report PDF" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                        <asp:Button ID="btnShowExcel" runat="server" Text="Show Report Excel" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShowExcel_Click" ValidationGroup="valShowReport" />
                        <asp:Button ID="btnShowPhoto" runat="server" Text="Show Report Photo PDF" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShowPhoto_Click" ValidationGroup="valShowReport" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-----------------------------------------------------------// Start Cut Form //--------------------------------------------------------------------------------------%>
    <div class="col-md-12 col-xs-12">
        <asp:Panel ID="PanelReportRegister" runat="server" Height="500px" Width="100%" CssClass="datagrid">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                WaitMessageFont-Size="14pt">
                <LocalReport ReportPath="Report\ReportResultSurvey.rdlc">
                </LocalReport>
            </rsweb:ReportViewer>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <script type="text/javascript">
    $(document).ready(function () {
        $(".select2").select2({});

        $("input[type=checkbox]").addClass("flat");
    });
    </script>
</asp:Content>
