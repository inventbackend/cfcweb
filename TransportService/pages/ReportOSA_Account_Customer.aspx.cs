﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        public static class Globals
        {
            public static int NOCustomer = 0;
            public static string prevPage = string.Empty;
            public static string gAccount = string.Empty;
            public static string gArea = string.Empty;
            public static string gWeekFrom = string.Empty;
            public static string gWeekTo = string.Empty;
            public static string gTahun = string.Empty;
            public static string gUserId = string.Empty;
            public static string gRole = string.Empty;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {
                if (Request.QueryString["AreaId"] == null)
                {
                    //get user branchid
                    Globals.gUserId = Session["User"].ToString();
                    var vUser = UserFacade.GetUserbyID(Globals.gUserId);
                    Globals.gArea = vUser.BranchID;


                    List<String> AreaList = Globals.gArea.Split('|').ToList();
                }
                else
                {
                    Globals.gArea = Request.QueryString["Area"];
                }

                Globals.NOCustomer = 0;
                Globals.gWeekFrom = Request.QueryString["week"];
                Globals.gWeekTo = Request.QueryString["week"];
                Globals.gAccount = Request.QueryString["Account"];
                Globals.gArea = Request.QueryString["Area"];
                Globals.gTahun = Request.QueryString["year"];
                Globals.gRole = Request.QueryString["Role"];
                BindDataWeeks();
                BindData(Globals.gWeekFrom, Globals.gWeekTo, "0");
            }
        }

        private void BindData(string lWeekFrom, string lWeekTo, string lFlag)
        {
            var listOSAGrandTotal = new List<Core.Model.mdlOSAData>();
            var mdlOSACustomerList = new List<Core.Model.mdlOSACustomer>();
            if (Request.QueryString["AreaId"] != null)
            {
                listOSAGrandTotal = OSAFacade.LoadGrandTotalCustomerByAccount(Globals.gAccount, Globals.gArea, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,Globals.gRole, Globals.gUserId);
                mdlOSACustomerList = OSAFacade.GetCustomerByAccount(Globals.gAccount, Globals.gArea);
            }
            else
            {
                listOSAGrandTotal = OSAFacade.LoadGrandTotalCustomerByAccount2(Globals.gAccount, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,Globals.gRole, Globals.gUserId);
                mdlOSACustomerList = OSAFacade.GetCustomerByAccount2(Globals.gAccount);
            }
            
            PagedDataSource pgitems2 = new PagedDataSource();
            pgitems2.DataSource = listOSAGrandTotal;
            RptChildGrandTotal.DataSource = pgitems2;
            RptChildGrandTotal.DataBind();

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = mdlOSACustomerList;

            RptOSACustomerlist.DataSource = pgitems;
            RptOSACustomerlist.DataBind();
        }

        private void BindDataWeeks()
        {
            var mdlWeeks = OSAFacade.GetWeeks(Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun);
            PagedDataSource pgitemsweek = new PagedDataSource();
            pgitemsweek.DataSource = mdlWeeks;
            RptWeeks.DataSource = pgitemsweek;
            RptWeeks.DataBind();
        }

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            var listOSACustomer = new List<Core.Model.mdlOSAAccountCustomer>();
            string lParamCustomer = getParamCustomer();
            if (lParamCustomer == "")
            {
                return;
            }

            if (Request.QueryString["AreaId"] != null)
            {
                listOSACustomer = OSAFacade.LoadOSACustomerByAccount(lParamCustomer, Globals.gAccount, Globals.gArea, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,Globals.gRole, Globals.gUserId);
            }
            else
            {
                listOSACustomer = OSAFacade.LoadOSACustomerByAccount2(lParamCustomer, Globals.gAccount, Globals.gWeekFrom, Globals.gWeekTo, Globals.gTahun,Globals.gRole, Globals.gUserId);
            }

           
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildPercentlist");
                childRepeater.DataSource = listOSACustomer;
                childRepeater.DataBind();
            }
        }

        private string getParamCustomer()
        {
            String lResult = "";
            var listCustomer = new List<Core.Model.mdlOSACustomer>();
            if (Request.QueryString["AreaId"] != null)
            {
                listCustomer = OSAFacade.GetCustomerByAccount(Globals.gAccount, Globals.gArea);
            }
            else
            {
                listCustomer = OSAFacade.GetCustomerByAccount2(Globals.gAccount);
            }


            int i = Globals.NOCustomer;

            lResult = listCustomer[i].CustomerID;

            Globals.NOCustomer += 1;

            return lResult;
        }

        protected void RptOSACustomerlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void back_Click(object sender, EventArgs e)
        {
            Response.Redirect(Globals.prevPage);
        }
    }
}