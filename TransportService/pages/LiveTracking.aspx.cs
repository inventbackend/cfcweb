﻿/* documentation
 * 001 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;
using System.Text;


namespace TransportService.pages
{
    public partial class LiveTracking : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId;

        public string getBranch()
        {
            //get user branchid
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gBranchId = vUser.BranchID;

            return gBranchId;
        }

        protected void timer1_tick(object sender, EventArgs e)
        {
            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            var listLiveTracking = new List<Core.Model.mdlLiveTracking>();
            listLiveTracking.AddRange(Core.Manager.TrackingFacade.GetLiveTracking(ddlSearchBranchID.SelectedValue, blEmployeeStrList));
            if (listLiveTracking.Count == 0)
            {
                Panelmaps1.Visible = false;
                Panelmaps2.Visible = true;
                lblAlert.Text = "Location unavailable";

            }
            else
            {
                rptLiveTrackingMarkers.DataSource = listLiveTracking;
                rptLiveTrackingMarkers.DataBind();
                Panelmaps1.Visible = true;
                Panelmaps2.Visible = false;
                lblAlert.Text = "";
                ParameterPanel.Visible = false;
            }
        } 

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                if (getBranch() == "")
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadBranch2(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = string.Empty;
                    //txtBranchID.ReadOnly = false;
                }
                else
                {
                    ddlSearchBranchID.DataSource = BranchFacade.LoadSomeBranch(getBranch());
                    ddlSearchBranchID.DataTextField = "BranchName";
                    ddlSearchBranchID.DataValueField = "BranchID";
                    ddlSearchBranchID.DataBind();
                    //txtBranchID.Text = getBranch();
                    //txtBranchID.ReadOnly = true;
                }
                rptBranchCoordinate.DataSource = ddlSearchBranchID.DataSource;
                rptBranchCoordinate.DataBind();

                Panel1.Visible = false;
                Panelmaps1.Visible = false;
                Panelmaps2.Visible = true;
                timer1.Enabled = false;
                //txtBranchID.Text = string.Empty;
                txtEmployee.Text = string.Empty;
                ParameterPanel.Visible = true;
                btnSelectAll.Visible = false;
                btnUnSelectAll.Visible = false;

            }

        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchBranchID(string prefixText, int count)
        {
            List<string> lBranchs = new List<string>();
            lBranchs = BranchFacade.AutoComplBranch(prefixText, count, "BranchID");
            return lBranchs;
        }

        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> SearchEmployee(string prefixText, int count)
        {
            List<string> lEmployees = new List<string>();
            lEmployees = EmployeeFacade.AutoComplEmployee(prefixText, count, "EmployeeID");
            return lEmployees;
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {

            // Create the list to store.
            List<String> blEmployeeStrList = new List<string>();

            // Loop through each item.
            foreach (ListItem item in blEmployee.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blEmployeeStrList.Add(item.Value);
                }
            }

            var listLiveTracking = new List<Core.Model.mdlLiveTracking>();
            listLiveTracking.AddRange(Core.Manager.TrackingFacade.GetLiveTracking(ddlSearchBranchID.SelectedValue, blEmployeeStrList));
            if (listLiveTracking.Count == 0)
            {
                Panelmaps1.Visible = false;
                Panelmaps2.Visible = true;
                lblAlert.Text = "Location unavailable";
                ParameterPanel.Visible = false;

            }
            else
            {
                rptLiveTrackingMarkers.DataSource = listLiveTracking;
                rptLiveTrackingMarkers.DataBind();
                Panelmaps1.Visible = true;
                Panelmaps2.Visible = false;
                ParameterPanel.Visible = false;
                lblAlert.Text = "";
            }

            //timer1.Enabled = true;

        }

        protected void btnEnd_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            ParameterPanel.Visible = true;
        }

        protected void btnShowEmployee_Click(object sender, EventArgs e)
        {
            //--002
            if (txtEmployee.Text == "")
            {
                blEmployee.DataSource = EmployeeFacade.LoadEmployeelistReport(ddlSearchBranchID.SelectedValue);
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();

            }
            else
            {
                blEmployee.DataSource = EmployeeFacade.LoadEmployeelistReport2(txtEmployee.Text,ddlSearchBranchID.SelectedValue);
                blEmployee.DataTextField = "EmployeeName";
                blEmployee.DataValueField = "EmployeeID";
                blEmployee.DataBind();
            }
            //002--

            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
            Panel1.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blEmployee.Items)
            {
                item.Selected = false;
            }
        }
    }
}