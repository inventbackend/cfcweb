﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="Status_Device.aspx.cs" Inherits="TransportService.pages.StatusDevices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
        rel="stylesheet">
    <style>
        th, td
        {
            white-space: nowrap;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="form_body" runat="server">
        <div class="page-title">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="title_left">
                    <h3>
                        Status Device</h3>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 ">
                <ol class="breadcrumb pull-right">
                    <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
                    <li class="active">Status Device</li>
                </ol>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    <div class="form-group">
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                        <div class="col-md-6 col-sm-12 col-xs-12 ">
                            <asp:Label ID="Label1" runat="server" Text="Date" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <asp:TextBox ID="txtdate" runat="server" CssClass="form-control"
                                PlaceHolder="Date">
                            </asp:TextBox>
                            <asp:CheckBox ID="cbDownload" runat="server"  ClientIDMode="Static" /> Is Download
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            <asp:Button ID="btnShow" runat="server" Text="Show" class="btn btn-primary"
                                OnClick="btnShow_Click"></asp:Button>
                                </div>
                            <script type="text/javascript">
                                var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=txtdate.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020],
                                                     format: 'MM-DD-YYYY'
                                                 }
                                                                          );
                                        </script>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <br />
                        </div>
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        
                                        <th>
                                            Device
                                        </th>
                                        <th>
                                            Employee Name
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptStatusDevicelist" runat="server" OnItemCommand="RptStatusDevicelist_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                
                                                <td>
                                                    <asp:Label ID="dtDevice" runat="server" Text='<%# Eval("Device")%>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="dtEmployeeName" runat="server" Text='<%# Eval("EmployeeName")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <!-- Datatables -->
    <script type="text/javascript" src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script type="text/javascript" src="../vendors/jszip/dist/jszip.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Datatables -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').DataTable({
            });
        });
    </script>
    <!-- /Datatables -->
</asp:Content>
