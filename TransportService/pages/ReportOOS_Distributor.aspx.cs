﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Net;
using System.Text;

namespace TransportService.pages
{
    public partial class ReportOOS_Distributor : System.Web.UI.Page
    {
        private List<Core.Model.mdlOOS> listOOS;
        private List<Core.Model.mdlOOS> listAccountOOS;
        private List<int> listWeek;
        public static string user;


        private List<Core.Model.mdlOOS> listAccountProductOOS;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = Session["User"].ToString();

            if (!IsPostBack)
            {
                DateTime dtmNow = DateTime.Now;
                List<int> dsWeek = new List<int>();
                List<int> dsWeekFrom = new List<int>();
                List<int> dsyear = new List<int>();
                int weekNow = DateFacade.GetIso8601WeekOfYear(dtmNow);
                for (int i = weekNow; i >= 1; i--)
                {
                    dsWeekFrom.Add(i);
                }
                for (int i = 54; i >= 1; i--)
                {
                    dsWeek.Add(i);
                }

                for (int i = dtmNow.Year - 3; i <= dtmNow.Year; i++)
                {
                    dsyear.Add(i);
                }


                ddlYear.DataSource = dsyear;
                ddlYear.SelectedIndex = dsyear.Count - 1;
                ddlYear.DataBind();

                ddlWeekFrom.DataSource = dsWeekFrom;
                ddlWeekFrom.DataBind();

                ddlWeekTo.DataSource = dsWeek;
                ddlWeekTo.DataBind();

                var listBranch = BranchFacade.LoadBranch();

                cblArea.DataSource = listBranch;
                cblArea.DataTextField = "BranchID";
                cblArea.DataValueField = "BranchName";
                cblArea.DataBind();

                cblArea.Visible = true;
                cblAccount.Visible = false;
                PanelReportOOS.Visible = false;
                divTable.Visible = false;
                divTableAccount.Visible = false;



                rblDistributor.DataSource = DistributorFacade.GetDistributor("");
                rblDistributor.DataTextField = "DistributorID";
                rblDistributor.DataValueField = "DistributorID";
                rblDistributor.DataBind();

                cblAccount.DataSource = AccountFacade.GetAccount(StringFacade.JoinListItemCollection(cblArea));
                cblAccount.DataTextField = "AccountID";
                cblAccount.DataValueField = "AccountID";
                cblAccount.DataBind();
            }   
        }

        protected void btnShowExcel_Click(object sender, EventArgs e)
        {



            try
            {
                if (ddlBy.SelectedItem.ToString() == "AREA")
                {

                    string area = getArea();


                    var ds = OOSFacade.GetReportOOSAccountBranchExcel(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), area, Convert.ToInt16(ddlYear.SelectedValue),rblRole.SelectedValue, user);
                    ReportDataSource rds = new ReportDataSource("dsOOS", ds);
                    reportOOS.LocalReport.DataSources.Clear();
                    reportOOS.LocalReport.DataSources.Add(rds);
                }
                else
                {

                    string account = getAccount();
                    var ds = OOSFacade.GetOOSProductPerAccount(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), account, Convert.ToInt16(ddlYear.SelectedValue),rblRole.SelectedValue, user);
                    ReportDataSource rds = new ReportDataSource("dsOOSProduct", ds);
                    reportOOS.LocalReport.DataSources.Clear();
                    reportOOS.LocalReport.DataSources.Add(rds);
                }

            }
            catch
            {
                //DateTime txtdateStart = DateTime.ParseExact(txtdate1.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //DateTime txtdateEnd = DateTime.ParseExact(txtdate2.Text, "MM-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                //var ds = OOSFacade.;
                //ReportDataSource rds = new ReportDataSource("dsVisit", ds);
                //reportOOS.LocalReport.DataSources.Clear();
                //reportOOS.LocalReport.DataSources.Add(rds);
            }

            reportOOS.ShowReportBody = true;
            if (ddlBy.SelectedItem.ToString() == "AREA")
            {
                reportOOS.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOOS.rdlc");

                ReportParameter[] parameter = new ReportParameter[1];
                parameter[0] = new ReportParameter("Type", ddlBy.SelectedItem.ToString());
                reportOOS.LocalReport.SetParameters(parameter);
            }
            else
            {
                reportOOS.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOOSProduct.rdlc");
            }



            reportOOS.LocalReport.Refresh();

            PanelReportOOS.Visible = true;

            //to directly export to pdf
            reportOOS.ProcessingMode = ProcessingMode.Local;


            //byte[] bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = reportOOS.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            // Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            if (ddlBy.SelectedItem.ToString() == "AREA")
            {
                Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOOS" + "." + extension);
            }
            else
            {
                Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOOSProduct" + "." + extension);
            }

            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {



            if (ddlBy.SelectedValue == "AREA")
            {
                divTable.Visible = true;

                string area = "";
                foreach (ListItem item in cblArea.Items)
                {
                    if (item.Selected == true)
                    {
                        if (item == cblArea.Items[cblArea.Items.Count - 1])
                        {
                            area += item.Text;
                        }
                        else
                        {
                            area += item.Text + ",";
                        }
                    }
                }

                listOOS = OOSFacade.GetReportOOSByBranchDistributor(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), area, Convert.ToInt16(ddlYear.SelectedValue),rblDistributor.SelectedValue,rblRole.SelectedValue, user);
                listWeek = listOOS.Select(fld => fld.VisitWeek).Distinct().ToList();
                var listBranch = listOOS.Select(fld => fld.BranchID).Distinct().ToList();
                var newlistBranch = new List<Core.Model.mdlOOSArea>();
                foreach (var temp in listBranch)
                {
                    var model = new Core.Model.mdlOOSArea();
                    model.Area = temp;
                    model.Link = "ReportOOS_DistributorAccountBranch.aspx?fromweek=" + ddlWeekFrom.SelectedValue + "&toweek=" + ddlWeekTo.SelectedValue + "&branchid=" + temp + "&year=" + ddlYear.SelectedValue + "&distributor=" + rblDistributor.SelectedValue + "&role=" + rblRole.SelectedValue;
                    newlistBranch.Add(model);
                }
                rptHeader.DataSource = listWeek;
                rptHeader.DataBind();
                Control HeaderTemplate = rptHeader.Controls[0].Controls[0];

                Label lblHeader = HeaderTemplate.FindControl("lblHeader") as Label;
                lblHeader.Text = "AREA";


                var listOOSGrandTotal = new List<decimal>();
                foreach (int i in listWeek)
                {
                    decimal totalOOS = listOOS.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.OutOfStock);
                    decimal totalListed = totalOOS + listOOS.Where(fld => fld.VisitWeek.Equals(i)).Sum(r => r.Listed);
                    decimal average = 0;

                    if (totalListed != 0)
                       average = (totalOOS / totalListed) * 100;
                    
                    //decimal average = listOOS.Where(fld => fld.VisitWeek.Equals(i)).Average(r => r.OOS);
                    listOOSGrandTotal.Add(decimal.Round(average, 2));
                }

                rptparent.DataSource = newlistBranch;
                rptparent.DataBind();

                var listChartBranch = new List<Core.Model.mdlOOSChart>();
                int chartID = 1;
                foreach (var branch in newlistBranch)
                {
                    var tempListOOS = listOOS.Where(fld => fld.BranchID.Equals(branch.Area));
                    //var listChartBranchData = new List<Core.Model.mdlOOSChartData>();
                    var listChartBranchData = tempListOOS.Select(fld => new { fld.VisitWeek, fld.OOS }).OrderBy(fld => fld.VisitWeek).ToList();

                    string json = Core.Services.RestPublisher.Serialize(listChartBranchData);
                    var chart = new Core.Model.mdlOOSChart();
                    chart.ID = "chart" + chartID.ToString();
                    chart.Area = branch.Area;
                    chart.json = json;
                    listChartBranch.Add(chart);

                    chartID++;

                }




                rptBranchChart.DataSource = listChartBranch;
                rptBranchChart.DataBind();

                rptGrandTotal.DataSource = listOOSGrandTotal;
                rptGrandTotal.DataBind();
            }
            else
            {
                divTableAccount.Visible = true;
                string account = "";
                var listAcc = new List<Core.Model.mdlAccount>();

                foreach (ListItem item in cblAccount.Items)
                {

                    if (item.Selected == true)
                    {
                        var mdlAcc = new Core.Model.mdlAccount();
                        mdlAcc.AccountID = item.Text;
                        mdlAcc.AccountName = item.Text;
                        mdlAcc.Description = "";
                        mdlAcc.Link = "ReportOOS_DistributorAccountBranchDetail.aspx?fromweek=" + ddlWeekFrom.SelectedValue + "&toweek=" + ddlWeekTo.SelectedValue + "&account=" + mdlAcc.AccountID + "&year=" + ddlYear.SelectedValue + "&distributor=" + rblDistributor.SelectedValue + "&role=" + rblRole.SelectedValue;
                        listAcc.Add(mdlAcc);
                        if (item == cblArea.Items[cblArea.Items.Count - 1])
                        {
                            account += item.Text;
                        }
                        else
                        {
                            account += item.Text + ",";
                        }
                    }

                }

                listAccountProductOOS = OOSFacade.GetReportOOSByAccountDistributor(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), account, Convert.ToInt16(ddlYear.SelectedValue),rblDistributor.SelectedValue,rblRole.SelectedValue, user);
                listWeek = listAccountProductOOS.Select(fld => fld.VisitWeek).Distinct().ToList();



                rptHeaderReportAccount.DataSource = listWeek;
                rptHeaderReportAccount.DataBind();
                Label lblControl = rptHeaderReportAccount.Controls[0].Controls[0].FindControl("lblHeader") as Label;
                lblControl.Text = "ACCOUNT";

                /////////////////////
                rptParentReportAccount.DataSource = listAcc;
                rptParentReportAccount.DataBind();

                var listChartAccount = new List<Core.Model.mdlOOSChart>();
                int chartID = 1;
                foreach (var acc in listAcc)
                {
                    var tempListOOS = listAccountProductOOS.Where(fld => fld.Account.Equals(acc.AccountID));
                    //var listChartBranchData = new List<Core.Model.mdlOOSChartData>();
                    var listChartBranchData = tempListOOS.Select(fld => new { fld.VisitWeek, fld.OOS }).OrderBy(fld => fld.VisitWeek).ToList();

                    string json = Core.Services.RestPublisher.Serialize(listChartBranchData);
                    var chart = new Core.Model.mdlOOSChart();
                    chart.ID = "chart" + chartID.ToString();
                    chart.Area = acc.AccountID;
                    chart.json = json;
                    listChartAccount.Add(chart);

                    chartID++;

                }

                rptBranchChart.DataSource = listChartAccount;
                rptBranchChart.DataBind();


                var listOOSGrandTotal = new List<decimal>();
                foreach (int i in listWeek)
                {
                    decimal totalOOS = 0;
                    decimal totalListed = 0;
                    decimal average = 0;
                    foreach (var acc in listAcc)
                    {
                        totalOOS = totalOOS + listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i) && fld.Account.Equals(acc.AccountID)).Sum(r => r.OutOfStock);
                        if (listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i) && fld.Account.Equals(acc.AccountID)).Sum(r => r.OutOfStock) > 0)
                            totalListed = totalListed + listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i) && fld.Account.Equals(acc.AccountID)).Sum(r => r.Listed);

                    }
                    if ((totalListed + totalOOS) != 0)
                    {
                        average = totalOOS / (totalListed + totalOOS) * 100;
                    }
                    //decimal average = listAccountProductOOS.Where(fld => fld.VisitWeek.Equals(i)).Average(r => r.OOS);
                    listOOSGrandTotal.Add(decimal.Round(average, 2));
                }

                rptAccountGrandTotal.DataSource = listOOSGrandTotal;
                rptAccountGrandTotal.DataBind();






            }



        }

        private string branchID;

        protected void AccountItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childParentAccountRepeater = (Repeater)args.Item.FindControl("rptChildAccount");
                Label lblAccount = ri.FindControl("lblDataParenAccount") as Label;



                var listAccount = listAccountOOS.Where(fld => fld.Account.Equals(lblAccount.Text) & fld.BranchID.Equals(branchID)).ToList();

                foreach (int i in listWeek)
                {
                    var temp = listAccount.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                    if (temp == null)
                    {
                        var newmodel = new Core.Model.mdlOOS();
                        newmodel.BranchID = branchID;
                        newmodel.Account = lblAccount.Text;
                        newmodel.Listed = 0;
                        newmodel.OutOfStock = 0;
                        newmodel.VisitWeek = i;
                        newmodel.OOS = 0;
                        listAccount.Add(newmodel);

                    }
                }

                listAccount = listAccount.OrderBy(fld => fld.VisitWeek).ToList();

                childParentAccountRepeater.DataSource = listAccount;
                childParentAccountRepeater.DataBind();
            }

        }

        protected void ReportAccountItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblAccount = ri.FindControl("lblAccount") as Label;
                Label lblProductID = ri.FindControl("lblProductID") as Label;
                string account = lblAccount.Text;
                Repeater childRepeater = (Repeater)args.Item.FindControl("rptChildAccountStock");



                var listChild = listAccountProductOOS.Where(fld => fld.Account.Equals(account)).ToList();
                var listChildFinal = new List<Core.Model.mdlOOS>();
                foreach (int i in listWeek)
                {
                    var tempList = listChild.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                    if (tempList != null)
                    {
                        var model = new Core.Model.mdlOOS();
                        model.Account = tempList.Account;
                        model.BranchID = tempList.BranchID;
                        model.Listed = tempList.Listed;
                        model.OutOfStock = tempList.OutOfStock;
                        model.OOS = tempList.OOS;
                        model.VisitWeek = tempList.VisitWeek;

                        listChildFinal.Add(model);

                    }
                    else
                    {
                        var model = new Core.Model.mdlOOS();

                        //model.Account = "";
                        model.VisitWeek = i;
                        //model.Link = "";
                        //model.OOSCustomer = 0;
                        //model.ProductID = "";
                        //model.ProductName = "";
                        listChildFinal.Add(model);


                    }

                }

                listChildFinal = listChildFinal.OrderBy(fld => fld.VisitWeek).ToList();


                childRepeater.DataSource = listChildFinal;
                childRepeater.DataBind();
            }
        }

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            RepeaterItem ri = args.Item;
            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (ddlBy.SelectedItem.ToString() == "AREA")
                {
                    Label lblBranch = ri.FindControl("lblArea") as Label;
                    string branch = lblBranch.Text;
                    branchID = branch;
                    Repeater childRepeater = (Repeater)args.Item.FindControl("rptChild");

                    Repeater totalRepeater = (Repeater)args.Item.FindControl("rptTotal");






                    Repeater parentAccountRepeater = (Repeater)args.Item.FindControl("rptParentAccount");
                    var listAcc = AccountFacade.GetAccount(branch);
                    var listAccFinal = new List<Core.Model.mdlAccount>();

                    string account = "";

                    foreach (var item in listAcc)
                    {

                        var mdlAcc = new Core.Model.mdlAccount();
                        mdlAcc.AccountID = item.AccountID;
                        mdlAcc.AccountName = item.AccountName;
                        mdlAcc.Description = item.Description;
                        mdlAcc.Link = "ReportOOSAccountBranchDetail.aspx?fromweek=" + ddlWeekFrom.SelectedValue + "&toweek=" + ddlWeekTo.SelectedValue + "&account=" + mdlAcc.AccountID + "&branchid=" + branch + "&year=" + ddlYear.SelectedValue + "&role=" + rblRole.SelectedValue;
                        listAccFinal.Add(mdlAcc);
                        if (item == listAcc[listAcc.Count - 1])
                        {
                            account += item.AccountID;
                        }
                        else
                        {
                            account += item.AccountID + ",";
                        }
                    }

                    listAccountOOS = OOSFacade.GetReportOOSByAccountBranch(Convert.ToInt16(ddlWeekFrom.SelectedValue), Convert.ToInt16(ddlWeekTo.SelectedValue), account, branch, Convert.ToInt16(ddlYear.SelectedValue),rblRole.SelectedValue, user);

                    //parentAccountRepeater.DataSource = listAccFinal;
                    //parentAccountRepeater.DataBind();

                    var listOOSTotal = new List<decimal>();
                    var listBranch = listOOS.Where(fld => fld.BranchID.Equals(branch)).ToList();
                    foreach (int i in listWeek)
                    {
                        //branch 
                        var tempBranch = listBranch.FirstOrDefault(fld => fld.VisitWeek.Equals(i));
                        if (tempBranch == null)
                        {
                            var model = new Core.Model.mdlOOS();

                            model.BranchID = branch;
                            model.Listed = 0;
                            model.OOS = 0;
                            model.OutOfStock = 0;
                            model.VisitWeek = i;
                            listBranch.Add(model);
                        }


                        foreach (var acc in listAcc)
                        {
                            var accountExist = listAccountOOS.FirstOrDefault(fld => fld.Account.Equals(acc.AccountID) & fld.VisitWeek.Equals(i));
                            if (accountExist == null)
                            {
                                var model = new Core.Model.mdlOOS();
                                model.Account = acc.AccountID;
                                model.BranchID = branch;
                                model.Listed = 0;
                                model.OOS = 0;
                                model.OutOfStock = 0;
                                model.VisitWeek = i;
                                listAccountOOS.Add(model);
                            }
                        }
                        decimal average = listAccountOOS.Where(fld => fld.VisitWeek.Equals(i)).Average(r => r.OOS);
                        listOOSTotal.Add(decimal.Round(average, 2));

                    }

                    listBranch = listBranch.OrderBy(fld => fld.VisitWeek).ToList();

                    childRepeater.DataSource = listBranch;
                    childRepeater.DataBind();


                    //totalRepeater.DataSource = listOOSTotal;
                    //totalRepeater.DataBind();




                }
                //else
                //{
                //    Label lblBranch = ri.FindControl("lblAccount") as Label;
                //    string branch = lblBranch.Text;
                //    Repeater childRepeater = (Repeater)args.Item.FindControl("rptChild");

                //    var listChild = listOOS.Where(fld => fld.Account.Equals(branch)).ToList();
                //    childRepeater.DataSource = listChild;
                //    childRepeater.DataBind();







                //}




            }
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            if (ddlBy.SelectedValue.ToString() == "AREA")
            {
                SetCblAreaChecked();
                SetCblAccountUnchecked();
            }
            else
            {

                SetCblAccountChecked();
                SetCblAreaUnchecked();
            }
        }



        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            SetCblAreaUnchecked();
            SetCblAccountUnchecked();
        }

        protected void ddlBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBy.SelectedValue.ToString() == "AREA")
            {
                divTable.Visible = false;
                divTableAccount.Visible = false;
                cblArea.Visible = true;
                cblAccount.Visible = false;
            }
            else
            {
                divTable.Visible = false;
                divTableAccount.Visible = false;
                cblArea.Visible = false;
                cblAccount.Visible = true;
            }

            SetCblAreaUnchecked();
            SetCblAccountUnchecked();
        }


        ///////////////////////////////////////////////////////////////////////////////////

        private string getAccount()
        {
            string account = "";
            foreach (ListItem item in cblAccount.Items)
            {
                if (item.Selected == true)
                {
                    if (item == cblArea.Items[cblArea.Items.Count - 1])
                    {
                        account += item.Text;
                    }
                    else
                    {
                        account += item.Text + ",";
                    }
                }

            }

            return account;
        }

        private string getArea()
        {
            string area = "";
            foreach (ListItem item in cblArea.Items)
            {
                if (item.Selected == true)
                {
                    if (item == cblArea.Items[cblArea.Items.Count - 1])
                    {
                        area += item.Text;
                    }
                    else
                    {
                        area += item.Text + ",";
                    }
                }

            }

            return area;
        }
        private void SetCblAreaUnchecked()
        {
            foreach (ListItem item in cblArea.Items)
            {
                item.Selected = false;
            }
        }

        private void SetCblAreaChecked()
        {
            foreach (ListItem item in cblArea.Items)
            {
                item.Selected = true;
            }
        }

        private void SetCblAccountChecked()
        {
            foreach (ListItem item in cblAccount.Items)
            {
                item.Selected = true;
            }
        }



        private void SetCblAccountUnchecked()
        {
            foreach (ListItem item in cblAccount.Items)
            {
                item.Selected = false;
            }
        }
    }
}