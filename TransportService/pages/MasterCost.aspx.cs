﻿/* Documentation
 * 001 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Collections;

namespace TransportService.pages
{
    public partial class MasterCost : System.Web.UI.Page
    {
        public string gRoleId, gUserId;

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Cost";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            txtSearch.Text = string.Empty;

            btnCancel.Visible = false;
            btnInsert.Visible = false;
            btnUpdate.Visible = false;

            PanelCost.Visible = false; 

        
        }

        private void BindData(string CostID)
        {
            var listCost = CostFacade.GetSearchCost(CostID);

            var role = getRole();
            foreach (var cost in listCost)
            {
                cost.Role = role;
                btnNew.Visible = role;
            }

            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = listCost;
            pgitems.AllowPaging = true;
            pgitems.PageSize = 10;
            pgitems.CurrentPageIndex = PageNumber;
            if (pgitems.PageCount > 1)
            {
                lnkNext.Visible = true;
                lnkPrevious.Visible = true;
                rptPaging.Visible = true;

                ArrayList pages = new ArrayList();
                for (int i = 0; i < pgitems.PageCount; i++)
                    pages.Add((i + 1).ToString());
                rptPaging.DataSource = pages;
                rptPaging.DataBind();

                Control control;
                control = rptPaging.Items[PageNumber].FindControl("btnPage");
                if (control != null)
                {
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
                    ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
                }

                if (PageNumber == 0)
                {
                    lnkPrevious.Enabled = false;
                    lnkNext.Enabled = true;
                }
                else if (PageNumber == pgitems.PageCount - 1)
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = false;
                }
                else
                {
                    lnkPrevious.Enabled = true;
                    lnkNext.Enabled = true;
                }
            }
            else
            {
                rptPaging.Visible = false;
                lnkPrevious.Visible = false;
                lnkNext.Visible = false;
            }

            RptCostlist.DataSource = pgitems;
            RptCostlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }

                BindData(txtSearch.Text); 
                ClearContent();
            }
        }

        protected void RptCostlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                Control control;
                control = e.Item.FindControl("lnkCostId");
                if (control != null)
                {
                    string lid = ((LinkButton)control).Text;
                    var lCost = CostFacade.GetCostbyID(lid);
                    if (lCost.DailyCosts.Count == 0)
                        CostFacade.DeleteCost(lid);
                    else
                    {
                        Response.Write("<script>alert('Delete Cost Failed. Cost In already used in Transaction');</script>");
                        return;
                    }
                    BindData(txtSearch.Text); 

                }
            }
            if (e.CommandName == "Link")
            {
                Control control;
                control = e.Item.FindControl("lnkCostId");
                if (control != null)
                {

                    txtCostID.Text = ((LinkButton)control).Text;
                    var lCost = CostFacade.GetCostbyID(txtCostID.Text);
                    txtCostName.Text = lCost.CostName;
                    txtCostID.ReadOnly = true;
                    ddlCostType.SelectedValue = lCost.Type;

                    //btnViewDetail.Visible.Visible = true;
                    btnInsert.Visible = false;
                    btnCancel.Visible = true;
                    btnUpdate.Visible = true;
                    PanelCost.Visible = true;
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            CostFacade.UpdateCost(txtCostID.Text, txtCostName.Text, ddlCostType.SelectedValue);
            Response.Redirect("MasterCost.aspx");
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            var lCost = CostFacade.GetCostbyID(txtCostID.Text);
            if (lCost != null)
            {
                Response.Write("<script>alert('ID already Exist');</script>");
              
                return;
            }
            CostFacade.InsertCost(txtCostID.Text, txtCostName.Text, ddlCostType.SelectedValue);
            Response.Redirect("MasterCost.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PanelCost.Visible = false;
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            txtCostID.Text = string.Empty;
            txtCostName.Text = string.Empty;
            btnInsert.Visible = true;
            btnUpdate.Visible = false;
            btnCancel.Visible = true;
            PanelCost.Visible = true;
        }

        protected void rptPaging_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            PageNumber = Convert.ToInt32(e.CommandArgument) - 1;

            BindData(txtSearch.Text); 
        }

        protected void lnkPrevious_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber - 1;

            BindData(txtSearch.Text); 
        }

        protected void lnkNext_Click(object sender, EventArgs e)
        {
            PageNumber = PageNumber + 1;

            BindData(txtSearch.Text); 
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            PageNumber = 0;
            BindData(txtSearch.Text); 
        }
    }
}