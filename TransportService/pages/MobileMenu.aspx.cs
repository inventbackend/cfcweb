﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Linq;

namespace TransportService.pages
{
    public partial class MobileMenu : System.Web.UI.Page
    {
        private void LoadList()
        {
            var listMenu2 = UserFacade.GetAccessMobileMenu(ddlRoleID.SelectedValue, "true");
            foreach (var Menu2 in listMenu2)
            {
                LbMenu2.Items.Add(Menu2.MenuName);

                //var listsubMenu2 = UserFacade.GetAccessSubMenu(ddlRoleID.SelectedValue, Menu2.MenuID);
                //foreach (var subMenu2 in listsubMenu2)
                //{
                //    LbMenu2.Items.Add(subMenu2.name);
                //}
            }
            //LbMenu2.DataTextField = "MenuName";
            //LbMenu2.DataValueField = "MenuID";
            //LbMenu2.DataSource = listMenu2;
            //LbMenu2.DataBind();

            var listMenu1 = UserFacade.GetMobileMenu();
            foreach (var Menu1 in listMenu1)
            {
                LbMenu1.Items.Add(Menu1.MenuName);

                //var listsubMenu1 = UserFacade.GetSubMenu(Menu1.MenuID);
                //foreach (var subMenu1 in listsubMenu1)
                //{
                //    LbMenu1.Items.Add(subMenu1.name);
                //}
            }
            //LbMenu1.DataTextField = "name";
            //LbMenu1.DataValueField = "menu";
            //LbMenu1.DataSource = subMenu1;
            //LbMenu1.DataBind();

            //foreach (ListItem item in LbMenu2.Items)
            //{
            //    LbMenu1.Items.Remove(item);
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == "move")
            {
                ListItem item = LbMenu1.SelectedItem;
                if (item != null)
                {
                    var getMenu = new Core.Model.mdlMenu2();

                    getMenu = MenuFacade.LoadMenuId(item.ToString().Replace(" - ", ""));

                    var getSubMenu = new List<Core.Model.mdlSubMenu>();

                    getSubMenu = MenuFacade.LoadSubMenu(getMenu.menuID, getMenu.type);

                    foreach (var submenu in getSubMenu)
                    {
                        if (!LbMenu2.Items.Contains(LbMenu2.Items.FindByText(submenu.name)))
                        {
                            LbMenu2.SelectedIndex = -1;
                            LbMenu2.Items.Add(submenu.name);
                        }
                        else
                        {
                        }
                        //LbMenu1.Items.Remove(submenu.name);
                    }

                    //LbMenu2.SelectedIndex = -1;
                    //LbMenu2.Items.Add(item);
                    //LbMenu1.Items.Remove(item);
                }
            }
            LbMenu1.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(LbMenu1, "move"));

            if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] == "move2")
            {
                ListItem item = LbMenu2.SelectedItem;
                if (item != null)
                {
                    //var getMenu = new Core.Model.mdlMenu2();

                    //getMenu = MenuFacade.LoadMenuId(item.ToString().Replace(" - ", ""));

                    //if (getMenu.menuID.StartsWith("M"))
                    //{
                    //    var getSubMenu = new List<Core.Model.mdlSubMenu>();

                    //    getSubMenu = MenuFacade.LoadSubMenu(getMenu.menuID, getMenu.type);

                    //    foreach (var submenu in getSubMenu)
                    //    {
                    //        LbMenu1.SelectedIndex = -1;
                    //        //LbMenu1.Items.Add(submenu.name);
                    //        LbMenu2.Items.Remove(submenu.name);
                    //    }
                    //}
                    //else
                    //{
                        LbMenu1.SelectedIndex = -1;
                        LbMenu2.Items.Remove(item);
                    //}
                    //LbMenu1.SelectedIndex = -1;
                    //LbMenu1.Items.Add(item);
                    //LbMenu2.Items.Remove(item);
                }
            }
            LbMenu2.Attributes.Add("ondblclick", ClientScript.GetPostBackEventReference(LbMenu2, "move2"));


            if (!IsPostBack)
            {
                var listRole = UserFacade.GetddlRole();
                ddlRoleID.DataTextField = "RoleName";
                ddlRoleID.DataValueField = "RoleID";
                ddlRoleID.DataSource = listRole;
                ddlRoleID.DataBind();

                LoadList();

                //btnShowModifyRole_Click(sender, e);
                //PanelModifyRole.Visible = false;
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ListItem item = LbMenu1.SelectedItem;
            if (item != null)
            {
                //var getMenu = new Core.Model.mdlMenu2();

                //getMenu = MenuFacade.LoadMenuId(item.ToString().Replace(" - ", ""));

                //var getSubMenu = new List<Core.Model.mdlSubMenu>();

                //getSubMenu = MenuFacade.LoadSubMenu(getMenu.menuID, getMenu.type);

                //foreach (var submenu in getSubMenu)
                //{
                //    if (!LbMenu2.Items.Contains(LbMenu2.Items.FindByText(submenu.name)))
                //    {
                //        LbMenu2.SelectedIndex = -1;
                //        LbMenu2.Items.Add(submenu.name);
                //    }
                //    else
                //    {
                //    }
                //    //LbMenu1.Items.Remove(submenu.name);
                //}

                LbMenu2.SelectedIndex = -1;
                LbMenu2.Items.Add(item);
                LbMenu1.Items.Remove(item);
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            PanelAddUserRole.Visible = true;
        }

        protected void btnSaveUR_Click(object sender, EventArgs e)
        {
            UserAccessFacade.InsertUserRole(txtRoleID.Text, txtRoleNm.Text);
            Response.Redirect("UserRole.aspx");
        }

        protected void btnCancelUR_Click(object sender, EventArgs e)
        {
            txtRoleID.Text = string.Empty;
            txtRoleNm.Text = string.Empty;
            PanelAddUserRole.Visible = false;
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            int lCount = LbMenu1.Items.Count;
            for (int i = 0; i < lCount; i++)
            {
                ListItem item = LbMenu1.Items[i];
                LbMenu2.SelectedIndex = -1;
                LbMenu2.Items.Add(item);
                //LbMenu1.Items.Remove(item);
            }

            //LbMenu1.Items.Clear();

            //foreach (ListItem item in LbMenu1.Items)
            //{
            //    LbMenu2.SelectedIndex = -1;
            //    LbMenu2.Items.Add(item);
            //    LbMenu1.Items.Remove(item); 
            //}
        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            ListItem item = LbMenu2.SelectedItem;
            if (item != null)
            {
                //var getMenu = new Core.Model.mdlMenu2();

                //getMenu = MenuFacade.LoadMenuId(item.ToString().Replace(" - ", ""));

                //if (getMenu.menuID.StartsWith("M"))
                //{
                //    var getSubMenu = new List<Core.Model.mdlSubMenu>();

                //    getSubMenu = MenuFacade.LoadSubMenu(getMenu.menuID, getMenu.type);

                //    foreach (var submenu in getSubMenu)
                //    {
                //        LbMenu1.SelectedIndex = -1;
                //        //LbMenu1.Items.Add(submenu.name);
                //        LbMenu2.Items.Remove(submenu.name);
                //    }
                //}
                //else
                //{
                    LbMenu1.SelectedIndex = -1;
                    LbMenu2.Items.Remove(item);
                //}
                //LbMenu1.SelectedIndex = -1;
                //LbMenu1.Items.Add(item);
                //LbMenu2.Items.Remove(item);
            }
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            LbMenu2.Items.Clear();

            //var listMenu1 = UserFacade.GetMenu();
            //LbMenu1.DataTextField = "MenuName";
            //LbMenu1.DataValueField = "MenuID";
            //LbMenu1.DataSource = listMenu1;
            //LbMenu1.DataBind();
        }

        private ListItemCollection GetComparison(ListItemCollection lMenu1,ListItemCollection lMenu2)
        {
            ListItemCollection except = new ListItemCollection();

            for (int i = 0; i < lMenu1.Count; i++)
            {
                if (lMenu2.Contains(lMenu1[i]))
                {
                    
                }
                else
                {
                    except.Add(lMenu1[i]);
                }
            }


            return except;

            


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            //cek Access RoleId Sudah Dibuat atu belum

            var differenceList = GetComparison(LbMenu1.Items, LbMenu2.Items);
 

            
            int lCount2 = LbMenu1.Items.Count;
            for (int i = 0; i < lCount2; i++)
            {
                ListItem item = LbMenu1.Items[i];
                if (item != null)
                {

                    if (differenceList.Contains(item))
                    {



                        //var getMenu = new Core.Model.mdlMenu2();

                        //getMenu = MenuFacade.LoadMenuId(item.ToString().Replace(" - ", ""));

                        bool bAccessRole = UserFacade.CheckAccessMobileUserbyID(ddlRoleID.SelectedValue, LbMenu1.Items[i].ToString());
                        if (bAccessRole == false)
                        {
                            UserFacade.InsertAccessMobileUser(ddlRoleID.SelectedValue, LbMenu1.Items[i].ToString(), "false");
                        }
                        else
                        {
                            
                            UserFacade.UpdateAccessMobileUser(ddlRoleID.SelectedValue, LbMenu1.Items[i].ToString(), "false");
                        }
                    }
                   
                }
            }

            int lCount = LbMenu2.Items.Count;
            for (int i = 0; i < lCount; i++)
            {
                ListItem item = LbMenu2.Items[i];
                if (item != null)
                {
                    //var getMenu = new Core.Model.mdlMenu2();

                    //getMenu = MenuFacade.LoadMenuId(item.ToString().Replace(" - ", ""));

                    Boolean bAccessRole = UserFacade.CheckAccessMobileUserbyID(ddlRoleID.SelectedValue, item.ToString());
                    if (bAccessRole == false)
                    {
                        UserFacade.InsertAccessMobileUser(ddlRoleID.SelectedValue, LbMenu2.Items[i].ToString(), "true");
                    }
                    else
                    {
                        UserFacade.UpdateAccessMobileUser(ddlRoleID.SelectedValue, LbMenu2.Items[i].ToString(), "true");
                    }
                }
            }

            // Create the list to store.
            List<String> blMenuLevelStrList = new List<string>();

            // Loop through each item.
            //foreach (ListItem li in blModifyRole.Items)
            //{
            //    if (li.Selected)
            //    {
            //        // If the item is selected, add the value to the list.
            //        blMenuLevelStrList.Add(li.Value);
            //    }
            //}
            //UserFacade.UpdateAccessUser2(ddlRoleID.SelectedValue, blMenuLevelStrList);

            //PanelModifyRole.Visible = false;
            Response.Write("<script>alert('Success Update Access');</script>");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserRole.aspx");
        }

        protected void ddlRoleID_SelectedIndexChanged(object sender, EventArgs e)
        {
            LbMenu1.Items.Clear();
            LbMenu2.Items.Clear();
            //btnShowModifyRole_Click(sender, e);
            //PanelModifyRole.Visible = false;

            LoadList();
        }

        //protected void btnShowModifyRole_Click(object sender, EventArgs e)
        //{
        //    var SelectedMenu = UserFacade.GetEditableMenu(ddlRoleID.SelectedValue, "true");
        //    foreach (ListItem li in blModifyRole.Items)
        //    {
        //        if (SelectedMenu.Select(fld => fld.menu).Contains(li.Value))
        //            li.Selected = true;
        //        else
        //            li.Selected = false;
        //    }

        //    PanelModifyRole.Visible = true;
        //}
    }
}