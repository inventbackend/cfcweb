﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class QuestionCategorys : System.Web.UI.Page
    {
        //fernandes
        public string gBranchId, gUserId, gRoleId;
        public Core.Model.User vUser;

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }


        public string lParam { get; set; }

        private void ClearContent()
        {
            BindData();

            txtQuestionCategoryID.Value = "";
            txtQuestionCategoryText.Text = "";

            PanelFormQuestionCategory.Visible = false;
        }

        private void BindData()
        {
            var mdlQuestionCategoryList = new List<Core.Model.mdlQuestion_Category>();
            var role = getRole();
            mdlQuestionCategoryList = QuestionCategoryFacade.LoadQuestionCategory(Boolean.Parse(ddlIsactive.SelectedValue), gUserId);

            foreach (var mdlQuestionCategory in mdlQuestionCategoryList)
            {
                mdlQuestionCategory.Role = role;
                btnNew.Visible = role;
            }

            RptQuestionCategorylist.DataSource = mdlQuestionCategoryList;
            RptQuestionCategorylist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gUserId = Session["User"].ToString();
            if (!IsPostBack)
            {
                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                ClearContent();
            }
        }



        protected void RptQuestionCategorylist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtQuestionCategoryID = StringFacade.NulltoStringEmpty(((LinkButton)e.Item.FindControl("lnkQuestionCategoryID")).Text);
            var dtQuestionCategoryText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionCategoryText")).Text);

            if (e.CommandName == "Link")
            {
                String Url = string.Format("Question_CategoryDetail.aspx?QuestionCategoryID={0}&QuestionCategoryText={1}", dtQuestionCategoryID,dtQuestionCategoryText);
                Response.Redirect(Url);
            }

            if (e.CommandName == "Update")
            {
                txtQuestionCategoryID.Value = dtQuestionCategoryID;
                txtQuestionCategoryText.Text = dtQuestionCategoryText;

                btnInsert.Visible = false;
                btnUpdate.Visible = true;


                PanelFormQuestionCategory.Visible = true;
            }

            if (e.CommandName == "Delete")
            {
                QuestionCategoryFacade.NonActiveQuestioncategory(dtQuestionCategoryID,gUserId);
                ClearContent();
                
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var lmdlQuestion_Category = new mdlQuestion_Category();
            lmdlQuestion_Category.QuestionCategoryID = txtQuestionCategoryID.Value;
            lmdlQuestion_Category.QuestionCategoryText = txtQuestionCategoryText.Text;
            QuestionCategoryFacade.UpdateQuestionCategory(lmdlQuestion_Category,gUserId);
            ClearContent();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {

            var lmdlQuestion_Category = new mdlQuestion_Category();
            lmdlQuestion_Category.QuestionCategoryID = txtQuestionCategoryID.Value;
            lmdlQuestion_Category.QuestionCategoryText = txtQuestionCategoryText.Text;
            QuestionCategoryFacade.InsertQuestionCategory(lmdlQuestion_Category,gUserId);

            ClearContent();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormQuestionCategory.Visible = true;
            txtQuestionCategoryID.Value = QuestionCategoryFacade.GenerateQuestionCategoryID(gUserId);

            btnInsert.Visible = true;
            btnUpdate.Visible = false; 
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}