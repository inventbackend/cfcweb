﻿/* documentation
 *001 nanda - 13 Okt 2016 
 *002 nanda - 21 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using Core.Model;
using System.Collections;

namespace TransportService.pages
{
    public partial class Questions : System.Web.UI.Page
    {
        public string gBranchId, gUserId, gRoleId, gQuestionSetId, gQuestionSetText;
        public Core.Model.User vUser;
        public string lParam { get; set; }

        public bool getRole()
        {
            //get user accesssrole
            gUserId = Session["User"].ToString();
            var vUser = UserFacade.GetUserbyID(gUserId);
            gRoleId = vUser.RoleID;
            string menuName = "Question";
            var vRole = UserFacade.CheckUserLeverage(gRoleId, menuName);

            return vRole;
        }

        private void ClearContent()
        {
            BindData();

            txtNo.Text = "";
            txtQuestionID.Value = "";
            txtQuestionText.Text = "";
            cbMandatory.Checked = false;
            ddlRole.SelectedValue = null;
 
            PanelFormQuestion.Visible = false;
        }

        private void BindData()
        {
            var mdlQuestionList = QuestionFacade.LoadQuestion(gQuestionSetId, false, Boolean.Parse(ddlIsactive.SelectedValue), gUserId);

            RptQuestionlist.DataSource = mdlQuestionList;
            RptQuestionlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            gUserId = Session["User"].ToString();
            gQuestionSetId = Request.QueryString["QuestionSetID"];

            if (!IsPostBack)
            {
                var listRole = UserFacade.GetddlRole();
                ddlRole.DataSource = listRole;
                ddlRole.DataTextField = "RoleName";
                ddlRole.DataValueField = "RoleID";
                ddlRole.DataSource = listRole;
                ddlRole.DataBind();

                var dsAnswerType = AnswerTypeFacade.LoadAnswerTypeDDL(gUserId);
                ddlAnswerType.DataSource = dsAnswerType;
                ddlAnswerType.DataTextField = "AnswerTypeText";
                ddlAnswerType.DataValueField = "AnswerTypeID";
                ddlAnswerType.DataBind();

                ddlQuestionCategory.DataSource = QuestionCategoryFacade.LoadQuestionCategoryDDL(true, gUserId);
                ddlQuestionCategory.DataTextField = "QuestionCategoryText";
                ddlQuestionCategory.DataValueField = "QuestionCategoryID";
                ddlQuestionCategory.DataBind();

                string[] controlArray = { "True", "False" };
                ddlIsactive.DataSource = controlArray;
                ddlIsactive.DataBind();

                ClearContent();
                var qsQuestionSetID = Request.QueryString["QuestionSetID"];
                var qsQuestionSetText = Request.QueryString["QuestionSetText"];

                if (qsQuestionSetID != null)
                {
                    gQuestionSetId = qsQuestionSetID;
                }

                if (qsQuestionSetText != null)
                {
                    gQuestionSetText = qsQuestionSetText;
                    
                }

                lblQuestionSet.Text = gQuestionSetText;
                BindData();
                
            }
        }

        protected void RptQuestionlist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtSequence = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtSequence")).Text);
            var dtNo = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtNo")).Text);
            var dtQuestionID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionID")).Text);
            var dtQuestionText = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionText")).Text);
            var dtQuestionCategoryID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtQuestionCategoryID")).Text);
            var dtAnswerTypeID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtAnswerTypeID")).Text);
            var dtMandatory = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtMandatory")).Text);
            //var dtIsActive = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtIsActive")).Text);
            var dtIsMultiple = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtIsMultiple")).Text);

            if (e.CommandName == "Answer")
            {
                String Url = string.Format("Answer.aspx?QuestionID={0}&QuestionText={1}&IsMultiple={2}&QuestionSetID={3}", dtQuestionID, dtQuestionText, dtIsMultiple, gQuestionSetId);
                Response.Redirect(Url);
            }

            if (e.CommandName == "Update")
            {
                ClearContent();

                txtNo.Text = dtNo;
                txtQuestionID.Value = dtQuestionID;
                txtQuestionText.Text = dtQuestionText;
                ddlQuestionCategory.SelectedValue = dtQuestionCategoryID;
                ddlAnswerType.SelectedValue = dtAnswerTypeID;
                cbMandatory.Checked = Boolean.Parse(dtMandatory);
                txtSeq.Text  = dtSequence;
                List<string> selectedRole = QuestionFacade.GetQuestionRole(txtQuestionID.Value, gUserId);
                foreach (string row in selectedRole)
                {
                    ddlRole.Items.FindByValue(row).Selected = true;
                }


                btnInsert.Visible = false;
                btnUpdate.Visible = true;

                PanelFormQuestion.Visible = true;

            }

            if (e.CommandName == "Delete")
            {
                QuestionFacade.NonActiveQuestion(dtQuestionID, gUserId);
                ClearContent();

                BindData();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var lmdlQuestion = new mdlQuestion();
            lmdlQuestion.AnswerID = "";
            lmdlQuestion.AnswerTypeID = ddlAnswerType.SelectedValue;
            lmdlQuestion.IsSubQuestion = false;
            lmdlQuestion.Mandatory = cbMandatory.Checked;
            lmdlQuestion.No = txtNo.Text;
            lmdlQuestion.QuestionCategoryID = ddlQuestionCategory.SelectedValue;
            lmdlQuestion.QuestionID = txtQuestionID.Value;
            lmdlQuestion.QuestionText = txtQuestionText.Text;
            lmdlQuestion.QuestionSetID = gQuestionSetId;
            lmdlQuestion.Sequence = Int32.Parse(txtSeq.Text);
            lmdlQuestion.IsActive = true;
            // Create the role list to store.
            List<string> blRoleStrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in ddlRole.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blRoleStrList.Add(item.Value.Trim());
                }
            }
            lmdlQuestion.Role = blRoleStrList;

            String lResult = QuestionFacade.UpdateQuestion(lmdlQuestion, gUserId);
            ClearContent();

            BindData();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            String lResult = "";
            var lmdlQuestion = new mdlQuestion();
            lmdlQuestion.AnswerID = "";
            lmdlQuestion.AnswerTypeID = ddlAnswerType.SelectedValue;
            lmdlQuestion.IsSubQuestion = false;
            lmdlQuestion.Mandatory = cbMandatory.Checked;
            lmdlQuestion.No = txtNo.Text;
            lmdlQuestion.QuestionCategoryID = ddlQuestionCategory.SelectedValue;
            lmdlQuestion.QuestionID = txtQuestionID.Value;
            lmdlQuestion.QuestionText = txtQuestionText.Text;
            lmdlQuestion.QuestionSetID = gQuestionSetId;
            lmdlQuestion.Sequence = Int32.Parse(txtSeq.Text);
            lmdlQuestion.IsActive = true;
            // Create the role list to store.
            List<string> blRoleStrList = new List<string>();
            // Loop through each item.
            foreach (ListItem item in ddlRole.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    blRoleStrList.Add(item.Value.Trim());
                }
            }
            lmdlQuestion.Role = blRoleStrList;

            var lCheck = AnswerTypeFacade.CheckAnswer(ddlAnswerType.SelectedValue, gUserId);
            if (lCheck == true)
            {
                lResult = QuestionFacade.InsertQuestion(lmdlQuestion, gUserId);
            }
            else
            {
                var lmdlAnswer = new mdlAnswer();
                lmdlAnswer.AnswerID = ""; //generate by system
                lmdlAnswer.AnswerText = ddlAnswerType.SelectedItem.Text.Trim().Split('-')[2];
                lmdlAnswer.IsActive = true;
                lmdlAnswer.IsSubQuestion = false;
                lmdlAnswer.No = "1";
                lmdlAnswer.QuestionID = txtQuestionID.Value;
                lmdlAnswer.Sequence = Int32.Parse(txtSeq.Text);
                lmdlAnswer.SubQuestion = false;

                lResult = QuestionFacade.InsertQuestionNotMultiple(lmdlQuestion,lmdlAnswer,false ,"", gUserId);
            }
                    
            ClearContent();

            BindData();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ClearContent();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearContent();
            PanelFormQuestion.Visible = true;
            txtQuestionID.Value = QuestionFacade.GenerateQuestionID(false, gUserId);

            btnInsert.Visible = true;
            btnUpdate.Visible = false;
        }

        protected void ddlIsactive_TextChanged(object sender, EventArgs e)
        {
            BindData();
        }
    }
}