﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;

namespace TransportService.pages
{
    public partial class MasterCustomer : System.Web.UI.Page
    {
      
        protected void btnSubmit_Click(Object sender,EventArgs e)
        {

            string res = CustomerFacade.AddNewCustomer(txtCustomerName.Text, txtAddress.Text, ddlCustomerType.SelectedValue, ddlCity.SelectedValue, ddlCountryRegionCode.SelectedValue, ddlAccount.SelectedValue, ddlDistributor.SelectedValue, ddlEmployee.SelectedValue);
            if (res == "0")
            {
                string script = "alert('Insert Failed');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
            }
        }
        protected void btnUpdate_Click(Object sender, EventArgs e)
        {
            string customerID = hidCustomerID.Value;


            string res = CustomerFacade.UpdateCustomer(txtCustomerName.Text, txtAddress.Text, ddlCustomerType.SelectedValue, ddlCity.SelectedValue, ddlCountryRegionCode.SelectedValue, ddlAccount.SelectedValue, ddlDistributor.SelectedValue, ddlEmployee.SelectedValue, customerID);
            if (res == "0")
            {
                string script = "alert('Insert Failed');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "Alert", script, true);
            }


            //var listCustomer = CustomerFacade.LoadCustomersForAddnew(1, pagesize);
            //RptCustomerlist.DataSource = listCustomer;
            //RptCustomerlist.DataBind();
            

        }

        private void Bind_Customer()
        {
            int total = 0;
            var listCustomer = CustomerFacade.LoadCustomersForAddnew();
            if (listCustomer.Count == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "<script>alert('Customer Kosong');</script>", false);
            }
            else
            {
                total = Convert.ToInt32(listCustomer.First().Total);
            }


            RptCustomerlist.DataSource = listCustomer;
            RptCustomerlist.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                var listCustomerType = CustomerFacade.LoadCustomerType();
                var listBranch = BranchFacade.LoadBranch();
                var listAccount = CustomerFacade.LoadAccount();
                var listDistributor = CustomerFacade.LoadDistributor();
                
                var listEmployee = EmployeeFacade.LoadEmployee();

                Bind_Customer();

                ddlEmployee.DataSource = listEmployee.EmployeeList;
                ddlEmployee.DataTextField = "EmployeeName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataBind();

                PanelFormCustomer.Visible = false;


                ddlCustomerType.DataSource = listCustomerType;
                ddlCustomerType.DataTextField = "CustomerTypeName";
                ddlCustomerType.DataValueField = "CustomerTypeID";
                ddlCustomerType.DataBind();

                ddlCountryRegionCode.DataSource = listBranch;
                ddlCountryRegionCode.DataTextField = "BranchDescription";
                ddlCountryRegionCode.DataValueField = "BranchID";
                ddlCountryRegionCode.DataBind();

                ddlAccount.DataSource = listAccount;
                ddlAccount.DataTextField = "AccountName";
                ddlAccount.DataValueField = "AccountID";
                ddlAccount.DataBind();

                ddlDistributor.DataSource = listDistributor;
                ddlDistributor.DataTextField = "DistributorName";
                ddlDistributor.DataValueField = "DistributorID";
                ddlDistributor.DataBind();
            }
        }

        protected void rptCustomer_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtCustomerID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtCustomerID")).Text);
            var dtCustomerName = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtCustomerName")).Text);
            var dtCustomerAddress = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtCustomerAddress")).Text);
            var dtCustomerTypeID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtCustomerTypeID")).Text);
            var dtCity = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtCity")).Text);
            var dtCountryRegionCode = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtCountryRegionCode")).Text);
            var dtEmployeeID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtEmployeeID")).Text);

            if (e.CommandName == "Update")
            {
                txtCustomerName.Text = dtCustomerName;
                txtAddress.Text = dtCustomerAddress;

                if (dtCustomerTypeID != "")
                    ddlCustomerType.SelectedValue = dtCustomerTypeID;

                if (dtCity != "")
                    ddlCity.SelectedValue = dtCity;

                if (dtCountryRegionCode != "")
                    ddlCountryRegionCode.SelectedValue = dtCountryRegionCode;

                if (dtEmployeeID != "")
                    ddlEmployee.SelectedValue = dtEmployeeID;


                btnSubmit.Visible = false;
                btnUpdate.Visible = true;
                PanelFormCustomer.Visible = true;
            }
        }

        protected void rptResultSurvey_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var dtSurveyID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtSurveyID")).Text);
            var dtEmployeeID = StringFacade.NulltoStringEmpty(((Label)e.Item.FindControl("dtEmployeeID")).Text);

            if (e.CommandName == "Show")
            {
                lblSurveyID.Text = dtSurveyID;
                var mdlCustomer = ResultSurveyFacade.MappingSurveyToCustomer(dtSurveyID);
                lblCustomerID.Text = mdlCustomer.CustomerID;
                lblCustomerName.Text = mdlCustomer.CustomerName;
                lblAddress.Text = mdlCustomer.CustomerAddress;
                lblArea.Text = mdlCustomer.BranchID;

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "<script>$('#ModalImportCustomer').modal('show');</script>", false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "<script>$('#ModalInfoReg').modal('show');</script>", false);
            }
        }

        protected void Page_Changed(object sender, EventArgs e)
        {
            //int pageIndex = int.Parse((sender as LinkButton).CommandArgument);
            //currentIndex = pageIndex;

            var listCustomer = CustomerFacade.LoadCustomersForAddnew();

            RptCustomerlist.DataSource = listCustomer;
            RptCustomerlist.DataBind();

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            btnSubmit.Visible = true;
            btnUpdate.Visible = false;
            PanelFormCustomer.Visible = true;
        }

        protected void btnMigrasiCustomer_Click(object sender, EventArgs e)
        {
            var listSurveyCustomer = ResultSurveyFacade.LoadSurveyCustomer(DateTime.Parse("2017-12-01"), DateTime.Now);
            rptResultSurvey.DataSource = listSurveyCustomer;
            rptResultSurvey.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "<script>$('#ModalImportCustomer').modal('show');</script>", false);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string lresult_customer = CustomerFacade.InsertCustomerbySurvey(lblCustomerID.Text,lblCustomerName.Text,lblAddress.Text,lblArea.Text,lblEmployeeID.Text);

            if (lresult_customer == "1")
            {
                ResultSurveyFacade.UpdateSurveyForCustomer(lblCustomerID.Text, lblSurveyID.Text);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "<script>alert('Success Export')</script>", false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "<script>alert('Failed Export')</script>", false);
            }

            Bind_Customer();
            
        }
    }
}