﻿/* documentation
 * 001 nanda 06/11/2017 - Optimasi Looping Data
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Core.Manager;
using System.Globalization;
using System.Data;
using System.IO;
using Core.Model;
using Microsoft.Reporting.WebForms;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace TransportService.pages
{

    public partial class ReportOSA_Area : System.Web.UI.Page
    {
        public static class Globals
        {
            public static int NOBranch = 0;
            public static List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            public static DataTable DtArea = new DataTable();
            public static List<Core.Model.mdlWeek> mdlOSAWeekList = new List<Core.Model.mdlWeek>();
            public static string gUserId = "";
        }

        public int PageNumber
        {
            get
            {
                if (ViewState["PageNumber"] != null)
                    return Convert.ToInt32(ViewState["PageNumber"]);
                else
                    return 0;
            }
            set
            {
                ViewState["PageNumber"] = value;
            }
        }

        private void ClearContent()
        {
            Globals.NOBranch = 0;
            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            Globals.DtArea = new DataTable();
            Globals.mdlOSAWeekList = new List<Core.Model.mdlWeek>();

            //put year in dropdown
            ddlTahun.Items.Clear();

            var currentYear = DateTime.Today.Year;
            for (int i = 0; i <= 5; i++)
            {
                // Now just add an entry that's the current year minus the counter
                ddlTahun.Items.Add((currentYear - i).ToString());
            }

            //put Week in dropdown
            int lWeekNow = DateFacade.GetIso8601WeekOfYear(DateTime.Now);
            for (int i = lWeekNow; i >= 0; i--)
            {
                // Now just add an entry that's the current year minus the counter
                ddlWeekFrom.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            for (int i = 52; i >= 0; i--)
            {

                ddlWeekTo.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }

            PnlBranch.Visible = false;
            btnSelectAll.Visible = false;
            btnUnSelectAll.Visible = false;
        }

        private bool CheckArea()
        {
            //validasi jika branch checkbox nya kosong
            bool lReturn = false;
            int icount = 0;
            foreach (ListItem Item in blBranch.Items)
            {
                if (Item.Selected == true)
                {
                    icount++;
                }
            }

            if (icount == 0)
            {
                Response.Write("<script>alert('Branch is Empty');</script>");
                lReturn = true ;
            }

            return lReturn;
        }
        
        private string getParamBranch()
        {
            String lResult = "";

            List<String> listBranch = new List<String>();
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {
                    // If the item is selected, add the value to the list.
                    listBranch.Add(item.Value);
                }
            }

            int i = Globals.NOBranch;

            lResult = listBranch[i]; 
            Globals.NOBranch += 1;
            return lResult;
        }

        //<<chartcode
        private void GetChartData(List<Core.Model.mdlOSAData> mdlOSAArealist)
        {
            String lData = string.Empty;
            String lPercent = string.Empty;
            //List<Core.Model.mdlGraphData> Temp_dataList = new List<Core.Model.mdlGraphData>();
            Core.Model.mdlGraphData details = new Core.Model.mdlGraphData();
            foreach (var lParam in mdlOSAArealist)
            {
                details = new Core.Model.mdlGraphData();
                details.element = lParam.ID;

                if (lParam.Percent != "")
                {
                    lData += "{ weeks: '" + lParam.Week + "', percents: " + lParam.Percent.Replace("%", "") + "},";
                }

                details.data = lData;
                lPercent = lParam.Percent;
            }

                if(details.data != null)
                    Globals.Temp_dataList.Add(details);
        }
        //chartcode>>

        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            List<String> listBranch = new List<String>();
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {// If the item is selected, add the value to the list.

                    listBranch.Add(item.Value);
                }
            }

            string lParamBranch = getParamBranch();
            if (lParamBranch == "")
            {
                return;
            }

            //<<01 Nanda
            /* Desription row
             * row[0] : Field ID
             * row[1] : Field Week
             * row[4] : Field Percent
            */
            var listOSAArea = new List<Core.Model.mdlOSAData>();
            DataRow[] result = Globals.DtArea.Select("BranchID = '"+lParamBranch+"'");
            int nx = 0;
            var mdlOSA = new Core.Model.mdlOSAData();
            foreach (DataRow row in result)
            {
                for (int i = nx; i < Globals.mdlOSAWeekList.Count; i++)
                {
                    if (Globals.mdlOSAWeekList[i].Week == row[1].ToString())
                    {
                        mdlOSA = new Core.Model.mdlOSAData();
                        mdlOSA.ID = row[0].ToString();
                        mdlOSA.Week = row[1].ToString();

                        
                        mdlOSA.Percent = row[2].ToString() + "%";

                        listOSAArea.Add(mdlOSA);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSA = new Core.Model.mdlOSAData();
                        mdlOSA.ID = row[0].ToString();
                        mdlOSA.Week = row[1].ToString();
                        mdlOSA.Percent = "";
                        listOSAArea.Add(mdlOSA);
                       
                    }
                }  
            }

            if (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater childRepeater = (Repeater)args.Item.FindControl("RptChildPercentlist");
                childRepeater.DataSource = listOSAArea;
                childRepeater.DataBind();
            }

            
            GetChartData(listOSAArea); //chartcode
        }

        private void BindData(string lWeekFrom, string lWeekTo)
        {
            List<String> listBranch = new List<String>();
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {// If the item is selected, add the value to the list.

                    listBranch.Add(item.Value);
                }
            }

            string role = rblRole.SelectedValue;

            Globals.DtArea = OSAFacade.LoadOSAAreaDT(listBranch, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue,role, Globals.gUserId);
            var listOSAGrandTotal = OSAFacade.LoadGrandTotalAreaByWeek(listBranch,ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue,role, Globals.gUserId);
            PagedDataSource pgitems2 = new PagedDataSource();
            pgitems2.DataSource = listOSAGrandTotal;
            RptChildGrandTotal.DataSource = pgitems2;
            RptChildGrandTotal.DataBind();


            var mdlOSAAreaList = new List<Core.Model.mdlOSAData>();
            var mdlOSAArea = new Core.Model.mdlOSAData();
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {
                    mdlOSAArea = new Core.Model.mdlOSAData();
                    mdlOSAArea.ID = item.Value;

                    mdlOSAAreaList.Add(mdlOSAArea);
                }
            }


            PagedDataSource pgitems = new PagedDataSource();
            pgitems.DataSource = mdlOSAAreaList;
            //pgitems.AllowPaging = true;
            //pgitems.PageSize = 10;
            //pgitems.CurrentPageIndex = PageNumber;
            //if (pgitems.PageCount > 1)
            //{
            //    lnkNext.Visible = true;
            //    lnkPrevious.Visible = true;
            //    rptPaging.Visible = true;

            //    ArrayList pages = new ArrayList();
            //    for (int i = 0; i < pgitems.PageCount; i++)
            //        pages.Add((i + 1).ToString());
            //    rptPaging.DataSource = pages;
            //    rptPaging.DataBind();

            //    Control control;
            //    control = rptPaging.Items[PageNumber].FindControl("btnPage");
            //    if (control != null)
            //    {
            //        ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["class"] = "active";
            //        ((LinkButton)(rptPaging.Items[PageNumber].FindControl("btnPage"))).Attributes["ForeColor"] = "Black";
            //    }

            //    if (PageNumber == 0)
            //    {
            //        lnkPrevious.Enabled = false;
            //        lnkNext.Enabled = true;
            //    }
            //    else if (PageNumber == pgitems.PageCount - 1)
            //    {
            //        lnkPrevious.Enabled = true;
            //        lnkNext.Enabled = false;
            //    }
            //    else
            //    {
            //        lnkPrevious.Enabled = true;
            //        lnkNext.Enabled = true;
            //    }
            //}
            //else
            //{
            //    rptPaging.Visible = false;
            //    lnkPrevious.Visible = false;
            //    lnkNext.Visible = false;
            //}

            RptOSAArealist.DataSource = pgitems;
            RptOSAArealist.DataBind();

            //<<chartcode
            PagedDataSource pgitems3 = new PagedDataSource();
            pgitems3.DataSource = Globals.Temp_dataList;
            RptChartlist.DataSource = pgitems3;
            RptChartlist.DataBind();
            //chartcode>>
            
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.NOBranch = 0;
            Globals.gUserId = Session["User"].ToString();

            if (!IsPostBack)
            {
                if (Session["User"] == null)
                {
                    Response.Redirect("Index.aspx");
                }
                else
                    Globals.gUserId = Session["User"].ToString(); 

                ClearContent();
                
            }
        }

        protected void btnShowBranch_Click(object sender, EventArgs e)
        {
            if (txtBranchID.Text == "")
            {
                var lBranchlist = OSAFacade.GetAreas();
                blBranch.DataSource = lBranchlist;
                blBranch.DataBind();
            }
            else
            {
                var lEmployeelist = OSAFacade.GetAreas(txtBranchID.Text); 
                blBranch.DataSource = lEmployeelist;
                blBranch.DataBind();
            }


            PnlBranch.Visible = true;
            btnSelectAll.Visible = true;
            btnUnSelectAll.Visible = true;
        }

        protected void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blBranch.Items)
            {
                item.Selected = false;
            }
        }

        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListItem item in blBranch.Items)
            {
                item.Selected = true;
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            //Check Checkbox Branch
            if (CheckArea() == true)
                return;

            Globals.Temp_dataList = new List<Core.Model.mdlGraphData>();  //chartcode
            if (Int16.Parse(ddlWeekFrom.SelectedValue) > Int16.Parse(ddlWeekTo.SelectedValue))
            {
                Response.Write("<script>alert('Week From may not more than Week To');</script>");
                return;
            }

            if (ddlWeekFrom.SelectedValue != "")
            {
                Globals.mdlOSAWeekList = OSAFacade.GetWeeks(ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue);
                PagedDataSource pgitemsweek = new PagedDataSource();
                pgitemsweek.DataSource = Globals.mdlOSAWeekList;
                RptWeeks.DataSource = pgitemsweek;
                RptWeeks.DataBind();
            }

            Globals.NOBranch = 0;
            BindData(ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue);
        }

        protected void RptOSAArealist_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ToAccount")
            {
                Control control;
                control = e.Item.FindControl("lnkToAccount");
                if (control != null)
                {
                    string id;

                    id = ((LinkButton)control).Text;
                    Response.Redirect("ReportOSA_Account.aspx?AreaId=" + id + "&WeekFrom=" + ddlWeekFrom.SelectedValue + "&WeekTo=" + ddlWeekTo.SelectedValue + "&Tahun=" + ddlTahun.SelectedValue + "&Role=" + rblRole.SelectedValue);
                }
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            string role = rblRole.SelectedValue;
            //Check Checkbox Branch
            if (CheckArea() == true)
                return;

            List<String> listBranch = new List<String>();
            foreach (ListItem item in blBranch.Items)
            {
                if (item.Selected)
                {// If the item is selected, add the value to the list.

                    listBranch.Add(item.Value);
                }
            }

            
            var mdlOSAAreaExcel = Core.Manager.OSAFacade.LoadOSAAreaExcel(listBranch, ddlWeekFrom.SelectedValue, ddlWeekTo.SelectedValue, ddlTahun.SelectedValue,role, Globals.gUserId);
            ReportDataSource rds = new ReportDataSource("DataSet1", mdlOSAAreaExcel);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(rds);

            ReportViewer1.ShowReportBody = true;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReportOSAArea.rdlc");
            ReportViewer1.LocalReport.Refresh();

            //download excel file
            ReportViewer1.ProcessingMode = ProcessingMode.Local;

            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;
            byte[] bytes = ReportViewer1.LocalReport.Render("EXCEL", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + "EXC_ReportOSA_Area" + "." + extension);
            Response.BinaryWrite(bytes); // create the file
            Response.Flush(); // send it to the client to download
            
            
            //Export Excel to Datagrid
            ////Create a dummy GridView
            //GridView GridView1 = new GridView();
            //GridView1.AllowPaging = false;
            //GridView1.DataSource = dt;
            //GridView1.DataBind();

            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition",
            // "attachment;filename=EXC_ReportOSA_Area.xls");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //StringWriter sw = new StringWriter();
            //HtmlTextWriter hw = new HtmlTextWriter(sw);

            //for (int i = 0; i < GridView1.Rows.Count; i++)
            //{
            //    //Apply text style to each Row
            //    GridView1.Rows[i].Attributes.Add("class", "textmode");
            //}
            //GridView1.RenderControl(hw);

            ////style to format numbers to string
            //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            //Response.Write(style);
            //Response.Output.Write(sw.ToString());
            //Response.Flush();
            //Response.End();
        }

    }
}