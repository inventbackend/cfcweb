﻿<%@ Page Title="Report Price List" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportPrice.aspx.cs" Inherits="TransportService.pages.ReportPrice" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- morris.js -->
    <script type="text/javascript" src="js/journey/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../vendors/raphael/raphael.min.js"></script>
    <script type="text/javascript" src="../vendors/morris.js/morris.min.js"></script>
    <!--bar fix-->
    <script>
        var arrChart = [];
    </script>
    <!--bar fix-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Price List
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <%--<<box parameter--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Parameter <small>Price</small>
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <div class="form-group">
                    <asp:ScriptManager ID="ScriptManager2" runat="server">
                    </asp:ScriptManager>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblWeekFrom" runat="server" Text="Week From" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="lblttdua1" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlWeekFrom" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblWeekTo" runat="server" Text="Week To" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="lblttdua2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlWeekTo" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>                  
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblArea" runat="server" Text="Area" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlArea" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px" AutoPostBack=true
                            ontextchanged="ddlArea_TextChanged">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblTahun" runat="server" Text="Tahun" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="lblttdua3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlTahun" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="Label1" runat="server" Text="Account" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:DropDownList ID="ddlAccount" runat="server" class="form-control" Style="margin-left: 0px;
                            width: 200px">
                        </asp:DropDownList>
                        
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <br />
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="Label5" runat="server" Text="Customer ID" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label10" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:TextBox ID="txtSearchCustomer" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"
                            PlaceHolder="Search Customer"></asp:TextBox>
                        <asp:Button ID="btnCustomer" runat="server" CssClass="btn btn-success" Text="Show Customer"
                            Style="margin-left: 10px" OnClick="btnCustomer_Click" />
                        <asp:Label ID="Label11" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                        <asp:Panel ID="pnlCustomer" CssClass="form-control" Height="200px" Width="400px" runat="server"
                            ScrollBars="Vertical" Style="margin-left: 10px">
                            <asp:CheckBoxList ID="blCustomer" runat="server" Width="500px">
                            </asp:CheckBoxList>
                        </asp:Panel>
                        <br />
                        <asp:Button ID="btnSelectAllCustomer" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                            OnClick="btnSelectAllCustomer_Click" Style="margin-left: 10px" />
                        <asp:Button ID="btnUnSelectAllCustomer" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL"
                            OnClick="btnUnSelectAllCustomer_Click" Style="margin-left: 10px" />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="Label7" runat="server" Text="Product" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label8" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:TextBox ID="txtProduct" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"
                            PlaceHolder="Search Product"></asp:TextBox>
                        <asp:Button ID="btnProduct" runat="server" CssClass="btn btn-success" Text="Show Product"
                            Style="margin-left: 10px" OnClick="btnProduct_Click" />
                        <asp:Label ID="Label9" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                        <asp:Panel ID="pnlProduct" CssClass="form-control" Height="200px" Width="400px" runat="server"
                            ScrollBars="Vertical" Style="margin-left: 10px">
                            <asp:CheckBoxList ID="blProduct" runat="server" Width="500px">
                            </asp:CheckBoxList>
                        </asp:Panel>
                        <br />
                        <asp:Button ID="btnSelectAllProduct" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                            OnClick="btnSelectAllProduct_Click" Style="margin-left: 10px" />
                        <asp:Button ID="btnUnSelectAllProduct" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL"
                            OnClick="btnUnSelectAllProduct_Click" Style="margin-left: 10px" />
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <br />
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport"/>
                        <asp:Button ID="btnExportExcel" runat="server" Text="Export To Excel" class="btn btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnExportExcel_Click" ValidationGroup="valShowReport" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--box parameter>>--%>
    <%--<<tab chart and table--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <i class="fa fa-bars"></i>Charts & Tables<small>Area</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                        <li role="presentation" class=""><a href="#tab_content11" id="home-tabb" role="tab"
                            data-toggle="tab" aria-controls="home" aria-expanded="true">Charts</a> </li>
                        <li role="presentation" class="active"><a href="#tab_content22" role="tab" id="profile-tabb"
                            data-toggle="tab" aria-controls="profile" aria-expanded="false">Tables</a> </li>
                    </ul>
                    <div id="myTabContent2" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade " id="tab_content11" aria-labelledby="home-tab">
                            <asp:Repeater ID="RptChartlist" runat="server">
                                <ItemTemplate>
                                    <!-- bar chart -->
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>
                                                    <small>Customer : </small>
                                                    <%# Eval("element")%></h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                                </ul>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="x_content">
                                                <div id="<%# Eval("element") %>_line" style="width:100%; height:280px;">
                                                </div>
                                                <script type="text/javascript">
//                                                     $(document).ready(function () {
//                                                     Morris.Line({
//                                                        element: '<%# Eval("element") %>_line',
//                                                        data: [<%# Eval("data") %>],
//		                                                xkey: 'week',
//                                                        ykeys: [<%# Eval("yKey") %>],
//                                                        labels: [<%# Eval("labels") %>],
//                                                        parseTime: false,
//                                                        hideHover: 'auto',
//                                                        pointSize: 2,
//                                                        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
//                                                        resize: true
//                                                     });
//                                                     $MENU_TOGGLE.on('click', function () {
//                                                        $(window).resize();
//                                                     });

                                                     arrChart.push(new 
                                                     Morris.Line({
                                                        element: '<%# Eval("element") %>_line',
                                                        data: [<%# Eval("data") %>],
		                                                xkey: 'week',
                                                        ykeys: [<%# Eval("yKey") %>],
                                                        labels: [<%# Eval("labels") %>],
                                                        parseTime: false,
                                                        hideHover: 'auto',
                                                        pointSize: 2,
                                                        lineColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                                        resize: true,
                                                        redraw:true 
                                                     }));
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /bar charts -->
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content22" aria-labelledby="profile-tab">
                            <div class="datagrid" style="overflow-x:scroll;overflow-y:hidden;width:1000px;height:100%   ">
                                <table style="width: 100%; margin-bottom: 0px;">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" Width="100px">
                                                <center>Area</center>
                                            </th>
                                            <th rowspan="2">
                                                Product
                                            </th>
                                            <th rowspan="2">
                                                Customer
                                            </th>
                                            <asp:Repeater ID="RptWeeks" runat="server">
                                                <ItemTemplate>
                                                    <th colspan="2">
                                                        <center><asp:Label ID="lWeek" runat="server" Text='<%# Eval("Week") %>'></asp:Label></center>
                                                    </th>
                                                    
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                        <tr>
                                        <asp:Repeater ID="RptType" runat="server">
                                        <ItemTemplate>
                                                <th>
                                                    <center><asp:Label ID="Label1" runat="server" Text="Normal" Width="50px" Font-Bold="True"></asp:Label></center>
                                                </th>
                                                <th>
                                                    <center><asp:Label ID="Label4" runat="server" Text="Promo" Width="50px" Font-Bold="True"></asp:Label></center>
                                                </th>                                            
                                        </ItemTemplate>
                                        </asp:Repeater>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <asp:Repeater ID="RptPricelist" runat="server" OnItemDataBound="ItemBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%# Eval("BranchID") %>
                                                        </td>
                                                        <td>
                                                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("ProductName") %>' Width="300px" ></asp:Label>     
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("CustomerName") %>' Width="200px" ></asp:Label> 
                                                        </td>
                                                        <%--Repeater for child data percentage--%>
                                                        <asp:Repeater ID="RptChildPricelist" runat="server">
                                                            <ItemTemplate>
                                                                <td>
                                                                     <%# Eval("NormalPrice")%>  
                                                                </td>
                                                                <td>
                                                                     <%# Eval("PromoPrice")%>  
                                                                </td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </tbody>
                        
                                </table>
                            </div>
                        </div>
                        
                        <asp:Panel ID="PanelReport" runat="server" Height="500px" Width="100%" CssClass="datagrid"
                            Visible="false">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                                Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                                WaitMessageFont-Size="14pt">
                                <LocalReport ReportPath="Reports\ReportPrice.rdlc"></LocalReport>
                            </rsweb:ReportViewer>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--bar fix-->
    <script type="text/javascript">
        $(document).ready(function () {

            $('.nav-tabs a').on('shown.bs.tab', function (event) {
                for (var i = 0; i < arrChart.length; i++) {
                    arrChart[i].redraw();
                }
            });
        });
    </script>
<!--bar fix-->
    <%--tab chart and table>>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
