﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="ReportOSA_Account_Customer_Product.aspx.cs" Inherits="TransportService.pages.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="page-title">
        <div class="title_left">
            <h3>
                OSA Stock Product
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <br />
                <div class="form-group">

                    <div class="col-md-12">
                        <input action="action" type="button" class="btn btn-sm btn-embossed btn-primary" value="Back"  onclick="window.history.go(-1);" />
                    </div>
                    <br />
                    <div class="datagrid" style="width: 1000px">
                        <table style="width: 100%; margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>
                                        Product
                                    </th>
                                    <asp:Repeater ID="RptWeeks" runat="server">
                                        <ItemTemplate>
                                            <th>
                                                <asp:Label ID="lWeek" runat="server" Text='<%# Eval("Week") %>'></asp:Label>
                                            </th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <asp:Repeater ID="RptOSAProductlist" runat="server" OnItemDataBound="ItemBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Product" runat="server" Text='<%# Eval("ProductID") %>'></asp:Label> - <asp:Label ID="ProductName" runat="server" Text='<%# Eval("ProductName") %>'></asp:Label>
                                                </td>
                                                <%--Repeater for child data percentage--%>
                                                <asp:Repeater ID="RptChildStocklist" runat="server">
                                                    <ItemTemplate>
                                                        <td>
                                                           <%# Eval("Stock") %>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                          
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
