﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="UserConfig.aspx.cs" Inherits="TransportService.pages.UserConfig" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form User Config</h3>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <asp:TextBox ID="txtSearchUserConfig" PlaceHolder="Search by Device Id or Employee Id..."
                    runat="server" class="form-control"></asp:TextBox>
                <span class="input-group-btn">
                    <button id="btnSearchUserConfig" runat="server" class="btn btn-default" type="button"
                        onserverclick="btnSearchUserConfig_Click">
                        Search</button>
                </span>
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <%--<div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <div class="form-group">
                    <br />
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <p>
                        <asp:Label ID="lbl1" runat="server" Text="License :" Width="100px" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px;" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lblLicense" runat="server" Width="100px" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px; margin-left: -30px;" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lbl2" runat="server" Text="License Registered :" Width="160px" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px; margin-left: 10px" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lblLicenseRegistered" runat="server" Width="100px" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px; margin-left: -20px;" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lbl3" runat="server" Text="License Available :" Width="160px" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px; margin-left: 10px" Font-Bold="True"></asp:Label>
                        <asp:Label ID="lblLicenseAvailable" runat="server" Width="10px" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px; margin-left: -30px;" Font-Bold="True"></asp:Label>
                        <br />
                        <br />
                        <asp:Button ID="btnNew" runat="server" Text="Add New" class="btn btn-primary" OnClick="btnNew_Click">
                        </asp:Button>
                        
                            <div class="datagrid" style="width: 100%">
                            <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                                <table style="width: 100%">
                                    <thead>
                                        <tr>
                                            
                                            <th>
                                                Device ID
                                            </th>
                                            <th>
                                                Employee ID
                                            </th>
                                            <th>
                                                Branch ID
                                            </th>
                                            <th>
                                                Branch Name
                                            </th>
                                            <th>
                                                Vehicle Number
                                            </th>
                                            <th>
                                                Ip Local
                                            </th>
                                            <th>
                                                Port Local
                                            </th>
                                            <th>
                                                Ip Public
                                            </th>
                                            <th>
                                                Port Public
                                            </th>
                                            <th>
                                                Ip Alt
                                            </th>
                                            <th>
                                                Port Alt
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="RptUserConfiglist" runat="server" OnItemCommand="rptUserConfig_ItemCommand">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkDeviceId" runat="server" Enabled='<%# Eval("Role") %>' CommandName="Link"
                                                            Text='<%# Eval("DeviceID")%>'></asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblEmployeeId" runat="server" Text='<%# Eval("EmployeeID")%>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <%# Eval("BranchID")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("BranchName")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("VehicleNumber")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("IpLocal")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("PortLocal")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("IpPublic")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("PortPublic")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("IpAlternative")%>
                                                    </td>
                                                    <td>
                                                        <%# Eval("PortAlternative")%>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkDelete" Text="Delete" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Delete this User Config ?');"
                                                            runat="server" CommandName="Delete" />
                                                    </td>
                                                </tr>
                                                
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="12">
                                                    <div id="paging">
                                                        <ul>
                                                            <li>
                                                                <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                            <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                            runat="server"><span><%# Container.DataItem%></span></asp:LinkButton>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <li>
                                                                <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                </table>
                                </asp:Panel>
                            </div>
                        
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PanelFormUserConfig" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form User Config Management
                    </h2>
                    <%--<ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Settings 1</a> </li>
                                <li><a href="#">Settings 2</a> </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>--%>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblDeviceID" runat="server" Text="Device ID" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="*" runat="server"
                                    ControlToValidate="txtDeviceID" ValidationGroup="valUserConfig" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtDeviceID" PlaceHolder="Device ID" runat="server" Width="400px"
                                    class="form-control" data-inputmask="'mask': '**:**:**:**:**:**'" MaxLength="17"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblBranchID" runat="server" Text="Branch ID" Font-Bold="True"></asp:Label>
                                <%--<asp:TextBox ID="txtBranchID" PlaceHolder="Branch" runat="server" Width="400px"
                                        class="form-control"></asp:TextBox>--%>
                                <asp:UpdatePanel ID="updatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlSearchBranchID" AutoPostBack="true" runat="server" class="form-control"
                                            Style="margin-left: 0px; width: 150px" OnSelectedIndexChanged="ddlSearchBranchID_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:TextBox ID="txtBranchNm" runat="server" Width="400px" class="form-control" Visible="false"></asp:TextBox>
                                <%-- <ajaxToolkit:AutoCompleteExtender ID="txtBranchID_AutoCompleteExtender" runat="server"
                                            ServiceMethod="SearchBranchID" MinimumPrefixLength="0" EnableCaching="false"
                                        DelimiterCharacters="" Enabled="True" TargetControlID="txtBranchID">
                                        </ajaxToolkit:AutoCompleteExtender>--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" Font-Bold="True"></asp:Label>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtEmployee" ValidationGroup="valUserConfig" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>

                                    <asp:TextBox ID="txtEmployee" PlaceHolder="Employee" runat="server" Width="400px"
                                    class="form-control" onkeyup = "SetContextKey()"></asp:TextBox>--%>
                                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
                                <asp:UpdatePanel ID="updatePanel" style="width: 100%; height: 100px; overflow: scroll"
                                    runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="rblEmployee" runat="server">
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <%-- onblur="Warning()"--%>
                                <%--<ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender0" runat="server"
                                    ServiceMethod="SearchEmployee" MinimumPrefixLength="4" EnableCaching="false"
                                    DelimiterCharacters="" Enabled="True" TargetControlID="txtEmployee" UseContextKey="true">
                                    </ajaxToolkit:AutoCompleteExtender>--%>
                                <%--<script type = "text/javascript">
                                        function SetContextKey() {
                                            $find('<%=AutoCompleteExtender0.ClientID%>').set_contextKey($get("<%=ddlSearchBranchID.ClientID %>").value);
                                        }
                                    </script>--%>
                                <%--<script type="text/javascript">
                                        function Warning() {
                                            var txtEmpl = document.getElementById("<%=txtEmployee.ClientID%>").value;

                                            if (txtEmpl == "King") {
                                                alert("The Employee Code is not Exist");
                                            }
                                        }
                                    </script>--%>
                            </div>
                        </div>
                        <div class="form-group" style="visibility: hidden;">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblVehicleNumber" runat="server" Text="Vehicle Number" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtVehicleNumber" PlaceHolder="Vehicle Number" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>
                                <ajaxToolkit:AutoCompleteExtender ID="txtVehicleNumber_AutoCompleteExtender" runat="server"
                                    ServiceMethod="SearchVehicleNumber" MinimumPrefixLength="3" EnableCaching="false"
                                    DelimiterCharacters="" Enabled="True" TargetControlID="txtVehicleNumber">
                                </ajaxToolkit:AutoCompleteExtender>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblIpLocal" runat="server" Text="Ip Local" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtIpLocal" PlaceHolder="Ip Local" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblPortLocal" runat="server" Text="Port Local" Font-Bold="True"></asp:Label>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtPortLocal"
                                    ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$" ValidationGroup="valUserConfig">
                                    </asp:RegularExpressionValidator>--%>
                                <%-- <asp:TextBox ID="txtPortLocal" PlaceHolder="Port Local" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>--%>
                                <input id="txtPortLocal2" style="width: 400px;" class="form-control" onkeypress='return validateQty(event);'
                                    type="text" runat="server" placeholder="Port Local" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblIpPublic" runat="server" Text="Ip Public" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtIpPublic" PlaceHolder="Ip Public" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblPortPublic" runat="server" Text="Port Public" Font-Bold="True"></asp:Label>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtPortPublic"
                                    ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$" ValidationGroup="valUserConfig">
                                    </asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtPortPublic" PlaceHolder="Port Public" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox> --%>
                                <input id="txtPortPublic2" style="width: 400px;" class="form-control" onkeypress='return validateQty(event);'
                                    type="text" runat="server" placeholder="Port Public" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblIpAlternative" runat="server" Text="Ip Alternative" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtIpAlternative" PlaceHolder="Ip Alternative" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblPortAlternative" runat="server" Text="Port Alternative" Font-Bold="True"></asp:Label>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPortAlternative"
                                    ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$" ValidationGroup="valUserConfig">
                                    </asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtPortAlternative" PlaceHolder="Port Alternative" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>--%>
                                <input id="txtPortAlternative2" style="width: 400px;" class="form-control" onkeypress='return validateQty(event);'
                                    type="text" runat="server" placeholder="Port Alternative" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <br />
                                <asp:Label ID="lblPasswrod" runat="server" Text="Password" Font-Bold="True"></asp:Label>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPortAlternative"
                                    ErrorMessage="Please Enter Only Numbers" ForeColor="Red" ValidationExpression="^\d+$" ValidationGroup="valUserConfig">
                                    </asp:RegularExpressionValidator>
                                <asp:TextBox ID="txtPortAlternative" PlaceHolder="Port Alternative" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>--%>
                                <asp:TextBox ID="txtPassword" PlaceHolder="Password" runat="server" Width="400px"
                                    class="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <br />
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click"
                                    ValidationGroup="valUserConfig" />
                                <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                    ValidationGroup="valUserConfig" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- jquery.inputmask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- jquery.inputmask -->
    <script>
        $(document).ready(function () {
            $(":input").inputmask();
        });
    </script>
    <!-- /jquery.inputmask -->
    <script>
        function validateQty(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        };
    </script>
    <script type="text/javascript">
        function myFunction() {
            // Declare variables 
            var input, filter, table, tr, td, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("<%= rblEmployee.ClientID %>");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

</script>
</asp:Content>
