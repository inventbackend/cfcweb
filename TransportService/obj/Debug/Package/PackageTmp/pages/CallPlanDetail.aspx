﻿<%@ Page Title="Call Plan Detail" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="True" CodeBehind="CallPlanDetail.aspx.cs" Inherits="CallPlanDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Itenary Detail</h3>
        </div>
        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <asp:TextBox ID="txtSearchCallPlan" PlaceHolder="Search Itenary..." runat="server"
                    class="form-control"></asp:TextBox>
                    <span class="input-group-btn">
                        <button id="btnSearchCallPlan" runat="server" class="btn btn-default" type="button"
                        onserverclick="btnSearchCallPlan_Click">
                        Search</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <%--<div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <br />
                    <div class="row">
                        <div class="col-lg-12">
                            <%--<div class="form-group">--%>
                            <%-- <asp:Label ID="lblSearch" runat="server" Text=" Call Plan ID : "></asp:Label>--%>
                            <%-- <asp:TextBox ID="txtSearch" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="tb5" ></asp:TextBox>--%>
                            <%--<asp:Label ID="lblSearch2" runat="server" Text=" Customer ID : "></asp:Label>
                                        <asp:TextBox ID="txtSearch2" runat="server" Style="margin-left: 9px; margin-top: 0px"
                                            Width="150px" CssClass="tb5"></asp:TextBox>--%>
                            <%--<asp:Button ID="btnSearch2" runat="server" Text="Search"  OnClick="btnSearch2_Click" class="btn btn-primary"></asp:Button>--%>
                            <asp:Button ID="btnAdd" runat="server" Text="Add New" class="btn btn-primary" OnClick="btnAdd_Click" />
                            <asp:Button ID="btnHome" runat="server" Text="Itenary" class="btn btn-primary" OnClick="btnHome_Click" />
                            <%--</div>--%>
                            <br />
                            <br />
                            <div class="datagrid" style="width: 1000px">
                                <table style="width: 100%; margin-bottom: 0px;" id="tbCallPlan">
                                    <thead>
                                        <tr>
                                            <th style="display: none;">
                                                CP Detail ID
                                            </th>
                                            <th>
                                                Itenary ID
                                            </th>
                                            <th>
                                                Customer ID
                                            </th>
                                            <th>
                                                Customer Name
                                            </th>
                                            <th>
                                                Time
                                            </th>
                                            <th>
                                                Sequence
                                            </th>
                                            <th>
                                            </th>
                                            <th>
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="RptCPDetaillist" runat="server" 
                                            OnItemCommand="rptCPDetail_ItemCommand"> 
                                            <%--onitemdatabound="RptCPDetaillist_ItemDataBound">--%>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="display: none;">
                                                        <asp:Label ID="lnkCPDetailId" runat="server" Text='<%# Eval("CPDetailID") %>'></asp:Label>
                                                        <%--<asp:LinkButton ID="lnkCallPlanId" runat="server" Text='<%# Eval("CallPlanID") %>'
                                                                CommandName=""></asp:LinkButton>--%>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lnkCallPlanId" runat="server" Text='<%# Eval("CallPlanID") %>'></asp:Label>
                                                        <%--<asp:LinkButton ID="lnkCallPlanId" runat="server" Text='<%# Eval("CallPlanID") %>'
                                                                CommandName=""></asp:LinkButton>--%>
                                                    </td>
                                                    <td>
                                                     <asp:Label ID="CustomerId" runat="server" Text='<%# Eval("CustomerID") %>'></asp:Label>
                                                        <%--<asp:Label ID="CustomerId" runat="server" Text='<%# Eval("CustomerID") %>'
                                                            CommandName="showDO"></asp:LinkButton>--%>
                                                        <%--<asp:Label ID="CustomerID" runat="server" Text='<%# Eval("CustomerID") %>'></asp:Label>--%>
                                                        <%-- <%# Eval("CustomerID") %>--%>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="CustomerName" runat="server" Text='<%# Eval("CustomerName") %>'></asp:Label>
                                                        <%--   <%# Eval("Customer.CustomerName")%>--%>
                                                    </td>
                                                    <td>
                                                     <asp:Label ID="WarehouseId" runat="server" Text='<%# Eval("Time") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Sequence" runat="server" Text='<%# Eval("Sequence") %>'></asp:Label>
                                                        <%-- <%# Eval("Sequence") %>--%>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkMove" Text="Move" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Move this Call Plan Detail ?');"
                                                            runat="server" CommandName="Move" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" Text="Edit" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Edit this Call Plan Detail ?');"
                                                            runat="server" CommandName="Edit" />
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkDelete" Text="Delete" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to Delete this Call Plan Detail ?');"
                                                            runat="server" CommandName="Delete" />
                                                    </td>
                                                </tr>

                                                <%--DROPDOWN PANEL DELIVERY ORDER--%>
                                                
                                                  <%--<tr>
                                                  <td colspan="8">
                                                 <div class="datagridChild" style="width: 1000px">
                                <table style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>
                                                DO Number
                                            </th>
                                            <th>
                                                DO Date
                                            </th>
                                            <th>
                                                DO Status
                                            </th>
                                            <th>
                                                Description
                                            </th>
                                            <th>
                                                Customer ID
                                            </th>
                                            <th>
                                                Warehouse ID
                                            </th>
                                            <th>
                                                Employee ID
                                            </th>
                                            <th>
                                                Branch ID
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <asp:Repeater ID="RptDOlist" runat="server">
                                    <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%# Eval("DONumber") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("DODate") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("DOStatus") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Description") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("CustomerID") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("WarehouseID") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("EmployeeID") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("BranchID") %>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </ItemTemplate>
                                    </asp:Repeater>
                                    </tbody>
                                </table>
                                </div>
                                               </td>
                                               </tr>--%>
                                               
                                               <%--end of dropdown list--%>

                                    </tbody>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tfoot>
                                            <tr>
                                                <td colspan="8">
                                                    <div id="paging">
                                                        <ul>
                                                            <li>
                                                                <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span> Previous </span></asp:LinkButton></li>
                                                            <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                            runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <li>
                                                                <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span> Next </span></asp:LinkButton></li>
                                                        </ul>
                                                    </div>
                                            </tr>
                                        </tfoot>
                                </table>
                            </div>

                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<%--<script type="text/javascript">
    $(function () {
        $("td[colspan=8]").find("table").hide();
        $("table[id=tbCallPlan]").click(function (event) {
            event.stopPropagation();
            var $target = $(event.target);
            if ($target.closest("td").attr("colspan") > 1) {
                $target.slideUp();
            }
            else {
                $target.closest("tr").next().find("table").slideToggle();
            }
        });
    });
</script>--%>
</asp:Content>
