﻿<%@ Page Title="HOME" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="Index.aspx.cs" Inherits="TransportService.pages.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="dvMap" style="width: 100%; height: 540px">
        </div>
    </div>
    <%--             <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Chart OSA Area <small>2 Weeks</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="barChartOSA"></canvas>
                  </div>
                </div>
              </div>
           
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Bar graph <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <canvas id="barChartOOS"></canvas>
                  </div>
                </div>
              </div>
--%>
    <%-- <div><label> test <%=jsonOSA%></label></div>--%>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4a8ijWorZUkJKOtzheGKtV1RlyfDYW24"></script>
    <script type="text/javascript" src="../js/GoogleMap.js"></script>
    <script src="../js/mapwithmarker.js" type="text/javascript"></script>


    <!-- Chart.js -->
<%--    <script>
      Chart.defaults.global.legend = {
        enabled: false
      };

     

      // Bar chart
      var ctx = document.getElementById("barChartOSA");
      var mybarChart = new Chart(ctx, {
        type: 'bar',
    data: <%=jsonOSA%>,

        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                max: 100
              }
            }]
          }
        }
    });


var ctx = document.getElementById("barChartOOS");
var mybarChart = new Chart(ctx, {
    type: 'bar',
    data: <%=jsonOOS%>,

    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: 100
                }
            }]
        }
    }
});


     
    </script>--%>
    <style type="text/css">
        .labels
        {
            color: black;
            background-color: #FF8075;
            font-family: Arial;
            font-size: 9px;
            font-weight: bold;
            text-align: center;
            width: 100px;
        }
    </style>
    <script type="text/javascript">
        var markers = [
        <asp:Repeater ID="rptLiveTrackingMarkers" runat="server">
        <ItemTemplate>
                    {
                    "lTrackingDate": '<%# Eval("TrackingDate") %>',
                    "lat": '<%# Eval("Latitude") %>',
                    "lng": '<%# Eval("Longitude") %>',
                    "lEmployeeID": '<%# Eval("EmployeeID") %>',
                    "lEmployeeName": '<%# Eval("EmployeeName") %>',
                    "lBranchID": '<%# Eval("BranchID") %>',
                    "lVehicleID": '<%# Eval("VehicleID") %>',
                    "lStreetName": '<%# Eval("StreetName") %>'
                   

                }
        </ItemTemplate>
        <SeparatorTemplate>
            ,
        </SeparatorTemplate>
        </asp:Repeater>
        ];
    </script>
    <script type="text/javascript">
        function initialize() {
            var latlng = new google.maps.LatLng(-6.305964, 106.865102);
            var myOptions = {
                zoom: 9,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                marker: true
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), myOptions);

            var infoWindow = new google.maps.InfoWindow();

            for (i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);




                var marker = new MarkerWithLabel({
                    position: myLatlng,
                    map: map,
                    title: data.lEmployeeName,
                    labelContent: data.lEmployeeName,
                    labelAnchor: new google.maps.Point(48, 65),
                    labelClass: "labels", // the CSS class for the label
                    labelInBackground: false,
                    icon: "../Images/Visit.png"
                });

                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent('<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h5 id="firstHeading" class="firstHeading">Employee : ' + data.lEmployeeName + ' | ' + data.lTrackingDate + '</h1><hr>' +
                            '<h5 id="secondHeading class="secondHeading">' + data.lStreetName + '</h3>' +
                            '<h5 id="secondHeading class="secondHeading">Branch ID : ' + data.lBranchID + '</h5>' +
                            '</div>');
                        infoWindow.open(map, marker);
                    });
                })(marker, data);

            }
        }
        window.onload = initialize;
    </script>
</asp:Content>
