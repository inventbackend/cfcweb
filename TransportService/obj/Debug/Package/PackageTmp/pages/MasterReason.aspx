﻿<%@ Page Title="Master Reason" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="MasterReason.aspx.cs" Inherits="TransportService.pages.MasterReason" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <script type="text/javascript">
        var picker1 = new Pikaday(
                                                        {
                                                            field: document.getElementById('<%=txtDate.ClientID%>'),
                                                            firstday: 1,
                                                            minDate: new Date('2000-01-01'),
                                                            maxDate: new Date('2020-12-31'),
                                                            yearRange: [2000, 2020]
                                                            //                                    setDefaultDate: true,
                                                            //                                    defaultDate : new Date()
                                                        }
                                                        );
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                Form Reason</h3>
        </div>
        <div class="title_right">
            <%--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <%--<div class="x_title">
                <h2>
                    Form Basic Elements <small>different form elements</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>--%>
            <div class="x_content">
                <br />
                <div class="datagrid">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <div class="form-group">
                                    <br />
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>

                                    <asp:Label ID="lblSearch" runat="server" Text="Search :" Font-Bold="True" Width="70px" Cssclass="control-label col-md-3 col-sm-3 col-xs-12"></asp:Label>
                                    <asp:TextBox ID="txtSearch" runat="server"
                                        Width="160px" CssClass="form-control col-md-3 col-xs-12" placeholder="Find By Reason Type or Name Here"></asp:TextBox>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" Style="margin-left: 9px;
                                        margin-top: 0px" class="btn btn-primary" OnClick="btnSearch_Click"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="Add New" class="btn btn-primary" OnClick="btnNew_Click" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="datagrid">
                        <table style="width: 100%">
                            <thead>
                                <tr>
                                    <th>
                                        Reason ID
                                    </th>
                                    <th>
                                        Reason Type
                                    </th>
                                    <th>
                                        Value
                                    </th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptReasonlist" runat="server" OnItemCommand="RptReasonlist_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:LinkButton ID="lnkReasonId" runat="server" Text='<%# Eval("ReasonID") %>' Enabled='<%# Eval("Role") %>' CommandName="Link"></asp:LinkButton>
                                            </td>
                                            <td>
                                                <%# Eval("ReasonType") %>
                                            </td>
                                            <td>
                                                <%# Eval("Value") %>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkDelete" Text="Delete" Visible='<%# Eval("Role") %>' OnClientClick="return confirm('Are you sure you want to delete this Cost?');"
                                                    runat="server" CommandName="Delete" />
                                            </td>
                                        </tr>
                                        </tbody>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <div id="paging">
                                            <ul>
                                                <li>
                                                    <asp:LinkButton ID="lnkPrevious" runat="server" OnClick="lnkPrevious_Click"><span>Previous</span></asp:LinkButton></li>
                                                <asp:Repeater ID="rptPaging" runat="server" OnItemCommand="rptPaging_ItemCommand">
                                                    <ItemTemplate>
                                                        <li>
                                                            <asp:LinkButton ID="btnPage" CommandName="Page" CommandArgument="<%# Container.DataItem %>"
                                                                runat="server"><span><%# Container.DataItem %></span></asp:LinkButton>
                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <li>
                                                    <asp:LinkButton ID="lnkNext" runat="server" OnClick="lnkNext_Click"><span>Next</span></asp:LinkButton></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<asp:Panel ID="PanelReason" runat="server" Width="100%">
    <div class="col-md-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    Form Reason Management</h2>
                <%--<ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a> </li>
                            <li><a href="#">Settings 2</a> </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>--%>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                    <fieldset>
                        <p>
                            <asp:Label ID="lblReasonID" runat="server" Text="Reason ID" Font-Bold="True"></asp:Label><br />
                            <asp:TextBox ID="txtReasonID" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                ControlToValidate="txtReasonID" ValidationGroup="valReason" ForeColor="#FF3300" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="Label1" runat="server" Text="Reason Name" Font-Bold="True"></asp:Label><br />
                            <asp:TextBox ID="txtReasonName" runat="server" Width="400px" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                ControlToValidate="txtReasonName" ValidationGroup="valReason" ForeColor="#FF3300"
                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </p>
                        <p>
                        <asp:Label ID="Label2" runat="server" Text="Reason Type" Font-Bold="True"></asp:Label><br />
                        <asp:DropDownList ID="ddlReasonType" runat="server" class="form-control" Style="margin-left:0px; width:150px">
                        <asp:ListItem Text="Delivery" Value="delivery"></asp:ListItem>
                        <asp:ListItem Text="Visit" Value="visit"></asp:ListItem>
                        <asp:ListItem Text="Delivery Product" Value="deliveryproduct"></asp:ListItem>
                        </asp:DropDownList>
                        </p>
                        <p>
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-primary" OnClick="btnUpdate_Click"
                                ValidationGroup="valCost" />
                            <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                ValidationGroup="servicestockin" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" OnClick="btnCancel_Click" />
                        </p>
                    </fieldset>
                
            </div>
        </div>
    </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
