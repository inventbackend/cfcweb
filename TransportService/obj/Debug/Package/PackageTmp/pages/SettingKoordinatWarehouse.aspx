﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="SettingKoordinatWarehouse.aspx.cs" 
Inherits="TransportService.pages.SettingKoordinatWarehouse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="page-title">
        <div class="title_left">
            <h3>
                Setting Koordinat Warehouse</h3>
        </div>
        <div class="title_right">
            <%--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            Go!</button>
                    </span>
                </div>
            </div>--%>
        </div>
    </div>
    <div class="clearfix">
    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>
                                    Warehouse Coordinate<small>Control Panel</small></h2>
                                <%--<ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a> </li>
                                            <li><a href="#">Settings 2</a> </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                </ul>--%>
                                <div class="clearfix">
                                </div>
                            </div>
                            <div class="x_content">
                                <br />
                 <div class="box-body">
                 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="box-body">
        <div id="divCabang" runat="server" class="form-group">
            <label>
            Cabang</label>
            <asp:DropDownList ID="ddlCabang" runat="server" class="form-control" 
                AutoPostBack="True" onselectedindexchanged="ddlCabang_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div id="divSales" runat="server" class="form-group">
           <label>
                Customer</label>
                <table>
                <tr>
                <td><asp:TextBox ID="txtSearch" runat="server" CssClass="form-control col-md-3 col-xs-12" placeholder="Search"></asp:TextBox></td><td>
                    <asp:Button ID="btnSearch" runat="server" Text="Show" onclick="btnSearch_Click"></asp:Button></td>
                </tr>
                </table>
                <br />
            <asp:Panel ID="Panel1" runat="server" Height="200px" ScrollBars="Horizontal">
             
                <table>
              
                <tr>
                <asp:DataGrid ID="gvCustomer" class="table table-bordered" runat="server" 
                         AutoGenerateColumns="false" AllowPaging="False" PageSize="10" 
                         onpageindexchanged="gvCustomer_PageIndexChanged" 
                         onitemcommand="gvCustomer_ItemCommand">
                    <Columns>
                                        <asp:ButtonColumn DataTextField="CustomerId" HeaderText="Customer ID" CommandName="id">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="CustomerId" HeaderText="Customer ID" Visible=false>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CustomerName" HeaderText="Customer Name">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CustomerAddress" HeaderText="Address">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                       
                                    </Columns>
                                     <PagerStyle CssClass="table_page" PrevPageText="previous ‹.." NextPageText="..› next" BackColor="#F7F7DE" ForeColor="Gray" HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Names="verdana" Font-Overline="False" Font-Size="8pt" Font-Strikeout="False" Font-Underline="False" />
                    </asp:DataGrid>
                </tr>
                </table>
            </asp:Panel>
           
          
        </div>

        <br />
        <div id="divWarehouse" runat="server" class="form-group">
           <label>
                Warehouse</label>
                <table>
                <tr>
                <td><asp:TextBox ID="txtCustomerID" runat="server" ReadOnly="true" CssClass="form-control col-md-3 col-xs-12" placeholder="Customer ID"></asp:TextBox></td>
                </tr>
                <tr>
                <td><asp:TextBox ID="txtWarehouseID" runat="server" CssClass="form-control col-md-3 col-xs-12" placeholder="Warehouse ID"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                ControlToValidate="txtWarehouseID" ValidationGroup="valShowSettingKoordinatWarehouse" ForeColor="#FF3300"
                                                SetFocusOnError="True"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                <td><asp:TextBox ID="txtSearchWarehouse" runat="server" CssClass="form-control col-md-3 col-xs-12" placeholder="Search"></asp:TextBox></td><td>
                    <asp:Button ID="btnSearchWarehouse" runat="server" Text="Show" onclick="btnSearchWarehouse_Click"></asp:Button></td>
                </tr>
                </table>
                <br />
            <asp:Panel ID="PanelWarehouse" runat="server" Height="200px" ScrollBars="Horizontal">
             
                <table>
              
                <tr>
                <asp:DataGrid ID="gvWarehouse" class="table table-bordered" runat="server" 
                         AutoGenerateColumns="false" AllowPaging="False" PageSize="10" 
                         onpageindexchanged="gvWarehouse_PageIndexChanged" 
                         onitemcommand="gvWarehouse_ItemCommand">
                    <Columns>

                                        <asp:ButtonColumn DataTextField="WarehouseId" HeaderText="Warehouse ID" CommandName="wid">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="WarehouseId" HeaderText="Warehouse ID" Visible=false>
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="WarehouseName" HeaderText="Warehouse Name">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="WarehouseAddress" HeaderText="Address">
                                            <HeaderStyle Wrap="False" />
                                            <ItemStyle Wrap="False" />
                                        </asp:BoundColumn>
                                       
                                    </Columns>
                                     <PagerStyle CssClass="table_page" PrevPageText="previous ‹.." NextPageText="..› next" BackColor="#F7F7DE" ForeColor="Gray" HorizontalAlign="Right" Font-Bold="False" Font-Italic="False" Font-Names="verdana" Font-Overline="False" Font-Size="8pt" Font-Strikeout="False" Font-Underline="False" />
                    </asp:DataGrid>
                </tr>
                </table>
            </asp:Panel>
           
          
        </div>

      <div id="divTanggal" runat="server" class="form-group">
            <label>
            Tanggal</label>
            <table>
                <tr>
                    <td>
                    <asp:TextBox ID="StartDate" runat="server" 
                                            Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="Start"></asp:TextBox>
                                        
                                        <script type="text/javascript">
                                            var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=StartDate.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                        </script>
                    </td>
                   <td>
                        &nbsp;&nbsp; To  &nbsp;&nbsp;</td>
                    <td>
                    <asp:TextBox ID="EndDate" runat="server" 
                                            Width="150px" CssClass="form-control col-md-3 col-xs-12" PlaceHolder="End"></asp:TextBox>
                                        <script type="text/javascript">
                                            var picker1 = new Pikaday(
                                                 {
                                                     field: document.getElementById('<%=EndDate.ClientID%>'),
                                                     firstday: 1,
                                                     minDate: new Date('2000-01-01'),
                                                     maxDate: new Date('2020-12-31'),
                                                     yearRange: [2000, 2020]
                                                     //                                    setDefaultDate: true,
                                                     //                                    defaultDate : new Date()
                                                 }
                                                                          );
                                        </script>
                    </td>
                </tr>
            </table>
        </div>
        
        <div class="box-footer">
            <asp:Button ID="btnShow" runat="server" Text="Show" class="btn btn-primary" onclick="btnShow_Click" ValidationGroup="valShowSettingKoordinatWarehouse">
            </asp:Button>
      
                    </div>
                    </div>


          
                    <div>
                        
                        </div>
                            </div>
                        
                    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">

<link rel="stylesheet" href="../Scripts/JQuery/themes/base/jquery.ui.all.css" />
    <script type="text/javascript" src="../Scripts/JQuery/jquery.blockUI.js?v2.38"></script>
    <script type="text/javascript" src="../Scripts/FormatNumber.js"></script>
    <!-- Jquery Basic -->
    <script src="../Scripts/JQuery/jquery-ui-1.9.2.custom/js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="../Scripts/JQuery/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
    <!-- Module Javascript -->
    <script type="text/javascript" src="../Scripts/FormatNumber.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>

</asp:Content>
