﻿<%@ Page Title="Report OSA Customer" Language="C#" MasterPageFile="~/pages/Site.Master"
    AutoEventWireup="true" CodeBehind="ReportOSA_Customer.aspx.cs" Inherits="TransportService.pages.ReportOSA_Customer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- morris.js -->
    <script type="text/javascript" src="js/journey/jquery-1.11.3.js"></script>
    <script type="text/javascript" src="../vendors/raphael/raphael.min.js"></script>
    <script type="text/javascript"src="../vendors/morris.js/morris.min.js"></script>
    <!--bar fix-->
    <script>
        var arrChart = [];
    </script>
    <!--bar fix-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="title_left">
            <h3>
                OSA Customer
            </h3>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <%--<<box parameter--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
       
                    <input action="action" type="button" class="btn btn-sm btn-embossed btn-primary" value="Back"  onclick="window.history.go(-1);" />
            
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <div class="form-group">
                <asp:ScriptManager ID="ScriptManager2" runat="server">
                </asp:ScriptManager>
                    <div runat="server" id="dvdirect">
                        <div class="col-md-6 col-xs-6">
                            <asp:Label ID="lblWeekFrom" runat="server" Text="Week From" Width="100px" Font-Bold="True"
                                CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                            <asp:Label ID="lblttdua1" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                                Style="margin-top: 5px"></asp:Label>
                            <asp:DropDownList ID="ddlWeekFrom" runat="server" class="form-control" Style="margin-left: 0px;
                                width: 200px">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <asp:Label ID="lblWeekTo" runat="server" Text="Week To" Width="100px" Font-Bold="True"
                                CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                            <asp:Label ID="lblttdua2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                                Style="margin-top: 5px"></asp:Label>
                            <asp:DropDownList ID="ddlWeekTo" runat="server" class="form-control" Style="margin-left: 0px;
                                width: 200px">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <br />
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <asp:Label ID="lblTahun" runat="server" Text="Tahun" Width="100px" Font-Bold="True"
                                CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                            <asp:Label ID="lblttdua3" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                                Style="margin-top: 5px"></asp:Label>
                            <asp:DropDownList ID="ddlTahun" runat="server" class="form-control" Style="margin-left: 0px;
                                width: 200px">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <%--<asp:Label ID="Label1" runat="server" Text="Area" Width="100px" Font-Bold="True"
                                CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                            <asp:Label ID="Label2" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                                Style="margin-top: 5px"></asp:Label>
                            <asp:DropDownList ID="ddlArea" runat="server" class="form-control" Style="margin-left: 0px;
                                width: 200px"  >
                                AutoPostBack="True" onselectedindexchanged="ddlArea_SelectedIndexChanged"
                            </asp:DropDownList>--%>

                            <asp:Label ID="Label3" runat="server" Text="Account" Width="100px" Font-Bold="True"
                                CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                            <asp:Label ID="Label5" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                                Style="margin-top: 5px"></asp:Label>
                            <asp:DropDownList ID="ddlAccount" runat="server" class="form-control" Style="margin-left: 0px;
                                width: 200px">
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <br />
                        </div>
                        <div class="col-md-6 col-xs-6">
                            
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <asp:Label ID="lblCustomerID" runat="server" Text="Customer ID" Width="100px" Font-Bold="True"
                            CssClass="control-label col-md-3 col-sm-3 col-xs-12" Style="margin-top: 5px"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text=":" Width="10px" Font-Bold="True" CssClass="control-label col-md-3 col-sm-3 col-xs-12"
                            Style="margin-top: 5px"></asp:Label>
                        <asp:TextBox ID="txtCustomerID" runat="server" Width="200px" CssClass="form-control col-md-3 col-xs-12"
                            PlaceHolder="Search Customer"></asp:TextBox>
                        <asp:Button ID="btnShowCustomer" runat="server" CssClass="btn btn-success" Text="Show Customer"
                            Style="margin-left: 10px" OnClick="btnShowCustomer_Click" />
                        <asp:Label ID="lblValblCustomer" runat="server" Style="color: Red;" Visible="false"></asp:Label>
                        <asp:Panel ID="PnlCustomer" CssClass="form-control" Height="200px" Width="400px" runat="server"
                            ScrollBars="Vertical" Style="margin-left: 10px">
                            <asp:CheckBoxList ID="blCustomer" runat="server" Width="500px">
                            </asp:CheckBoxList>
                        </asp:Panel>
                        <br />
                        <asp:Button ID="btnSelectAll" runat="server" CssClass="btn btn-success" Text="SELECT ALL"
                            OnClick="btnSelectAll_Click" Style="margin-left: 10px" />
                        <asp:Button ID="btnUnSelectAll" runat="server" CssClass="btn btn-success " Text="UNSELECT ALL"
                            OnClick="btnUnSelectAll_Click" Style="margin-left: 10px" />
                    </div>
                    <div class="col-md-12 col-xs-12">
                            <br />
                        </div>
                        <div class="col-md-12 col-xs-12">
                        <asp:RadioButtonList ID="rblRole" RepeatDirection="Horizontal"  runat="server">
                            <asp:ListItem Text="All &nbsp &nbsp" Value="" />
                            <asp:ListItem Text="MD &nbsp &nbsp" Value="R0001" />
                            <asp:ListItem Text="BP" Value="R0006" />
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report" class="btn btn-sm btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnShow_Click" ValidationGroup="valShowReport" />
                        <asp:Button ID="btnExportExcel" runat="server" Text="Export To Excel" class="btn btn-embossed btn-primary"
                            Font-Size="14px" OnClick="btnExportExcel_Click" 
                                ValidationGroup="valShowReport" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--box parameter>>--%>
    <%--<<tab chart and table--%>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>
                    <i class="fa fa-bars"></i>Charts & Tables<small>Area</small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix">
                </div>
            </div>
            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab1" class="nav nav-tabs bar_tabs right" role="tablist">
                        <li role="presentation" class=""><a href="#tab_content11" id="home-tabb" role="tab"
                            data-toggle="tab" aria-controls="home" aria-expanded="true">Charts</a> </li>
                        <li role="presentation" class="active"><a href="#tab_content22" role="tab" id="profile-tabb"
                            data-toggle="tab" aria-controls="profile" aria-expanded="false">Tables</a> </li>
                    </ul>
                    <div id="myTabContent2" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade" id="Div1" aria-labelledby="profile-tab">
                            <asp:Repeater ID="RptChartlist" runat="server">
                                <ItemTemplate>
                                    <!-- bar chart -->
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>
                                                    <small>Branch : </small>
                                                    <%# Eval("element")%></h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                                                </ul>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <div class="x_content">
                                                <div id='<%# Eval("element") %>_chart' style="width: 100%; height: 280px;">
                                                </div>
                                                <script type="text/javascript">
                                                arrChart.push(new Morris.Bar({
                                                        element: '<%# Eval("element") %>_chart',
                                                        data: [<%# Eval("data") %>],
                                                        xkey: 'weeks',
                                                        ykeys: ['percents'],
                                                        labels: ['Percent'],
                                                        barRatio: 0.4,
                                                        barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
                                                        xLabelAngle: 35,
                                                        hideHover: 'auto',
                                                        resize: true,
                                                     redraw:true 
                                                     }));
//                                                     $(document).ready(function () {
//                                                     Morris.Bar({
//                                                     
//                                                     element: '<%# Eval("element") %>_chart' ,
//                                                     data: [<%# Eval("data") %>],
//                                                     xkey: 'weeks',
//                                                     ykeys: ['percents'],
//                                                     labels: ['Percent'],
//                                                     barRatio: 0.4,
//                                                     barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
//                                                     xLabelAngle: 35,
//                                                     hideHover: 'auto',
//                                                     resize: true
//                                                       });
//                                                    $MENU_TOGGLE.on('click', function () {
//                                                        $(window).resize();
//                                                    });
//                                                });
                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /bar charts -->
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div role="tabpanel"  class="tab-pane fade active in"  id="tab_content22" aria-labelledby="home-tab" >
                            <div class="datagrid" style="width: 1000px">
                        <table style="width: 100%; margin-bottom: 0px;">
                            <thead>
                                <tr>
                                    <th>
                                        Customer
                                    </th>
                                    <asp:Repeater ID="RptWeeks" runat="server">
                                        <ItemTemplate>
                                            <th>
                                                <asp:Label ID="lWeek" runat="server" Text='<%# Eval("Week") %>'></asp:Label>
                                            </th>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <asp:Repeater ID="RptOSACustomerlist" runat="server" OnItemDataBound="ItemBound" 
                                        onitemcommand="RptOSACustomerlist_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <a href='<%# Eval("Link") %>'> <%# Eval("CustomerID") %> </a> - 
                                                    <asp:Label ID="CustomerName" runat="server" Text='<%# Eval("CustomerName") %>'></asp:Label>
                                                </td>
                                                <%--Repeater for child data percentage--%>
                                                <asp:Repeater ID="RptChildPercentlist" runat="server">
                                                    <ItemTemplate>
                                                        <td>
                                                            <asp:Label ID="Percent" runat="server" Text='<%# Eval("Percent") %>'></asp:Label>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                <td>
                                <b>Grand Total</b>
                                </td>
                                    <asp:Repeater ID="RptChildGrandTotal" runat="server">
                                        <ItemTemplate>
                                            <td>
                                                <b><asp:Label ID="GrandTotal" runat="server" Text='<%# Eval("Percent") %>'></asp:Label></b>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                            
                        </table>
                    </div>
                        </div>
                       <asp:Panel ID="PanelReport" runat="server" Height="500px" Width="100%" 
                            CssClass="datagrid" Visible="False" > 
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"
                            Width="100%" Height="500px" InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana"
                            WaitMessageFont-Size="14pt">
                            <LocalReport ReportPath="Reports\ReportOSACustomer.rdlc">
                            </LocalReport>
                        </rsweb:ReportViewer>
                    </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--bar fix-->
    <script type="text/javascript">
        $(document).ready(function () {

            $('.nav-tabs a').on('shown.bs.tab', function (event) {
                for (var i = 0; i < arrChart.length; i++) {
                    arrChart[i].redraw();
                }
            });
        });
    </script>
<!--bar fix-->
    <%--tab chart and table>>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
