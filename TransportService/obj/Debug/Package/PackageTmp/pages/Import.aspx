﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true" CodeBehind="Import.aspx.cs" Inherits="TransportService.pages.Import" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div class="page-title">
        <div class="title_left">

            <h3>Import</h3>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Import </h2>
                    
                    <div class="clearfix"></div>
                  </div>
                   <div class="x_content">
                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Import Type</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          

                             <asp:DropDownList ID="ddlImport" CssClass="form-control" runat="server">
                                <asp:ListItem Text="ITENARY (AutoGenerate ID)" Value="CALLPLAN" />
                                <asp:ListItem Text="CUSTOMER" Value="CUSTOMER" />
                                <asp:ListItem Text="EMPLOYEE" Value="EMPLOYEE" />
                                <asp:ListItem Text="PRODUCT" Value="PRODUCT" />
                                <asp:ListItem Text="COMPETITOR" Value="COMPETITOR" />
                                 <asp:ListItem Text="COMPETITOR ACTIVITY" Value="COMPETITORACTIVITY" />
                                 <asp:ListItem Text="COMPETITOR PRODUCT" Value="COMPETITORPRODUCT" />
                                 <asp:ListItem Text="POSM PRODUCT" Value="POSMPRODUCT" />
                                 <asp:ListItem Text="PROMO" Value="PROMO" />
                             </asp:DropDownList>
                        </div>


                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          

                                <asp:FileUpload ID="fileUpload" runat="server" />
                        </div>

                        <div class="in_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" style="margin-top:10px;">
                           
                            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn btn-success" OnClick="btnUpload_Click" />
                                
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3" style="margin-top:10px;">
                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                            </div>
                      </div>
                  </div>


                   </div>   
                   </div>
                
            </div>
            </div>
</div>
  

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
</asp:Content>
