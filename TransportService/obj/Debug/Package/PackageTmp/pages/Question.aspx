﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/Site.Master" AutoEventWireup="true"
    CodeBehind="Question.aspx.cs" Inherits="TransportService.pages.Questions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"
        rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"
        rel="stylesheet">
     <style>th, td {
    white-space: nowrap;
}</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-title">
        <div class="col-md-6 col-sm-6 col-xs-12 ">
        <div class="title_left">
            <h3>
                Question</h3>
        </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 ">
        <ol class="breadcrumb pull-right">
        			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i> Home</a></li>
        			<li><a href="Question_Set.aspx"> Question Set</a></li>
        			<li class="active"> Question</li>
        			<li class="active"> Answer</li>
				</ol>
                </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="alert alert-warning fade in" style="padding-bottom: 0px; padding-right: 5px;
            padding-top: 10px; padding-left: 15px; margin-bottom: 10px;">
            <h4>
                <i class="fa fa-caret-square-o-right"></i>&nbsp&nbsp Question Set :  
                <br /><asp:Label ID="lblQuestionSet" runat="server" style="padding-left:30px" Text="" Font-Bold="True"></asp:Label>
            </h4>
        </div>
    </div>
    <div class="col-md-9 col-sm-6 col-xs-12">
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <asp:DropDownList ID="ddlIsactive" runat="server" CssClass="form-control" 
                            Width="111px" AutoPostBack="True" ontextchanged="ddlIsactive_TextChanged">
                         </asp:DropDownList>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <asp:Button ID="btnNew" runat="server" Text="Add New" class="btn btn-primary pull-right"
                            OnClick="btnNew_Click"></asp:Button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-sm-12 col-xs-12 ">
                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Horizontal" Width="100%">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>
                                    </th>
                                    <th>
                                        Sequence
                                    </th>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Question ID
                                    </th>
                                    <th>
                                        Question Text
                                    </th>
                                    <th>
                                        Question Category
                                    </th>
                                    <th>
                                        Answer Type
                                    </th>
                                    <th>
                                        Mandatory
                                    </th>
              <%--                      <th>
                                        Is Active
                                    </th>--%>
                                
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptQuestionlist" runat="server" OnItemCommand="RptQuestionlist_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Button ID="btnAnswer"  runat="server" Text="Answer" class="btn btn-success btn-xs"
                                                    CommandName="Answer"></asp:Button>
                                                    
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-warning btn-xs"
                                                    CommandName="Update"></asp:Button>
                                              
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" class="btn btn-danger btn-xs"
                                                    CommandName="Delete"></asp:Button>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" Text='<%# Eval("Sequence")%>' ID="dtSequence"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" Text='<%# Eval("No")%>' ID="dtNo"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" Text='<%# Eval("QuestionID")%>' ID="dtQuestionID"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label  runat="server" Text='<%# Eval("QuestionText")%>' ID="dtQuestionText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label Visible="false" runat="server" Text='<%# Eval("QuestionCategoryID")%>' ID="dtQuestionCategoryID"></asp:Label>
                                                <asp:Label  runat="server" Text='<%# Eval("QuestionCategoryText")%>' ID="dtQuestionCategoryText"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label Visible="false" runat="server" Text='<%# Eval("AnswerTypeID")%>' ID="dtAnswerTypeID"></asp:Label>
                                                <asp:Label  runat="server" Text='<%# Eval("AnswerTypeText")%>' ID="dtAnswerTypeText"></asp:Label>
                                                <asp:Label Visible="false" runat="server" Text='<%# Eval("IsMultiple")%>' ID="dtIsMultiple"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" Text='<%# Eval("Mandatory")%>' ID="dtMandatory"></asp:Label>
                                            </td>
                                           <%-- <td>
                                                <asp:Label runat="server" Text='<%# Eval("IsActive")%>' ID="dtIsActive"></asp:Label>
                                            </td>--%>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="PanelFormQuestion" runat="server" Width="100%">
        <div class="col-md-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Form Question Management
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblNo" runat="server" Text="No" Font-Bold="True"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtNo" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtNo" PlaceHolder="No" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="lblSeq" runat="server" Text="Sequence" Font-Bold="True" ></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtSeq" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtSeq" PlaceHolder="Sequence" runat="server" class="form-control" type="number"></asp:TextBox>
                                <br />  
                      <%--          <input  id="txtSeq" name="txtSeq" runat="server"  type="number" class="form-control"  placeholder="Sequence" />--%>
                           
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblQuestionID" runat="server" Text="Question ID" Font-Bold="True" ></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtQuestionID" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <%--<asp:TextBox ID="txtQuestionID" PlaceHolder="Question ID" runat="server" class="form-control"></asp:TextBox>--%>
                                <input  id="txtQuestionID" name="txtQuestionID" runat="server"  type="text" class="form-control" readonly="readonly" placeholder="Question ID" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br />
                                <asp:Label ID="Label1" runat="server" Text="Question Text" Font-Bold="True"></asp:Label>
                                <asp:TextBox ID="txtQuestionText" PlaceHolder="Question Text" runat="server" class="form-control"
                                    TextMode="MultiLine"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtQuestionText" ValidationGroup="valQuestionSet" ForeColor="#FF3300"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="Label2" runat="server" Text="Answer Type ID" Font-Bold="True"></asp:Label>
                                <asp:DropDownList ID="ddlAnswerType" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <asp:Label ID="Label3" runat="server" Text="Question Category" Font-Bold="True"></asp:Label>
                                <asp:DropDownList ID="ddlQuestionCategory" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br />
                                <input ID="cbMandatory" type="checkbox" runat="server" class="js-switch" /> Mandatory
                               
                                <%--<asp:CheckBox ID="cbMandatory" Text="Mandatory"  runat="server" />--%>
                            </div>
                        </div>
                        <div class="row">
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-warning" OnClick="btnUpdate_Click"
                                    ValidationGroup="valQuestionSet" />
                                <asp:Button ID="btnInsert" runat="server" Text="Insert" class="btn btn-primary" OnClick="btnInsert_Click"
                                    ValidationGroup="valQuestionSet" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentJS" runat="server">
    <!-- Datatables -->
    <script type="text/javascript" src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script type="text/javascript" src="../vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script type="text/javascript" src="../vendors/jszip/dist/jszip.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- Datatables -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').DataTable({
               
               });
        });
    </script>
    <!-- /Datatables -->
</asp:Content>
