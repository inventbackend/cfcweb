﻿/* documentation
 * 001 17 Okt 2016 fernandes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.Transactions;


namespace Core.Manager
{
    public class ResultSurveyFacade : Base.Manager
    {
        public static List<Model.mdlResultSurvey> LoadResultSurvey(List<string> lEmployeeList, String lBranchID, String lStartDate, String lEndDate, String lQuestionCategory)
        {
            var mdlResultSurveyList = new List<Model.mdlResultSurvey>();

            String lParam = "";
            foreach (var lEmployee in lEmployeeList)
            {
                if (lParam == "")
                {
                    lParam = lEmployee;
                }
                else
                {
                    lParam += "," + lEmployee;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@StartDate", SqlDbType = SqlDbType.Date, Value= lStartDate },
                new SqlParameter() {ParameterName = "@FinishDate", SqlDbType = SqlDbType.Date, Value= lEndDate },
                new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@QuestionCategory", SqlDbType = SqlDbType.NVarChar, Value = lQuestionCategory },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID }
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT a.surveyid,a.employeeid,c.employeename, a.branchid, a.visitid,d.questiontext,e.answertext,b.[Value] ,a.[Date],f.QuestionSetText,
                                                                    a.StartTime, a.EndTime, a.CompletedPercentage
			                                                        FROM resultsurvey a
	                                                                INNER JOIN resultsurveydetail b ON a.surveyid=b.surveyid
	                                                                INNER JOIN Employee c ON a.employeeid=c.employeeid
	                                                                INNER JOIN question d ON b.questionid=d.questionid
	                                                                LEFT JOIN answer e  ON b.answerid=e.answerid
                                                                    RIGHT JOIN Question_Set f ON d.QuestionSetID=f.QuestionSetID
	                                                                WHERE (a.[Date] BETWEEN @StartDate AND DATEADD(DAY, 1,@FinishDate)) AND 
				                                                        (a.QuestionCategoryID = @QuestionCategory) AND 
				                                                        (a.BranchID = @BranchID) AND
				                                                            a.EmployeeID IN (SELECT Value FROM fn_Split(@EmployeeID,','))", sp);

            foreach (DataRow row in dtDeliveryOrder.Rows)
            {
                var mdlResultSurvey = new Model.mdlResultSurvey();
                mdlResultSurvey.AnswerText = row["AnswerText"].ToString();
                mdlResultSurvey.BranchId = row["BranchId"].ToString();
                mdlResultSurvey.Date = DateTime.Parse(row["Date"].ToString());
                mdlResultSurvey.EmployeeId = row["EmployeeId"].ToString();
                mdlResultSurvey.EmployeeName = row["EmployeeName"].ToString();
                mdlResultSurvey.QuestionText = row["QuestionText"].ToString();
                mdlResultSurvey.SurveyId = row["SurveyId"].ToString();
                mdlResultSurvey.Value = row["Value"].ToString();
                mdlResultSurvey.VisitId = row["VisitId"].ToString();
                mdlResultSurvey.QuestionSet = row["QuestionSetText"].ToString();
                mdlResultSurvey.StartTime = row["StartTime"].ToString();
                mdlResultSurvey.EndTime = row["EndTime"].ToString();
                mdlResultSurvey.CompletedPercentage = row["CompletedPercentage"].ToString();

                mdlResultSurveyList.Add(mdlResultSurvey);
            }

            return mdlResultSurveyList;
        }

        public static List<Model.mdlResultSurvey> LoadResultSurveyPhoto(List<string> lEmployeeList, String lBranchID, String lStartDate, String lEndDate, String lQuestionCategory)
        {
            var mdlResultSurveyList = new List<Model.mdlResultSurvey>();

            String lParam = "";
            foreach (var lEmployee in lEmployeeList)
            {
                if (lParam == "")
                {
                    lParam = lEmployee;
                }
                else
                {
                    lParam += "," + lEmployee;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@StartDate", SqlDbType = SqlDbType.Date, Value= lStartDate },
                new SqlParameter() {ParameterName = "@FinishDate", SqlDbType = SqlDbType.Date, Value= lEndDate },
                new SqlParameter() {ParameterName = "@EmployeeID", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                //new SqlParameter() {ParameterName = "@QuestionCategory", SqlDbType = SqlDbType.NVarChar, Value = lQuestionCategory },
                //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID }
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT a.DocNumber,b.QuestionSetText,a.CustomerID,c.CustomerName,a.ImageDate, a.ImageBase64,
                                                                d.BranchID,d.EmployeeID,d.[Date],e.EmployeeName 
                                                                FROM CustomerImage a
                                                                INNER JOIN Question_Set b ON b.QuestionSetID = a.DocNumber
                                                                LEFT JOIN Customer c ON c.CustomerID = a.CustomerID
                                                                INNER JOIN CallPlan d ON d.CallPlanID = a.VisitID AND d.EmployeeID IN (SELECT Value FROM fn_Split(@EmployeeID,','))
                                                                LEFT JOIN Employee e ON e.EmployeeID = d.EmployeeID
                                                                where (a.ImageDate between @StartDate and DATEADD(DAY, 1,@FinishDate)) and a.ImageType='checklist'
                                                                order by ImageDate", sp);

            foreach (DataRow row in dtDeliveryOrder.Rows)
            {
                var mdlResultSurvey = new Model.mdlResultSurvey();
                mdlResultSurvey.BranchId = row["BranchID"].ToString();
                mdlResultSurvey.Date = DateTime.Parse(row["Date"].ToString());
                mdlResultSurvey.ImageDate = DateTime.Parse(row["ImageDate"].ToString());
                mdlResultSurvey.EmployeeId = row["EmployeeID"].ToString();
                mdlResultSurvey.EmployeeName = row["EmployeeName"].ToString();
                mdlResultSurvey.QuestionSet = row["QuestionSetText"].ToString();
                mdlResultSurvey.Image = row["ImageBase64"].ToString();
                mdlResultSurvey.CustomerID = row["CustomerID"].ToString();
                mdlResultSurvey.CustomerName = row["CustomerName"].ToString();

                mdlResultSurveyList.Add(mdlResultSurvey);
            }

            return mdlResultSurveyList;
        }

        public static List<Model.mdlSurveyCustomer> LoadSurveyCustomer(DateTime lStartDate, DateTime lEndDate)
        {
            var mdlSurveyCustomerList = new List<Model.mdlSurveyCustomer>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@StartDate", SqlDbType = SqlDbType.Date, Value= lStartDate },
                new SqlParameter() {ParameterName = "@FinishDate", SqlDbType = SqlDbType.Date, Value= lEndDate },
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT a.SurveyID,b.EmployeeName,c.CustomerName,a.BranchID,a.[Date],a.EmployeeID
															FROM ResultSurvey a LEFT JOIN Employee b ON a.EmployeeID = b.EmployeeID
															LEFT JOIN Customer c ON a.CustomerID = c.CustomerID
															WHERE a.CustomerID = '' AND a.QuestionCategoryID = 'QCD-0004' AND a.[Date] BETWEEN @StartDate AND DATEADD(DAY,1,@FinishDate);", sp);

            foreach (DataRow row in dtDeliveryOrder.Rows)
            {
                var mdlResultSurvey = new Model.mdlSurveyCustomer();
                mdlResultSurvey.SurveyId = row["SurveyId"].ToString();
                mdlResultSurvey.BranchId = row["BranchId"].ToString();
                mdlResultSurvey.Date = DateTime.Parse(row["Date"].ToString());
                mdlResultSurvey.EmployeeName = row["EmployeeName"].ToString();
                mdlResultSurvey.CustomerName = row["CustomerName"].ToString();
                mdlResultSurvey.EmployeeID = row["EmployeeID"].ToString();
                mdlSurveyCustomerList.Add(mdlResultSurvey);
            }

            return mdlSurveyCustomerList;
        }

        public static List<Model.mdlSurveyInfo> LoadSurveyInfo(String lSurveyID)
        {
            var mdlSurveyInfoList = new List<Model.mdlSurveyInfo>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@SurveyID", SqlDbType = SqlDbType.Date, Value= lSurveyID }
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT a.SurveyID,a.QuestionID,b.QuestionText,a.[Value] 
			                                FROM ResultSurveyDetail a INNER JOIN Question b ON a.QuestionID = b.QuestionID WHERE a.SurveyID = @SurveyID ;", sp);

            foreach (DataRow row in dtDeliveryOrder.Rows)
            {
                var mdlSurveyInfo = new Model.mdlSurveyInfo();
                mdlSurveyInfo.SurveyID = row["SurveyId"].ToString();
                mdlSurveyInfo.QuestionID = row["QuestionID"].ToString();
                mdlSurveyInfo.QuestionText = row["QuestionText"].ToString();
                mdlSurveyInfo.Value = row["Value"].ToString();
                mdlSurveyInfoList.Add(mdlSurveyInfo);
            }

            return mdlSurveyInfoList;
        }

        public static Model.mdlCustomer MappingSurveyToCustomer(String lSurveyID)
        {
            var mdlCustomer = new Model.mdlCustomer();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@SurveyID", SqlDbType = SqlDbType.NVarChar, Value= lSurveyID }
            };

            DataTable dtDeliveryOrder = DataFacade.DTSQLCommand(@"SELECT a.SurveyID,a.QuestionID,b.QuestionText,a.[Value] 
			FROM ResultSurveyDetail a INNER JOIN Question b ON a.QuestionID = b.QuestionID WHERE a.SurveyID = @SurveyID ;", sp);

            mdlCustomer.CustomerID = CustomerFacade.GenerateCustomerId();
            foreach (DataRow row in dtDeliveryOrder.Rows)
            {
                String Question = row["QuestionID"].ToString();

                if (Question == "QNM-2018-03-0002")//NAMA TOKO
                {
                    mdlCustomer.CustomerName = row["Value"].ToString();
                }

                if (Question == "QNM-2018-03-0006")//ALAMAT
                {
                    mdlCustomer.CustomerAddress = row["Value"].ToString();
                }

                if (Question == "QNM-2018-03-0013")//AREA
                {
                    mdlCustomer.BranchID = row["Value"].ToString();
                }
            }

            return mdlCustomer;
        }

        public static string UpdateSurveyForCustomer(string lCustomerID, string lSurveyID)
        {
            List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@SurveyID", SqlDbType = SqlDbType.NVarChar, Value = lSurveyID },
                    new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomerID }
                };

            string query = @"UPDATE ResultSurvey SET CustomerID = @CustomerID WHERE SurveyID = @SurveyID";
            string lresult = Manager.DataFacade.DTSQLVoidCommand(query, sp);

            return lresult;
        }

    }
}
