﻿/* documentation
 * 001 nanda 13 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Core.Model;

namespace Core.Manager
{
    public class AnswerTypeFacade : Base.Manager
    {
        public static List<Model.mdlAnswer_Type> LoadAnswerTypeDDL(string user)
        {
            string key = "LoadAnswerTypeDDL";
            var mdlAnswer_TypeList = new List<Model.mdlAnswer_Type>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadAnswerType",user, key, sp);

            foreach (DataRow dr in dt.Rows)
            {
                var mdlAnswer_Type = new Model.mdlAnswer_Type();
                mdlAnswer_Type.AnswerTypeID = dr["AnswerTypeID"].ToString();
                mdlAnswer_Type.AnswerTypeText = dr["AnswerTypeID"].ToString() + " - " + dr["AnswerTypeText"].ToString();

                mdlAnswer_TypeList.Add(mdlAnswer_Type);
            }
            return mdlAnswer_TypeList;
        }

        public static Boolean CheckAnswer(string lAnswerTypeID, string user)
        {
            string key = "CheckAnswer";
            Boolean lCheck = false;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lAnswerTypeID}
            };

            DataTable dt = Manager.DataFacade.GetSP("spCheckAnswerTypeIsMultiple", user, key, sp);
            foreach (DataRow dr in dt.Rows)
            {
                lCheck = Boolean.Parse(dr["IsMultiple"].ToString());
            }

            return lCheck;
        }

    }
}
