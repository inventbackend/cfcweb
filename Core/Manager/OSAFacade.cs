﻿/* documentation
 * 001 andy yo 08/11/2017 - dashboard modif
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Dynamic;
using System.Globalization;
using Core.Model;

namespace Core.Manager
{
    public class OSAFacade
    {
        public static List<Model.mdlRptOSA> LoadOSAReport()
        {
            var mdlOSAList = new List<Model.mdlRptOSA>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            string sql = @"SELECT a.Account, a.[Percent], a.Weeks, b.Customer, b.Product, b.SisaStock 
				               FROM OSA a 
			               INNER JOIN OSA_detail b ON a.Account = b.Account AND a.Weeks = b.Weeks;";

  //          string sql = @"SELECT Account, Weeks, [Percent] FROM OSA ORDER BY Weeks;";

            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drOSA in dtOSA.Rows)
            {
                var mdlOSA = new Model.mdlRptOSA();
                mdlOSA.Account = drOSA["Account"].ToString();
                mdlOSA.Weeks = drOSA["Weeks"].ToString();
                mdlOSA.Percent = drOSA["Percent"].ToString();
                mdlOSA.Product = drOSA["Product"].ToString();
                mdlOSA.Customer = drOSA["Customer"].ToString();
                mdlOSA.Stock = drOSA["SisaStock"].ToString();

                mdlOSAList.Add(mdlOSA);
            }
            return mdlOSAList;
        }

        public static List<Model.mdlWeek> GetWeeks(string lWeekFrom, string lWeekTo, string lTahun)
        {
            var mdlOSAWeekList = new List<Model.mdlWeek>();


            for (int i = int.Parse(lWeekFrom); i <= int.Parse(lWeekTo); i++)
            {
                var mdlOSAWeek = new Model.mdlWeek();
                Boolean lCheck = CheckWeek(i.ToString(), lTahun);
                if (lCheck == true)
                {
                    mdlOSAWeek.Week = i.ToString();
                    mdlOSAWeekList.Add(mdlOSAWeek);
                }

            }

            return mdlOSAWeekList;
        }

        public static Boolean CheckWeek(string lWeek, string lTahun)
        {
            Boolean lCheck = false;
            List<string> lParamAreas = new List<string>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.NVarChar, Value =  lWeek},
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value =  lTahun}
            };

            string sql = @"SELECT DISTINCT VisitWeek FROM Visit WHERE VisitWeek = @Week AND YEAR(VisitDate) = @Year;";

            DataTable dtWeek = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drWeek in dtWeek.Rows)
            {
                lCheck = true;
            }
            return lCheck;
        }

        public static DataTable LoadOSAAreaDT(List<String> lBranchList, string lWeekFrom, string lWeekTo, string lYear,string lRole,string user)
        {
            string key = "LoadOSAAreaDT";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lBranchID in lBranchList)
            {
                if (lParam == "")
                {
                    lParam = lBranchID;
                }
                else
                {
                    lParam += "," + lBranchID;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@BranchList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAArea",user, key, sp);

            return dtOSA;
        }

        public static List<Model.mdlOSAExcel> LoadOSAAreaExcel(List<String> lBranchList, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSAAreaExcel";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lBranchID in lBranchList)
            {
                if (lParam == "")
                {
                    lParam = lBranchID;
                }
                else
                {
                    lParam += "," + lBranchID;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@BranchList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAArea", user, key, sp);
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                var mdlOSA = new Model.mdlOSAExcel();
                mdlOSA.ID = drOSA["BranchID"].ToString();
                mdlOSA.Week = drOSA["VisitWeek"].ToString();
                mdlOSA.Status_zero = drOSA["Listed"].ToString();
                mdlOSA.Status_zeroNone = drOSA["Outofstock"].ToString();
                mdlOSA.Percent = drOSA["Percentage"].ToString();

                mdlOSAList.Add(mdlOSA);
            }

            return mdlOSAList;
        }
        
        public static List<Model.mdlOSAData> LoadGrandTotalAreaByWeek(List<String> lBranchList, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalAreaByWeek";
            string lParam = string.Empty;
            foreach (var lBranchID in lBranchList)
            {
                if (lParam == "")
                {
                    lParam = lBranchID;
                }
                else
                {
                    lParam += "," + lBranchID;
                }
            }

            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam2 in mdlWeeks)
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam2.Week) },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@BranchList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };
                DataTable dtOSA = DataFacade.GetSP("spOSAGrandTotalAreaByWeek", user, key, sp);

                foreach (DataRow drOSA in dtOSA.Rows)
                {

                    var mdlOSAArea = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSAArea.Percent = drOSA["Percentage"].ToString().ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSAArea.Percent = "";
                    }
                    
                    mdlOSAAreaList.Add(mdlOSAArea);
                } 
            }
            return mdlOSAAreaList;
        }       

        public static List<string> GetAreas()
        {
            List<string> lParamAreas = new List<string>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            string sql = @"SELECT DISTINCT BranchID FROM Customer   
                                     ORDER BY BranchID ASC;";

            DataTable dtArea = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drArea in dtArea.Rows)
            {
                lParamAreas.Add(drArea["BranchID"].ToString());
            }
            
            return lParamAreas;
        }

        public static List<string> GetAreas(string keyword)
        {
            List<string> lParamAreas = new List<string>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Keyword", SqlDbType = SqlDbType.NVarChar, Value =  "%"+ keyword +"%"}
            };

            string sql = @"SELECT DISTINCT BranchID FROM Customer WHERE a.BranchID LIKE @Keyword   
                                     ORDER BY BranchID ASC ;";

            DataTable dtArea = Manager.DataFacade.DTSQLCommand(sql, sp);
            foreach (DataRow drArea in dtArea.Rows)
            {
                lParamAreas.Add(drArea["BranchID"].ToString());
            }
            return lParamAreas;
        }

        public static DataTable LoadOSAAccountDT(List<String> lAccountList, string lBranch, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSAAccountDT";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lAccountID in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccountID;
                }
                else
                {
                    lParam += "," + lAccountID;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranch },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAAccount", user, key, sp);
            return dtOSA;
        }

        public static DataTable LoadOSAAccountDT2(List<String> lAccountList, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSAAccountDT2";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lAccountID in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccountID;
                }
                else
                {
                    lParam += "," + lAccountID;
                }
            }


            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole}
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAAccount2", user, key, sp);
            return dtOSA;
        }

        public static List<Model.mdlOSAExcel> LoadOSAAccountExcel(string lFlag,List<String> lAccountList, string lBranch, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSAAccountExcel";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lAccountID in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccountID;
                }
                else
                {
                    lParam += "," + lAccountID;
                }
            }

            DataTable dtOSA = new DataTable();
            if(lFlag == "0"){
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                    new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranch },
                    new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                dtOSA = Manager.DataFacade.GetSP("spOSAAccount", user, key, sp);
            }
            else{
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                    new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                     new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                dtOSA = Manager.DataFacade.GetSP("spOSAAccount2", user, key, sp);
            }

            
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                var mdlOSA = new Model.mdlOSAExcel();
                mdlOSA.ID = drOSA["Account"].ToString();
                mdlOSA.Week = drOSA["VisitWeek"].ToString();
                mdlOSA.Status_zero = drOSA["Status_zero"].ToString();
                mdlOSA.Status_zeroNone = drOSA["Status_zeroNone"].ToString();
                mdlOSA.Percent = drOSA["Percentage"].ToString();

                mdlOSAList.Add(mdlOSA);
            }

            return mdlOSAList;
        }

        public static List<Model.mdlOSAData> LoadGrandTotalAccountByWeek(List<String> lAccountList, string lBranchID, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalAccountByWeek";
            string lParam = string.Empty;
            foreach (var lAccountID in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccountID;
                }
                else
                {
                    lParam += "," + lAccountID;
                }
            }

            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam2 in mdlWeeks)
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam2.Week) },
                    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGrandTotalAccountByWeek", user, key, sp);
                foreach (DataRow drOSA in dtOSA.Rows)
                {
                    var mdlOSA = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSA.Percent = "";
                    }
                    mdlOSAAreaList.Add(mdlOSA);
                }
            }

            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSAData> LoadGrandTotalAccountByWeek2(List<String> lAccountList,  string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalAccountByWeek2";
            string lParam = string.Empty;
            foreach (var lAccountID in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccountID;
                }
                else
                {
                    lParam += "," + lAccountID;
                }
            }
            //Declare AccountList>>

            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam2 in mdlWeeks)
            {

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam2.Week) },
                    //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGrandTotalAccountByWeek2", user, key, sp);
                foreach (DataRow drOSA in dtOSA.Rows)
                {
                    var mdlOSA = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSA.Percent = "";
                    }
                    mdlOSAAreaList.Add(mdlOSA);
                }
            }

            return mdlOSAAreaList;
        }

        public static List<string> GetAccounts()
        {
            List<string> lParamAccount = new List<string>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            string sql = @"SELECT a.Account FROM Customer a
								RIGHT JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID 
                                WHERE DATALENGTH(a.Account) > 0  
								GROUP BY a.Account ORDER BY a.Account ASC;";

            DataTable dtAccount = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drAccount in dtAccount.Rows)
            {

                lParamAccount.Add(drAccount["Account"].ToString());
            }
            return lParamAccount;
        }

        public static List<string> GetAccounts(string lArea)
        {
            List<string> lParamAccount = new List<string>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lArea }
            };

            string sql = @"SELECT a.Account FROM Customer a
								INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID 
                                WHERE a.BranchID = @Area 
								GROUP BY a.Account";

            //            string sql = @"SELECT a.Account FROM Customer  as
            //								INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID WHERE b.BranchID = @Area
            //								GROUP BY a.Account;";

            DataTable dtAccount = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drAccount in dtAccount.Rows)
            {
                lParamAccount.Add(drAccount["Account"].ToString());
            }
            return lParamAccount;
        }

        public static List<string> GetAccounts(string keyword, string lArea)
        {
            List<string> lParamAreas = new List<string>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Keyword", SqlDbType = SqlDbType.NVarChar, Value =  "%"+ keyword +"%"},
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lArea }
            };

            string sql = @"SELECT a.Account FROM Customer  a
								INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID WHERE b.BranchID = @Area AND a.Account LIKE @Keyword
								GROUP BY a.Account
                                ORDER BY Account ASC;";

            DataTable dtAccount = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drAccount in dtAccount.Rows)
            {
                lParamAreas.Add(drAccount["Account"].ToString());
            }
            return lParamAreas;
        }

        public static DataTable LoadOSACustomerDT(List<String> lCustomerList, string lAccount, string lBranch, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSACustomerDT";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lCustomerID in lCustomerList)
            {
                if (lParam == "")
                {
                    lParam = lCustomerID;
                }
                else
                {
                    lParam += "," + lCustomerID;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranch },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSACustomer", user, key, sp);
            return dtOSA;
        }

        public static DataTable LoadOSACustomerDT2(List<String> lCustomerList, string lAccount, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSACustomerDT2";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lCustomerID in lCustomerList)
            {
                if (lParam == "")
                {
                    lParam = lCustomerID;
                }
                else
                {
                    lParam += "," + lCustomerID;
                }
            }

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lParam }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSACustomer2", user, key, sp);
            return dtOSA;
        }

        public static List<Model.mdlOSAExcel> LoadOSACustomerExcel(string lFlag,List<String> lCustomerList, string lAccount, string lBranch, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "GetReportOOSByBranch";
            var mdlOSAList = new List<Model.mdlOSAExcel>();
            string lParam = string.Empty;
            foreach (var lCustomerID in lCustomerList)
            {
                if (lParam == "")
                {
                    lParam = lCustomerID;
                }
                else
                {
                    lParam += "," + lCustomerID;
                }
            }

            

            DataTable dtOSA =  new DataTable();
            if (lFlag == "0")
            {
                List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranch },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };
                dtOSA = Manager.DataFacade.GetSP("spOSACustomer", user, key, sp);
            }
            else
            {
                List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };
                dtOSA = Manager.DataFacade.GetSP("spOSACustomer2", user, key, sp);
            }
            
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                var mdlOSA = new Model.mdlOSAExcel();
                mdlOSA.ID = drOSA["CustomerID"].ToString();
                mdlOSA.Week = drOSA["VisitWeek"].ToString();
                mdlOSA.Status_zero = drOSA["Status_zero"].ToString();
                mdlOSA.Status_zeroNone = drOSA["Status_zeroNone"].ToString();
                mdlOSA.Percent = drOSA["Percentage"].ToString();

                mdlOSAList.Add(mdlOSA);
            }

            return mdlOSAList;
        }

        public static List<Model.mdlOSAData> LoadGrandTotalCustomerByWeek(List<String> lCustomerList, string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalCustomerByWeek";
            string lParam = string.Empty;
            foreach (var lCustomerID in lCustomerList)
            {
                if (lParam == "")
                {
                    lParam = lCustomerID;
                }
                else
                {
                    lParam += "," + lCustomerID;
                }
            }

            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam2 in mdlWeeks)
            {

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam2.Week) },
                    new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGrandTotalCustomerByWeek", user, key, sp);
                foreach (DataRow drOSA in dtOSA.Rows)
                {
                    var mdlOSA = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSA.Percent = "";
                    }

                    
                    mdlOSAAreaList.Add(mdlOSA);
                }
            }
            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSAData> LoadGrandTotalCustomerByWeek2(List<String> lCustomerList, string lAccount, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalCustomerByWeek2";
            string lParam = string.Empty;
            foreach (var lCustomerID in lCustomerList)
            {
                if (lParam == "")
                {
                    lParam = lCustomerID;
                }
                else
                {
                    lParam += "," + lCustomerID;
                }
            }

            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam2 in mdlWeeks)
            {

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam2.Week) },
                    new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                    //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@CustomerList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };


                DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGrandTotalCustomerByWeek2", user, key, sp);
                foreach (DataRow drOSA in dtOSA.Rows)
                {
                    var mdlOSA = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSA.Percent = "";
                    }


                    mdlOSAAreaList.Add(mdlOSA);
                }
            }
            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSACustomer> GetCustomer(string keyword, string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear,string lRole)
        {
            string sql = string.Empty;
            var mdlOSACustomerList = new List<Model.mdlOSACustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Keyword", SqlDbType = SqlDbType.NVarChar, Value =  "%"+keyword+"%"}
            };


            if (keyword == "")
            {
                sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName 
                                          FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                           WHERE b.BranchID = @BranchID AND a.Account = @Account ;";
            }
            else
            {
                sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName 
                                          FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                                        WHERE b.BranchID = @BranchID AND  a.Account = @Account AND (a.CustomerID LIKE @Keyword OR a.CustomerName LIKE @Keyword) ;";
            }

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(sql, sp);
            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlOSACustomer();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString();
                mdlCustomer.Link = String.Format("ReportOSA_Product.aspx?AreaId={0}&Account={1}&Customer={2}&WeekFrom={3}&WeekTo={4}&Tahun={5}&Role={6}", lBranchID, lAccount, drCustomer["CustomerID"].ToString(), lWeekFrom, lWeekTo, lYear,lRole);
                mdlOSACustomerList.Add(mdlCustomer);
            }

            return mdlOSACustomerList;
        }

        public static List<Model.mdlOSACustomer> GetCustomer2(string keyword, string lAccount, string lWeekFrom, string lWeekTo, string lYear,string lRole)
        {
            string sql = string.Empty;
            var mdlOSACustomerList = new List<Model.mdlOSACustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@Keyword", SqlDbType = SqlDbType.NVarChar, Value =  "%"+keyword+"%"}
            };

            if (keyword == "")
            {

                sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName , a.BranchID
                                          FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                                        WHERE  a.Account = @Account ;";
            }
            else
            {
                sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName , a.BranchID
                                          FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                                        WHERE  a.Account = @Account AND (a.CustomerID LIKE @Keyword OR a.CustomerName LIKE @Keyword) ;";
            }

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlOSACustomer();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString() + " - " + drCustomer["BranchID"].ToString();
                mdlCustomer.Link = String.Format("ReportOSA_Product.aspx?AreaId={0}&Account={1}&Customer={2}&WeekFrom={3}&WeekTo={4}&Tahun={5}&Role={6}", drCustomer["BranchID"].ToString(), lAccount, drCustomer["CustomerID"].ToString(), lWeekFrom, lWeekTo, lYear,lRole);
                mdlOSACustomerList.Add(mdlCustomer);
            }

            return mdlOSACustomerList;
        }

        public static string GetAreabyCustomerID(string lCustomerID, string lAccount, string lWeekFrom, string lWeekTo, string lYear,string lRole)
        {
            string sql,lLink = string.Empty;
            var mdlOSACustomerList = new List<Model.mdlOSACustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomerID }
            };

            sql = @"SELECT TOP 1 BranchID FROM SisaStockVisit WHERE  CustomerID = @CustomerID;";
      

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                lLink = String.Format("ReportOSA_Product.aspx?AreaId={0}&Account={1}&Customer={2}&WeekFrom={3}&WeekTo={4}&Tahun={5}&Role={6}", drCustomer["BranchID"].ToString(), lAccount, lCustomerID, lWeekFrom, lWeekTo, lYear,lRole);
            }

            return lLink;
        }

        public static List<Model.mdlOSAStock> LoadOSAStock(string lProduct, string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lCustomerID, string lYear,string lRole, string user)
        {
            string key = "LoadOSAStock";
            var mdlOSAStockList = new List<Model.mdlOSAStock>();
            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                
                new SqlParameter() {ParameterName = "@ProductID", SqlDbType = SqlDbType.NVarChar, Value = lProduct },
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomerID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAStock", user, key, sp);
            var mdlOSA = new Model.mdlOSAStock();
            int nx = 0;
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                for (int i = nx; i < mdlWeeks.Count; i++)
                {

                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
                    {
                        mdlOSA = new Model.mdlOSAStock();
                        mdlOSA.ProductID = drOSA["ProductID"].ToString();
                        string lStatus = drOSA["Status"].ToString();

                        if (lStatus == "2")
                        {
                            mdlOSA.Stock = string.Format("<b><font color='red'>{0} (NL)</font></b>", drOSA["TotalSisaStock"].ToString());
                        }
                        else
                        {
                            mdlOSA.Stock = drOSA["TotalSisaStock"].ToString();
                        }

                        mdlOSAStockList.Add(mdlOSA);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSA = new Model.mdlOSAStock();
                        mdlOSA.ProductID = drOSA["ProductID"].ToString();
                        mdlOSA.Stock = "";
                        mdlOSAStockList.Add(mdlOSA);
                    }
                }
            }

            return mdlOSAStockList;
        }

        public static List<Model.mdlOSAStock> GetProduct(string lAccount, string lBranchID, string lCustomerID,string lRole, string user)
        {
            string key = "GetProduct";
            var mdlOSAAreaList = new List<Model.mdlOSAStock>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomerID },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGetProduct", user, key, sp);
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                var mdlOSAStock = new Model.mdlOSAStock();
                mdlOSAStock.ProductID = drOSA["ProductID"].ToString();
                mdlOSAStock.ProductName = drOSA["ProductName"].ToString();

                mdlOSAAreaList.Add(mdlOSAStock);

            }

            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSAStockExcel> LoadOSAStockExcel(string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lCustomerID, string lYear,string lRole, string user)
        {
            string key = "LoadOSAStockExcel";
            var mdlOSAStockList = new List<Model.mdlOSAStockExcel>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomerID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSAStock2", user, key, sp);
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                var mdlOSA = new Model.mdlOSAStockExcel();
                mdlOSA.ProductID = drOSA["ProductID"].ToString();
                mdlOSA.ProductName = drOSA["ProductName"].ToString();
                string lStatus = drOSA["Status"].ToString();
                if (lStatus == "2")
                {
                    mdlOSA.TotalSisaStock = drOSA["TotalSisaStock"].ToString() + " (NL) ";
                }
                else
                {
                    mdlOSA.TotalSisaStock = drOSA["TotalSisaStock"].ToString();
                }


                mdlOSA.VisitWeek = drOSA["VisitWeek"].ToString();
                mdlOSAStockList.Add(mdlOSA);
            }


            return mdlOSAStockList;
        }

        //<<Andy Yo
        public static List<Model.mdlOSACustomer> GetCustomerByAccount(string lAccount, string lBranchID)
        {
            var mdlCustomerList = new List<Model.mdlOSACustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                //new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                //new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID }
                //new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
            };

            string sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName 
                                            FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                            WHERE a.BranchID = @BranchID AND a.Account = @Account;";

            //            string sql = @"SELECT a.CustomerID,b.CustomerName FROM SisaStockVisit a
            //                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
            //                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
            //                                    WHERE b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo) 
            //                                    GROUP BY b.Account, c.VisitWeek,a.CustomerID,b.CustomerName;";

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlOSACustomer();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString();
                mdlCustomerList.Add(mdlCustomer);
            }

            return mdlCustomerList;
        }

        public static List<Model.mdlOSACustomer> GetCustomerByAccount2(string lAccount)
        {
            var mdlCustomerList = new List<Model.mdlOSACustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                //new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                //new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID }
                //new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
            };

            string sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName
                                            FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                            WHERE a.Account = @Account;";

            //            string sql = @"SELECT a.CustomerID,b.CustomerName FROM SisaStockVisit a
            //                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
            //                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
            //                                    WHERE b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo) 
            //                                    GROUP BY b.Account, c.VisitWeek,a.CustomerID,b.CustomerName;";

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlOSACustomer();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString();
                mdlCustomerList.Add(mdlCustomer);
            }

            return mdlCustomerList;
        }

        public static List<Model.mdlOSAAccountCustomer> LoadOSACustomerByAccount(string lCustomer, string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSACustomerByAccount";
            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAAccountCustomer>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Customer", SqlDbType = SqlDbType.NVarChar, Value = lCustomer },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lYear }
            };

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSACustomerByAccount", user, key, sp);
            var mdlOSA = new Model.mdlOSAAccountCustomer();
            int nx = 0;
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                for (int i = nx; i < mdlWeeks.Count; i++)
                {

                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
                    {
                        mdlOSA = new Model.mdlOSAAccountCustomer();
                        mdlOSA.ID = drOSA["Account"].ToString();
                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
                        mdlOSA.Percent = drOSA["Percentage"].ToString().ToString().Substring(0, 5) + "%";
                        mdlOSA.ListingAktif = drOSA["ListingAktif"].ToString();
                        mdlOSA.Listing = drOSA["Listing"].ToString();
                        mdlOSA.Link = "reportOSA_Account_Customer_Product.aspx?Week=" + mdlOSA.Week + "&Account=" + mdlOSA.ID + "&Year=" + lYear + "&Area=" + lBranchID + "&Customer=" + lCustomer + "&Role=" + lRole;
                        mdlOSAAreaList.Add(mdlOSA);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSA = new Model.mdlOSAAccountCustomer();
                        mdlOSA.ID = drOSA["Account"].ToString();
                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
                        mdlOSA.Percent = "";
                        mdlOSA.ListingAktif = "";
                        mdlOSA.Listing = "";
                        mdlOSAAreaList.Add(mdlOSA);
                    }
                }

            }

            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSAAccountCustomer> LoadOSACustomerByAccount2(string lCustomer, string lAccount, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadOSACustomerByAccount2";
            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAAccountCustomer>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@CustomerID", SqlDbType = SqlDbType.NVarChar, Value = lCustomer },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            

            DataTable dtOSA = Manager.DataFacade.GetSP("spOSACustomerByAccount2", user, key, sp);
            var mdlOSA = new Model.mdlOSAAccountCustomer();
            int nx = 0;
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                for (int i = nx; i < mdlWeeks.Count; i++)
                {

                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
                    {
                        mdlOSA = new Model.mdlOSAAccountCustomer();
                        mdlOSA.ID = drOSA["Account"].ToString();
                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
                        mdlOSA.Percent = drOSA["Percentage"].ToString().ToString().Substring(0, 5) + "%";
                        mdlOSA.ListingAktif = drOSA["ListingAktif"].ToString();
                        mdlOSA.Listing = drOSA["Listing"].ToString();
                        mdlOSA.Link = "reportOSA_Account_Customer_Product.aspx?Week=" + mdlOSA.Week + "&Account=" + mdlOSA.ID + "&Year=" + lYear + "&Area=" + drOSA["BranchID"].ToString() + "&Customer=" + lCustomer + "&Role=" + lRole;
                        mdlOSAAreaList.Add(mdlOSA);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSA = new Model.mdlOSAAccountCustomer();
                        mdlOSA.ID = drOSA["Account"].ToString();
                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
                        mdlOSA.Percent = "";
                        mdlOSA.ListingAktif = "";
                        mdlOSA.Listing = "";
                        mdlOSAAreaList.Add(mdlOSA);
                    }
                }

            }

            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSAData> LoadGrandTotalCustomerByAccount(string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalCustomerByAccount";
            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam in mdlWeeks)
            {

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam.Week) },
                    new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                    new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGrandTotalCustomerByAccount", user, key, sp);
                foreach (DataRow drOSA in dtOSA.Rows)
                {
                    var mdlOSA = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSA.Percent = "";
                    }


                    mdlOSAAreaList.Add(mdlOSA);
                }
            }
            return mdlOSAAreaList;
        }

        public static List<Model.mdlOSAData> LoadGrandTotalCustomerByAccount2(string lAccount, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalCustomerByAccount2";
            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();
            foreach (var lParam in mdlWeeks)
            {

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lParam.Week) },
                    new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                    //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                    new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                    new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };

                DataTable dtOSA = Manager.DataFacade.GetSP("spOSAGrandTotalCustomerByAccount2", user, key, sp);
                foreach (DataRow drOSA in dtOSA.Rows)
                {
                    var mdlOSA = new Model.mdlOSAData();
                    string Percent = drOSA["Percentage"].ToString();
                    if (Percent != "")
                    {
                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
                    }
                    else
                    {
                        mdlOSA.Percent = "";
                    }


                    mdlOSAAreaList.Add(mdlOSA);
                }
            }
            return mdlOSAAreaList;
        }
        //Andy Yo>>

        //<<001 Andy Yo
        public static List<Model.mdlOSAData> LoadOSAArea(string lBranch, string lWeekFrom, string lWeekTo, string lYear)
        {
            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            var mdlOSAAreaList = new List<Model.mdlOSAData>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranch },
                new SqlParameter() {ParameterName = "@lYear", SqlDbType = SqlDbType.NVarChar, Value = lYear }
            };

            string sqlArea = @"SELECT x.BranchID, x.VisitWeek,ROUND((CONVERT(decimal,x.JmlStat0) / CONVERT(decimal,y.JmlStat10)) * 100,2) AS Percentage FROM
                                                (
                                                    SELECT b.BranchID, count(a.Status) JmlStat0, c.VisitWeek FROM SisaStockVisit a
                                                    INNER JOIN Customer b ON a.CustomerID = b.CustomerID
                                                    INNER JOIN Visit c ON a.VisitID = c.VisitID 
                                                    WHERE a.status=0 AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @lYear AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
                                                    GROUP BY b.BranchID, c.VisitWeek
                                                ) x
                                                    INNER JOIN
                                                (
                                                    SELECT b.BranchID, count(a.Status) JmlStat10, c.VisitWeek FROM SisaStockVisit a
                                                    INNER join Customer b ON a.CustomerID = b.CustomerID
                                                    INNER join Visit c ON a.VisitID = c.VisitID 
                                                    WHERE ( a.status=0 OR a.Status=1 ) AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @lYear AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
                                                    GROUP BY b.BranchID, c.VisitWeek
                                                ) y 
                                                    ON x.BranchID=y.BranchID AND x.VisitWeek = y.VisitWeek
                                                    ORDER BY x.VisitWeek ASC;";



            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sqlArea, sp);

            var mdlOSAArea = new Model.mdlOSAData();
            int nx = 0;
            foreach (DataRow drOSA in dtOSA.Rows)
            {
                for (int i = nx; i < mdlWeeks.Count; i++)
                {
                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
                    {
                        mdlOSAArea = new Model.mdlOSAData();
                        mdlOSAArea.Week = drOSA["VisitWeek"].ToString();
                        mdlOSAArea.Percent = drOSA["Percentage"].ToString().ToString().Substring(0, 5) + "%";
                        mdlOSAAreaList.Add(mdlOSAArea);
                        nx = i + 1;
                        break;
                    }
                    else
                    {
                        mdlOSAArea = new Model.mdlOSAData();
                        mdlOSAArea.Week = drOSA["VisitWeek"].ToString();
                        mdlOSAArea.Percent = "";
                        mdlOSAAreaList.Add(mdlOSAArea);
                    }

                }

            }

            return mdlOSAAreaList;
        }
        
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static Model.mdlBarChart LoadOSADashboard(string lWeekFrom, string lWeekTo, string lYear)
        {

            var mdlOSADataList = new List<Model.mdlOSAData>();



            var mdlOSAData = new Model.mdlOSAData();
            int nx = 0;

            var dataChart = new Model.mdlBarChart();
            List<string> listBranch = new List<string>();
            List<decimal> listWeekFrom = new List<decimal>();
            List<decimal> listWeekTo = new List<decimal>();
            List<Model.Dataset> listDetail = new List<Model.Dataset>();


            var dataDetailFrom = new Model.Dataset();
            dataDetailFrom.label = "Percent OSA Week " + lWeekFrom;
            dataDetailFrom.backgroundColor = "#26B99A";

            var dataDetailTo = new Model.Dataset();
            dataDetailTo.label = "Percent OSA Week " + lWeekTo;
            dataDetailTo.backgroundColor = "#03586A";

            var allBranch = Manager.BranchFacade.LoadBranch();

            foreach (var branch in allBranch)
            {
                string branchID = branch.BranchDescription.ToString();
                listBranch.Add(branchID);
                var listOSAArea = LoadOSAArea(branchID, lWeekFrom, lWeekTo, lYear);
                decimal percentFrom = 0;
                decimal percentTo = 0;
                foreach (var drOSA in listOSAArea)
                {

                    //dataChart.labels.Add(branchID);
                    if (drOSA.Percent != "")
                    {

                        if (drOSA.Week.ToString() == lWeekFrom)
                        {
                            percentFrom = Convert.ToDecimal(drOSA.Percent.Replace('%', ' ').Trim());


                        }
                        else
                        {
                            percentTo = Convert.ToDecimal(drOSA.Percent.Replace('%', ' ').Trim());
                        }
                    }

                }
                listWeekFrom.Add(percentFrom);
                listWeekTo.Add(percentTo);
            }


            dataDetailFrom.data = listWeekFrom;
            dataDetailTo.data = listWeekTo;

            dataChart.labels = listBranch;
            listDetail.Add(dataDetailFrom);
            listDetail.Add(dataDetailTo);
            dataChart.datasets = listDetail;
            return dataChart;
        }
        //001 Andy Yo>>
        
        //        public static List<Model.mdlOSAData> LoadOSAArea(string lBranch, string lWeekFrom, string lWeekTo, string lYear)
        //        {
        //            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
        //            var mdlOSAAreaList = new List<Model.mdlOSAData>();

        //            List<SqlParameter> sp = new List<SqlParameter>()
        //            {
        //                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
        //                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
        //                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranch },
        //                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
        //            };

        //            //Get Data Area
        ////            string sqlArea = @"SELECT * FROM temp_OSA 
        ////                                                WHERE VisitWeek BETWEEN @WeekFrom AND @WeekTo AND BranchID = @BranchID";

        //            string sqlArea = @"SELECT x.BranchID, x.VisitWeek,ROUND((CONVERT(decimal,x.JmlStat0) / CONVERT(decimal,y.JmlStat10)) * 100,2) AS Percentage FROM
        //                                                (
        //                                                    SELECT b.BranchID, count(a.Status) JmlStat0, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b ON a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c ON a.VisitID = c.VisitID 
        //                                                    WHERE a.status=0 AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
        //                                                    GROUP BY b.BranchID, c.VisitWeek
        //                                                ) x
        //                                                    INNER JOIN
        //                                                (
        //                                                    SELECT b.BranchID, count(a.Status) JmlStat10, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER join Customer b ON a.CustomerID = b.CustomerID
        //                                                    INNER join Visit c ON a.VisitID = c.VisitID 
        //                                                    WHERE ( a.status=0 OR a.Status=1 ) AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
        //                                                    GROUP BY b.BranchID, c.VisitWeek
        //                                                ) y 
        //                                                    ON x.BranchID=y.BranchID AND x.VisitWeek = y.VisitWeek
        //                                                    ORDER BY x.VisitWeek ASC;";

        ////            string sqlAreaPrivot = @"SELECT * FROM
        ////                                                (
        ////                                                    select x.BranchID, x.VisitWeek,(convert(decimal,x.JmlStat0) / convert(decimal,y.JmlStat10)) * 100 as Percentage from
        ////                                                (
        ////                                                    select b.BranchID, count(a.Status) JmlStat0, c.VisitWeek from SisaStockVisit a
        ////                                                    inner join Customer b on a.CustomerID = b.CustomerID
        ////                                                    inner join Visit c on a.VisitID = c.VisitID 
        ////                                                    where a.status=0
        ////                                                    group by b.BranchID, c.VisitWeek
        ////                                                ) x
        ////                                                    inner join
        ////                                                (
        ////                                                    select b.BranchID, count(a.Status) JmlStat10, c.VisitWeek from SisaStockVisit a
        ////                                                    inner join Customer b on a.CustomerID = b.CustomerID
        ////                                                    inner join Visit c on a.VisitID = c.VisitID 
        ////                                                    where a.status=0 or a.Status=1
        ////                                                    group by b.BranchID, c.VisitWeek) y on x.BranchID=y.BranchID 
        ////                                                ) as pvt
        ////                                                PIVOT
        ////                                                (
        ////                                                    SUM(Percentage)
        ////                                                    FOR pvt.VisitWeek IN (" + lParamWeeks + @")
        ////                                                ) AS PivotTable;";

        //            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sqlArea, sp);

        //            var mdlOSAArea = new Model.mdlOSAData();
        //            int nx = 0;
        //            foreach (DataRow drOSA in dtOSA.Rows)
        //            {
        //                for (int i = nx; i < mdlWeeks.Count; i++)
        //                {
        //                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
        //                    {
        //                        mdlOSAArea = new Model.mdlOSAData();
        //                        mdlOSAArea.ID = drOSA["BranchID"].ToString();
        //                        mdlOSAArea.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
        //                        mdlOSAArea.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSAAreaList.Add(mdlOSAArea);
        //                        nx = i + 1;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        mdlOSAArea = new Model.mdlOSAData();
        //                        mdlOSAArea.ID = drOSA["BranchID"].ToString();
        //                        mdlOSAArea.Percent = "";
        //                        mdlOSAArea.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSAAreaList.Add(mdlOSAArea);
        //                    }

        //                }

        //            }

        //            return mdlOSAAreaList;
        //        }

        //        public static List<Model.mdlOSAData> LoadOSACustomer(string lCustomer, string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear)
        //        {
        //            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
        //            var mdlOSAAreaList = new List<Model.mdlOSAData>();

        //            List<SqlParameter> sp = new List<SqlParameter>()
        //            {
        //                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
        //                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
        //                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
        //                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
        //                new SqlParameter() {ParameterName = "@Customer", SqlDbType = SqlDbType.NVarChar, Value = lCustomer },
        //                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
        //            };

        //            //Get Data Account
        //            string sql = @"SELECT x.CustomerID,x.CustomerName, x.Account, x.VisitWeek, ROUND((CONVERT(DECIMAL,x.JmlStat0) / CONVERT(DECIMAL,y.JmlStat10)) * 100 ,2) AS Percentage FROM
        //                                                (
        //                                                    SELECT  a.CustomerID,b.CustomerName, b.Account, COUNT(a.Status) JmlStat0, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE a.status=0 AND a.CustomerID = @Customer AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
        //                                                    GROUP BY b.Account, c.VisitWeek,a.CustomerID,b.CustomerName)
        //                                                 x
        //                                                    INNER JOIN
        //                                                (
        //                                                    SELECT a.CustomerID,b.CustomerName, b.Account, COUNT(a.Status) JmlStat10, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE (a.status=0 OR a.Status=1) AND a.CustomerID = @Customer AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo) 
        //                                                    GROUP BY b.Account, c.VisitWeek,a.CustomerID,b.CustomerName
        //                                                ) y 
        //                                                    ON x.Account=y.Account and x.CustomerID=y.CustomerID AND x.VisitWeek = y.VisitWeek
        //                                                    ORDER BY x.VisitWeek ASC;";

        //            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sql, sp);
        //            var mdlOSA = new Model.mdlOSAData();
        //            int nx = 0;
        //            foreach (DataRow drOSA in dtOSA.Rows)
        //            {
        //                for (int i = nx; i < mdlWeeks.Count; i++)
        //                {

        //                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
        //                        mdlOSA.CustomerName = drOSA["CustomerName"].ToString();
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                        nx = i + 1;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = "";
        //                        mdlOSA.CustomerName = drOSA["CustomerName"].ToString();
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                    }
        //                }

        //            }

        //            return mdlOSAAreaList;
        //        }

        //        public static List<Model.mdlOSAData> LoadOSACustomer2(string lCustomer, string lAccount, string lWeekFrom, string lWeekTo, string lYear)
        //        {
        //            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
        //            var mdlOSAAreaList = new List<Model.mdlOSAData>();

        //            List<SqlParameter> sp = new List<SqlParameter>()
        //            {
        //                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
        //                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
        //                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
        //                //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
        //                new SqlParameter() {ParameterName = "@Customer", SqlDbType = SqlDbType.NVarChar, Value = lCustomer },
        //                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
        //            };

        //            //Get Data Account
        //            string sql = @"SELECT x.CustomerID,x.CustomerName, x.Account, x.VisitWeek, ROUND((CONVERT(DECIMAL,x.JmlStat0) / CONVERT(DECIMAL,y.JmlStat10)) * 100 ,2) AS Percentage , x.BranchID FROM
        //                                                (
        //                                                    SELECT  a.CustomerID,b.CustomerName, b.Account, COUNT(a.Status) JmlStat0, c.VisitWeek,  a.BranchID  FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE a.status=0 AND a.CustomerID = @Customer AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
        //                                                    GROUP BY b.Account, c.VisitWeek,a.CustomerID,b.CustomerName,a.BranchID)
        //                                                 x
        //                                                    INNER JOIN
        //                                                (
        //                                                    SELECT a.CustomerID,b.CustomerName, b.Account, COUNT(a.Status) JmlStat10, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE (a.status=0 OR a.Status=1) AND a.CustomerID = @Customer AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo) 
        //                                                    GROUP BY b.Account, c.VisitWeek,a.CustomerID,b.CustomerName
        //                                                ) y 
        //                                                    ON x.Account=y.Account and x.CustomerID=y.CustomerID AND x.VisitWeek = y.VisitWeek
        //                                                    ORDER BY x.VisitWeek ASC;";

        //            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sql, sp);
        //            var mdlOSA = new Model.mdlOSAData();
        //            int nx = 0;
        //            foreach (DataRow drOSA in dtOSA.Rows)
        //            {
        //                for (int i = nx; i < mdlWeeks.Count; i++)
        //                {

        //                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
        //                        mdlOSA.CustomerName = drOSA["CustomerName"].ToString();
        //                        mdlOSA.Link = drOSA["BranchID"].ToString();
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                        nx = i + 1;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Link = drOSA["BranchID"].ToString();
        //                        mdlOSA.Percent = "";
        //                        mdlOSA.CustomerName = drOSA["CustomerName"].ToString();
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                    }
        //                }

        //            }

        //            return mdlOSAAreaList;
        //        }

        //        public static List<Model.mdlOSAData> LoadOSAAccount(string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear)
        //        {
        //            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
        //            var mdlOSAAreaList = new List<Model.mdlOSAData>();

        //            List<SqlParameter> sp = new List<SqlParameter>()
        //            {
        //                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
        //                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
        //                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
        //                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
        //                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
        //            };

        //            //Get Data Account
        //            string sqlArea = @"SELECT x.Account, x.VisitWeek,ROUND((CONVERT(DECIMAL,x.JmlStat0) / CONVERT(DECIMAL,y.JmlStat10)) * 100,2) AS Percentage FROM
        //                                                (
        //                                                    SELECT  b.Account, COUNT(a.Status) JmlStat0, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE a.status=0 AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
        //                                                    GROUP BY b.Account, c.VisitWeek
        //                                                ) x
        //                                                    INNER JOIN
        //                                                (
        //                                                    SELECT b.Account, COUNT(a.Status) JmlStat10, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE ( a.status=0 OR a.Status=1 ) AND b.BranchID = @BranchID AND YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo) 
        //                                                    GROUP BY b.Account, c.VisitWeek
        //												) y 
        //												    ON x.Account=y.Account AND x.VisitWeek = y.VisitWeek
        //                                                    ORDER BY x.VisitWeek ASC;";

        //            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sqlArea, sp);
        //            var mdlOSA = new Model.mdlOSAData();
        //            int nx = 0;
        //            foreach (DataRow drOSA in dtOSA.Rows)
        //            {
        //                for (int i = nx; i < mdlWeeks.Count; i++)
        //                {
        //                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
        //                        //Andy Yo Modif
        //                        mdlOSA.Link = "reportOSA_Account_Customer.aspx?week=" + mdlOSA.Week + "&Account=" + mdlOSA.ID + "&year=" + lYear + "&Area=" + lBranchID;
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                        nx = i + 1;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = "";
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                    }
        //                }
        //            }

        //            return mdlOSAAreaList;
        //        }

        //        public static List<Model.mdlOSAData> LoadOSAAccount2(string lAccount, string lWeekFrom, string lWeekTo, string lYear)
        //        {
        //            //string lParam = string.Empty;
        //            //foreach (var lBranchID in lBranchList)
        //            //{
        //            //    if (lParam == "")
        //            //    {
        //            //        lParam = " b.BranchID =" + "'" + lBranchID + "'";
        //            //    }
        //            //    else
        //            //    {
        //            //        lParam += " OR b.BranchID =" + "'" + lBranchID + "'";
        //            //    }
        //            //}
        //            //lParam = "(" + lParam + ")";


        //            var mdlWeeks = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
        //            var mdlOSAAreaList = new List<Model.mdlOSAData>();

        //            List<SqlParameter> sp = new List<SqlParameter>()
        //            {
        //                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
        //                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
        //                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
        //                //new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
        //                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear }
        //            };

        //            //Get Data Account
        //            string sqlArea = @"SELECT x.Account, x.VisitWeek,ROUND((CONVERT(DECIMAL,x.JmlStat0) / CONVERT(DECIMAL,y.JmlStat10)) * 100,2) AS Percentage FROM
        //                                                (
        //                                                    SELECT  b.Account, COUNT(a.Status) JmlStat0, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE a.status=0 AND  YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo)
        //                                                    GROUP BY b.Account, c.VisitWeek
        //                                                ) x
        //                                                    INNER JOIN
        //                                                (
        //                                                    SELECT b.Account, COUNT(a.Status) JmlStat10, c.VisitWeek FROM SisaStockVisit a
        //                                                    INNER JOIN Customer b on a.CustomerID = b.CustomerID
        //                                                    INNER JOIN Visit c on a.VisitID = c.VisitID 
        //                                                    WHERE ( a.status=0 OR a.Status=1 ) AND  YEAR(c.VisitDate) = @Year AND b.Account = @Account AND (c.VisitWeek BETWEEN @WeekFrom AND @WeekTo) 
        //                                                    GROUP BY b.Account, c.VisitWeek
        //												) y 
        //												    ON x.Account=y.Account AND x.VisitWeek = y.VisitWeek
        //                                                    ORDER BY x.VisitWeek ASC;";

        //            DataTable dtOSA = Manager.DataFacade.DTSQLCommand(sqlArea, sp);
        //            var mdlOSA = new Model.mdlOSAData();
        //            int nx = 0;
        //            foreach (DataRow drOSA in dtOSA.Rows)
        //            {
        //                for (int i = nx; i < mdlWeeks.Count; i++)
        //                {
        //                    if (mdlWeeks[i].Week == drOSA["VisitWeek"].ToString())
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = drOSA["Percentage"].ToString().Substring(0, 5) + "%";
        //                        //Andy Yo Modif
        //                        mdlOSA.Link = "reportOSA_Account_Customer.aspx?week=" + mdlOSA.Week + "&Account=" + mdlOSA.ID + "&year=" + lYear ;
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                        nx = i + 1;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        mdlOSA = new Model.mdlOSAData();
        //                        mdlOSA.ID = drOSA["Account"].ToString();
        //                        mdlOSA.Week = drOSA["VisitWeek"].ToString();
        //                        mdlOSA.Percent = "";
        //                        mdlOSAAreaList.Add(mdlOSA);
        //                    }
        //                }
        //            }

        //            return mdlOSAAreaList;
        //        }

    }
}
