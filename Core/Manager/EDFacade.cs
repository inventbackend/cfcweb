﻿/* documentation
 * 
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Dynamic;
using System.Globalization;
using Core.Model;

namespace Core.Manager
{
    public class EDFacade
    {
        public static Boolean CheckWeek(string lWeek, string lTahun, string lAccount, string lArea, string lCustomerName)
        {
            Boolean lCheck = false;
            List<string> lParamAreas = new List<string>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.NVarChar, Value =  lWeek},
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value =  lTahun},
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value =  lAccount},
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value =  lArea},
                new SqlParameter() {ParameterName = "@CustomerName", SqlDbType = SqlDbType.NVarChar, Value =  lCustomerName},
            };

            string sql = @"SELECT TOP 1	c.CustomerName
                              FROM (SELECT VisitID,VisitDate,VisitWeek FROM Visit WHERE (VisitWeek = @Week) AND year(VisitDate) = @Year) a 
				                              INNER JOIN (SELECT SisaStockTypeID,CustomerID,Status,VisitID FROM SisaStockVisit WHERE SisaStockTypeID IN ('0','6','9','12')) b ON a.VisitID = b.VisitID 
				                              INNER JOIN Customer c ON b.CustomerID = c.CustomerID 
				                              WHERE (b.Status = 1 OR b.Status = 0) AND
						                            c.Account = @Account AND 
						                            c.BranchID = @Area AND 
						                            c.CustomerName = @CustomerName
				                              ORDER BY c.CustomerName,a.VisitWeek,convert(int,b.SisaStockTypeID);";

            DataTable dtWeek = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow drWeek in dtWeek.Rows)
            {
                lCheck = true;
            }
            return lCheck;
        }

        public static List<Model.mdlRptEDExcel> LoadEDExcel(string lBranchID,List<String> lAccountList, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadEDExcel";
            string lParam = string.Empty;
            foreach (var lAccount in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccount;
                }
                else
                {
                    lParam += "," + lAccount;
                }
            }

            var mdlRptEDExcelList = new List<Model.mdlRptEDExcel>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@LastWeek", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtED = Manager.DataFacade.GetSP("spReportEDExcel", user, key, sp);
            foreach (DataRow drED in dtED.Rows)
            {
                var mdlRptEDExcel = new Model.mdlRptEDExcel();
                mdlRptEDExcel.Distributor = drED["Distributor"].ToString();
                mdlRptEDExcel.Channel = drED["Channel"].ToString();
                mdlRptEDExcel.Account = drED["Account"].ToString();
                mdlRptEDExcel.CustomerName = drED["CustomerName"].ToString();
                mdlRptEDExcel.ProductName = drED["ProductName"].ToString();
                mdlRptEDExcel.SisaStockTypeID = drED["SisaStockTypeID"].ToString();
                mdlRptEDExcel.Value = drED["Value"].ToString();

                mdlRptEDExcelList.Add(mdlRptEDExcel);
            }

            return mdlRptEDExcelList;
        }

        public static List<Model.mdlOSACustomer> GetCustomer(string keyword, string lAccount, string lBranchID, string lWeekFrom, string lWeekTo, string lYear)
        {
            string sql = string.Empty;
            var mdlOSACustomerList = new List<Model.mdlOSACustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@BranchID", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Keyword", SqlDbType = SqlDbType.NVarChar, Value =  "%"+keyword+"%"}
            };


            if (keyword == "")
            {
                sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName 
                                          FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                           WHERE b.BranchID = @BranchID AND a.Account = @Account ;";
            }
            else
            {
                sql = @"SELECT DISTINCT a.CustomerID, a.CustomerName 
                                          FROM Customer a INNER JOIN SisaStockVisit b ON a.CustomerID = b.CustomerID
                                                        WHERE b.BranchID = @BranchID AND  a.Account = @Account AND (a.CustomerID LIKE @Keyword OR a.CustomerName LIKE @Keyword) ;";
            }

            DataTable dtCustomer = Manager.DataFacade.DTSQLCommand(sql, sp);
            foreach (DataRow drCustomer in dtCustomer.Rows)
            {
                var mdlCustomer = new Model.mdlOSACustomer();
                mdlCustomer.CustomerID = drCustomer["CustomerID"].ToString();
                mdlCustomer.CustomerName = drCustomer["CustomerName"].ToString();
                mdlCustomer.Link = String.Format("ReportOSA_Product.aspx?AreaId={0}&Account={1}&Customer={2}&WeekFrom={3}&WeekTo={4}&Tahun={5}", lBranchID, lAccount, drCustomer["CustomerID"].ToString(), lWeekFrom, lWeekTo, lYear);
                mdlOSACustomerList.Add(mdlCustomer);
            }

            return mdlOSACustomerList;
        }

        public static string GetWeeks(string lWeekFrom, string lWeekTo, string lTahun)
        {
            string week = string.Empty;
            for (int i = int.Parse(lWeekFrom); i <= int.Parse(lWeekTo); i++)
            {
                

                Boolean lCheck = OSAFacade.CheckWeek(i.ToString(), lTahun);
                if (lCheck == true)
                {
                    week = i.ToString();
                    
                }
            }
            return week;
        }

        public static List<Model.mdlRptED> LoadED(string lBranchID, List<String> lAccountList, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadED";
            string lParam = string.Empty;
            foreach (var lAccount in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccount;
                }
                else
                {
                    lParam += "," + lAccount;
                }
            }
            var mdlRptEDList = new List<Model.mdlRptED>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtED = Manager.DataFacade.GetSP("spReportED", user, key, sp);
            foreach (DataRow drED in dtED.Rows)
            {
                var mdlRptED = new Model.mdlRptED();
                mdlRptED.CustomerName = drED["CustomerName"].ToString();
                mdlRptED.Account = drED["Account"].ToString();
                mdlRptED.Channel = drED["Channel"].ToString();
                mdlRptEDList.Add(mdlRptED);
            }

            return mdlRptEDList;
        }

        public static List<Model.mdlRptEDDetail> LoadED_Detail(string lBranchID, List<String> lAccountList, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadED_Detail";
            var mdlEDWeekList = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            string lParam = string.Empty;
            foreach (var lAccount in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccount;
                }
                else
                {
                    lParam += "," + lAccount;
                }
            }
            var mdlRptEDList = new List<Model.mdlRptEDDetail>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtED = Manager.DataFacade.GetSP("spReportEDDetail", user, key, sp);

            decimal lTotalSummary = 0;
            foreach (DataRow drED in dtED.Rows)
            {
                var mdlRptED = new Model.mdlRptEDDetail();
                mdlRptED.VisitWeek = drED["VisitWeek"].ToString();
                mdlRptED.Channel = drED["Channel"].ToString();
                mdlRptED.Account = drED["Account"].ToString();
                mdlRptED.CustomerName = drED["CustomerName"].ToString();
                mdlRptED.SisaStockTypeID = drED["SisaStockTypeID"].ToString();
                mdlRptED.Value = drED["Value"].ToString(); //di bawah ini harus di ganti dengan parameter customer name --NANDA--
                mdlRptED.LinkValue = "reportED_SKU.aspx?TypeID=" + mdlRptED.SisaStockTypeID + "&Week=" + mdlRptED.VisitWeek + "&Account=" + mdlRptED.Account + "&Customer=" + mdlRptED.CustomerName + "&Year=" + lYear + "&Area=" + lBranchID + "&role=" + lRole;
                mdlRptEDList.Add(mdlRptED);
                lTotalSummary += Decimal.Parse(mdlRptED.Value);

                if (mdlRptED.SisaStockTypeID == "12")
                {
                    mdlRptED = new Model.mdlRptEDDetail();
                    mdlRptED.VisitWeek = drED["VisitWeek"].ToString();
                    mdlRptED.Channel = drED["Channel"].ToString();
                    mdlRptED.Account = drED["Account"].ToString();
                    mdlRptED.CustomerName = drED["CustomerName"].ToString();
                    mdlRptED.SisaStockTypeID = "Summary";
                    mdlRptED.Value = "<b>"+lTotalSummary.ToString()+"</b>";
                    mdlRptED.LinkValue = "reportED_SKU.aspx?TypeID=summary&Week=" + mdlRptED.VisitWeek + "&Account=" + mdlRptED.Account + "&Channel=" + mdlRptED.Channel + "&Customer=" + mdlRptED.CustomerName + "&Year=" + lYear + "&Area=" + lBranchID + "&role=" + lRole;
                    mdlRptEDList.Add(mdlRptED);
                    lTotalSummary = 0;
                } 
            }

            return mdlRptEDList;
        }

        public static List<Model.mdlSisaStockType> GetlistSisaStockTypeID()
        {
            var mdlSisaStockTypeList = new List<Model.mdlSisaStockType>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            string sql = @"SELECT SisaStockTypeID, Description FROM SisaStockType where Category = 'Month' ORDER BY Seq";

            DataTable dt = Manager.DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow dr in dt.Rows)
            {
                var mdlSisaStockType = new Model.mdlSisaStockType();
                mdlSisaStockType.SisaStockTypeID = dr["SisaStockTypeID"].ToString();
                mdlSisaStockType.Description = dr["Description"].ToString();
                mdlSisaStockTypeList.Add(mdlSisaStockType);

                if (mdlSisaStockType.SisaStockTypeID == "12")
                {
                    mdlSisaStockType = new Model.mdlSisaStockType();
                    mdlSisaStockType.SisaStockTypeID = "Summary";
                    mdlSisaStockType.Description = "Summary";
                    mdlSisaStockTypeList.Add(mdlSisaStockType);
                }  
            }
            return mdlSisaStockTypeList;
        }

        public static List<Model.mdlRptEDDetail> LoadGrandTotalED(string lBranchID, List<String> lAccountList, string lWeekFrom, string lWeekTo, string lYear,string lRole, string user)
        {
            string key = "LoadGrandTotalED";
            var mdlEDWeekList = OSAFacade.GetWeeks(lWeekFrom, lWeekTo, lYear);
            string lParam = string.Empty;
            foreach (var lAccount in lAccountList)
            {
                if (lParam == "")
                {
                    lParam = lAccount;
                }
                else
                {
                    lParam += "," + lAccount;
                }
            }
            var mdlRptEDList = new List<Model.mdlRptEDDetail>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@WeekFrom", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekFrom) },
                new SqlParameter() {ParameterName = "@WeekTo", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeekTo) },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lBranchID },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
            };

            DataTable dtED = Manager.DataFacade.GetSP("spReportGrandTotalED",user, key, sp);

            decimal lTotalSummary = 0;
            foreach (DataRow drED in dtED.Rows)
            {
                var mdlRptED = new Model.mdlRptEDDetail();

                mdlRptED.VisitWeek = drED["VisitWeek"].ToString();
                mdlRptED.SisaStockTypeID = drED["SisaStockTypeID"].ToString();
                mdlRptED.Value = drED["Value"].ToString();
                //mdlRptED.Account = drED["Account"].ToString();
                //mdlRptED.Channel = drED["Channel"].ToString();
                mdlRptED.LinkValue = "reportED_SKU.aspx?TypeID=" + mdlRptED.SisaStockTypeID + "&Week=" + mdlRptED.VisitWeek + "&Account=" + lParam + "&Year=" + lYear + "&Area=" + lBranchID + "&role=" + lRole;
                mdlRptEDList.Add(mdlRptED);
                lTotalSummary += Decimal.Parse(mdlRptED.Value);

                if (mdlRptED.SisaStockTypeID == "12")
                {
                    mdlRptED = new Model.mdlRptEDDetail();
                    mdlRptED.VisitWeek = drED["VisitWeek"].ToString();
                    mdlRptED.SisaStockTypeID = "Summary";
                    mdlRptED.Value = lTotalSummary.ToString();
                    mdlRptEDList.Add(mdlRptED);
                    lTotalSummary = 0;
                }
            }

            return mdlRptEDList;
        }

        //====================================================== ED by SKU =============================================================================================================================

        public static List<Model.mdlRptEDbySKU> LoadED_SKU(List<Model.mdlSisaStockType> lSisaStockTypeList, string lArea, string lAccount, string lWeek, string lYear, string lCustomer,string lRole, string user)
        {
            //string key = "GetReportOOSByBranch";
            string key = "LoadED_SKU";
            string lParam = string.Empty;
            foreach (var lSisaStockType in lSisaStockTypeList)
            {
                if (lParam == "")
                {
                    lParam = lSisaStockType.SisaStockTypeID;
                }
                else
                {
                    lParam += "," + lSisaStockType.SisaStockTypeID;
                }
            }
            var mdlRptEDbySKUList = new List<Model.mdlRptEDbySKU>();

            DataTable dtED = new DataTable();
            if (lCustomer != "")
            {
                List<SqlParameter> sp = new List<SqlParameter>()
                {
                new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeek) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lArea },
                new SqlParameter() {ParameterName = "@Account", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@Customer", SqlDbType = SqlDbType.NVarChar, Value = lCustomer },
                new SqlParameter() {ParameterName = "@SisaStockTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };
                dtED = Manager.DataFacade.GetSP("spReportEDbySKUbyCustomerID", user, key, sp);
            }
            else
            {
                string lParam2 = string.Empty;
                

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                new SqlParameter() {ParameterName = "@Week", SqlDbType = SqlDbType.Int, Value = int.Parse(lWeek) },
                new SqlParameter() {ParameterName = "@Year", SqlDbType = SqlDbType.NVarChar, Value = lYear },
                new SqlParameter() {ParameterName = "@Area", SqlDbType = SqlDbType.NVarChar, Value = lArea },
                new SqlParameter() {ParameterName = "@AccountList", SqlDbType = SqlDbType.NVarChar, Value = lAccount },
                new SqlParameter() {ParameterName = "@SisaStockTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = lRole }
                };
                dtED = Manager.DataFacade.GetSP("spReportEDbySKU", user, key, sp);
            }
            
            //decimal lTotalSummary = 0;
            foreach (DataRow drED in dtED.Rows)
            {
                var mdlRptEDbySKU = new Model.mdlRptEDbySKU();
                //mdlRptEDbySKU.VisitWeek = drED["VisitWeek"].ToString();
                mdlRptEDbySKU.ProductID = drED["ProductID"].ToString();
                mdlRptEDbySKU.ProductName = drED["ProductName"].ToString();
                mdlRptEDbySKU.SisaStockTypeID = drED["SisaStockTypeID"].ToString();
                mdlRptEDbySKU.Value = Convert.ToDecimal(drED["Value"]);
                mdlRptEDbySKUList.Add(mdlRptEDbySKU);
                //lTotalSummary += Decimal.Parse(mdlRptEDbySKU.Value);

                //if (mdlRptED.SisaStockTypeID == "12")
                //{
                //    mdlRptED = new Model.mdlRptEDDetail();
                //    mdlRptED.VisitWeek = drED["VisitWeek"].ToString();
                //    mdlRptED.Channel = drED["Channel"].ToString();
                //    mdlRptED.Account = drED["Account"].ToString();
                //    mdlRptED.CustomerName = drED["CustomerName"].ToString();

                //    mdlRptED.SisaStockTypeID = "Summary";
                //    mdlRptED.Value = "<b>" + lTotalSummary.ToString() + "</b>";

                //    mdlRptEDList.Add(mdlRptED);
                //    lTotalSummary = 0;
                //}
            }

            return mdlRptEDbySKUList;
        }

        
    }
}
