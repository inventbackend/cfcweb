﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Core.Manager
{
    public class DistributorFacade
    {
        public static List<Model.Distributor> GetDistributor(string area)
        {

            string sql = "";
            var list = new List<Model.Distributor>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
               new SqlParameter() {ParameterName = "@area", SqlDbType = SqlDbType.NVarChar, Value = area }
            };
            if (area != "")
                sql = "select distinct a.DistributorID from Distributor a inner join Customer b on a.DistributorID = b.Distributor WHERE b.BranchID IN (@area)";
            else
                sql = "select distinct a.DistributorID from Distributor a inner join Customer b on a.DistributorID = b.Distributor";

            DataTable dt = DataFacade.DTSQLCommand(sql, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.Distributor();
                model.DistributorID = row["DistributorID"].ToString();
                model.DistributorName = "";
                model.Description = "";

                list.Add(model);

            }

            return list;

        }
    }
}
