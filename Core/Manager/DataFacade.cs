﻿/* documentation
 *001
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.Configuration;
using Core.Model;

namespace Core.Manager
{
    public class DataFacade
    {
        public static DataTable DTSQLCommand(string sqlQuery,List<SqlParameter> listParam)
        {
           
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
            string commandText = sqlQuery;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {
                    try
                    {

                        command.Parameters.AddRange(listParam.ToArray());
                        connection.Open();
                        dt.Load(command.ExecuteReader());
                        command.Parameters.Clear();
                    }
                    catch (Exception ex)
                    {
                        ////Get Param Query
                        //String lSqlParam = "";
                        //foreach (var lParam in listParam)
                        //{
                        //    lSqlParam += lParam.ParameterName + "=" + lParam.Value + " , ";
                        //}

                        ////Insert log_exception 
                        //LogFacade.InsertLogException(ex.ToString(), sqlQuery + " " + lSqlParam, Globals.gKey, Globals.gUserId);
                    }
                    finally
                    {
                        connection.Close();
                    }

                }

            }
            return dt;
        }

        public static string DTSQLVoidCommand(string sqlQuery, List<SqlParameter> listParam)
        {

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
            string commandText = sqlQuery;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlCommand command = new SqlCommand(sqlQuery, connection))
                {
                    try
                    {
                        command.Parameters.AddRange(listParam.ToArray());
                        connection.Open();
                        command.ExecuteNonQuery();
                        command.Parameters.Clear();
                        return "1";
                    }
                    catch (SqlException ex)
                    {
                        ////Get Param Query
                        //String lSqlParam = "";
                        //foreach (var lParam in listParam)
                        //{
                        //    lSqlParam += lParam.ParameterName + "=" + lParam.Value + " , ";
                        //}

                        ////Insert log_exception 
                        //LogFacade.InsertLogException(ex.ToString(), sqlQuery + " " + lSqlParam, Globals.gKey, Globals.gUserId);
                        return "0";
                    }
                    finally
                    {
                        connection.Close();
                    }

                }
            }

        }

        public static DataTable GetSP(string spname, string user, string key, List<SqlParameter> listParam)
        {
            string lCommand = "";
            string commandText = spname;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {

                    using (SqlCommand command = new SqlCommand(spname, connection))
                    {
                        lCommand = command.ToString();
                        try
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddRange(listParam.ToArray());
                            connection.Open();
                            dt.Load(command.ExecuteReader());
                            command.Parameters.Clear();
                        }
                        catch (Exception ex)
                        {
                            //Get Param Query
                            String lSqlParam = "";
                            foreach (var lParam in listParam)
                            {
                                lSqlParam += lParam.ParameterName + "=" + lParam.Value + " , ";
                            }

                            //Insert log_exception 
                            LogFacade.InsertLogException(ex.ToString(), spname + " " + lSqlParam, key, user);
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
            return dt;
        }

        public static string GetSP_Void(string spname, string user, string key, List<SqlParameter> listParam)
        {
            string lCommand = "";
            string lReturn_Status = "";
            string commandText = spname;
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            
            
                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {

                    using (SqlCommand command = new SqlCommand(spname, connection))
                    {
                        lCommand = command.ToString();
                        try
                        {
                            
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.AddRange(listParam.ToArray());
                            connection.Open();
                            command.ExecuteNonQuery();
                            command.Parameters.Clear();

                            lReturn_Status = "Success";
                        }
                        catch (Exception ex)
                        {
                            //Get Param Query
                            String lSqlParam = "";
                            foreach (var lParam in listParam)
                            {
                                lSqlParam += lParam.ParameterName + "=" + lParam.Value + " , ";

                            }
                            //Insert log_exception 
                            LogFacade.InsertLogException(ex.ToString(), spname + " " + lSqlParam, key, user);
                            lReturn_Status = "Error";
                        }
                        finally
                        {
                            connection.Close();
                        }
                        
                    }

                }
                return lReturn_Status;
        }

        public static string TransSP_Void_2sp(string spname1, List<SqlParameter> listParam1, string spname2, List<SqlParameter> listParam2, string user, string key)
        {
            string lReturn_Status = "";
         
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlTransaction objTrans = null;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                objTrans = connection.BeginTransaction();
                SqlCommand command1 = new SqlCommand(spname1, connection,objTrans);
                SqlCommand command2 = new SqlCommand(spname2, connection, objTrans);      
                    try
                    {
                        command1.CommandType = CommandType.StoredProcedure;
                        command1.Parameters.AddRange(listParam1.ToArray());
                        command1.ExecuteNonQuery();

                        command2.CommandType = CommandType.StoredProcedure;
                        command2.Parameters.AddRange(listParam2.ToArray());
                        command2.ExecuteNonQuery();
                        objTrans.Commit();

                        command1.Parameters.Clear();

                        lReturn_Status = "Success";
                    }
                    catch (Exception ex)
                    {
                        //Get Param Query
                        String lSqlParam1 = "";
                        foreach (var lParam in listParam1)
                        {
                            lSqlParam1 += lParam.ParameterName + "=" + lParam.Value + " , ";

                        }
                        String lSqlParam2 = "";
                        foreach (var lParam in listParam1)
                        {
                            lSqlParam2 += lParam.ParameterName + "=" + lParam.Value + " , ";

                        }

                        //Insert log_exception 
                        LogFacade.InsertLogException(ex.ToString(), spname1 + " " + lSqlParam1 + " " + spname2 + " " + lSqlParam2, key, user);

                        //Insert log_exception 
                        objTrans.Rollback();  
                        lReturn_Status = "Error";
                    }
                    finally
                    {
                        connection.Close();
                    }

                

            }
            return lReturn_Status;
        }

        public static string TransSP_Void_2sp(string spname1, List<SqlParameter> listParam1, string spname2, List<SqlParameter> listParam2, string spname3, List<SqlParameter> listParam3, string user, string key)
        {
            string lReturn_Status = "";

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlTransaction objTrans = null;

            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                objTrans = connection.BeginTransaction();
                SqlCommand command1 = new SqlCommand(spname1, connection, objTrans);
                SqlCommand command2 = new SqlCommand(spname2, connection, objTrans);
                SqlCommand command3 = new SqlCommand(spname3, connection, objTrans);
                try
                {
                    command1.CommandType = CommandType.StoredProcedure;
                    command1.Parameters.AddRange(listParam1.ToArray());
                    command1.ExecuteNonQuery();

                    command2.CommandType = CommandType.StoredProcedure;
                    command2.Parameters.AddRange(listParam2.ToArray());
                    command2.ExecuteNonQuery();
                
                    command3.CommandType = CommandType.StoredProcedure;
                    command3.Parameters.AddRange(listParam3.ToArray());
                    command3.ExecuteNonQuery();

                    objTrans.Commit();
                    command1.Parameters.Clear();

                    lReturn_Status = "Success";
                }
                catch (Exception ex)
                {
                    //Get Param Query
                    String lSqlParam1 = "";
                    foreach (var lParam in listParam1)
                    {
                        lSqlParam1 += lParam.ParameterName + "=" + lParam.Value + " , ";

                    }
                    String lSqlParam2 = "";
                    foreach (var lParam in listParam1)
                    {
                        lSqlParam2 += lParam.ParameterName + "=" + lParam.Value + " , ";

                    }

                    //Insert log_exception 
                    LogFacade.InsertLogException(ex.ToString(), spname1 + " " + lSqlParam1 + " " + spname2 + " " + lSqlParam2, key, user);

                    //Insert log_exception 
                    objTrans.Rollback();
                    lReturn_Status = "Error";
                }
                finally
                {
                    connection.Close();
                }

            }
            return lReturn_Status;
        }

        public static string DTSQLListInsert<T>(List<T> list, string tablename, string user, string key)
        {
            DataTable dt = new DataTable();
            dt = Common.HelperFacade.ConvertToDataTable(list);

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();

                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 660;
                            bulkcopy.DestinationTableName = tablename;
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }
                        return "1";

                    }
                    catch(SqlException ex)
                    {

                        //Insert log_exception 
                        LogFacade.InsertLogException(ex.ToString(), "DTSQLListInsert", key, user);


                        //return ex.ToString();
                        return ex.ToString();
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
              

        }

        public static string DTSQLListUpdate<T>(List<T> list, string TableName, List<string> updateCollumn, List<string> onCollumn, string user, string key)
        {
            DataTable dt = new DataTable("#TmpTable");
            //clsBulkOperation blk = new clsBulkOperation();
            dt = Common.HelperFacade.ConvertToDataTable(list);

            //ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString))
            {
                using (SqlCommand command = new SqlCommand("", conn))
                {
                    try
                    {
                        conn.Open();

                        //Creating temp table on database
                        command.CommandText = "SELECT TOP 1 * INTO #TmpTable FROM " + TableName + ";DELETE FROM #TmpTable";
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(conn))
                        {
                            bulkcopy.BulkCopyTimeout = 660;
                            bulkcopy.DestinationTableName = "#TmpTable";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }

                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 300;
                        command.CommandText = "UPDATE " + TableName + " SET " + Common.HelperFacade.UpdateCollumnSetBuilder(updateCollumn) + " FROM " + TableName + " a INNER JOIN #TmpTable Temp ON " + Common.HelperFacade.OnBySetBuilder(onCollumn) + "; DROP TABLE #TmpTable;";
                        command.ExecuteNonQuery();

                        return "1";
                    }
                    catch (SqlException ex)
                    {
                        //Insert log_exception 
                        LogFacade.InsertLogException(ex.ToString(), "DTSQLListUpdate ", key, user);

                        // Handle exception properly
                        //return ex.ToString();
                        return "0";
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }


        }

        public static void RunStoredProcedure(string sp)
        {

            //string conString = @"Data Source=JOSHUA-PC\SQLEXPRESS;Initial Catalog=sfa_courierNew;User ID=sa;Password=cuki123";
            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["sp"].ConnectionString;
            SqlConnection conn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            Int32 rows;

            cmd.CommandText = sp;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conn;

            try
            {
                conn.Open();
                rows = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }




        }

        public static string RunStoredProcedureWithParameter(string storedProcedure, List<SqlParameter> listParam)
        {
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(storedProcedure, con))
                {
                    cmd.Parameters.AddRange(listParam.ToArray());
                    cmd.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();

                        return "SQL Command Success";
                    }
                    catch (SqlException ex)
                    {
                        return ex.ToString();
                    }
                }
            }
        }
    }
}
