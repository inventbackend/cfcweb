﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace Core.Manager
{
    public class StringFacade
    {
        public static string NormalizedBranch(string branches)
        {

            if (branches == "")
            {
                return "";
            }

            if (branches.Contains('|'))
            {


                var sb = new StringBuilder();
                string[] arrBranch = branches.Split('|');
                if (arrBranch.Count() == 1)
                {
                    return arrBranch.FirstOrDefault().Trim();
                }
                else if (arrBranch.Count() > 1)
                {
                    string last = arrBranch[arrBranch.Count() - 1];
                    foreach (string str in arrBranch)
                    {
                        if (str == last)
                        {
                            sb.Append("'" + str + "'");
                        }
                        else
                        {
                            sb.Append("'" + str + "',");
                        }
                    }

                    return sb.ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "'" + branches + "'";
            }

        }

        public static string JoinListItemCollection(ListControl list)
        {
            string area = "";

            foreach (ListItem item in list.Items)
            {
                if (item.Selected == true && item != list.Items[list.Items.Count - 1])
                {
                    area += item.Value + "|";
                }
                else
                {
                    area += item.Value;
                }

            }

            return area;
        }

        public static string NulltoStringEmpty(string lParam)
        {
            if (lParam == null)
                lParam = "";

            return lParam;
        }

        public static string EmptyStringtoZeroString(string lParam)
        {
            if (lParam == "")
                lParam = "0";

            return lParam;
        }

        public static string NulltoZero(string lParam)
        {
            if (lParam == null)
                lParam = "0";

            return lParam;
        }
    }
}
