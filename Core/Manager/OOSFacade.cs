﻿/* documentation
 * 001 andy yo 08/11/2017 - dashboard modif
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Core.Model;

namespace Core.Manager
{
    public class OOSFacade
    {
        public static List<Model.mdlOOS> GetReportOOSByBranch(int weekFrom,int weekTo, string branchid,int year,string role, string user)
        {
            string key = "GetReportOOSByBranch";
            var list = new List<Model.mdlOOS>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branchid },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@Role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            DataTable dt = DataFacade.GetSP("spReportOOSByBranch", user, key, sp);

            foreach(DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOS();
                model.BranchID = row["BranchID"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOS> GetReportOOSByAccount(int weekFrom, int weekTo, string account, int year,string role, string user)
        {
            string key = "GetReportOOSByAccount";
            var list = new List<Model.mdlOOS>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            DataTable dt = DataFacade.GetSP("spReportOOSByAccount", user, key, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOS();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOS> GetReportOOSByAccountBranch(int weekFrom, int weekTo, string account,string branch, int year,string role, string user)
        {
            string key = "GetReportOOSByAccountBranch";
            var list = new List<Model.mdlOOS>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            DataTable dt = DataFacade.GetSP("spReportOOSByAccountBranch", user, key, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOS();
                model.BranchID = branch;
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
             
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOSProduct> GetOOSProductPerAccount(int weekFrom, int weekTo, string account, int year,string role, string user)
        {
            string key = "GetOOSProductPerAccount";
            var list = new List<Model.mdlOOSProduct>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerAccount";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProduct();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();
                
                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);
                    
                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSProduct> GetOOSProductPerAccountBranch(int weekFrom, int weekTo, string account,string branch, int year,string role, string user)
        {
            string key = "GetOOSProductPerAccountBranch";
            var list = new List<Model.mdlOOSProduct>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerAccountBranch";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProduct();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();

                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);

                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSCustomer> GetOOSCustomers(int visitWeek, int year, string productID, string account,string role, string user)
        {
            string key = "GetReportOOSByBranch";
            var list = new List<Model.mdlOOSCustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@visitWeek", SqlDbType = SqlDbType.Int, Value = visitWeek },

                new SqlParameter() {ParameterName = "@productID", SqlDbType = SqlDbType.NVarChar, Value = productID },
              
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.Int, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSCustomers";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSCustomer();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.CustomerID = row["CustomerID"].ToString();
                model.Status = row["Status"].ToString();
                model.Channel = row["Channel"].ToString();
                model.BranchID = row["BranchID"].ToString();
                model.CustomerName = row["CustomerName"].ToString();


                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSCustomer> GetOOSCustomersAccountBranch(int visitWeek, int year, string productID, string account,string branch,string role, string user)
        {
            string key = "GetOOSCustomersAccountBranch";
            var list = new List<Model.mdlOOSCustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@visitWeek", SqlDbType = SqlDbType.Int, Value = visitWeek },

                new SqlParameter() {ParameterName = "@productID", SqlDbType = SqlDbType.NVarChar, Value = productID },
              
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                 new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.Int, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSCustomersAccountBranch";


            DataTable dt = DataFacade.GetSP(sql,user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSCustomer();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.CustomerID = row["CustomerID"].ToString();
                model.Status = row["Status"].ToString();
                model.Channel = row["Channel"].ToString();
                model.BranchID = row["BranchID"].ToString();
                model.CustomerName = row["CustomerName"].ToString();


                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOS> GetReportOOSAccountBranchExcel(int weekFrom, int weekTo, string branch, int year,string role, string user)
        {

            string[] listBranch = branch.Split(',');
            var listFinal = new List<Model.mdlOOS>();


            foreach(string br in listBranch)
            {
                var listAccount = AccountFacade.GetAccount(br);
                string account = "";
                foreach(var acc in listAccount)
                {
                    if(acc == listAccount.Last())
                    {
                        account += acc.AccountID;
                    }
                    else
                    {
                        account += acc.AccountID + ",";
                    }
                }
                var accountBranch = GetReportOOSByAccountBranch(weekFrom,weekTo,account,br,year,role,user);


                var listWeek = accountBranch.Select(fld=>fld.VisitWeek).Distinct().ToList();

                foreach (int i in listWeek)
                {
                    foreach (var acc in listAccount)
                    {
                        var temp = accountBranch.FirstOrDefault(fld => fld.Account.Equals(acc.AccountID) && fld.VisitWeek.Equals(i));
                        if (temp == null)
                        {
                            var model = new Model.mdlOOS();
                            model.Account = acc.AccountID;
                            model.BranchID = br;
                            model.Listed = 0;
                            model.VisitWeek = i;
                            model.OutOfStock = 0;
                            model.OOS = 0;
                            listFinal.Add(model);
                        }
                    }
                }


                //foreach (var acc in listAccount)
                //{
                //    var temp = accountBranch.FirstOrDefault(fld => fld.Account.Equals(acc.AccountID));
                //    if (temp == null)
                //    {
                //        foreach (int i in listWeek)
                //        {
                //            var model = new Model.mdlOOS();
                //            model.Account = acc.AccountID;
                //            model.BranchID = br;
                //            model.Listed = 0;
                //            model.VisitWeek = i;
                //            model.OutOfStock = 0;
                //            model.OOS = 0;
                //            listFinal.Add(model);
                //        }
                //    }
                //}

                listFinal.AddRange(accountBranch);

            }

            






            return listFinal;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Report OOS SKU

        public static List<Model.mdlOOSProductPerBranch> GetOOSProductPerBranchBySKU(int weekFrom, int weekTo, string branch, string product, int year,string role, string user)
        {
            string key = "GetReportOOSByBranch";
            var list = new List<Model.mdlOOSProductPerBranch>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
                new SqlParameter() {ParameterName = "@product", SqlDbType = SqlDbType.NVarChar, Value = product },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerBranchBySKU";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProductPerBranch();
                model.BranchID = row["BranchID"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();

                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);

                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSProductPerAccountBranch> GetOOSProductPerAccountBranchBySKU(int weekFrom, int weekTo, string branch, string product, int year,string role, string user)
        {
            string key = "GetOOSProductPerAccountBranchBySKU";
            var list = new List<Model.mdlOOSProductPerAccountBranch>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
                
                new SqlParameter() {ParameterName = "@product", SqlDbType = SqlDbType.NVarChar, Value = product },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerAccountBranchBySKU";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProductPerAccountBranch();
                model.BranchID = row["BranchID"].ToString();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();

                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);

                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSCustomer> GetOOSCustomersAccountBranchBySKU(int visitWeek, int year, string productID, string account, string branch,string role, string user)
        {
            string key = "GetOOSCustomersAccountBranchBySKU";
            var list = new List<Model.mdlOOSCustomer>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@visitWeek", SqlDbType = SqlDbType.Int, Value = visitWeek },

                new SqlParameter() {ParameterName = "@productID", SqlDbType = SqlDbType.NVarChar, Value = productID },
              
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                 new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.Int, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSCustomersAccountBranchBySKU";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSCustomer();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.CustomerID = row["CustomerID"].ToString();
                model.Status = row["Status"].ToString();
                model.Channel = row["Channel"].ToString();
                model.BranchID = row["BranchID"].ToString();
                model.CustomerName = row["CustomerName"].ToString();


                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSProduct> GetOOSProductPerAccountBySKU(int weekFrom, int weekTo, string account, int year,string product,string role, string user)
        {
            string key = "GetOOSProductPerAccountBySKU";
            var list = new List<Model.mdlOOSProduct>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@product", SqlDbType = SqlDbType.NVarChar, Value = product },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerAccountBySKU";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProduct();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();

                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);

                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSByBranchSKU> GetOOSByBranchSKU(int weekFrom, int weekTo, string branchid, int year, string productid,string role, string user)
        {
            string key = "GetOOSByBranchSKU";
            var list = new List<Model.mdlOOSByBranchSKU>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
                new SqlParameter() {ParameterName = "@productid", SqlDbType = SqlDbType.NVarChar, Value = productid },
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branchid },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSByBranchSKU";

            DataTable dt = DataFacade.GetSP(sql, user, key, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSByBranchSKU();
                model.BranchID = row["BranchID"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.ProductID = row["ProductID"].ToString();
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOSByAccountBranchSKU> GetOOSByAccountBranchSKU(int weekFrom, int weekTo, string branch, string product, int year,string role, string user)
        {
            string key = "GetReportOOSByBranch";
            var list = new List<Model.mdlOOSByAccountBranchSKU>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
                
                new SqlParameter() {ParameterName = "@product", SqlDbType = SqlDbType.NVarChar, Value = product },

        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }

            };

            string sql = @"spOOSByAccountBranchSKU";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSByAccountBranchSKU();
                model.BranchID = row["BranchID"].ToString();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.ProductID = row["ProductID"].ToString();
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                model.ProductName = row["ProductName"].ToString();
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }

            return list;


        }
    
        public static List<Model.mdlOOSByAccountSKU> GetOOSByAccountSKU(int weekFrom, int weekTo, string account, string product, int year,string role,string user)
        {
            string key = "GetOOSByAccountSKU";
            var list = new List<Model.mdlOOSByAccountSKU>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                
                new SqlParameter() {ParameterName = "@product", SqlDbType = SqlDbType.NVarChar, Value = product },
        
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSByAccountSKU";


            DataTable dt = DataFacade.GetSP(sql,user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSByAccountSKU();
               
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.ProductID = row["ProductID"].ToString();
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                model.ProductName = row["ProductName"].ToString();
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }

            return list;
        }
        /////////////////////////////////////////DISTRIBUTOR

        public static List<Model.mdlOOS> GetReportOOSByBranchDistributor(int weekFrom, int weekTo, string branchid, int year,string distributor,string role,string user)
        {
            string key = "GetReportOOSByBranchDistributor";
            var list = new List<Model.mdlOOS>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branchid },
                new SqlParameter() {ParameterName = "@distributor", SqlDbType = SqlDbType.NVarChar, Value = distributor },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            DataTable dt = DataFacade.GetSP("spReportOOSByBranchDistributor", user, key, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOS();
                model.BranchID = row["BranchID"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOS> GetReportOOSByAccountDistributor(int weekFrom, int weekTo, string account, int year, string distributor,string role,string user)
        {
            string key = "GetReportOOSByAccountDistributor";
            var list = new List<Model.mdlOOS>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
                new SqlParameter() {ParameterName = "@distributor", SqlDbType = SqlDbType.NVarChar, Value = distributor },
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            DataTable dt = DataFacade.GetSP("spReportOOSByAccountDistributor", user, key, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOS();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOS> GetReportOOSByAccountBranchDistributor(int weekFrom, int weekTo, string account, string branch, int year,string distributor,string role, string user)
        {
            string key = "GetReportOOSByAccountBranchDistributor";
            var list = new List<Model.mdlOOS>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
                new SqlParameter() {ParameterName = "@distributor", SqlDbType = SqlDbType.NVarChar, Value = distributor },
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            DataTable dt = DataFacade.GetSP("spReportOOSByAccountBranchDistributor", user, key, sp);

            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOS();
                model.BranchID = branch;
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt16(row["VisitWeek"]);
                model.Listed = Convert.ToInt32(row["listed"]);
                model.OutOfStock = Convert.ToInt32(row["outofstock"]);
                if (row.IsNull("oos"))
                {
                    model.OOS = 0;
                }
                else
                {
                    model.OOS = Convert.ToDecimal(row["oos"]);
                }
                list.Add(model);
            }


            return list;
        }

        public static List<Model.mdlOOSProduct> GetOOSProductPerAccountByDistributor(int weekFrom, int weekTo, string account, int year,string distributor,string role, string user)
        {
            string key = "GetOOSProductPerAccountByDistributor";
            var list = new List<Model.mdlOOSProduct>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@distributor", SqlDbType = SqlDbType.NVarChar, Value = distributor },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerAccountByDistributor";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProduct();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();

                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);

                list.Add(model);

            }

            return list;


        }

        public static List<Model.mdlOOSProduct> GetOOSProductPerAccountBranchByDistributor(int weekFrom, int weekTo, string account, string branch, int year,string distributor,string role, string user)
        {
            string key = "GetOOSProductPerAccountBranchByDistributor";
            var list = new List<Model.mdlOOSProduct>();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@weekFrom", SqlDbType = SqlDbType.Int, Value = weekFrom },
                new SqlParameter() {ParameterName = "@weekTo", SqlDbType = SqlDbType.Int, Value = weekTo },
           
                new SqlParameter() {ParameterName = "@account", SqlDbType = SqlDbType.NVarChar, Value = account },
                new SqlParameter() {ParameterName = "@branch", SqlDbType = SqlDbType.NVarChar, Value = branch },
                new SqlParameter() {ParameterName = "@distributor", SqlDbType = SqlDbType.NVarChar, Value = distributor },
                new SqlParameter() {ParameterName = "@year", SqlDbType = SqlDbType.NVarChar, Value = year },
                 new SqlParameter() {ParameterName = "@role", SqlDbType = SqlDbType.NVarChar, Value = role }
            };

            string sql = @"spOOSProductPerAccountBranchByDistributor";


            DataTable dt = DataFacade.GetSP(sql, user, key, sp);
            foreach (DataRow row in dt.Rows)
            {
                var model = new Model.mdlOOSProduct();
                model.Account = row["Account"].ToString();
                model.VisitWeek = Convert.ToInt32(row["VisitWeek"]);
                model.ProductID = row["ProductID"].ToString();
                model.ProductName = row["ProductName"].ToString();

                model.OOSCustomer = Convert.ToInt32(row["OOSCustomer"]);

                list.Add(model);

            }

            return list;


        }

        //<<001 Andy Yo
        public static Model.mdlBarChart LoadOOSDashboard(string lWeekFrom, string lWeekTo, string lYear, string user)
        {

            var mdlOOSAreaList = new List<Model.mdlOOSArea>();



            var mdlOOSArea = new Model.mdlOOSArea();
            int nx = 0;

            var dataChart = new Model.mdlBarChart();
            List<string> listBranch = new List<string>();
            List<decimal> listWeekFrom = new List<decimal>();
            List<decimal> listWeekTo = new List<decimal>();
            List<Model.Dataset> listDetail = new List<Model.Dataset>();


            var dataDetailFrom = new Model.Dataset();
            dataDetailFrom.label = "Percent OOS Week " + lWeekFrom;
            dataDetailFrom.backgroundColor = "#26B99A";

            var dataDetailTo = new Model.Dataset();
            dataDetailTo.label = "Percent OOS Week " + lWeekTo;
            dataDetailTo.backgroundColor = "#03586A";

            var allBranch = Manager.BranchFacade.LoadBranch();

            foreach (var branch in allBranch)
            {
                string branchID = branch.BranchDescription.ToString();
                listBranch.Add(branchID);
                var listOSAArea = GetReportOOSByBranch(Convert.ToInt32(lWeekFrom), Convert.ToInt32(lWeekTo), branchID, Convert.ToInt32(lYear),"", user);
                decimal percentFrom = 0;
                decimal percentTo = 0;
                foreach (var drOSA in listOSAArea)
                {

                    //dataChart.labels.Add(branchID);     
                    if (drOSA.VisitWeek.ToString() == lWeekFrom)
                    {
                        percentFrom = Convert.ToDecimal(drOSA.OOS.ToString().Replace('%', ' ').Trim());


                    }
                    else
                    {
                        percentTo = Convert.ToDecimal(drOSA.OOS.ToString().Replace('%', ' ').Trim());
                    }

                }
                listWeekFrom.Add(percentFrom);
                listWeekTo.Add(percentTo);
            }


            dataDetailFrom.data = listWeekFrom;
            dataDetailTo.data = listWeekTo;

            dataChart.labels = listBranch;
            listDetail.Add(dataDetailFrom);
            listDetail.Add(dataDetailTo);
            dataChart.datasets = listDetail;
            return dataChart;
        }
        //001 Andy Yo>>
    }
}
