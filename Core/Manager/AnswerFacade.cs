﻿/* documentation
 * 001 nanda 13 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Core.Model;

namespace Core.Manager
{
    public class AnswerFacade : Base.Manager
    {

        public static List<Model.mdlAnswer> LoadAnswer(string lQuestionID, Boolean IsSubQuestion,Boolean IsActive, string user)
        {
            string key = "LoadAnswer";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadAnswer", user, key, sp);
            var mdlAnswerList = new List<Model.mdlAnswer>();
            foreach (DataRow dr in dt.Rows)
            {
                var mdlAnswer = new Model.mdlAnswer();
                mdlAnswer.AnswerID = dr["AnswerID"].ToString();
                mdlAnswer.AnswerText = dr["AnswerText"].ToString();
                mdlAnswer.IsActive = Boolean.Parse(dr["IsActive"].ToString());
                mdlAnswer.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
                mdlAnswer.No = dr["No"].ToString();
                mdlAnswer.QuestionID = dr["QuestionID"].ToString();
                mdlAnswer.Sequence = Int32.Parse(dr["Sequence"].ToString());
                mdlAnswer.SubQuestion = Boolean.Parse(dr["SubQuestion"].ToString());
                mdlAnswerList.Add(mdlAnswer);

            }

            return mdlAnswerList;
        }

        public static Int32 LoadAnswerSeq(string lQuestionID, string user)
        {
            string key = "LoadAnswerSeq";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID}
            };

            Int32 lSeq = 0;
            DataTable dt = Manager.DataFacade.GetSP("spLoadAnswerSeq", user, key, sp);
            var mdlAnswerList = new List<Model.mdlAnswer>();
            foreach (DataRow dr in dt.Rows)
            {
                lSeq = Int32.Parse(dr["Sequence"].ToString()) + 1;
            }

            return lSeq;
        }

        public static String InsertAnswer(Model.mdlAnswer lParam, string user)
        {
            string key = "InsertAnswer";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerText", SqlDbType = SqlDbType.NText, Value = lParam.AnswerText},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@SubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.SubQuestion},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@CreatedBy", SqlDbType = SqlDbType.NVarChar, Value = user},
            };

            result = Manager.DataFacade.GetSP_Void(@"spInsertAnswer", user, key, sp);
            return result;
        }

        public static String InsertSubAnswer(Model.mdlAnswer lParam, string user)
        {
            string key = "InsertSubAnswer";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerText", SqlDbType = SqlDbType.NText, Value = lParam.AnswerText},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@SubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.SubQuestion},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@CreatedBy", SqlDbType = SqlDbType.NVarChar, Value = user},
            };

            result = Manager.DataFacade.GetSP_Void(@"spInsertAnswer", user, key, sp);
            return result;
        }

        public static String UpdateAnswer(Model.mdlAnswer lParam, string user)
        {
            string key = "UpdateAnswer";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerText", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerText},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@SubQuestion", SqlDbType = SqlDbType.NVarChar, Value = lParam.SubQuestion},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spUpdateAnswer", user, key, sp);

            return result;
        }

        public static String NonActiveAnswer(string lAnswerID, string user)
        {
            string key = "NonActiveAnswer";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lAnswerID},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user},
            };
            result = Manager.DataFacade.GetSP_Void("spNonActiveAnswer", user, key, sp);

            return result;
        }

        //public static List<Model.mdlAnswer> LoadSubAnswer(string lQuestionID)
        //{

        //    List<SqlParameter> sp = new List<SqlParameter>()
        //    {
        //        new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID}
        //    };

        //    DataTable dt = Manager.DataFacade.GetSP("spLoadSubAnswer", sp);
        //    var mdlAnswerList = new List<Model.mdlAnswer>();
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        var mdlAnswer = new Model.mdlAnswer();
        //        mdlAnswer.AnswerID = dr["AnswerID"].ToString();
        //        mdlAnswer.AnswerText = dr["AnswerText"].ToString();
        //        mdlAnswer.IsActive = Boolean.Parse(dr["AnswerText"].ToString());
        //        mdlAnswer.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
        //        mdlAnswer.No = dr["No"].ToString();
        //        mdlAnswer.QuestionID = dr["QuestionID"].ToString();
        //        mdlAnswer.Sequence = Int32.Parse(dr["QuestionID"].ToString());
        //        mdlAnswer.SubQuestion = Boolean.Parse(dr["SubQuestion"].ToString());
        //        mdlAnswerList.Add(mdlAnswer);

        //    }

        //    return mdlAnswerList;
        //}

        public static Boolean CheckAnswer(string lAnswerID, string user)
        {
            string key = "CheckAnswer";
            Boolean lCheck = false;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lAnswerID}
            };

            DataTable dtQS = Manager.DataFacade.GetSP("spCheckAnswer", user, key, sp);
            foreach (DataRow drQS in dtQS.Rows)
            {
                lCheck = true;
            }

            return lCheck;
        }

        public static String GenerateAnswerID(Boolean IsSubQuestion, string user)
        {
            string key = "GenerateAnswerID";
            String lAnswerID = "";
            int runningNo = 0;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion}
            };

            DataTable dt = Manager.DataFacade.GetSP("spGetLastAnswerID", user, key, sp);
            foreach (DataRow dr in dt.Rows)
            {
                runningNo = Int32.Parse( dr["AnswerID"].ToString().Split('-')[3]) + 1;
            }

            String code = "";
            if (IsSubQuestion == false)
                code = "ANM-";
            else
                code = "ANS-";


            lAnswerID = code + DateTime.Now.ToString("yyyy-MM-") + runningNo.ToString("0000");

            return lAnswerID;
        }


    }
}
