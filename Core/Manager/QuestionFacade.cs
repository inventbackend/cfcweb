﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Core.Model;

namespace Core.Manager
{
    public class QuestionFacade :Base.Manager
    {

        public static Model.mdlQuestion LoadQuestionbyAnswerID(string lAnswerID, string user)
        {
            string key = "LoadQuestionbyAnswerID";
            var mdlQuestion = new Model.mdlQuestion();
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lAnswerID}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionbyAnswerIDzz",user, key, sp);
            foreach (DataRow dr in dt.Rows)
            {
                
                mdlQuestion.AnswerID = dr["AnswerID"].ToString();
                mdlQuestion.AnswerTypeID = dr["AnswerTypeID"].ToString();
                mdlQuestion.IsActive = Boolean.Parse(dr["IsActive"].ToString());
                mdlQuestion.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
                mdlQuestion.Mandatory = Boolean.Parse(dr["Mandatory"].ToString());
                mdlQuestion.No = dr["No"].ToString();
                mdlQuestion.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
                mdlQuestion.QuestionID = dr["QuestionID"].ToString();
                mdlQuestion.QuestionSetID = dr["QuestionSetID"].ToString();
                mdlQuestion.QuestionText = dr["QuestionText"].ToString();
                mdlQuestion.Sequence = Int32.Parse(dr["Sequence"].ToString());


            }

            return mdlQuestion;
        }

        public static List<Model.mdlQuestion> LoadQuestion(String lQuestionSetID, Boolean IsSubQuestion, Boolean IsActive, string user)
        {
            string key = "LoadQuestion";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestion", user, key, sp);
            var mdlQuestionList = new List<Model.mdlQuestion>();
            foreach (DataRow dr in dt.Rows)
            {
                var mdlQuestion = new Model.mdlQuestion();
                mdlQuestion.AnswerID = dr["AnswerID"].ToString();
                mdlQuestion.AnswerTypeID = dr["AnswerTypeID"].ToString();
                mdlQuestion.IsActive = Boolean.Parse(dr["IsActive"].ToString());
                mdlQuestion.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
                mdlQuestion.Mandatory = Boolean.Parse(dr["Mandatory"].ToString());
                mdlQuestion.No = dr["No"].ToString();
                mdlQuestion.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
                mdlQuestion.QuestionID = dr["QuestionID"].ToString();
                mdlQuestion.QuestionSetID = dr["QuestionSetID"].ToString();
                mdlQuestion.QuestionText = dr["QuestionText"].ToString();
                mdlQuestion.Sequence = Int32.Parse(dr["Sequence"].ToString());

                mdlQuestion.IsMultiple = Boolean.Parse( dr["IsMultiple"].ToString());
                mdlQuestion.QuestionCategoryText = dr["QuestionCategoryText"].ToString();
                mdlQuestion.AnswerTypeText = dr["AnswerTypeText"].ToString();

                mdlQuestionList.Add(mdlQuestion);
            }

            return mdlQuestionList;
        }

        public static Int32 LoadQuestionSeq(string lQuestionSetID, string user)
        {
            string key = "LoadQuestionSeq";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID}
            };

            Int32 lSeq = 0;
            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionSeq",user, key, sp);
            var mdlAnswerList = new List<Model.mdlAnswer>();
            foreach (DataRow dr in dt.Rows)
            {
                lSeq = Int32.Parse(dr["Sequence"].ToString()) + 1;
            }

            return lSeq;
        }

        public static String InsertQuestion(Model.mdlQuestion lParam, string user)
        {
            string key = "InsertQuestion";
            string result = "";

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerTypeID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParam.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionText},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = lParam.User}

            };
            result = Manager.DataFacade.GetSP_Void(@"spInsertQuestion", user, key, sp);

            foreach (String item in lParam.Role)
            {
                sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                    new SqlParameter() {ParameterName = "@RoleID", SqlDbType = SqlDbType.NVarChar, Value = item}
                };

                result = Manager.DataFacade.GetSP_Void(@"spInsertQuestionRole", user, key, sp);
            }

            return result;
        }

        public static String InsertQuestionNotMultiple(Model.mdlQuestion lParamQuestion, Model.mdlAnswer lParamAnswer, Boolean lIsSubQuestion, String lQuestionID, string user)
        {
            string key = "InsertQuestionNotMultiple";
            string result = "";
            List<SqlParameter> sp1 = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.AnswerTypeID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParamQuestion.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParamQuestion.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParamQuestion.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionText},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParamQuestion.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}

            };

            lParamAnswer.AnswerID = AnswerFacade.GenerateAnswerID(lIsSubQuestion, user);
            List<SqlParameter> sp2 = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParamAnswer.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerText", SqlDbType = SqlDbType.NText, Value = lParamAnswer.AnswerText},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParamAnswer.QuestionID},
                new SqlParameter() {ParameterName = "@SubQuestion", SqlDbType = SqlDbType.Bit, Value = lParamAnswer.SubQuestion},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParamAnswer.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParamAnswer.Sequence},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParamAnswer.No},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParamAnswer.IsActive},
                new SqlParameter() {ParameterName = "@CreatedBy", SqlDbType = SqlDbType.NVarChar, Value = user},
            };

            if (lQuestionID == "")
            {
                result = Manager.DataFacade.TransSP_Void_2sp(@"spInsertQuestion", sp1, "spInsertAnswer", sp2, user, key);
            }
            else
            {
                List<SqlParameter> sp3 = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                    new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.AnswerID},
                };

                result = Manager.DataFacade.TransSP_Void_2sp(@"spInsertQuestion", sp1, "spInsertAnswer", sp2, "spUpdateAnswerToSub", sp3, user, key);
            }

            List<SqlParameter>  spInsertQuestionRole = new List<SqlParameter>();
            foreach (String item in lParamQuestion.Role)
            {
                spInsertQuestionRole = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParamQuestion.QuestionID},
                    new SqlParameter() {ParameterName = "@RoleID", SqlDbType = SqlDbType.NVarChar, Value = item}
                };

                result = Manager.DataFacade.GetSP_Void(@"spInsertQuestionRole", user, key, spInsertQuestionRole);
            }

            return result;
        }

        public static String UpdateQuestion(Model.mdlQuestion lParam, string user)
        {
            string key = "UpdateQuestion";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerTypeID},  
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParam.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionText},
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.Int, Value = lParam.User}
            };
            result = Manager.DataFacade.GetSP_Void("spUpdateQuestion", user, key, sp);

            //delete question role by question id
            sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID}
            };
            result = Manager.DataFacade.GetSP_Void("spDeleteQuestionRole", user, key, sp);

            //reinsert question role by question id
            foreach (String item in lParam.Role)
            {
                sp = new List<SqlParameter>()
                {
                    new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                    new SqlParameter() {ParameterName = "@RoleID", SqlDbType = SqlDbType.NVarChar, Value = item}
                };

                result = Manager.DataFacade.GetSP_Void(@"spInsertQuestionRole", user, key, sp);
            }

            return result;
        }

        public static String NonActiveQuestion(String lQuestionID, string user)
        {
            string key = "NonActiveQuestion";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spNonActiveQuestion", user, key, sp);

            return result;
        }

        public static String InsertSubQuestion(Model.mdlQuestion lParam, string lQuestionID, string user)
        {
            string key = "InsertSubQuestion";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionID},
                new SqlParameter() {ParameterName = "@QuestionText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionText},
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
                new SqlParameter() {ParameterName = "@AnswerTypeID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerTypeID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = lParam.IsActive},
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = lParam.IsSubQuestion},
                new SqlParameter() {ParameterName = "@Mandatory", SqlDbType = SqlDbType.Bit, Value = lParam.Mandatory},
                new SqlParameter() {ParameterName = "@No", SqlDbType = SqlDbType.NVarChar, Value = lParam.No},
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},            
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID}, 
                new SqlParameter() {ParameterName = "@Sequence", SqlDbType = SqlDbType.Int, Value = lParam.Sequence},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}

            };

            List<SqlParameter> sp2 = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID},
                new SqlParameter() {ParameterName = "@AnswerID", SqlDbType = SqlDbType.NVarChar, Value = lParam.AnswerID},
            };
            result = Manager.DataFacade.TransSP_Void_2sp("spInsertQuestion", sp, "spUpdateAnswerToSub", sp2, user, key);
            return result;
        }

        public static String GenerateQuestionID(Boolean IsSubQuestion, string user)
        {
            string key = "GenerateQuestionID";
            String lQuestionID = "";
            int runningNo = 0;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion}
            };

            DataTable dt = Manager.DataFacade.GetSP("spGetLastQuestionID", user, key, sp);
            foreach (DataRow dr in dt.Rows)
            {
                String ID = dr["QuestionID"].ToString().Split('-')[3];
                runningNo = Int32.Parse(ID) + 1;
            }
            String code = "";
            if (IsSubQuestion == false)
                code = "QNM-";
            else
                code = "QNS-";


            lQuestionID = code + DateTime.Now.ToString("yyyy-MM-") + runningNo.ToString("0000");

            return lQuestionID;
        }

        public static List<String> GetQuestionRole(String lQuestionID, string user)
        {
            string key = "LoadQuestion";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionID}
            };

            DataTable dt = Manager.DataFacade.GetSP("spGetQuestionRole",user, key, sp);
            var mdlQuestionList = new List<Model.mdlQuestion>();
            List<String> roleList = new List<String>();
            foreach (DataRow dr in dt.Rows)
            {
                var mdlQuestion = new Model.mdlQuestion();
                mdlQuestion.QuestionID = dr["QuestionID"].ToString();
                roleList.Add(dr["RoleID"].ToString());

                mdlQuestionList.Add(mdlQuestion);
            }

            return roleList;
        }

        //public static Model.mdlQuestion LoadQuestionbyId(String lQuestionSetID, Boolean IsSubQuestion, String lQuestionID)
        //{
        //    List<SqlParameter> sp = new List<SqlParameter>()
        //    {
        //        new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
        //        new SqlParameter() {ParameterName = "@QuestionID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
        //        new SqlParameter() {ParameterName = "@IsSubQuestion", SqlDbType = SqlDbType.Bit, Value = IsSubQuestion}
        //    };

        //    DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionbyId", sp);
        //    var mdlQuestion = new Model.mdlQuestion();
        //    foreach (DataRow dr in dt.Rows)
        //    {

        //        mdlQuestion.AnswerID = dr["AnswerID"].ToString();
        //        mdlQuestion.AnswerTypeID = dr["AnswerTypeID"].ToString();
        //        mdlQuestion.IsActive = Boolean.Parse(dr["IsActive"].ToString());
        //        mdlQuestion.IsSubQuestion = Boolean.Parse(dr["IsSubQuestion"].ToString());
        //        mdlQuestion.Mandatory = Boolean.Parse(dr["Mandatory"].ToString());
        //        mdlQuestion.No = dr["No"].ToString();
        //        mdlQuestion.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
        //        mdlQuestion.QuestionID = dr["QuestionID"].ToString();
        //        mdlQuestion.QuestionSetID = dr["QuestionSetID"].ToString();
        //        mdlQuestion.QuestionText = dr["QuestionText"].ToString();
        //        mdlQuestion.Sequence = Int32.Parse(dr["Sequence"].ToString());


        //    }

        //    return mdlQuestion;
        //}
    }
}
