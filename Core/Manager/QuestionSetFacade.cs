﻿/* documentation
 * 001 nanda 13 Okt 2016
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Core.Model;

namespace Core.Manager
{
    public class QuestionSetFacade : Base.Manager
    {
        public static List<Model.mdlQuestion_Set> LoadQuestionSetDDL(Boolean IsActive, string user)
        {
            string key = "LoadQuestionSetDDL";
            var mdlQuestionSetList = new List<Model.mdlQuestion_Set>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestion_Set", user, key, sp);

            foreach (DataRow dr in dt.Rows)
            {
                var mdlQuestion_Set = new Model.mdlQuestion_Set();
                mdlQuestion_Set.QuestionSetID = dr["QuestionSetID"].ToString();
                mdlQuestion_Set.QuestionSetText = dr["QuestionSetID"].ToString() + " - " + dr["QuestionSetText"].ToString();

                mdlQuestionSetList.Add(mdlQuestion_Set);
            }
            return mdlQuestionSetList;
        }

        public static List<Model.mdlQuestion_Set> LoadQuestionSet(Boolean IsActive, string user)
        {
            string key = "LoadQuestionSet";
            var mdlQuestion_SetList = new List<Model.mdlQuestion_Set>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dtBranch = Manager.DataFacade.GetSP("spLoadQuestion_Set", user, key, sp);

            foreach (DataRow drBranch in dtBranch.Rows)
            {
                var mdlQuestion_Set = new Model.mdlQuestion_Set();
                mdlQuestion_Set.QuestionSetID = drBranch["QuestionSetID"].ToString();
                mdlQuestion_Set.QuestionSetText = drBranch["QuestionSetText"].ToString();

                mdlQuestion_SetList.Add(mdlQuestion_Set);
            }


            return mdlQuestion_SetList;
        }

        public static List<Model.mdlQuestion_Set> LoadSomeQuestion_Set2(string keyword, string QuestionSetID, string user)
        {
            string key = "LoadSomeQuestion_Set2";
            var mdlQuestion_SetList = new List<Model.mdlQuestion_Set>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = QuestionSetID},
                new SqlParameter() {ParameterName = "@keyword", SqlDbType = SqlDbType.NVarChar, Value ='%'+keyword+'%'}
            };

            DataTable dtBranch = Manager.DataFacade.GetSP("spLoadQuestion_Set2", user, key, sp);

            foreach (DataRow drBranch in dtBranch.Rows)
            {
                var mdlQuestion_Set = new Model.mdlQuestion_Set();
                mdlQuestion_Set.QuestionSetID = drBranch["QuestionSetID"].ToString();
                mdlQuestion_Set.QuestionSetText = drBranch["QuestionSetText"].ToString();
                mdlQuestion_SetList.Add(mdlQuestion_Set);
            }
            return mdlQuestion_SetList;
        }

        public static Boolean CheckQuestionSet(String lQuestionSetID, string user)
        {
            string key = "CheckQuestionSet";
            Boolean lCheck = false;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID}
            };

            DataTable dtQS = Manager.DataFacade.GetSP("spCheckQuestionSet", user, key, sp);
            foreach (DataRow drQS in dtQS.Rows)
            {
                lCheck = true;
            }

            return lCheck;
        }

        public static String InsertQuestionSet(Model.mdlQuestion_Set lParam, string user)
        {
            string key = "InsertQuestionSet";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionSetText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetText},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };


            result = Manager.DataFacade.GetSP_Void("spInsertQuestion_Set", user, key, sp);

            return result;
        }

        public static String UpdateQuestionSet(Model.mdlQuestion_Set lParam, string user)
        {
            string key = "UpdateQuestionSet";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@QuestionSetText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetText},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spUpdateQuestion_Set", user, key, sp);

            return result;
        }

        public static String DeleteQuestionSet(string lQuestionSetID, string user)
        {
            string key = "DeleteQuestionSet";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID}
            };
            result = Manager.DataFacade.GetSP_Void(@"spDeleteQuestion_Set", user, key, sp);

            return result;
        }

        public static String GenerateQuestionSetID(string user)
        {
            string key = "GenerateQuestionSetID";
            String lQuestionID = "";
            int runningNo = 0;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            DataTable dt = Manager.DataFacade.GetSP("spGetLastQuestionSetID",user, key, sp);
            foreach (DataRow dr in dt.Rows)
            {
                String ID = dr["QuestionSetID"].ToString().Split('-')[1];
                runningNo = Int32.Parse(ID) + 1;
            }
            lQuestionID = "QST-" +  runningNo.ToString("000");

            return lQuestionID;
        }

        public static String NonActiveQuestionSet(String lQuestionSetID, string user)
        {
            string key = "NonActiveQuestionSet";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionSetID},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spNonActiveQuestionSet", user, key, sp);

            return result;
        }


        //public static List<Model.mdlQuestion_Set> GetSearch(string keyword, string lQuestionSetID)
        //{
        //    var QuestionSetlist = DataContext.Question_Sets.Where(fld => fld.QuestionSetID.Contains(lQuestionSetID) && fld.QuestionSetText.Contains(keyword)).OrderBy(fld => fld.QuestionSetID).ToList();

        //    var mdlQuestionSetList = new List<Model.mdlQuestion_Set>();
        //    foreach (var question in mdlQuestionSetList)
        //    {
        //        var mdlQuestion_Set = new Model.mdlQuestion_Set();
        //        mdlQuestion_Set.QuestionSetID = question.QuestionSetID;
        //        mdlQuestion_Set.QuestionSetText = question.QuestionSetText;

        //        mdlQuestionSetList.Add(mdlQuestion_Set);
        //    }

        //    return mdlQuestionSetList;
        //}


        //public static Model.mdlQuestion_Set LoadQuestionSetByID(string lQuestionSetID)
        //{
        //    var mdlQuestion_Set = new Model.mdlQuestion_Set();
        //    List<SqlParameter> sp = new List<SqlParameter>()
        //    {
        //        new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = '%'+lQuestionSetID+'%'}
        //    };

        //    DataTable dtBranch = Manager.DataFacade.DTSQLCommand("SELECT QuestionSetID,QuestionSetText FROM Question_Set where QuestionSetID  LIKE @QuestionSetID", sp);
        //    var listBranch = new List<string>();
        //    foreach (DataRow drBranch in dtBranch.Rows)
        //    {            
        //        mdlQuestion_Set.QuestionSetID = drBranch["QuestionSetID"].ToString();
        //        mdlQuestion_Set.QuestionSetText = drBranch["QuestionSetText"].ToString();
        //    }

        //    return mdlQuestion_Set;
        //}
        
    }
}
