﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Core.Model;

namespace Core.Manager
{
    public class QuestionCategoryFacade : Base.Manager
    {
        public static List<Model.mdlQuestion_Category> LoadQuestionCategoryDDL(Boolean IsActive, string user)
        {
            string key = "LoadQuestionCategoryDDL";
            var mdlQuestion_CategoryList = new List<Model.mdlQuestion_Category>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dt = Manager.DataFacade.GetSP("spLoadQuestionCategory", user, key, sp);

            foreach (DataRow dr in dt.Rows)
            {
                var mdlQuestion_Category = new Model.mdlQuestion_Category();
                mdlQuestion_Category.QuestionCategoryID = dr["QuestionCategoryID"].ToString();
                mdlQuestion_Category.QuestionCategoryText = dr["QuestionCategoryID"].ToString() + " - " + dr["QuestionCategoryText"].ToString();

                mdlQuestion_CategoryList.Add(mdlQuestion_Category);
            }
            return mdlQuestion_CategoryList;
        }

        public static List<Model.mdlQuestion_Category> LoadQuestionCategory(Boolean IsActive, string user)
        {
            string key = "LoadQuestionCategory";
            var mdlQuestionCategoryList = new List<Model.mdlQuestion_Category>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dtBranch = Manager.DataFacade.GetSP("spLoadQuestionCategory", user, key, sp);

            foreach (DataRow drBranch in dtBranch.Rows)
            {
                var mdlQuestion_Category = new Model.mdlQuestion_Category();
                mdlQuestion_Category.QuestionCategoryID = drBranch["QuestionCategoryID"].ToString();
                mdlQuestion_Category.QuestionCategoryText = drBranch["QuestionCategoryText"].ToString();

                mdlQuestionCategoryList.Add(mdlQuestion_Category);
            }


            return mdlQuestionCategoryList;
        }

        public static String NonActiveQuestioncategory(String lQuestionCategoryID, string user)
        {
            string key = "NonActiveQuestioncategory";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionCategoryID},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spNonActiveQuestionCategory", user, key, sp);

            return result;
        }

        public static String InsertQuestionCategory(Model.mdlQuestion_Category lParam, string user)
        {
            string key = "InsertQuestionCategory";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionCategoryText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryText},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };


            result = Manager.DataFacade.GetSP_Void("spInsertQuestionCategory", user, key, sp);

            return result;
        }

        public static String UpdateQuestionCategory(Model.mdlQuestion_Category lParam, string user)
        {
            string key = "UpdateQuestionCategory";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionCategoryText", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryText},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spUpdateQuestionCategory", user, key, sp);

            return result;
        }

        public static String GenerateQuestionCategoryID(string user)
        {
            string key = "GenerateQuestionCategoryID";
            String lQuestionID = "";
            int runningNo = 0;
            List<SqlParameter> sp = new List<SqlParameter>()
            {
            };

            DataTable dt = Manager.DataFacade.GetSP("spGetLastQuestionCategoryID", user, key, sp);
            foreach (DataRow dr in dt.Rows)
            {
                String ID = dr["QuestionCategoryID"].ToString().Split('-')[1];
                runningNo = Int32.Parse(ID) + 1;
            }
            lQuestionID = "QCD-" + runningNo.ToString("0000");

            return lQuestionID;
        }


        //=======================================Question Detail=========================================


        public static List<Model.mdlQuestion_CategoryDetail> LoadQuestionCategoryDetail(String lQuestionCategoryID, Boolean IsActive, string user)
        {
            string key = "GetReportOOSByBranch";
            var lmdlQuestion_CategoryDetailList = new List<Model.mdlQuestion_CategoryDetail>();

            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionCategoryID},
                new SqlParameter() {ParameterName = "@IsActive", SqlDbType = SqlDbType.Bit, Value = IsActive}
            };

            DataTable dtBranch = Manager.DataFacade.GetSP("spLoadQuestionCategoryDetail",user, key, sp);

            foreach (DataRow drBranch in dtBranch.Rows)
            {
                var lmdlQuestion_CategoryDetail = new Model.mdlQuestion_CategoryDetail();
                lmdlQuestion_CategoryDetail.QuestionCategoryID = drBranch["QuestionCategoryID"].ToString();
                lmdlQuestion_CategoryDetail.QuestionSetID = drBranch["QuestionSetID"].ToString();

                lmdlQuestion_CategoryDetailList.Add(lmdlQuestion_CategoryDetail);
            }


            return lmdlQuestion_CategoryDetailList;
        }

        //public static String NonActiveQuestionCategory(String lQuestionCategoryID)
        //{
        //    Globals.gReturn_Status = "";
        //    List<SqlParameter> sp = new List<SqlParameter>()
        //    {
        //        new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lQuestionCategoryID},
        //        new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = Globals.gUserId}
        //    };
        //    Globals.gReturn_Status = Manager.DataFacade.GetSP_Void("spNonActiveQuestionCategory", sp);

        //    return Globals.gReturn_Status;
        //}

        public static String InsertQuestionCategoryDetail(Model.mdlQuestion_CategoryDetail lParam, string user)
        {
            string key = "InsertQuestionCategoryDetail";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID}
            };


            result = Manager.DataFacade.GetSP_Void("spInsertQuestionCategoryDetail", user, key, sp);

            return result;
        }

        public static String UpdateQuestionCategoryDetail(Model.mdlQuestion_CategoryDetail lParam, string user)
        {
            string key = "UpdateQuestionCategoryDetail";
            string result = "";
            List<SqlParameter> sp = new List<SqlParameter>()
            {
                new SqlParameter() {ParameterName = "@QuestionCategoryID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionCategoryID},
                new SqlParameter() {ParameterName = "@QuestionSetID", SqlDbType = SqlDbType.NVarChar, Value = lParam.QuestionSetID},
                new SqlParameter() {ParameterName = "@User", SqlDbType = SqlDbType.NVarChar, Value = user}
            };
            result = Manager.DataFacade.GetSP_Void("spUpdateQuestionCategoryDetail", user, key, sp);

            return result;
        }


    }
}
