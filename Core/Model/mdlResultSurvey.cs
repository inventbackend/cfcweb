﻿/* documentation
 * 001 18 Okt'16 Fernandes
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    [DataContract]
    public class mdlResultSurvey
    {
        [DataMember]
        public string SurveyId { get; set; }

        [DataMember]
        public string QuestionSet { get; set; }

        [DataMember]
        public string EmployeeId { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public string BranchId { get; set; }

        [DataMember]
        public string VisitId { get; set; }

        [DataMember]
        public string QuestionText { get; set; }

        [DataMember]
        public string AnswerText { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string StartTime { get; set; }

        [DataMember]
        public string EndTime { get; set; }

        [DataMember]
        public string Image { get; set; }

        [DataMember]
        public DateTime ImageDate { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string CompletedPercentage { get; set; }
    }

    public class mdlSurveyCustomer
    {
        [DataMember]
        public string SurveyId { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public string EmployeeID { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string BranchID { get; set; }

        [DataMember]
        public string BranchId { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

    }

    public class mdlSurveyInfo
    {
        [DataMember]
        public string SurveyID { get; set; }

        [DataMember]
        public string QuestionID { get; set; }

        [DataMember]
        public string QuestionText { get; set; }

        [DataMember]
        public string Value { get; set; }

    }

    public class mdlMappingSurvey
    {
        [DataMember]
        public string NAMA_TOKO { get; set; }

        [DataMember]
        public string QuestionID { get; set; }

        [DataMember]
        public string QuestionText { get; set; }

        [DataMember]
        public string Value { get; set; }

    }

}
