﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
 
        public class Dataset
        {
            public string label { get; set; }
            public string backgroundColor { get; set; }
            public List<decimal> data { get; set; }
        }

        public class mdlBarChart
        {
            public List<string> labels { get; set; }
            public List<Dataset> datasets { get; set; }
        }
    
}
