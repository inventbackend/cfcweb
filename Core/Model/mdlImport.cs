﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public class mdlImport
    {
    }

    public class mdlImportCallPlan
    {
        public string EmployeeID { get; set; }
        public string BranchID { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string CustomerID { get; set; }
        public string Sequence { get; set; }
    }


    public class mdlImportCallPlanFinal
    {
        public List<Model.CallPlan> callPlan { get; set; }
        public List<Model.CallPlanDetail> callPlanDetail { get; set; }
    }

    public class mdlInsertCallPlan
    {
        public string CallPlanID { get; set; }
        public string EmployeeID { get; set; }
        public string BranchID { get; set; }
        public string VehicleID { get; set; }
        public string Date { get; set; }
        public string CreatedDate { get; set; }
        public string LastDate { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdateBy { get; set; }
        public string IsFinish { get; set; }

        public string Helper1 { get; set; }
        public string Helper2 { get; set; }
        public string FileName { get; set; }
        public string IsDownload { get; set; }
 
    }


    public class mdlInsertCallPlanDetail
    {
        public string CallPlanID { get; set; }
        public string CustomerID { get; set; }
        public string WarehouseID { get; set; }
        public string Time { get; set; }
        public string Sequence { get; set; }
        public string IsDownload { get; set; }
        

    }





    
}
