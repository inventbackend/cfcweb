﻿/* documentation
 *001 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlRptEDExcel
    {
        [DataMember]
        public string VisitWeek { get; set; }

        [DataMember]
        public string Distributor { get; set; }

        [DataMember]
        public string Channel { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string SisaStockTypeID { get; set; }

        [DataMember]
        public string Value { get; set; }
    }

    public class mdlRptEDDetail
    {
        [DataMember]
        public string VisitWeek { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string Channel { get; set; }

        [DataMember]
        public string SisaStockTypeID { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string Summary { get; set; }

        [DataMember]
        public string LinkValue { get; set; }

        [DataMember]
        public string LinkSummary { get; set; }


    
    }

    public class mdlRptEDDetail2
    {
        [DataMember]
        public string VisitWeek { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string Channel { get; set; }

        [DataMember]
        public string SisaStockTypeID { get; set; }

        [DataMember]
        public decimal Value { get; set; }

        [DataMember]
        public string Summary { get; set; }

        [DataMember]
        public string LinkValue { get; set; }

        [DataMember]
        public string LinkSummary { get; set; }



    }


    public class mdlRptED
    {
        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string Channel { get; set; }
    }

    public class mdlRptEDbySKU
    {
        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string VisitWeek { get; set; }

        [DataMember]
        public string SisaStockTypeID { get; set; }

        [DataMember]
        public decimal Value { get; set; }
    }

}
