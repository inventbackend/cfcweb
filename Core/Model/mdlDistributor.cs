﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Model
{
    public class mdlDistributor
    {
        public string DistributorID { get; set; }
        public string DistributorName { get; set; }
        public string Description { get; set; }

        public string Link { get; set; }
    }
}
