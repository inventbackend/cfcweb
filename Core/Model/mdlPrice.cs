﻿/* documentation
 *001 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Core.Model
{
    public class mdlRptPriceDetail
    {
        [DataMember]
        public string BranchID { get; set; }

        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string VisitWeek { get; set; }

        [DataMember]
        public string NormalPrice { get; set; }

        [DataMember]
        public string PromoPrice { get; set; }  
    }

    public class mdlRptPrice
    {
        [DataMember]
        public string BranchID { get; set; }

        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }
    }

    public class mdlRptPriceExcel
    {
        [DataMember]
        public string BranchID { get; set; }

        [DataMember]
        public string CustomerID { get; set; }

        [DataMember]
        public string CustomerName { get; set; }
            
        [DataMember]
        public string ProductID { get; set; }

        [DataMember]
        public string ProductName { get; set; }

        [DataMember]
        public string VisitWeek { get; set; }

        [DataMember]
        public string NormalPrice { get; set; }

        [DataMember]
        public string PromoPrice { get; set; }
    }
}
